package flux

import (
	"errors"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

// We should mainly rely on Pavilion + Facility/Container to realize more comprehensive
// testing against Flux. Use these tests to ensure we properly handle errors, goroutines,
// and channel communications where possible.

func genericBatchMng(ctrl *gomock.Controller) *mock_batch.MockManager {
	m := mock_batch.NewMockManager(ctrl)
	m.EXPECT().BatchCmd(gomock.Any()).Return(DefaultAlloc).AnyTimes()
	m.EXPECT().StateCmd().Return(DefaultState).AnyTimes()
	m.EXPECT().StopCmd().Return("flux job kill").AnyTimes()
	m.EXPECT().UserArgs().Return("args").AnyTimes()
	m.EXPECT().MonitorTermination(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return().AnyTimes()
	m.EXPECT().TailFiles(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	m.EXPECT().NFSTimeout(gomock.Any(), gomock.Any()).Return().AnyTimes()
	return m
}

func genericMsg(ctrl *gomock.Controller) *mock_logging.MockMessenger {
	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn("No %s variable detected, please check your CI job if this is unexpected.", "SCHEDULER_PARAMETERS").AnyTimes()
	return m
}

func genericCfg(ctrl *gomock.Controller) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout: "1s",
	}).AnyTimes()
	return m
}

//

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		absExec     *abstracts.Executor
		mng         batch.Manager
		assertError func(*testing.T, error)
	}{
		"get_sources error": {
			absExec: &abstracts.Executor{
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().JobScriptOutput("/working/get_sources.bash").Return(errors.New("error msg"))
					return m
				}(),
				ScriptPath: "/working/get_sources.bash",
				Stage:      "get_sources",
			},
			mng: genericBatchMng(ctrl),
			assertError: func(t *testing.T, err error) {
				assert.Equal(t, "*execerr.SystemFailure", reflect.TypeOf(err).String())
			},
		},
		"after_script locally": {
			absExec: &abstracts.Executor{
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().JobScriptOutput("/working/after_script.bash").Return(nil)
					return m
				}(),
				ScriptPath: "/working/after_script.bash",
				Stage:      "after_script",
			},
			mng: genericBatchMng(ctrl),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"non-existent script path defined": {
			absExec: &abstracts.Executor{
				Msg:        genericMsg(ctrl),
				ScriptPath: t.TempDir() + "/missing.bash",
				Stage:      "step_script",
			},
			mng: genericBatchMng(ctrl),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "failed to update Flux job script")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.absExec,
				mng:     tt.mng,
				jobName: "ci-test_job",
			}

			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:       "5s",
		AllowIllegalArgs: false,
	}).AnyTimes()

	illegalMsg := mock_logging.NewMockMessenger(ctrl)
	illegalMsg.EXPECT().Notify("Flux job command: %s %s", "flux alloc", gomock.Any()).AnyTimes()
	illegalMsg.EXPECT().Warn(
		gomock.Eq("Illegal argument detected. Please remove: --job-name"),
	).Times(2)

	tests := map[string]struct {
		ae             *abstracts.Executor
		prepare        func(*testing.T)
		assertError    func(*testing.T, error)
		assertExecutor func(*testing.T, *executor)
	}{
		// We don't need to test for many failure states as we can
		// rely on that the data provided has already been parsed.
		"basic Flux executor created for non step_script stage": {
			ae: &abstracts.Executor{
				Stage: "after_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.Equal(t, e.absExec.Stage, "after_script")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"build Flux executor for step_script": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: t.TempDir(),
					},
				},
				Msg:   genericMsg(ctrl),
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.NotNil(t, e.mng)
				assert.Contains(t, e.jobName, "ci-101_")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"scheduler bin observed": {
			ae: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Batch().Return(configure.Batch{
						SchedulerBin:     "/ci/test/bin",
						NFSTimeout:       "5s",
						AllowIllegalArgs: false,
					}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: t.TempDir(),
					},
				},
				Msg:   genericMsg(ctrl),
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.Equal(t, "/ci/test/bin/flux alloc", e.mng.BatchCmd(""))
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"illegal argument results in error": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: t.TempDir(),
					},
				},
				Msg:   illegalMsg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "--job-name custom_name")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.Nil(t, e)
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "illegal arguments must be addressed before continuing")
			},
		},
		"illegal argument allowed": {
			ae: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Batch().Return(configure.Batch{
						NFSTimeout:       "5s",
						AllowIllegalArgs: true,
					}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: t.TempDir(),
					},
				},
				Msg:   illegalMsg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "--job-name custom_name")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepare != nil {
				tt.prepare(t)
			}

			got, err := NewExecutor(tt.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}
