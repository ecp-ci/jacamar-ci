package flux

import (
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/actions"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_executor_runFlux(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		absExec   *abstracts.Executor
		mng       batch.Manager
		tarAction actions.Type

		assertError func(*testing.T, error)
	}{
		"cancel job successful": {
			absExec: &abstracts.Executor{
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Notify("Canceling Flux job %s...", gomock.Any())
					m.EXPECT().Stdout(gomock.Any())
					m.EXPECT().Notify(gomock.Eq("output"))
					return m
				}(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().ReturnOutput(gomock.Any()).Return("output", nil)
					return m
				}(),
			},
			mng:       genericBatchMng(ctrl),
			tarAction: actions.CancelAction,
		},
		"cancel job error": {
			absExec: &abstracts.Executor{
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Notify("Canceling Flux job %s...", gomock.Any())
					m.EXPECT().Stdout(gomock.Any())
					m.EXPECT().Error(gomock.Eq("output"))
					return m
				}(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().ReturnOutput(gomock.Any()).Return("output", errors.New("error message"))
					return m
				}(),
			},
			mng:       genericBatchMng(ctrl),
			tarAction: actions.CancelAction,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"default submission failure": {
			absExec: &abstracts.Executor{
				Msg: logging.NewMessenger(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().JobScriptOutput(gomock.Any(), gomock.Eq("flux alloc")).Return(errors.New("error message"))
					m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("", nil)
					return m
				}(),
				Cfg:        genericCfg(ctrl),
				ScriptPath: t.TempDir(),
			},
			mng:       genericBatchMng(ctrl),
			tarAction: actions.DefaultAction,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"default functional": {
			absExec: &abstracts.Executor{
				Msg: logging.NewMessenger(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().JobScriptOutput(gomock.Any(), gomock.Eq("flux alloc")).Return(nil)
					m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": [{"id":123,"result":"COMPLETED","returncode":0}]}`, nil)
					return m
				}(),
				Cfg:        genericCfg(ctrl),
				ScriptPath: t.TempDir(),
			},
			mng:       genericBatchMng(ctrl),
			tarAction: actions.DefaultAction,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"default failed": {
			absExec: &abstracts.Executor{
				Msg: logging.NewMessenger(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().JobScriptOutput(gomock.Any(), gomock.Eq("flux alloc")).Return(nil)
					m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": [{"id":123,"result":"FAILED","returncode":42}]}`, nil)
					return m
				}(),
				Cfg:        genericCfg(ctrl),
				ScriptPath: t.TempDir(),
			},
			mng:       genericBatchMng(ctrl),
			tarAction: actions.DefaultAction,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "exit code: 42")
			},
		},
		"default timeout": {
			absExec: &abstracts.Executor{
				Msg: logging.NewMessenger(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().JobScriptOutput(gomock.Any(), gomock.Eq("flux alloc")).Return(nil)
					m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": [{"id":123,"result":"TIMEOUT","returncode":42}]}`, nil)
					return m
				}(),
				Cfg:        genericCfg(ctrl),
				ScriptPath: t.TempDir(),
			},
			mng:       genericBatchMng(ctrl),
			tarAction: actions.DefaultAction,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "job did not complete successfully")
			},
		},
		"default no result": {
			absExec: &abstracts.Executor{
				Msg: logging.NewMessenger(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().JobScriptOutput(gomock.Any(), gomock.Eq("flux alloc")).Return(nil)
					m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": [{"id":123,"result":""}]}`, nil)
					return m
				}(),
				Cfg:        genericCfg(ctrl),
				ScriptPath: t.TempDir(),
			},
			mng:       genericBatchMng(ctrl),
			tarAction: actions.DefaultAction,
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "still in progress")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec:   tt.absExec,
				mng:       tt.mng,
				jobName:   "ci-test_job",
				tarAction: tt.tarAction,
				sleepTime: 1 * time.Millisecond,
				stateFile: "state.out",
			}

			err := e.runFlux()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
