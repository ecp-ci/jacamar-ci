package flux

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type AllJobs struct {
	Jobs []JobDetails `json:"jobs"`
}

type JobDetails struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	Result     string `json:"result"`
	State      string `json:"state"`
	Success    bool   `json:"success"`
	ReturnCode int    `json:"returncode"`
}

// RetrieveJobDetails attempts to retrieve the details regarding a given job. Any errors encountered
// only indicate that there was a failure when attempting to retrieve this information, not that the
// target job has failed.
func RetrieveJobDetails(
	absExec *abstracts.Executor,
	jobName, stateCmd, stateFile string,
) (JobDetails, error) {
	cmdStr := constructJobCommand(jobName, stateCmd, absExec.Env)
	out, err := absExec.Runner.FileOutput(stateFile, cmdStr)
	if err != nil {
		return JobDetails{}, fmt.Errorf("unable to obtain job details: %s", out)
	}

	// Attempt to cleanup output to prevent cases where unexpected login/logout
	// scripts exist. We cannot account for all potential cases and will continue
	// to advise users to be aware of these edge cases.
	cleanOut := strings.TrimLeftFunc(out, func(r rune) bool {
		return r != '{'
	})
	cleanOut = strings.TrimRightFunc(cleanOut, func(r rune) bool {
		return r != '}'
	})

	jobs := AllJobs{}

	if err = json.Unmarshal([]byte(cleanOut), &jobs); err != nil {
		return JobDetails{}, fmt.Errorf("unable to parse jobs output: %s", out)
	}

	if len(jobs.Jobs) == 0 {
		return JobDetails{}, fmt.Errorf("no job found for %s", jobName)
	} else if len(jobs.Jobs) > 1 {
		return JobDetails{}, fmt.Errorf("multiple jobs found with identical names %s", jobName)
	}

	return jobs.Jobs[0], nil
}

func constructJobCommand(
	jobName, stateCmd string,
	env envparser.ExecutorEnv,
) string {
	return fmt.Sprintf(
		"%s --user=%s --name=%s -a --json",
		stateCmd,
		env.Username,
		jobName,
	)
}

//

type JobResultCode int

const (
	ResultCompleted JobResultCode = iota
	ResultFailed
	ResultCanceled
	ResultTimeout
	ResultNone
)

func (j JobResultCode) String() string {
	return []string{
		"COMPLETED",
		"FAILED",
		"CANCELED",
		"TIMEOUT",
		"NONE",
	}[j]
}

func (j JobDetails) ParseResult() JobResultCode {
	// https://flux-framework.readthedocs.io/projects/flux-core/en/latest/man1/flux-jobs.html#job-status
	switch j.Result {
	case "COMPLETED", "CD":
		return ResultCompleted
	case "FAILED", "F":
		return ResultFailed
	case "CANCELED", "CA":
		return ResultCanceled
	case "TIMEOUT", "TO":
		return ResultTimeout
	default:
		return ResultNone
	}
}
