package flux

import (
	"fmt"

	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/actions"
)

func (e *executor) runFlux() error {
	switch e.tarAction {
	case actions.DefaultAction:
		return e.defaultRun()
	case actions.CancelAction:
		return e.cancelRun()
	default:
		return fmt.Errorf("action %s not supported for Flux", e.tarAction.String())
	}
}

func (e *executor) defaultRun() error {
	return e.submitAndMonitor(e.constructAllocCommand())
}

func (e *executor) cancelRun() error {
	e.absExec.Msg.Notify("Canceling Flux job %s...", e.jobName)
	e.noScriptMsg()

	out, err := e.absExec.Runner.ReturnOutput(fmt.Sprintf(
		"%s %s",
		e.mng.StopCmd(),
		e.constructStopArg(),
	))
	if err != nil {
		e.absExec.Msg.Error(out)
	} else {
		e.absExec.Msg.Notify(out)
	}

	return err
}

//

func (e *executor) constructAllocCommand() (s string) {
	s = e.mng.BatchCmd(fmt.Sprintf(
		"--job-name=%s %s",
		e.jobName,
		e.mng.UserArgs(),
	))
	e.absExec.Msg.Notify("Flux job command: %s %s", s, e.absExec.ScriptPath)

	return
}

func (e *executor) noScriptMsg() {
	e.absExec.Msg.Stdout("Be advised no before_script+script will be run in this job")
}
