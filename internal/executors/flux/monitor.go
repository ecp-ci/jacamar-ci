package flux

import (
	"errors"
	"fmt"
	"strconv"
	"sync"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/execerr"
)

func (e *executor) submitAndMonitor(stdin string) error {
	var wg sync.WaitGroup
	wg.Add(2)

	cmdErr := make(chan error, 1)
	defer close(cmdErr)
	go func() {
		defer wg.Done()
		err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath, stdin)
		cmdErr <- err
	}()

	jobDone := make(chan struct{}, 1)
	defer close(jobDone)
	go func() {
		defer wg.Done()
		e.mng.MonitorTermination(
			e.absExec.Runner,
			jobDone,
			e.constructStopArg(),
			e.absExec.Env.StatefulEnv.ScriptDir,
		)
	}()

	<-cmdErr
	jobDone <- struct{}{}

	wg.Wait()

	// Though a delay will not directly affect output it can still restrict artifacts
	// and should be observed.
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	return e.verifyJobResults()
}

// verifyJobResults establish the primary check using 'flux jobs' to ensure a jobs has "COMPLETED"
// successfully, else an error will be returned. Cases encountered where a job is still running
// will result in 'flux kill' being invoked. This function will return the appropriate 'execerr'
// type (build or system) based upon the results. Its important to note that we only consider
// a "FAILED" job to be reasonable for build errors.
func (e *executor) verifyJobResults() error {
	jd, err := RetrieveJobDetails(e.absExec, e.jobName, e.mng.StateCmd(), e.stateFile)
	if err != nil {
		return execerr.CustomSysError(err)
	}

	code := jd.ParseResult()
	e.absExec.Msg.Notify("Flux job %s result: %s", e.jobName, code.String())

	switch code {
	case ResultNone:
		// We will likely be unable to handle output or any errors at this point
		// in the job so we make a best attempt to job the job if it is still running.
		command.NoOutputCmd(e.mng.StopCmd(), strconv.Itoa(jd.ID))
		return execerr.CustomSysError(
			errors.New("job still in progress, attempting to stop"),
		)
	case ResultCompleted:
		return nil
	case ResultFailed:
		return execerr.CustomBuildError(
			jd.ReturnCode,
			fmt.Errorf("failed with exit code: %d", jd.ReturnCode),
		)
	default:
		return execerr.CustomSysError(
			errors.New("job did not complete successfully"),
		)
	}
}

func (e *executor) constructStopArg() string {
	return fmt.Sprintf(
		"$(%s --no-header -a --count=1 --user=%s -o '{id}' --name='%s')",
		e.mng.StateCmd(),
		e.absExec.Env.Username,
		e.jobName,
	)
}
