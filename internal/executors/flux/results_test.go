package flux

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_executor_retrieveJobDetails(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		runner        runmechanisms.Runner
		assertError   func(*testing.T, error)
		assertDetails func(*testing.T, JobDetails)
	}{
		"failed command": {
			runner: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("", errors.New("error msg"))
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "unable to obtain job details")
			},
		},
		"empty output": {
			runner: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("", nil)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "unable to parse jobs output")
			},
		},
		"multiple jobs": {
			runner: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": [{"id":123}, {"id":456}]}`, nil)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "multiple jobs")
			},
		},
		"no job": {
			runner: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": []}`, nil)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "no job found")
			},
		},
		"job timeout": {
			runner: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": [{"id":123,"result":"TIMEOUT"}]}`, nil)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDetails: func(t *testing.T, jd JobDetails) {
				assert.Equal(t, ResultTimeout.String(), jd.Result)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := RetrieveJobDetails(
				&abstracts.Executor{
					Runner: tt.runner,
				},
				"ci-test_job",
				"flux jobs",
				"state.out",
			)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertDetails != nil {
				tt.assertDetails(t, got)
			}
		})
	}
}
