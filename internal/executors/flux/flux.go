package flux

import (
	"fmt"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/execerr"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/actions"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
)

const (
	DefaultAlloc  = "flux alloc"
	DefaultCancel = "flux cancel"
	DefaultState  = "flux jobs"
)

type executor struct {
	absExec *abstracts.Executor

	mng       batch.Manager
	jobName   string
	tarAction actions.Type
	sleepTime time.Duration

	stateFile string
}

func (e *executor) Run() error {
	if !(runmechanisms.StepScriptStage(e.absExec.Stage)) {
		err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
		return execerr.ExecPotentialBuildError(e.absExec.Stage, err)
	}

	// Verify that job scripts start with a script declaration line.
	step := augmenter.OpenStepScript(e.absExec.ScriptPath)
	step.LoginShell()
	if err := step.VerifyAndWrite(); err != nil {
		return fmt.Errorf("failed to update Flux job script: %w", err)
	}

	return e.runFlux()
}

// NewExecutor generates a valid flux executor that fulfills the executors.Executor interface.
func NewExecutor(ae *abstracts.Executor) (*executor, error) {
	e := &executor{
		absExec: ae,
	}

	if runmechanisms.StepScriptStage(e.absExec.Stage) {
		e.tarAction = actions.IdentifyAction(ae)
		e.jobName = e.tarAction.JobName(ae)
		e.sleepTime = batch.CommandDelay(ae.Cfg.Batch().CommandDelay, ae.Msg)

		e.stateFile = ae.Env.StatefulEnv.ScriptDir + "/state.out"

		set := batch.Settings{
			BatchCmd: batchCmd(e.tarAction),
			StopCmd:  DefaultCancel,
			StateCmd: DefaultState,
			IllegalArgs: []string{
				"--bg",
				"--job-name",
				"--output",
				"--error",
			},
		}

		var err error
		e.mng, err = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
		if err != nil {
			return nil, err
		}
	}

	return e, nil
}

//

func batchCmd(_ actions.Type) string {
	return DefaultAlloc
}
