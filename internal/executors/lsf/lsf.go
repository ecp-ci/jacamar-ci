package lsf

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/execerr"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
)

type executor struct {
	absExec *abstracts.Executor

	// batch submission only variables
	mng     batch.Manager
	jobName string
}

func (e *executor) Run() error {
	if !(runmechanisms.StepScriptStage(e.absExec.Stage)) {
		err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
		return execerr.ExecPotentialBuildError(e.absExec.Stage, err)
	}

	return e.runLSF()
}

func (e *executor) runLSF() error {
	var wg sync.WaitGroup
	wg.Add(2)

	cmdErr := make(chan error, 1)
	defer close(cmdErr)
	go func() {
		defer wg.Done()
		e.submitJob(cmdErr)
	}()

	jobDone := make(chan struct{}, 1)
	defer close(jobDone)
	go func() {
		defer wg.Done()
		if e.absExec.Cfg.Batch().LSFJobCancellation {
			// Monitoring the job only matter when we need to use 'bkill' to terminate.
			// Else we rely upon the interactive 'bsub' and any underlying scripts/wrappers.
			e.monitorJob(jobDone)
		}
	}()

	err := <-cmdErr
	jobDone <- struct{}{}

	wg.Wait()

	return err
}

func (e *executor) submitJob(cmdErr chan error) {
	var bsubStdin string
	if e.absExec.Cfg.Batch().LSFJobCancellation {
		bsubStdin = e.mng.BatchCmd(fmt.Sprintf(
			"-I -J %s %s",
			e.jobName,
			e.mng.UserArgs(),
		))
	} else {
		bsubStdin = e.mng.BatchCmd(
			fmt.Sprintf(
				"-I %s",
				e.mng.UserArgs(),
			))
	}
	e.absExec.Msg.Notify("LSF job command: %s %s", bsubStdin, e.absExec.ScriptPath)

	err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath, bsubStdin)
	cmdErr <- execerr.ExecPotentialBuildError(e.absExec.Stage, err)
}

func (e *executor) monitorJob(jobDone chan struct{}) {
	stopArg := fmt.Sprintf("-J %s", e.jobName)

	e.mng.MonitorTermination(e.absExec.Runner, jobDone, stopArg, e.absExec.Env.StatefulEnv.ScriptDir)
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)
}

// NewExecutor generates a valid LSF executor that fulfills the executors.Executor interface.
func NewExecutor(ae *abstracts.Executor) (executors.Executor, error) {
	var err error
	e := &executor{
		absExec: ae,
	}

	if runmechanisms.StepScriptStage(e.absExec.Stage) {
		set := batch.Settings{
			BatchCmd:    "bsub",
			StopCmd:     "bkill",
			IllegalArgs: []string{"-e", "-o", "-eo", "-I"},
		}

		if ae.Cfg.Batch().LSFJobCancellation {
			set.IllegalArgs = append(set.IllegalArgs, "-J")
		}

		e.jobName = fmt.Sprintf(
			"ci-%s_%d",
			ae.Env.RequiredEnv.JobID,
			time.Now().Unix(),
		)

		e.mng, err = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
		if err != nil {
			return nil, err
		}
	}

	return e, nil
}
