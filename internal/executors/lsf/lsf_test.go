package lsf

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/tools/exec_testing"
)

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"non-build job script execute successfully": {
			fields: fields{
				absExec: func() *abstracts.Executor {
					return exec_testing.MockAbsExecutor(
						ctrl,
						configure.Options{},
						"prepare_script",
						false,
					)
				}(),
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"non-build job script execute with error": {
			fields: fields{
				absExec: func() *abstracts.Executor {
					return exec_testing.MockAbsExecutor(
						ctrl,
						configure.Options{},
						"get_sources",
						true,
					)
				}(),
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"step_script executed successfully": {
			fields: fields{
				absExec: func() *abstracts.Executor {
					return exec_testing.MockAbsExecutor(
						ctrl,
						configure.Options{},
						"step_script",
						false,
					)
				}(),
				mng: func() *mock_batch.MockManager {
					m := mock_batch.NewMockManager(ctrl)
					exec_testing.PopulateMockManager(m)
					return m
				}(),
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"step_script executed successfully (LSFJobCancellation)": {
			fields: fields{
				absExec: func() *abstracts.Executor {
					return exec_testing.MockAbsExecutor(
						ctrl,
						configure.Options{
							Batch: configure.Batch{
								NFSTimeout:         "30s",
								LSFJobCancellation: true,
							},
						},
						"step_script",
						false,
					)
				}(),
				mng: func() *mock_batch.MockManager {
					m := mock_batch.NewMockManager(ctrl)
					m.EXPECT().NFSTimeout(gomock.Eq("30s"), gomock.Any())
					exec_testing.PopulateMockManager(m)
					return m
				}(),
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"step_script executed with error (LSFJobCancellation)": {
			fields: fields{
				absExec: func() *abstracts.Executor {
					return exec_testing.MockAbsExecutor(
						ctrl,
						configure.Options{
							Batch: configure.Batch{
								NFSTimeout:         "30s",
								LSFJobCancellation: true,
							},
						},
						"step_script",
						true,
					)
				}(),
				mng: func() *mock_batch.MockManager {
					m := mock_batch.NewMockManager(ctrl)
					m.EXPECT().NFSTimeout(gomock.Eq("30s"), gomock.Any())
					exec_testing.PopulateMockManager(m)
					return m
				}(),
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
				mng:     tt.fields.mng,
			}
			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn(
		gomock.Eq("Illegal argument detected. Please remove: -e"),
	).Times(1)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:        "30s",
		ArgumentsVariable: []string{"TEST_BSUB_PARAMETERS"},
	}).AnyTimes()

	type args struct {
		ae *abstracts.Executor
	}
	tests := map[string]struct {
		args           args
		prepare        func(*testing.T)
		assertError    func(*testing.T, error)
		assertExecutor func(*testing.T, executors.Executor)
	}{
		"new executor for step_script": {
			args: args{
				ae: &abstracts.Executor{
					Cfg:   cfg,
					Msg:   msg,
					Stage: "step_script",
				},
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"TEST_BSUB_PARAMETERS", "-A account123")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"illegal argument results in error": {
			args: args{
				ae: &abstracts.Executor{
					Cfg:   cfg,
					Msg:   msg,
					Stage: "step_script",
				},
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"TEST_BSUB_PARAMETERS", "-e test.txt")
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "illegal arguments must be addressed before continuing")
			},
			assertExecutor: func(t *testing.T, e executors.Executor) {
				assert.Nil(t, e)
			},
		},
		"new executor for after_script": {
			args: args{
				ae: &abstracts.Executor{
					Stage: "after_script",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExecutor: func(t *testing.T, e executors.Executor) {
				assert.NotNil(t, e)
			},
		},
		"illegal argument results in error (LSFJobCancellation)": {
			args: args{
				ae: &abstracts.Executor{
					Cfg: func() *mock_configure.MockConfigurer {
						m := mock_configure.NewMockConfigurer(ctrl)
						m.EXPECT().Batch().Return(configure.Batch{
							NFSTimeout:         "30s",
							ArgumentsVariable:  []string{"TEST_BSUB_PARAMETERS"},
							LSFJobCancellation: true,
						}).AnyTimes()
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Warn(
							gomock.Eq("Illegal argument detected. Please remove: -J"),
						).Times(1)
						return m
					}(),
					Stage: "step_script",
				},
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"TEST_BSUB_PARAMETERS", "-J name")
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "illegal arguments must be addressed before continuing")
			},
			assertExecutor: func(t *testing.T, e executors.Executor) {
				assert.Nil(t, e)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepare != nil {
				tt.prepare(t)
			}

			got, err := NewExecutor(tt.args.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}
