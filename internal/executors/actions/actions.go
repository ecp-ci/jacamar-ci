package actions

import (
	"fmt"
	"os/user"
	"path/filepath"
	"slices"
	"strings"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type Type int

const (
	// DefaultAction indicates the job should be submitted/monitored normally.
	DefaultAction = iota
	// AllocateAction indicates the job resources should be allocated but not monitored.
	AllocateAction
	// CancelAction indicates an existing job should be closed.
	CancelAction
	// DetachAction indicates the job should be submitted but not monitored.
	DetachAction
	// ReattachAction indicates an existing job should be used for submission.
	ReattachAction
)

const (
	defaultStr  = "default"
	allocateStr = "allocate"
	cancelStr   = "cancel"
	detachStr   = "detach"
	reattachStr = "reattach"
)

// IdentifyAction returns the target action for a batch executor type. In any cases where an error
// is encountered due to user miss configuration the "DefaultAction" will be used as a fallback.
func IdentifyAction(ae *abstracts.Executor) Type {
	if !ae.Cfg.Batch().SchedulerActions {
		return DefaultAction
	}

	val, found, _ := envparser.GetUserVar(envkeys.SchedulerAction, "alpha")
	if !found || val == "" {
		return DefaultAction
	}

	t, err := StringToType(val)
	if err != nil {
		ae.Msg.Warn(
			"unknown action declared in %s variable, using %s",
			envkeys.SchedulerAction,
			defaultStr,
		)
		return DefaultAction
	}

	if !t.checkAllowed(ae.Cfg.Batch()) {
		ae.Msg.Warn(
			"proposed action %s not allowed by configuration, using %s",
			t.String(),
			defaultStr,
		)
		return DefaultAction
	}

	if t != DefaultAction {
		ae.Msg.Stdout("Scheduler action: %s", t.String())
	}

	return t
}

func StringToType(s string) (Type, error) {
	switch strings.ToLower(s) {
	case defaultStr:
		return DefaultAction, nil
	case allocateStr:
		return AllocateAction, nil
	case cancelStr:
		return CancelAction, nil
	case detachStr:
		return DetachAction, nil
	case reattachStr:
		return ReattachAction, nil
	default:
		return -1, fmt.Errorf("no matching action type")
	}
}

func (t Type) String() string {
	return []string{
		defaultStr,
		allocateStr,
		cancelStr,
		detachStr,
		reattachStr,
	}[t]
}

// OutFile generates a single output file for a job's stdout/stderr based upon a combination
// of Type and configuration. A executor specific prefix (e.g., slurm-ci-) can be provided.
func (t Type) OutFile(ae *abstracts.Executor, filePrefix string) string {
	directory := ae.Env.StatefulEnv.ScriptDir

	if t == DetachAction {
		userDir, found, err := envparser.GetUserVar(envkeys.SchedulerOutputLocation, "qualifiedDir")
		if err != nil {
			ae.Msg.Warn(
				"Invalid directory defined in %s, reverting to default location ",
				envkeys.SchedulerOutputLocation,
			)
		}

		if !found || err != nil {
			u, err := user.Current()
			if err != nil {
				// We don't want to create a new un-managed directory for these logs and the existing
				// structure of Jacamar CI stateful directories shouldn't be modified to support this case.
				// Instead we should simply fall back to disregarding output as it's the safest option
				// to keep the detached job from failing once the CI job has completed.
				return "/dev/null"
			}

			ae.Msg.Stdout(
				"Job logs for detached jobs being stored in default %s, override with %s variable",
				u.HomeDir,
				envkeys.SchedulerOutputLocation,
			)
			directory = u.HomeDir
		} else {
			directory = userDir
		}
	}

	return filepath.Clean(fmt.Sprintf(
		"%s/%s%s.out",
		directory,
		filePrefix,
		ae.Env.RequiredEnv.JobID,
	))
}

func (t Type) JobName(ae *abstracts.Executor) string {
	if t == DefaultAction {
		if !ae.Cfg.Batch().DisableNamePrefix {
			val, found, err := envparser.GetUserVar(envkeys.SchedulerJobPrefix, "customUserKey")
			if err != nil {
				ae.Msg.Warn(
					"Alphanumeric, underscores, and hyphens only (%s) for %s, using default job name.",
					"between 4 and 64 characters",
					envkeys.SchedulerJobPrefix,
				)
			} else if found {
				return fmt.Sprintf(
					"%s_ci-%s",
					val,
					ae.Env.RequiredEnv.JobID,
				)
			}
		}

		return fmt.Sprintf(
			"ci-%s_%d",
			ae.Env.RequiredEnv.JobID,
			time.Now().Unix(),
		)
	}

	val, found, err := envparser.GetUserVar(envkeys.SchedulerJobKey, "customUserKey")
	if err != nil {
		val = fmt.Sprintf("ci-%s", ae.Env.StatefulEnv.PipelineID)
		ae.Msg.Warn(
			"Alphanumeric, underscores, and hyphens only (%s) for %s, defaulting to %s.",
			"between 4 and 64 characters",
			envkeys.SchedulerJobKey,
			val,
		)
	} else if !found {
		val = fmt.Sprintf("ci-%s", ae.Env.StatefulEnv.PipelineID)
	}

	return val
}

//

func (t Type) checkAllowed(cfg configure.Batch) bool {
	if len(cfg.AllowedActions) == 0 {
		return true
	}

	return slices.Contains(cfg.AllowedActions, t.String())
}
