package actions

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
)

var (
	tstMsg logging.Messenger
)

func init() {
	tstMsg = logging.NewMessenger()
}

func Test_IdentifyAction(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	allowedCfg := mock_configure.NewMockConfigurer(ctrl)
	allowedCfg.EXPECT().Batch().Return(configure.Batch{
		SchedulerActions: true,
	}).AnyTimes()

	tests := map[string]struct {
		ae     *abstracts.Executor
		tarEnv map[string]string
		want   string
	}{
		"no action defined": {
			ae: &abstracts.Executor{
				Cfg: allowedCfg,
				Msg: tstMsg,
			},
			want: defaultStr,
		},
		"empty val": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerAction: "",
			},
			ae: &abstracts.Executor{
				Cfg: allowedCfg,
				Msg: tstMsg,
			},
			want: defaultStr,
		},
		"invalid characters": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerAction: "Test!ng",
			},
			ae: &abstracts.Executor{
				Cfg: allowedCfg,
				Msg: tstMsg,
			},
			want: defaultStr,
		},
		"unknown": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerAction: "unknown",
			},
			ae: &abstracts.Executor{
				Cfg: allowedCfg,
				Msg: tstMsg,
			},
			want: defaultStr,
		},
		"DEFAULT": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerAction: "DEFAULT",
			},
			ae: &abstracts.Executor{
				Cfg: allowedCfg,
				Msg: tstMsg,
			},
			want: defaultStr,
		},
		"detach": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerAction: "detach",
			},
			ae: &abstracts.Executor{
				Cfg: allowedCfg,
				Msg: tstMsg,
			},
			want: detachStr,
		},
		"allocate": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerAction: "allocate",
			},
			ae: &abstracts.Executor{
				Cfg: allowedCfg,
				Msg: tstMsg,
			},
			want: allocateStr,
		},
		"cancel": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerAction: "cancel",
			},
			ae: &abstracts.Executor{
				Cfg: allowedCfg,
				Msg: tstMsg,
			},
			want: cancelStr,
		},
		"reattach": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerAction: "reattach",
			},
			ae: &abstracts.Executor{
				Cfg: allowedCfg,
				Msg: tstMsg,
			},
			want: reattachStr,
		},
		"feature not enabled": {
			ae: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Batch().Return(configure.Batch{})
					return m
				}(),
				Msg: tstMsg,
			},
			want: defaultStr,
		},
		"allocate not allowed in cfg": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerAction: "allocate",
			},
			ae: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Batch().Return(configure.Batch{
						SchedulerActions: true,
						AllowedActions:   []string{"detach"},
					}).AnyTimes()
					return m
				}(),
				Msg: tstMsg,
			},
			want: defaultStr,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.tarEnv {
				t.Setenv(k, v)
			}

			got := IdentifyAction(tt.ae)
			assert.Equal(t, tt.want, got.String(), name)
		})
	}
}

func Test_Type_JobName(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	defEnv := envparser.ExecutorEnv{
		RequiredEnv: envparser.RequiredEnv{
			JobID: "123",
		},
		StatefulEnv: envparser.StatefulEnv{
			ScriptDir:  "/ci",
			PipelineID: "456",
		},
	}

	defCfg := mock_configure.NewMockConfigurer(ctrl)
	defCfg.EXPECT().Batch().Return(configure.Batch{}).AnyTimes()

	noPrefixCfg := mock_configure.NewMockConfigurer(ctrl)
	noPrefixCfg.EXPECT().Batch().Return(configure.Batch{
		DisableNamePrefix: true,
	}).AnyTimes()

	tests := map[string]struct {
		at           Type
		ae           *abstracts.Executor
		tarEnv       map[string]string
		assertString func(*testing.T, string)
	}{
		"default": {
			ae: &abstracts.Executor{
				Env: defEnv,
				Msg: tstMsg,
				Cfg: defCfg,
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "ci-123")
			},
		},
		"user key": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerJobKey: "job-$USER-1",
			},
			at: DetachAction,
			ae: &abstracts.Executor{
				Env: defEnv,
				Msg: tstMsg,
				Cfg: defCfg,
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "ci-456")
			},
		},
		"valid key": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerJobKey: "job_789",
			},
			at: DetachAction,
			ae: &abstracts.Executor{
				Env: defEnv,
				Cfg: defCfg,
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "job_789")
			},
		},
		"missing key": {
			at: ReattachAction,
			ae: &abstracts.Executor{
				Env: defEnv,
				Cfg: defCfg,
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "ci-456")
			},
		},
		"invalid prefix": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerJobPrefix: "gpu$(test)",
			},
			at: DefaultAction,
			ae: &abstracts.Executor{
				Env: defEnv,
				Msg: tstMsg,
				Cfg: defCfg,
			},
			assertString: func(t *testing.T, s string) {
				assert.NotContains(t, s, "gpu$(test)")
				assert.Contains(t, s, "ci-123_")
			},
		},
		"valid prefix": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerJobPrefix: "gpu_test",
			},
			at: DefaultAction,
			ae: &abstracts.Executor{
				Env: defEnv,
				Cfg: defCfg,
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "gpu_test_ci-123", s)
			},
		},
		"disable prefix": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerJobPrefix: "gpu_test",
			},
			at: DefaultAction,
			ae: &abstracts.Executor{
				Env: defEnv,
				Cfg: noPrefixCfg,
			},
			assertString: func(t *testing.T, s string) {
				assert.NotEqual(t, "gpu_test_ci-123", s)
				assert.Contains(t, s, "ci-123_")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.tarEnv {
				t.Setenv(k, v)
			}

			got := tt.at.JobName(tt.ae)

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func Test_Type_OutFile(t *testing.T) {
	defEnv := envparser.ExecutorEnv{
		RequiredEnv: envparser.RequiredEnv{
			JobID: "123",
		},
		StatefulEnv: envparser.StatefulEnv{
			ScriptDir:  "/ci",
			PipelineID: "456",
		},
	}

	tests := map[string]struct {
		at           Type
		ae           *abstracts.Executor
		tarEnv       map[string]string
		assertString func(*testing.T, string)
	}{
		"default state + directory": {
			ae: &abstracts.Executor{
				Env: defEnv,
				Msg: tstMsg,
			},
			at: DefaultAction,
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "/ci/test-ci-123.out", s)
			},
		},
		"custom output": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerOutputLocation: "/example/jobs",
			},
			ae: &abstracts.Executor{
				Env: defEnv,
			},
			at: DetachAction,
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "/example/jobs/test-ci-123.out", s)
			},
		},
		"invalid custom": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerOutputLocation: "${HOME}/jobs",
			},
			ae: &abstracts.Executor{
				Env: defEnv,
				Msg: tstMsg,
			},
			at: DetachAction,
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "/test-ci-123.out", s)
			},
		},
		"no custom during detach": {
			ae: &abstracts.Executor{
				Env: defEnv,
				Msg: tstMsg,
			},
			at: DetachAction,
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "/test-ci-123.out", s)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.tarEnv {
				t.Setenv(k, v)
			}

			got := tt.at.OutFile(tt.ae, "test-ci-")

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}
