package slurm

import (
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_executor_monitorJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout: "5s",
	})

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().RequestContext().Return(nil).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn("Unable to monitor output file (%s): %s", gomock.Any(), gomock.Any())
	msg.EXPECT().Stdout("Jacamar will attempt to cancel job (%s)", gomock.Any())

	taiError := mock_batch.NewMockManager(ctrl)
	taiError.EXPECT().TailFiles(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("tail error"))
	taiError.EXPECT().MonitorTermination(gomock.Any(), nil, "--quiet '--name='", gomock.Any()).Return()
	taiError.EXPECT().NFSTimeout(gomock.Eq("5s"), gomock.Any())
	taiError.EXPECT().StopCmd().Return("scancel")
	taiError.EXPECT().NFSTimeout(gomock.Any(), gomock.Any()).Return().AnyTimes()

	t.Run("job completed, channel closed", func(t *testing.T) {
		e := &executor{
			absExec: &abstracts.Executor{
				Cfg:    cfg,
				Msg:    msg,
				Runner: run,
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: t.TempDir(),
					},
				},
			},
			mng: taiError,
		}

		e.monitorJob(nil)
	})
}

func Test_executor_constructStopArg(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	type fields struct {
		absExec *abstracts.Executor
		jobName string
	}
	tests := map[string]struct {
		targetEnv    map[string]string
		fields       fields
		assertString func(*testing.T, string)
	}{
		"default when invalid variable": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerSignal: "!TEST",
			},
			fields: fields{
				jobName: "test",
				absExec: &abstracts.Executor{
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Warn("Reverting standard scancel command: ", gomock.Any())
						return m
					}(),
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.NotContains(t, s, "--signal=")
			},
		},
		"user-defined signal": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.SchedulerSignal: "USR1",
			},
			fields: fields{
				jobName: "test",
				absExec: &abstracts.Executor{
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Stdout("Using signal USR1 if scancel is required")
						return m
					}(),
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "--signal=USR1")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			e := &executor{
				absExec: tt.fields.absExec,
				jobName: tt.fields.jobName,
			}
			got := e.constructStopArg()

			tt.assertString(t, got)
		})
	}
}

func Test_executor_verifyJobState(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{}).AnyTimes()

	fluxCfg := mock_configure.NewMockConfigurer(ctrl)
	fluxCfg.EXPECT().Batch().Return(configure.Batch{
		FluxSacctReplacement: true,
	}).AnyTimes()

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
		jobName string
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"unable to retrieve state": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("error output", errors.New("error message"))
						return m
					}(),
				},
				mng: genericBatchMng(ctrl),
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "error output")
			},
		},
		"job pending": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("PENDING", nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Notify(gomock.Any(), gomock.Eq("job-name"), gomock.Any())
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "job still in progress")
			},
		},
		"job completed": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("COMPLETED", nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Notify(gomock.Any(), gomock.Eq("job-name"), gomock.Any())
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"job failed": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("FAILED", nil)
						m.EXPECT().FileOutput(gomock.Eq("exit.out"), gomock.Any()).Return("  42:0  ", nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Notify(gomock.Any(), gomock.Eq("job-name"), gomock.Any())
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "job failed with exit code: 42")
			},
		},
		"empty sacct return": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("FAILED", nil)
						m.EXPECT().FileOutput(gomock.Eq("exit.out"), gomock.Any()).Return("", nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Notify(gomock.Any(), gomock.Eq("job-name"), gomock.Any())
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "unable to parse exit code")
			},
		},
		"sacct error": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("FAILED", nil)
						m.EXPECT().FileOutput(gomock.Eq("exit.out"), gomock.Any()).Return("", errors.New("sacct error"))
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Notify(gomock.Any(), gomock.Eq("job-name"), gomock.Any())
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "unable to obtain output")
			},
		},
		"job resizing": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("RS", nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Notify(gomock.Any(), gomock.Eq("job-name"), gomock.Any())
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "job still in progress")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec:      tt.fields.absExec,
				mng:          tt.fields.mng,
				jobName:      tt.fields.jobName,
				stateFile:    "state.out",
				exitCodeFile: "exit.out",
			}
			err := e.verifyJobState()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_verifyJobState_flux(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		FluxSacctReplacement: true,
	}).AnyTimes()

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
		jobName string
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"unable to retrieve state": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("error output", errors.New("error message"))
						return m
					}(),
				},
				mng: genericBatchMng(ctrl),
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "error output")
			},
		},
		"job completed": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": [{"id":123,"returncode":0,"result":"COMPLETED"}]}`, nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Stdout(gomock.Any(), "COMPLETED")
						m.EXPECT().Notify(gomock.Any(), gomock.Eq("job-name"), gomock.Any())
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"job failed": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": [{"id":123,"returncode":3,"result":"FAILED"}]}`, nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Stdout(gomock.Any(), "FAILED")
						m.EXPECT().Notify(gomock.Any(), gomock.Eq("job-name"), gomock.Any())
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "job failed with exit code: 3")
			},
		},
		"unknown status": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: cfg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return(`{"jobs": [{"id":123,"returncode":0,"result":"unknown"}]}`, nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Stdout(gomock.Any(), "NONE")
						m.EXPECT().Notify(gomock.Any(), gomock.Eq("job-name"), gomock.Any())
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "job did not complete successfully")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec:   tt.fields.absExec,
				mng:       tt.fields.mng,
				jobName:   tt.fields.jobName,
				stateFile: "state.out",
			}
			err := e.verifyJobState()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_waitForRunning(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
		jobName string
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"sigterm received": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().SigtermReceived().Return(true)
						return m
					}(),
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"multiple failed commands": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("error output", errors.New("error message")).Times(3)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := mock_logging.NewMockMessenger(ctrl)
						m.EXPECT().Error(gomock.Any(), gomock.Eq("job-name"))
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "error output")
			},
		},
		"job running": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("RUNNING", nil).After(
							m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("RESIZING", nil),
						)
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"job completed": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("COMPLETED", nil).After(
							m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("REQUEUED", nil),
						)
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "completed unexpectedly")
			},
		},
		"job failed": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("SUSPENDED", nil)
						return m
					}(),
				},
				mng:     genericBatchMng(ctrl),
				jobName: "job-name",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "submission unsuccessful")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec:   tt.fields.absExec,
				mng:       tt.fields.mng,
				jobName:   tt.fields.jobName,
				sleepTime: 0 * time.Second,
				stateFile: "state.out",
			}
			err := e.waitForRunning()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_retrieveJobID(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
		sacct   bool
	}
	tests := map[string]struct {
		fields      fields
		expected    string
		assertError func(*testing.T, error)
	}{
		"failed command": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("jobid.out"), gomock.Any()).Return("error output", errors.New("error message"))
						return m
					}(),
				},
				mng:   genericBatchMng(ctrl),
				sacct: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "failed to identify job")
			},
		},
		"no output": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("jobid.out"), gomock.Any()).Return("", nil)
						return m
					}(),
				},
				mng:   genericBatchMng(ctrl),
				sacct: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "no running job")
			},
		},
		"multiline output": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("jobid.out"), gomock.Any()).Return("1234\n5678\n", nil)
						return m
					}(),
				},
				mng:   genericBatchMng(ctrl),
				sacct: true,
			},
			expected: "1234",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"standard output": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("jobid.out"), gomock.Any()).Return("		 1234\n", nil)
						return m
					}(),
				},
				mng:   genericBatchMng(ctrl),
				sacct: true,
			},
			expected: "1234",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec:   tt.fields.absExec,
				mng:       tt.fields.mng,
				sacct:     tt.fields.sacct,
				jobIDFile: "jobid.out",
			}

			got, err := e.retrieveJobID()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.expected != "" {
				assert.Equal(t, tt.expected, got)
			}
		})
	}
}

func Test_executor_constructStateCmd(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	t.Run("sacct command", func(t *testing.T) {
		e := &executor{
			mng:     genericBatchMng(ctrl),
			jobName: "test",
			sacct:   true,
		}
		assert.Contains(t, e.constructStateCommand(), "sacct --noheader")
	})

	t.Run("squeue command", func(t *testing.T) {
		e := &executor{
			mng: func() *mock_batch.MockManager {
				m := mock_batch.NewMockManager(ctrl)
				m.EXPECT().StateCmd().Return("squeue")
				return m
			}(),
			jobName: "test",
		}
		assert.Equal(t, e.constructStateCommand(), "squeue --name=test --format='%.2t' --noheader")
	})
}

func Test_executor_constructExitCodeCommand(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_batch.NewMockManager(ctrl)
	m.EXPECT().StateCmd().Return("test").AnyTimes()

	t.Run("sacct", func(t *testing.T) {
		e := &executor{
			mng:     m,
			jobName: "test",
			sacct:   true,
		}
		assert.Contains(t, e.constructExitCodeCommand(), "--format='ExitCode'")
	})

	t.Run("verify squeue", func(t *testing.T) {
		e := &executor{
			mng:     m,
			jobName: "test",
		}
		assert.Contains(t, e.constructExitCodeCommand(), "--Format='exit_code'")
	})
}
