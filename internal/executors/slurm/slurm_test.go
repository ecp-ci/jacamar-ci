package slurm

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

// We should mainly rely on Pavilion + Facility/Container to realize more comprehensive
// testing against Slurm. Use these tests to ensure we properly handle errors, goroutines,
// and channel communications where possible.

func genericBatchMng(ctrl *gomock.Controller) *mock_batch.MockManager {
	m := mock_batch.NewMockManager(ctrl)
	m.EXPECT().BatchCmd(gomock.Any()).Return("sbatch").AnyTimes()
	m.EXPECT().StateCmd().Return("sacct").AnyTimes()
	m.EXPECT().StopCmd().Return("scancel").AnyTimes()
	m.EXPECT().UserArgs().Return("args").AnyTimes()
	m.EXPECT().MonitorTermination(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return().AnyTimes()
	m.EXPECT().TailFiles(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	m.EXPECT().NFSTimeout(gomock.Any(), gomock.Any()).Return().AnyTimes()
	return m
}

func genericMsg(ctrl *gomock.Controller) *mock_logging.MockMessenger {
	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Notify("Slurm job command: %s %s", "sbatch", gomock.Any()).AnyTimes()
	return m
}

//

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
		outFile string
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"working after_script launched locally": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput("/working/after_script.bash").Return(nil)
						return m
					}(),
					ScriptPath: "/working/after_script.bash",
					Stage:      "after_script",
				},
				mng: genericBatchMng(ctrl),
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"non-existent script path defined": {
			fields: fields{
				absExec: &abstracts.Executor{
					Msg:        genericMsg(ctrl),
					ScriptPath: "/file/does/not/exist.bash",
					Stage:      "step_script",
				},
				mng: genericBatchMng(ctrl),
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "failed to update Slurm job script")
			},
		},
		"failing get_sources launched locally": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput("/failing/get_sources.bash").Return(errors.New("error message"))
						return m
					}(),
					ScriptPath: "/failing/get_sources.bash",
					Stage:      "get_sources",
				},
				mng: genericBatchMng(ctrl),
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
				mng:     tt.fields.mng,
				jobName: "ci-test_job",
				outFile: tt.fields.outFile,
			}

			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_NewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tempDir := t.TempDir()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:       "5s",
		AllowIllegalArgs: false,
		SchedulerActions: true,
	}).AnyTimes()

	binCfg := mock_configure.NewMockConfigurer(ctrl)
	binCfg.EXPECT().Batch().Return(configure.Batch{
		SchedulerBin:     "/ci/test/bin",
		NFSTimeout:       "5s",
		AllowIllegalArgs: false,
	}).AnyTimes()

	allowCfg := mock_configure.NewMockConfigurer(ctrl)
	allowCfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:       "5s",
		AllowIllegalArgs: true,
	}).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn("No %s variable detected, please check your CI job if this is unexpected.", "SCHEDULER_PARAMETERS").AnyTimes()
	msg.EXPECT().Stdout(gomock.Any(), gomock.Any()).AnyTimes()

	illegalMsg := mock_logging.NewMockMessenger(ctrl)
	illegalMsg.EXPECT().Warn(
		gomock.Eq("Illegal argument detected. Please remove: -J"),
	).Times(2)
	msg.EXPECT().Stdout(gomock.Any(), gomock.Any()).AnyTimes()

	tests := map[string]struct {
		ae             *abstracts.Executor
		prepare        func(*testing.T)
		assertError    func(*testing.T, error)
		assertExecutor func(*testing.T, *executor)
	}{
		// We don't need to test for many failure states as we can
		// rely on that the data provided has already been parsed.
		"basic Slurm executor created for non step_script stage": {
			ae: &abstracts.Executor{
				Cfg:   cfg,
				Stage: "after_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.Equal(t, e.absExec.Stage, "after_script")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"build Slurm executor for step_script": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.NotNil(t, e.mng)
				assert.Contains(t, e.jobName, "ci-101_")
				assert.Equal(t, tempDir+"/slurm-ci-101.out", e.outFile)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"scheduler bin observed": {
			ae: &abstracts.Executor{
				Cfg: binCfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.Equal(t, "/ci/test/bin/sbatch", e.mng.BatchCmd(""))
				assert.Equal(t, "/ci/test/bin/scancel", e.mng.StopCmd())
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"illegal argument results in error": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   illegalMsg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "-J custom_name")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.Nil(t, e)
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "illegal arguments must be addressed before continuing")
			},
		},
		"illegal argument allowed": {
			ae: &abstracts.Executor{
				Cfg: allowCfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   illegalMsg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "-J custom_name")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"build Slurm reattach": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+envkeys.SchedulerAction, "reattach")
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "--test")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.Contains(t, e.mng.BatchCmd(""), "srun")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"squeue dev/test": {
			ae: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Batch().Return(configure.Batch{
						NFSTimeout:  "5s",
						SlurmSqueue: true,
					}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.False(t, e.sacct)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"build Slurm allocate": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+envkeys.SchedulerAction, "allocate")
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "--test")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.Contains(t, e.mng.BatchCmd(""), "salloc")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"prevent flux and actions": {
			ae: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Batch().Return(configure.Batch{
						FluxSacctReplacement: true,
						SchedulerActions:     true,
					}).AnyTimes()
					return m
				}(),
				Stage: "get_sources",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "scheduler actions cannot be supported")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepare != nil {
				tt.prepare(t)
			}

			got, err := NewExecutor(tt.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}
