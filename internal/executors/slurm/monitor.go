package slurm

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/execerr"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/flux"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

func (e *executor) monitorJob(jobDone chan struct{}) {
	stopArg := e.constructStopArg()
	stopTail := make(chan struct{}, 1)

	var wg sync.WaitGroup
	wg.Add(1)

	defer close(stopTail)
	go func() {
		defer wg.Done()
		// We generate output file in previous step from local host, add arbitrary short timeout.
		err := e.mng.TailFiles([]string{e.outFile}, stopTail, 10*time.Second, e.absExec.Msg)
		if err != nil {
			e.absExec.Msg.Warn("Unable to monitor output file (%s): %s", e.outFile, err.Error())
			e.absExec.Msg.Stdout("Jacamar will attempt to cancel job (%s)", e.jobName)

			command.NoOutputCmd(e.mng.StopCmd(), stopArg)
		}
	}()

	e.mng.MonitorTermination(e.absExec.Runner, jobDone, stopArg, e.absExec.Env.StatefulEnv.ScriptDir)
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	stopTail <- struct{}{}
	wg.Wait()
}

func (e *executor) constructStopArg() (stopArg string) {
	stopArg = fmt.Sprintf("--quiet '--name=%s'", e.jobName)

	signal, found, err := envparser.SchedulerSignal()
	if err != nil {
		// Always fallback to default arguments if the user has provided an invalid argument.
		e.absExec.Msg.Warn("Reverting standard scancel command: ", err.Error())
	} else if found {
		e.absExec.Msg.Stdout(
			fmt.Sprintf("Using signal %s if scancel is required", signal),
		)
		stopArg = fmt.Sprintf("'--signal=%s' %s", signal, stopArg)
	}

	return
}

// verifyJobState establish the primary check using 'sacct' to ensure a jobs has "COMPLETED"
// successfully, else an error will be returned. Cases encountered where a job is still running
// will result in 'scancel' being invoked. This function will return the appropriate 'execerr'
// type (build or system) based upon the results. Its important to note that we only consider
// a "FAILED" job to be reasonable for build errors.
func (e *executor) verifyJobState() error {
	var jsc jobStateCode
	var err error

	if e.absExec.Cfg.Batch().FluxSacctReplacement {
		jsc, err = e.fluxJobState()
	} else {
		jsc, err = e.retrieveJobState()
	}

	if err != nil {
		return execerr.CustomSysError(err)
	}

	e.absExec.Msg.Notify("Slurm job %s state: %s", e.jobName, jsc.String())

	switch jsc {
	case jobPending, jobRequeued, jobResizing, jobRunning:
		command.NoOutputCmd(e.mng.StopCmd(), fmt.Sprintf("'--name=%s'", e.jobName))
		return execerr.CustomSysError(
			errors.New("job still in progress, attempting to stop"),
		)
	case jobCompleted:
		return nil
	case jobFailed:
		return e.retrieveExitCode()
	default:
		return execerr.CustomSysError(
			errors.New("job did not complete successfully"),
		)
	}
}

// waitForRunning attempts to wait until the submitted job is running.
func (e *executor) waitForRunning() error {
	// Wait briefly before making the first inquiry into job state.
	time.Sleep(e.sleepTime)

	failedSacct := 0
	for {
		if e.absExec.Runner.SigtermReceived() {
			// Indicates that a job cancellation has occurred and we can stop checking.
			return nil
		}

		jsc, err := e.retrieveJobState()
		if err != nil {
			failedSacct++

			// We only want to cause a job failure in multiple sacct commands have failed to identify
			// the job state. Cancel the job to be safe and avoid wasting resources.
			if failedSacct >= 3 {
				command.NoOutputCmd(e.mng.StopCmd(), fmt.Sprintf("'--name=%s'", e.jobName))
				e.absExec.Msg.Error(
					"Failed to monitor job %s, refer to job/system logs for details",
					e.jobName,
				)
				return err
			}
		} else {
			switch jsc {
			case jobPending, jobRequeued, jobResizing:
				continue
			case jobCompleted:
				return errors.New("job completed unexpectedly")
			case jobRunning:
				return nil
			default:
				return errors.New("job submission unsuccessful")
			}
		}

		// Wait the given time until re-running the command.
		time.Sleep(e.sleepTime)
	}
}

func (e *executor) retrieveJobID() (string, error) {
	out, err := e.absExec.Runner.FileOutput(e.jobIDFile, e.constructJobIDCommand())
	if err != nil {
		return "", fmt.Errorf("failed to identify job in queue: %s", err.Error())
	}

	jobID := string(numCharsOnly(strings.Split(out, "\n")[0]))
	if jobID == "" {
		return jobID, fmt.Errorf("no running job (%s) identified", e.jobName)
	}

	return jobID, nil
}

// constructStateCmd builds sacct or squeue command for the current job to identify its
// current state.
func (e *executor) constructStateCommand() string {
	if e.sacct {
		yesterday := time.Now().AddDate(0, 0, -1)
		startDate := yesterday.Format("01/02/06")

		return fmt.Sprintf(
			"%s --noheader --name '%s' --format=state -S '%s'",
			e.mng.StateCmd(),
			e.jobName,
			startDate,
		)
	}

	// Fallback to using squeue.
	return fmt.Sprintf(
		"%s --name=%s --format='%s' --noheader",
		e.mng.StateCmd(),
		e.jobName,
		"%.2t",
	)
}

// constructExitCodeCommand builds the appropriate command that can be used to retrieve
// the exit code for a given job.
func (e *executor) constructExitCodeCommand() string {
	if e.sacct {
		yesterday := time.Now().AddDate(0, 0, -1)
		startDate := yesterday.Format("01/02/06")

		return fmt.Sprintf(
			"%s --noheader --name '%s' --format='ExitCode' -S '%s'",
			e.mng.StateCmd(),
			e.jobName,
			startDate,
		)
	}

	// Fallback to using squeue.
	return fmt.Sprintf(
		"%s --name=%s --Format='exit_code' --noheader",
		e.mng.StateCmd(),
		e.jobName,
	)
}

// constructJobIDCommand builds sacct or squeue command for the current job to identify its id.
func (e *executor) constructJobIDCommand() string {
	if e.sacct {
		return fmt.Sprintf(
			"%s --name='%s' --format='jobid' --noheader",
			e.mng.StateCmd(),
			e.jobName,
		)
	}

	// Fallback to using squeue.
	return fmt.Sprintf(
		"%s --name=%s --format='%s' --noheader",
		e.mng.StateCmd(),
		e.jobName,
		"%.18i",
	)
}

// parseSacctState parses stdout from the sacct cmd to retrieve the job's state
// expected: $ sacct -n -j # --format=state
func parseSacctState(out string) string {
	lines := strings.Split(out, "\n")
	if len(lines) > 0 {
		return strings.ToUpper(string(alphaCharsOnly(lines[0])))
	}

	return ""
}

func (e *executor) retrieveExitCode() error {
	var exitCode int

	if e.absExec.Cfg.Batch().FluxSacctReplacement {
		exitCode = e.fluxReturnCode
	} else {
		out, err := e.absExec.Runner.FileOutput(e.exitCodeFile, e.constructExitCodeCommand())
		if err != nil {
			return execerr.CustomSysError(
				fmt.Errorf("unable to obtain output via sacct: %s", out),
			)
		}

		// ExitCodes are organized as <exit code>:<signal>.
		// In all cases we are only interested in identifying
		// the exit code, not the potential signal leading to it.
		b := numCharsOnly(strings.Split(out, ":")[0])
		exitCode, err = strconv.Atoi(string(b))
		if err != nil {
			return execerr.CustomSysError(
				errors.New("unable to parse exit code from sacct"),
			)
		}

	}

	return execerr.CustomBuildError(
		exitCode,
		fmt.Errorf("job failed with exit code: %d", exitCode),
	)
}

// fluxJobState utilizes a 'flux jobs' command in order to retrieve the status for the job.
func (e *executor) fluxJobState() (jobStateCode, error) {
	jd, err := flux.RetrieveJobDetails(e.absExec, e.jobName, e.mng.StateCmd(), e.stateFile)
	if err != nil {
		return jobUnknown, fmt.Errorf("flux job state: %w", err)
	}

	result := jd.ParseResult()
	e.fluxReturnCode = jd.ReturnCode

	e.absExec.Msg.Stdout("Job status verified with Flux: %s", result.String())

	return translateFluxResult(result), nil
}

func alphaCharsOnly(s string) (b []byte) {
	for _, v := range []byte(s) {
		if ('a' <= v && v <= 'z') ||
			('A' <= v && v <= 'Z') {
			b = append(b, v)
		}
	}
	return
}

func numCharsOnly(s string) (b []byte) {
	for _, v := range []byte(s) {
		if '0' <= v && v <= '9' {
			b = append(b, v)
		}
	}
	return
}
