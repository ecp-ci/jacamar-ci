package slurm

import (
	"fmt"
	"sync"

	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/actions"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
)

func (e *executor) runSlurm() error {
	// Pre-create output file to avoid issues with jobs stuck in queue.
	batch.CreateFiles([]string{e.outFile}, e.absExec.Msg)

	switch e.tarAction {
	case actions.DefaultAction:
		return e.defaultRun()
	case actions.AllocateAction:
		return e.allocateRun()
	case actions.CancelAction:
		return e.cancelRun()
	case actions.DetachAction:
		return e.detachRun()
	case actions.ReattachAction:
		return e.reattachRun()
	default:
		return fmt.Errorf("action %s not supported for Slurm", e.tarAction.String())
	}
}

//

func (e *executor) defaultRun() error {
	e.submitAndMonitor(e.constructSbatchCommand())
	return e.verifyJobState()
}

func (e *executor) allocateRun() error {
	e.noScriptMsg()

	if err := e.absExec.Runner.PipeOutput(e.constructSallocCommand()); err != nil {
		e.absExec.Msg.Error("Failed to allocate compute resources")
		return err
	}

	err := e.waitForRunning()
	if err == nil {
		e.absExec.Msg.Notify("Allocation completed, job name: %s", e.jobName)
	}

	return nil
}

func (e *executor) cancelRun() error {
	e.absExec.Msg.Notify("Canceling Slurm job %s...", e.jobName)
	e.noScriptMsg()

	out, err := e.absExec.Runner.FileOutput(e.exitCodeFile, fmt.Sprintf(
		"%s %s",
		e.mng.StopCmd(),
		e.constructStopArg(),
	))
	if err != nil {
		e.absExec.Msg.Error(out)
	} else {
		e.absExec.Msg.Notify(out)
	}

	return err
}

func (e *executor) detachRun() error {
	if err := e.absExec.Runner.JobScriptOutput(
		e.absExec.ScriptPath,
		e.constructSbatchCommand(),
	); err != nil {
		return err
	}

	err := e.waitForRunning()
	if err == nil {
		e.absExec.Msg.Notify("Job submission detached, job name: %s", e.jobName)
	}

	return err
}

func (e *executor) reattachRun() error {
	stdin, err := e.constructSrunCommand()
	if err != nil {
		return err
	}

	e.submitAndMonitor(stdin)
	return nil
}

//

func (e *executor) submitAndMonitor(stdin string) {
	var wg sync.WaitGroup
	wg.Add(2)

	cmdErr := make(chan error, 1)
	defer close(cmdErr)
	go func() {
		defer wg.Done()
		err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath, stdin)
		cmdErr <- err
	}()

	jobDone := make(chan struct{}, 1)
	defer close(jobDone)
	go func() {
		defer wg.Done()
		e.monitorJob(jobDone)
	}()

	<-cmdErr
	jobDone <- struct{}{}

	wg.Wait()
}

func (e *executor) constructSbatchCommand() (s string) {
	s = e.mng.BatchCmd(fmt.Sprintf(
		"%s --job-name=%s --output=%s %s",
		e.sbatchWaitArg(),
		e.jobName,
		e.outFile,
		e.mng.UserArgs(),
	))

	e.absExec.Msg.Notify("Slurm job command: %s %s", s, e.absExec.ScriptPath)

	return
}

func (e *executor) constructSallocCommand() (s string) {
	s = e.mng.BatchCmd(fmt.Sprintf(
		"--no-shell --job-name=%s %s",
		e.jobName,
		e.mng.UserArgs(),
	))

	e.absExec.Msg.Notify("Slurm allocation command: %s", s)

	return
}

func (e *executor) constructSrunCommand() (string, error) {
	jobID, err := e.retrieveJobID()
	if err != nil {
		return "", err
	}

	s := e.mng.BatchCmd(fmt.Sprintf(
		"--wait=0 --jobid=%s --output=%s --job-name='%s' %s",
		jobID,
		e.outFile,
		e.jobName,
		e.mng.UserArgs(),
	))

	e.absExec.Msg.Notify("Slurm run command: %s %s", s, e.absExec.ScriptPath)

	return s, nil
}

func (e *executor) sbatchWaitArg() (s string) {
	if e.tarAction != actions.DetachAction {
		s = "--wait "
	}

	return
}

func (e *executor) noScriptMsg() {
	e.absExec.Msg.Stdout("Be advised no before_script+script will be run in this job")
}
