package slurm

import (
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/actions"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_executor_runSlurm(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testMsg := logging.NewMessenger()

	defCfg := mock_configure.NewMockConfigurer(ctrl)
	defCfg.EXPECT().Batch().Return(configure.Batch{}).AnyTimes()

	type fields struct {
		absExec   *abstracts.Executor
		mng       batch.Manager
		tarAction actions.Type
		outFile   string
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"default actions": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: defCfg,
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput(gomock.Eq("/tmp/example.script"), gomock.Any())
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("COMPLETED", nil)
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:     genericBatchMng(ctrl),
				outFile: t.TempDir() + "/test.out",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"detach actions": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: defCfg,
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput(gomock.Eq("/tmp/example.script"), gomock.Any())
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("RUNNING", nil)
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:       genericBatchMng(ctrl),
				outFile:   t.TempDir() + "/test.out",
				tarAction: actions.DetachAction,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"detach error": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: defCfg,
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput(gomock.Eq("/tmp/example.script"), gomock.Any()).Return(errors.New("error message"))
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:       genericBatchMng(ctrl),
				outFile:   t.TempDir() + "/test.out",
				tarAction: actions.DetachAction,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"reattach job": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: defCfg,
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput(gomock.Eq("/tmp/example.script"), gomock.Any())
						m.EXPECT().FileOutput(gomock.Eq("jobid.out"), gomock.Any()).Return("Submitted batch job 1234", nil)
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:       genericBatchMng(ctrl),
				outFile:   t.TempDir() + "/test.out",
				tarAction: actions.ReattachAction,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"unable to retrieve JobID": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: defCfg,
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("jobid.out"), gomock.Any()).Return("error output", errors.New("error message"))
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:       genericBatchMng(ctrl),
				outFile:   t.TempDir() + "/test.out",
				tarAction: actions.ReattachAction,
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "failed to identify job in queue")
			},
		},
		"cancel job": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: defCfg,
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("exit.out"), gomock.Any()).Return("error output", errors.New("error message"))
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:       genericBatchMng(ctrl),
				outFile:   t.TempDir() + "/test.out",
				tarAction: actions.CancelAction,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"cancel job successfully": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: defCfg,
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().FileOutput(gomock.Eq("exit.out"), gomock.Any()).Return("canceled", nil)
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:       genericBatchMng(ctrl),
				outFile:   t.TempDir() + "/test.out",
				tarAction: actions.CancelAction,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"slurm pending": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: func() *mock_configure.MockConfigurer {
						m := mock_configure.NewMockConfigurer(ctrl)
						m.EXPECT().Batch().Return(configure.Batch{}).AnyTimes()
						return m
					}(),
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput(gomock.Eq("/tmp/example.script"), gomock.Any())
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("PENDING", nil)
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:     genericBatchMng(ctrl),
				outFile: t.TempDir() + "/test.out",
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "job still in progress")
			},
		},
		"allocate": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: defCfg,
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().PipeOutput(gomock.Any()).Return(nil)
						m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("RUNNING", nil)
						m.EXPECT().SigtermReceived().Return(false).AnyTimes()
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:       genericBatchMng(ctrl),
				outFile:   t.TempDir() + "/test.out",
				tarAction: actions.AllocateAction,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"allocation error": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: defCfg,
					Msg: testMsg,
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().PipeOutput(gomock.Any()).Return(errors.New("error message"))
						return m
					}(),
					ScriptPath: "/tmp/example.script",
				},
				mng:       genericBatchMng(ctrl),
				outFile:   t.TempDir() + "/test.out",
				tarAction: actions.AllocateAction,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec:      tt.fields.absExec,
				mng:          tt.fields.mng,
				jobName:      "ci-test",
				outFile:      tt.fields.outFile,
				tarAction:    tt.fields.tarAction,
				sleepTime:    1 * time.Second,
				stateFile:    "state.out",
				jobIDFile:    "jobid.out",
				exitCodeFile: "exit.out",
			}

			err := e.runSlurm()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
