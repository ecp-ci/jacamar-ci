package slurm

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/execerr"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/actions"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/flux"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

type executor struct {
	absExec *abstracts.Executor

	mng       batch.Manager
	jobName   string
	outFile   string
	tarAction actions.Type
	sleepTime time.Duration
	// sacct indicates that the related command is being used to identify job state.
	sacct bool

	exitCodeFile   string
	jobIDFile      string
	stateFile      string
	fluxReturnCode int
}

func (e *executor) Run() error {
	if !(runmechanisms.StepScriptStage(e.absExec.Stage)) {
		// Simply run all other scripts via the local shell.
		err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
		return execerr.ExecPotentialBuildError(e.absExec.Stage, err)
	}

	// Job scripts to be submitted via sbatch, needs to be updated.
	step := augmenter.OpenStepScript(e.absExec.ScriptPath)
	step.LoginShell()
	if err := step.VerifyAndWrite(); err != nil {
		return fmt.Errorf("failed to update Slurm job script: %w", err)
	}

	return e.runSlurm()
}

// NewExecutor generates a valid Slurm executor that fulfills the executors.Executor interface.
func NewExecutor(ae *abstracts.Executor) (*executor, error) {
	e := &executor{
		absExec: ae,
	}

	if ae.Cfg.Batch().FluxSacctReplacement && ae.Cfg.Batch().SchedulerActions {
		return nil, errors.New(
			"defined scheduler actions cannot be supported when " +
				"flux_sacct_replacement has been enabled",
		)
	}

	if runmechanisms.StepScriptStage(e.absExec.Stage) {
		e.tarAction = actions.IdentifyAction(ae)
		e.jobName = e.tarAction.JobName(ae)
		e.outFile = e.tarAction.OutFile(ae, "slurm-ci-")
		e.sleepTime = batch.CommandDelay(ae.Cfg.Batch().CommandDelay, ae.Msg)
		e.sacct = !ae.Cfg.Batch().SlurmSqueue

		e.exitCodeFile = ae.Env.StatefulEnv.ScriptDir + "/exitCode.out"
		e.jobIDFile = ae.Env.StatefulEnv.ScriptDir + "/jobID.out"
		e.stateFile = ae.Env.StatefulEnv.ScriptDir + "/state.out"

		set := batch.Settings{
			BatchCmd: batchCmd(e.tarAction),
			StopCmd:  "scancel",
			StateCmd: stateCmd(ae.Cfg.Batch()),
			IllegalArgs: []string{
				"-o",
				"--output",
				"-J",
				"--job-name",
				"--wait",
			},
		}

		var err error
		e.mng, err = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
		if err != nil {
			return nil, err
		}
	}

	return e, nil
}

//

func batchCmd(state actions.Type) string {
	if state == actions.ReattachAction {
		return "srun"
	} else if state == actions.AllocateAction {
		return "salloc"
	}

	return "sbatch"
}

func stateCmd(cfg configure.Batch) string {
	if cfg.FluxSacctReplacement {
		return flux.DefaultState
	}

	if cfg.SlurmSqueue {
		return "squeue"
	}

	return "sacct"
}
