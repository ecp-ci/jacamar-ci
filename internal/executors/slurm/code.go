package slurm

import (
	"fmt"

	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/flux"
)

type jobStateCode int

const (
	jobBootFail jobStateCode = iota
	jobCancelled
	jobCompleted
	jobDeadline
	jobFailed
	jobNodeFail
	jobOOM
	jobPending
	jobRunning
	jobRequeued
	jobResizing
	jobRevoked
	jobSuspended
	jobTimeout
	jobUnknown
)

func (j jobStateCode) String() string {
	return []string{
		"BOOT_FAIL",
		"CANCELLED",
		"COMPLETED",
		"DEADLINE",
		"FAILED",
		"NODE_FAIL",
		"OUT_OF_MEMORY",
		"PENDING",
		"RUNNING",
		"REQUEUED",
		"RESIZING",
		"REVOKED",
		"SUSPENDED",
		"TIMEOUT",
		"UNKNOWN",
	}[j]
}

func (e *executor) retrieveJobState() (jobStateCode, error) {
	out, err := e.absExec.Runner.FileOutput(e.stateFile, e.constructStateCommand())
	if err != nil {
		return jobUnknown, fmt.Errorf("unable to obtained output via sacct: %s", out)
	}

	// https://slurm.schedmd.com/sacct.html#SECTION_JOB-STATE-CODES
	return translateStateOutput(out)
}

func translateStateOutput(out string) (jobStateCode, error) {
	switch parseSacctState(out) {
	case "BOOT_FAIL", "BF":
		return jobBootFail, nil
	case "CANCELLED", "CA":
		return jobCancelled, nil
	case "COMPLETED", "CD":
		return jobCompleted, nil
	case "DEADLINE", "DL":
		return jobDeadline, nil
	case "FAILED", "F":
		return jobFailed, nil
	case "NODEFAIL", "NF":
		return jobNodeFail, nil
	case "OUTOFMEMORY", "OOM":
		return jobOOM, nil
	case "PENDING", "PD":
		return jobPending, nil
	case "RUNNING", "R":
		return jobRunning, nil
	case "REQUEUED", "RQ":
		return jobRequeued, nil
	case "RESIZING", "RS":
		return jobResizing, nil
	case "REVOKED", "RV":
		return jobRevoked, nil
	case "SUSPENDED", "S":
		return jobSuspended, nil
	case "TIMEOUT", "TO":
		return jobTimeout, nil
	default:
		return jobUnknown, nil
	}
}

func translateFluxResult(r flux.JobResultCode) jobStateCode {
	switch r {
	case flux.ResultCanceled:
		return jobCancelled
	case flux.ResultCompleted:
		return jobCompleted
	case flux.ResultFailed:
		return jobFailed
	case flux.ResultTimeout:
		return jobTimeout
	default:
		return jobUnknown
	}
}
