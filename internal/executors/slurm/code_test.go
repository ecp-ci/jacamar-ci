package slurm

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_executor_retrieveJobState(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		mock        *mock_runmechanisms.MockRunner
		expected    string
		assertError func(*testing.T, error)
	}{
		"command error": {
			mock: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("", errors.New("error message"))
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"empty response": {
			mock: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("", nil)
				return m
			}(),
			expected: "UNKNOWN",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"shared name": {
			mock: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("NODE_FAIL\nCOMPLETED\nCOMPLETED", nil)
				return m
			}(),
			expected: "NODE_FAIL",
		},
		"clean string": {
			mock: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("CANCELLED+", nil)
				return m
			}(),
			expected: "CANCELLED",
		},
		"short oom": {
			mock: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("OOM", nil)
				return m
			}(),
			expected: "OUT_OF_MEMORY",
		},
		"running logout": {
			mock: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("  DL\nlogout", nil)
				return m
			}(),
			expected: "DEADLINE",
		},
		"logout only": {
			mock: func() *mock_runmechanisms.MockRunner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().FileOutput(gomock.Eq("state.out"), gomock.Any()).Return("logout", nil)
				return m
			}(),
			expected: "UNKNOWN",
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: &abstracts.Executor{
					Runner: tt.mock,
				},
				mng:       genericBatchMng(ctrl),
				stateFile: "state.out",
			}

			got, err := e.retrieveJobState()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.expected != "" {
				assert.Equal(t, tt.expected, got.String())
			}
		})
	}
}
