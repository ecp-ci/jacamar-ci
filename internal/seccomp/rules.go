//go:build linux
// +build linux

package seccomp

import (
	"fmt"
	"syscall"

	libseccomp "github.com/seccomp/libseccomp-golang"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

var (
	defaultAllowAct libseccomp.ScmpAction
	defaultBlockAct libseccomp.ScmpAction
)

func init() {
	defaultAllowAct = libseccomp.ActAllow
	defaultBlockAct = libseccomp.ActKillThread
}

type config struct {
	// defaultAction to be observed in filter.
	defaultAction libseccomp.ScmpAction
	// pluginFile (optional feature) to be used after filters established (prior to load).
	pluginFile string
	stage      string
	// rules complete list of all configured and internal that must be enabled.
	rules []rule
	// adminRules all configured calls and associated actions (block/allow).
	adminRules map[string]libseccomp.ScmpAction
	// apiLevel currently supported by the underlying system. A value of zero should indicate
	// a library older than v2.4.0, and we should avoid invalid configurations/log accordingly.
	// Source: https://github.com/seccomp/libseccomp/blob/main/doc/man/man3/seccomp_api_get.3
	apiLevel          uint
	disableNoNewPrivs bool
}

type rule struct {
	callName   string
	action     libseccomp.ScmpAction
	conditions []condition
}

type condition struct {
	compare libseccomp.ScmpCompareOp
	arg     uint
	val     uint64
}

func (f Factory) generate() (c config) {
	c.apiLevel = getAPI()
	defaultAct, allowAct, blockAct := f.setActions(c.apiLevel)

	c.defaultAction = defaultAct
	c.adminRules = make(map[string]libseccomp.ScmpAction)
	if !f.Opt.Auth.Seccomp.BlockAll {
		applyAdminRules(&c, blockAct, f.Opt.Auth.Seccomp.BlockCalls)
	}
	applyAdminRules(&c, allowAct, f.Opt.Auth.Seccomp.AllowCalls)

	setuidRules(&c, f.Opt)
	downscopeRules(&c, f.Opt, f.Usr)

	c.pluginFile = f.Opt.Auth.Seccomp.FilterPlugin
	c.stage = f.Stage
	c.disableNoNewPrivs = f.Opt.Auth.Seccomp.DisableNoNewPrivs

	return
}

func (f Factory) setActions(apiLvl uint) (defaultAct, allowAct, blockAct libseccomp.ScmpAction) {
	allowAct = defaultAllowAct
	blockAct = defaultBlockAct

	if f.Opt.Auth.Seccomp.LogAllowedActions {
		if apiLvl >= uint(3) {
			f.SysLog.Debug("logging (audit) all allowed seccomp actions")
			allowAct = libseccomp.ActLog
		} else {
			// Only support with  libseccomp API level 3 or higher. Log
			// invalid config but do not fail job.
			f.SysLog.Warn(fmt.Sprintf("unable to log_allowed_actions, current API level: %v", apiLvl))
		}
	}

	if f.Opt.Auth.Seccomp.ErrorNumBlockActions {
		blockAct = libseccomp.ActErrno.SetReturnCode(int16(syscall.EPERM))
	}

	if f.Opt.Auth.Seccomp.BlockAll {
		defaultAct = defaultBlockAct
	} else {
		defaultAct = allowAct
	}

	return
}

func applyAdminRules(c *config, action libseccomp.ScmpAction, list []string) {
	for _, v := range list {
		c.adminRules[v] = action
		c.rules = append(c.rules, rule{
			callName: v,
			action:   action,
		})
	}
}

func setuidRules(c *config, opt configure.Options) {
	if opt.Auth.Downscope == "setuid" {
		// Defined actions that would block all uses of 'ioctl' take priority.
		if opt.Auth.Seccomp.TTYRules && c.adminRules["ioctl"] != defaultBlockAct {
			ir := rule{
				callName: "ioctl",
				action:   defaultBlockAct,
				conditions: []condition{
					{
						compare: libseccomp.CompareEqual,
						arg:     1,
						val:     syscall.TIOCSTI,
					},
				},
			}
			c.rules = append(c.rules, ir)
		}
	}
}

func downscopeRules(c *config, opt configure.Options, usr authuser.UserContext) {
	if opt.Auth.Downscope == "setuid" {
		// Defined actions that would block setuid/setgid directly conflict with
		// the downscope method an admin defined and errors should be raised here.
		if opt.Auth.Seccomp.LimitSetuid {
			uid := rule{
				callName: "setuid",
				action:   defaultBlockAct,
				conditions: []condition{
					{
						compare: libseccomp.CompareNotEqual,
						arg:     0,
						val:     uint64(usr.UID),
					},
				},
			}
			gid := rule{
				callName: "setgid",
				action:   defaultBlockAct,
				conditions: []condition{
					{
						compare: libseccomp.CompareNotEqual,
						arg:     0,
						val:     uint64(usr.GID),
					},
				},
			}
			c.rules = append(c.rules, uid, gid)
		}
	}
}

// getAPI returns current API level, 0 indicates an error has been encountered.
// Source: https://pkg.go.dev/github.com/seccomp/libseccomp-golang#GetAPI
func getAPI() uint {
	apiLevel, _ := libseccomp.GetAPI()
	return apiLevel
}
