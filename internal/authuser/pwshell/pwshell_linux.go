//go:build linux && !getent
// +build linux,!getent

package pwshell

/*
   #include <pwd.h>
   #include <stdio.h>
   #include <unistd.h>
*/
import "C"
import (
	"errors"
	"strconv"
)

// FetchShell for Linux with CGO enabled, obtains the user's system defined
// shell via using the C getpwuid(3) function.
func FetchShell(uid string) (string, error) {
	u, err := strconv.ParseUint(uid, 10, 32)
	if err != nil {
		return "", err
	}

	s := C.getpwuid(C.__uid_t(u))
	if s == nil {
		return "", errors.New("no user found")
	}

	shell := C.GoString(s.pw_shell)

	return shell, nil
}
