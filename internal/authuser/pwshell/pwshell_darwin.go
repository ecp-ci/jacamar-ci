package pwshell

import (
	"errors"
)

// FetchShell targeting Darwin is not support and will generate an error.
func FetchShell(uid string) (string, error) {
	return "", errors.New("shell allowlist only supported on Linux")
}
