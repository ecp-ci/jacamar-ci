package fullauth

import (
	"errors"
	"io"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/validation"
	"gitlab.com/ecp-ci/jacamar-ci/internal/jobtoken"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_jobtoken"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_validation"
)

var (
	currentUsr *user.User
)

func init() {
	// Test involving user accounts must target a valid local account.
	currentUsr, _ = user.Current()
}

type authTests struct {
	auth    configure.Auth
	cfg     configure.Configurer
	env     envparser.ExecutorEnv
	factory Factory
	jwt     gljobctx.Claims
	job     jobtoken.EstablishedContext
	u       *authuser.UserContext
	valid   authuser.Validators

	mockEstValid func(configure.Configurer, envparser.ExecutorEnv) (authuser.Validators, error)
	mockCurUsr   func() (*user.User, error)

	assertError      func(*testing.T, error)
	assertContext    func(*testing.T, *authuser.UserContext)
	assertAuthorized func(*testing.T, authuser.Authorized)
}

func buildMockFullOpt() configure.Options {
	return configure.Options{
		Auth: configure.Auth{
			UserAllowlist:  []string{"u1", "u4", "u8"},
			UserBlocklist:  []string{"u3", "u4", "u8"},
			GroupAllowlist: []string{"g2", "g5", "g8"},
			GroupBlocklist: []string{"g7", "g8"},
		},
		General: configure.General{
			DataDir: "/ci",
		},
	}
}

func buildMockMinOpt() configure.Options {
	return configure.Options{
		Auth: configure.Auth{
			Downscope: "setuid",
		},
		General: configure.General{
			DataDir: "/ci",
		},
	}
}

func buildMockJWTCtx(
	ctrl *gomock.Controller,
	claims gljobctx.Claims,
) *mock_jobtoken.MockEstablishedContext {
	m := mock_jobtoken.NewMockEstablishedContext(ctrl)

	m.EXPECT().UserLogin().Return(claims.UserLogin).AnyTimes()
	m.EXPECT().DebugMsg().Return("{debug...}").AnyTimes()
	m.EXPECT().PipelineSource().Return(claims.PipelineSource).AnyTimes()
	m.EXPECT().ProjectPath().Return(claims.ProjectPath).AnyTimes()

	return m
}

func TestFactory_EstablishUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ll := logrus.New()
	ll.Out = io.Discard
	sysLog := logrus.NewEntry(ll)

	jobSuccess := mock_jobtoken.NewMockValidator(ctrl)
	jobSuccess.EXPECT().Run().Return(buildMockJWTCtx(ctrl, gljobctx.Claims{
		UserLogin:      currentUsr.Username,
		PipelineSource: "web",
		ProjectPath:    "group/project",
	}), nil).AnyTimes()

	jobFailed := mock_jobtoken.NewMockValidator(ctrl)
	jobFailed.EXPECT().Run().Return(nil, errors.New("error message")).AnyTimes()

	runasErr := mock_validation.NewMockRunAsValidator(ctrl)
	runasErr.EXPECT().Execute(gomock.Any(), gomock.Any(), gomock.Any()).Return(validation.RunAsOverride{}, errors.New("error message")).AnyTimes()

	runasOverride := mock_validation.NewMockRunAsValidator(ctrl)
	runasOverride.EXPECT().Execute(gomock.Any(), gomock.Any(), gomock.Any()).Return(validation.RunAsOverride{
		Username: currentUsr.Username,
		DataDir:  "/override/dir",
	}, nil).AnyTimes()

	tests := map[string]authTests{
		"invalid stateful username": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: "invalid-user",
					},
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					Job: jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to process state for invalid-user: user: unknown user invalid-user")
			},
		},
		"invalid jwt": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.failed.example",
						ServerURL: "failed.example",
					},
				},
				Valid: authuser.Validators{
					Job: jobFailed,
				},
				SysLog: sysLog,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"completed process from state, no downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: currentUsr.Username,
					},
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					Job: jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"context failed from state, setuid downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: currentUsr.Username,
					},
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					Job: jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "cannot execute as "+currentUsr.Username)
			},
		},
		"processes failed from jwt, setuid downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: currentUsr.Username,
					},
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					Job: jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "cannot execute as "+currentUsr.Username)
			},
		},
		"processes from jwt, no downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					Job: jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertAuthorized: func(t *testing.T, auth authuser.Authorized) {
				assert.Equal(t, currentUsr.Username, auth.CIUser().Username)
			},
		},
		"processes from jwt, invalid directory configurations": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir:        "/ci",
						CustomBuildDir: true,
					},
					Auth: configure.Auth{
						RootDirCreation: true,
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					Job: jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"processes from jwt, user block list pre-validation": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
					Auth: configure.Auth{
						ListsPreValidation: true,
						UserBlocklist: []string{
							currentUsr.Username,
						},
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					Job: jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "is not in the user allowlist and is in the user blocklist")
			},
		},
		"processes from jwt, user block list": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
					Auth: configure.Auth{
						UserBlocklist: []string{
							currentUsr.Username,
						},
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					Job: jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "is not in the user allowlist and is in the user blocklist")
			},
		},
		"processes from jwt, runas failure": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
					Auth: configure.Auth{
						UserBlocklist: []string{
							currentUsr.Username,
						},
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					RunAs: runasErr,
					Job:   jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to authorize user for CI job: invalid authorization target user: RunAs validation: error message")
			},
		},
		"processes from jwt, runas override": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					RunAs: runasOverride,
					Job:   jobSuccess,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertAuthorized: func(t *testing.T, authorized authuser.Authorized) {
				ctx := authorized.CIUser()
				assert.Equal(t, "/override/dir", ctx.DataDirOverride)
			},
		},
		"processes from jwt, root prevented": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.root.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
					},
					General: configure.General{
						DataDir: "/ci",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					Job: func() *mock_jobtoken.MockValidator {
						m := mock_jobtoken.NewMockValidator(ctrl)
						m.EXPECT().Run().Return(buildMockJWTCtx(ctrl, gljobctx.Claims{
							UserLogin:      "root",
							PipelineSource: "web",
							ProjectPath:    "group/project",
						}), nil)
						return m
					}(),
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "cannot execute as root while authorization is enforced")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.factory.EstablishUser()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertAuthorized != nil {
				tt.assertAuthorized(t, got)
			}
		})
	}
}

func Test_Authorized_Interface(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	working := targetCtx{
		usrCtx: authuser.UserContext{
			Username:  "username",
			HomeDir:   "/home/username",
			UID:       1000,
			GID:       2000,
			Groups:    []uint32{1, 2, 3},
			BaseDir:   "/base",
			BuildsDir: "/builds",
			CacheDir:  "/cache",
			ScriptDir: "/script",
		},
		job: buildMockJWTCtx(ctrl, gljobctx.Claims{}),
	}

	t.Run("verify basic interface functionality implemented", func(tt *testing.T) {
		working.BuildState()
		assert.Equal(tt, "Running as username UID: 1000 GID: 2000\n", working.PrepareNotification())
		assert.NotNil(tt, working.JobContext())
	})
}
