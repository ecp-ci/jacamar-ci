package validation

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
	"gitlab.com/ecp-ci/jacamar-ci/internal/jobtoken"

	"gitlab.com/ecp-ci/jacamar-ci/internal/rules"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	outputMessage = `RunAs script raw output: %s`
)

type runAsCfg struct {
	file   string
	sha256 string
	// targetUser is a user proposed login (e.g. service account)
	targetUser string
	env        []string
}

type runAsScript struct {
	msg logging.Messenger
	runAsCfg
}

// RunAsInit values are used to established user context for the upcoming authorization.
type RunAsInit struct {
	// TargetUser is a user proposed account (via the CI environment) meant to be the
	// replaced for the CurrentUser if the process is approved.
	TargetUser string
	// CurrentUser is the currently identified local user account of the CI trigger user.
	// This can differ from the JWT UserLogin depending on configuration of the authorization,
	// and should be observed when attempting to approve a local user account.
	CurrentUser string
	Job         jobtoken.EstablishedContext
}

func (rs runAsScript) Execute(
	job jobtoken.EstablishedContext,
	username string,
	sysLog *logrus.Entry,
) (over RunAsOverride, err error) {
	initializer, err := rs.runAsCfg.runAsPrep(job, username)
	if err != nil {
		return over, err
	}

	ok, output := rs.runAsCfg.invokeRunAsScript(rs.file, initializer, sysLog)
	err = parseScriptStdout(output, &over)

	if !ok {
		if over.UserMsg != "" {
			rs.msg.Stderr("RunAs message: %s", over.UserMsg)
		}

		if over.ErrorMsg != "" {
			// Log the complete output, so it is available for debug if expected.
			sysLog.Debug(fmt.Sprintf(outputMessage, output))

			return over, fmt.Errorf("script failed: %s", over.ErrorMsg)
		}

		// Default to the full output of the script being the error message.
		return over, fmt.Errorf("script failed - raw output: %s", string(output))
	}

	if err != nil {
		sysLog.Error(fmt.Sprintf(outputMessage, string(output)))
		return over, err
	}

	if over.UserMsg != "" {
		rs.msg.Stderr("RunAs message: %s", over.UserMsg)
	}
	sysLog.Debug(fmt.Sprintf(outputMessage, output))

	return finalizeOverride(initializer, over)
}

func finalizeOverride(initializer RunAsInit, over RunAsOverride) (RunAsOverride, error) {
	if over.Username == "" {
		if initializer.TargetUser != "" {
			over.Username = initializer.TargetUser
		} else {
			over.Username = initializer.CurrentUser
		}
	}

	if err := over.validator(); err != nil {
		return RunAsOverride{}, err
	}

	return over, nil
}

func (rc runAsCfg) runAsPrep(job jobtoken.EstablishedContext, username string) (RunAsInit, error) {
	if username == "" {
		return RunAsInit{},
			errors.New("unexpected system error encountered, no current user can be identified")
	}

	err := fileStatus(rc.file, rc.sha256)
	return rc.create(job, username), err
}

func (rc runAsCfg) create(job jobtoken.EstablishedContext, username string) RunAsInit {
	ra := RunAsInit{
		TargetUser:  rc.targetUser,
		CurrentUser: username,
		Job:         job,
	}
	return ra
}

// getRunAsUser attempts to find a user provided name in the custom executor
// provided environment. Nonexistent key/values will not result in an error.
func getRunAsUser(key string) (username string, err error) {
	if key != "" {
		username = strings.TrimSpace(os.Getenv(key))
	}

	v := rules.NewValidator()
	err = v.Var(username, "username")

	return
}

func runasEnv(initializer RunAsInit, sysLog *logrus.Entry) []string {
	env := initializer.Job.Environment()
	identities := initializer.Job.UserIdentities()
	if len(identities) > 0 {
		sysLog.Info(fmt.Sprintf("RunAs Identities found in Job: %s", identities))
	}

	env = append(env, []string{
		"RUNAS_TARGET_USER=" + initializer.TargetUser,
		"RUNAS_CURRENT_USER=" + initializer.CurrentUser,
	}...)

	return env
}

// invokeRunAsScript executes the provided script passing it the verifier and username
// as arguments ($ script verifier username), returning if true (exit status = 0)
// and  false (!=0) along with any stdout.
func (rc runAsCfg) invokeRunAsScript(script string, initializer RunAsInit, sysLog *logrus.Entry) (bool, []byte) {
	var cmd *exec.Cmd
	/* #nosec */
	// launching subprocess with variables required
	if initializer.TargetUser != "" {
		cmd = exec.Command(script, initializer.TargetUser, initializer.CurrentUser)
	} else {
		cmd = exec.Command(script, initializer.CurrentUser)
	}

	tarEnv := rc.env
	tarEnv = append(tarEnv, runasEnv(initializer, sysLog)...)
	for _, val := range os.Environ() {
		// We want to avoid passing potential tokens to more process than necessary.
		if !envparser.SupportedPrefix(val) {
			tarEnv = append(tarEnv, val)
		}
	}
	cmd.Env = tarEnv

	return runCommand(cmd)
}

// validator is used to ensure that all payload values confirm to Jacamar expectations.
func (ro RunAsOverride) validator() error {
	v := rules.NewValidator()

	// We only need to validate if a DataDir override is proposed.
	if ro.DataDir == "" {
		_ = v.RegisterValidation("directory", func(fl validator.FieldLevel) bool {
			return true
		})
	}

	return v.Struct(ro)
}
