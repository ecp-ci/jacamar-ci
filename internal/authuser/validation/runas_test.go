package validation

import (
	"io"
	"path/filepath"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_jobtoken"
)

func Test_runasEnv(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ll := logrus.New()
	ll.Out = io.Discard
	sysLog := logrus.NewEntry(ll)

	t.Run("all initializer value should translate to environment variables", func(t *testing.T) {
		job := mock_jobtoken.NewMockEstablishedContext(ctrl)
		job.EXPECT().UserIdentities().Return([]map[string]string{}).AnyTimes()
		job.EXPECT().Environment().Return([]string{
			"JWT_ISS=gitlab.example.com",
			"JWT_JOB_ID=jobID",
		})

		i := runAsCfg{
			targetUser: "targetUser",
		}.create(job, "currentUser")

		got := runasEnv(i, sysLog)
		assert.Contains(t, got, "RUNAS_TARGET_USER=targetUser")
		assert.Contains(t, got, "RUNAS_CURRENT_USER=currentUser")
		assert.Contains(t, got, "JWT_ISS=gitlab.example.com")
		assert.Contains(t, got, "JWT_JOB_ID=jobID")
	})

	t.Run("log job identities when encountered", func(t *testing.T) {
		job := mock_jobtoken.NewMockEstablishedContext(ctrl)
		job.EXPECT().UserIdentities().Return([]map[string]string{
			{"USER_ID_1": "test"},
		}).AnyTimes()
		job.EXPECT().Environment().Return([]string{
			"JWT_ISS=gitlab.example.com",
			"JWT_JOB_ID=jobID",
		})

		i := runAsCfg{
			targetUser: "targetUser",
		}.create(job, "currentUser")

		_ = runasEnv(i, sysLog)
	})
}

func Test_runasScript_Execute(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	job := mock_jobtoken.NewMockEstablishedContext(ctrl)

	job.EXPECT().UserIdentities().Return([]map[string]string{}).AnyTimes()
	job.EXPECT().UserLogin().Return("login").AnyTimes()
	job.EXPECT().Environment().Return([]string{
		"JWT_ISS=gitlab.example.com",
		"JWT_JOB_ID=jobID",
	}).AnyTimes()

	testScript, _ := filepath.Abs("../../../test/scripts/unit/runas.bash")
	envScript, _ := filepath.Abs("../../../test/scripts/unit/runas_env.bash")
	msgScript, _ := filepath.Abs("../../../test/scripts/unit/runas_msg.bash")

	testEnv := map[string]string{
		envkeys.UserEnvPrefix + "RUNAS_USER":   "pass",
		envkeys.UserEnvPrefix + "RUNAS_TARGET": "target",
		envkeys.UserEnvPrefix + "RUNAS_RANDOM": "random",
	}

	// Test logging using Pavilion2
	ll := logrus.New()
	ll.Out = io.Discard
	sysLog := logrus.NewEntry(ll)

	tests := map[string]struct {
		auth        configure.Auth
		curUsername string

		assertError         func(*testing.T, error)
		assertRunAsOverride func(*testing.T, RunAsOverride)
	}{
		"passing, valid script user + target": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					RunAsVariable:    "RUNAS_USER",
				},
			},
			curUsername: "user",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "pass", over.Username)
			},
		},
		"no current user specified": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					RunAsVariable:    "RUNAS_TARGET",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unexpected system error encountered, no current user can be identified",
				)
			},
		},
		"passing, valid user only": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
				},
			},
			curUsername: "gitlab",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "new", over.Username)
			},
		},
		"failing, invalid user + target": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					RunAsVariable:    "RUNAS_RANDOM",
				},
			},
			curUsername: "bad",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"failing, unable to unmarshall json returned": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
				},
			},
			curUsername: "json",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"invalid character ':' after top-level value",
				)
			},
		},
		"passing, shared group provided": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
				},
			},
			curUsername: "shared",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "new", over.Username)
			},
		},
		"passing, spaces appended/prepended to json": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
				},
			},
			curUsername: "spaces",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "new", over.Username)
			},
		},
		"passing, new lines appended/prepended to json": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
				},
			},
			curUsername: "newlines",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "new", over.Username)
			},
		},
		"correct sha256, no payload or target username provided": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					SHA256:           "a9c5d3f46b4afe03dac6f6401fa061c9e5affc57cede6d3b2fc00682b203b078",
				},
			},
			curUsername: "none",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "none", over.Username)
			},
		},
		"incorrect sha256 encountered for file": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: testScript,
					SHA256:           "123456",
				},
			},
			curUsername: "user",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "invalid checksum found for file")
				}
			},
		},
		"validation_env established": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: envScript,
					ValidationEnv:    []string{"VALIDATION_TEST_ENV=testing", "HELLO=WORLD"},
				},
			},
			curUsername: "gitlab",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "pass", over.Username)
			},
		},
		"validation_env no override": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: envScript,
					ValidationEnv:    []string{"RUNAS_CURRENT_USER=testing", "HELLO=WORLD"},
				},
			},
			curUsername: "gitlab",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "gitlab", over.Username)
			},
		},
		"passing, user message": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: msgScript,
				},
			},
			curUsername: "gitlab",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "new", over.Username)
				assert.Equal(t, "successful", over.UserMsg)
			},
		},
		"failing, user message": {
			auth: configure.Auth{
				RunAs: configure.RunAs{
					ValidationScript: msgScript,
				},
			},
			curUsername: "error",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "admin message")
			},
			assertRunAsOverride: func(t *testing.T, over RunAsOverride) {
				assert.Equal(t, "error encountered", over.UserMsg)
				assert.Equal(t, "admin message", over.ErrorMsg)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range testEnv {
				t.Setenv(k, v)
			}

			rs, err := NewRunAs(tt.auth)
			assert.NoError(t, err)

			got, err := rs.Execute(job, tt.curUsername, sysLog)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertRunAsOverride != nil {
				tt.assertRunAsOverride(t, got)
			}
		})
	}
}

func Test_finalizeOverride(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	job := mock_jobtoken.NewMockEstablishedContext(ctrl)
	job.EXPECT().UserLogin().Return("login").AnyTimes()

	type args struct {
		initializer RunAsInit
		over        RunAsOverride
	}
	tests := map[string]struct {
		args    args
		want    RunAsOverride
		wantErr bool
	}{
		"override username present": {
			args: args{
				over: RunAsOverride{
					Username: "good_user",
				},
			},
			want: RunAsOverride{
				Username: "good_user",
			},
		},
		"invalid username provided in override": {
			args: args{
				over: RunAsOverride{
					Username: "$(user)",
				},
			},
			wantErr: true,
		},
		"no override, use current user": {
			args: args{
				initializer: RunAsInit{
					CurrentUser: "current",
					Job:         job,
				},
			},
			want: RunAsOverride{
				Username: "current",
			},
		},
		"valid data_dir override provided": {
			args: args{
				initializer: RunAsInit{
					CurrentUser: "current",
					Job:         job,
				},
				over: RunAsOverride{
					DataDir: "/example/dir",
				},
			},
			want: RunAsOverride{
				Username: "current",
				DataDir:  "/example/dir",
			},
		},
		"invalid data_dir override provided": {
			args: args{
				initializer: RunAsInit{
					CurrentUser: "current",
					Job:         job,
				},
				over: RunAsOverride{
					DataDir: "\\invalid",
				},
			},
			wantErr: true,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := finalizeOverride(tt.args.initializer, tt.args.over)

			if (err != nil) != tt.wantErr {
				t.Errorf("finalizeOverride() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("finalizeOverride() got = %v, want %v", got, tt.want)
			}
		})
	}
}
