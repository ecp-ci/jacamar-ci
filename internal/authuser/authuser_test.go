package authuser_test

import (
	"os"
	"os/user"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

func TestUserContext_ProcessFromState(t *testing.T) {
	usr, err := authuser.CurrentUser()
	assert.NoError(t, err, "error identifying current user")

	type args struct {
		usr *user.User
		s   envparser.StatefulEnv
	}
	tests := map[string]struct {
		u                 *authuser.UserContext
		args              args
		assertError       func(*testing.T, error)
		assertUserContext func(*testing.T, authuser.UserContext)
	}{
		"error encountered setting interface UID": {
			args: args{
				usr: &user.User{
					Uid: "uid",
					Gid: "gid",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"error encountered setting interface GID": {
			args: args{
				usr: &user.User{
					Uid: "1000",
					Gid: "gid",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"update context from established state": {
			args: args{
				usr: usr,
				s: envparser.StatefulEnv{
					Username:  usr.Username,
					BaseDir:   "/base",
					BuildsDir: "/base/builds",
					CacheDir:  "/base/cache",
					ScriptDir: "/base/script",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertUserContext: func(t *testing.T, u authuser.UserContext) {
				assert.NotNil(t, u)
				assert.Equal(t, usr.Username, u.Username)
				assert.Equal(t, "/base", u.BaseDir)
				assert.Equal(t, usr.Gid, strconv.Itoa(int(u.GID)))
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			u, err := authuser.ProcessFromState(tt.args.usr, tt.args.s)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertUserContext != nil {
				tt.assertUserContext(t, u)
			}
		})
	}
}

func Test_CurrentUser(t *testing.T) {
	// At this time the CurrentUser function should simply defer to calling the
	// associated method in the user package. Enforce this until future changes may be made.
	sysUsr, sysErr := user.Current()
	usr, err := authuser.CurrentUser()

	assert.Equal(t, sysUsr, usr)
	assert.Equal(t, sysErr, err)
}

func TestEstablishValidators(t *testing.T) {
	type args struct {
		stage string
		opt   configure.Options
		env   envparser.ExecutorEnv
	}
	type estValidTests struct {
		args             args
		prepareEnv       func(*testing.T)
		assertValidators func(*testing.T, authuser.Validators)
		assertError      func(*testing.T, error)
	}

	tests := map[string]estValidTests{
		"standard validators, no runas": {
			args: args{
				stage: "config_exec",
				opt:   configure.Options{},
				env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID:     "1",
						ServerURL: "https://gitlab.example.com/",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertValidators: func(t *testing.T, validators authuser.Validators) {
				assert.Nil(t, validators.RunAs, "RunAs validator")
				assert.NotNil(t, validators.Job, "JWT validator")
			},
		},
		"error encountered creating RunAs validation": {
			args: args{
				opt: configure.Options{
					Auth: configure.Auth{
						RunAs: configure.RunAs{
							ValidationPlugin: "test",
						},
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"archive_cache stateful without delay config": {
			args: args{
				stage: "archive_cache",
				env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: "test",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	server := os.Getenv("CI_SERVER_URL")
	jobID := os.Getenv("CI_JOB_ID")
	userLogin := os.Getenv("GITLAB_USER_LOGIN")
	encodedID := os.Getenv("TEST_ID_JWT")

	if encodedID != "" {
		tests["valid id_token test"] = estValidTests{
			args: args{
				stage: "config_exec",
				opt: configure.Options{
					General: configure.General{
						JWTEnvVariable: "TEST_ID_JWT",
					},
					Auth: configure.Auth{},
				},
				env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID:     jobID,
						CIJobJWT:  encodedID,
						ServerURL: server,
					},
				},
			},
			prepareEnv: func(t *testing.T) {
				t.Setenv("CUSTOM_ENV_TEST_ID_JWT", encodedID)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertValidators: func(t *testing.T, validators authuser.Validators) {
				assert.Nil(t, validators.RunAs, "RunAs validator")
				assert.NotNil(t, validators.Job, "JWT validator")

				if validators.Job != nil {
					claims, err := validators.Job.Run()
					assert.NoError(t, err, "Job.Run() error")
					assert.Equal(t, userLogin, claims.UserLogin())
				}
			},
		}
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepareEnv != nil {
				tt.prepareEnv(t)
			}

			got, err := authuser.EstablishValidators(tt.args.stage, tt.args.opt, tt.args.env)

			if tt.assertValidators != nil {
				tt.assertValidators(t, got)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
