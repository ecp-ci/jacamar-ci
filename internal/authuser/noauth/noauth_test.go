package noauth

import (
	"errors"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_jobtoken"
)

func TestFactory_EstablishUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	claims := mock_jobtoken.NewMockEstablishedContext(ctrl)
	claims.EXPECT().DebugMsg().Return("debug...").AnyTimes()
	claims.EXPECT().ProjectPath().Return("group/project").AnyTimes()

	validJWT := mock_jobtoken.NewMockValidator(ctrl)
	validJWT.EXPECT().Run().Return(claims, nil).AnyTimes()

	invalidJWT := mock_jobtoken.NewMockValidator(ctrl)
	invalidJWT.EXPECT().Run().Return(nil, errors.New("error message")).AnyTimes()

	cur, _ := authuser.CurrentUser()

	tests := map[string]struct {
		f           Factory
		mockUser    func()
		assertAuth  func(*testing.T, authuser.Authorized)
		assertError func(*testing.T, error)
	}{
		"issue identifying user": {
			mockUser: func() {
				curUsr = func() (*user.User, error) {
					return nil, errors.New("error message")
				}
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"process current user from state": {
			f: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: cur.Username,
					},
				},
			},
			mockUser: func() {
				curUsr = authuser.CurrentUser
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertAuth: func(t *testing.T, auth authuser.Authorized) {
				assert.Equal(t, auth.CIUser().Username, cur.Username)
			},
		},
		"force process current user from state error": {
			f: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: cur.Username,
					},
				},
			},
			mockUser: func() {
				curUsr = func() (*user.User, error) {
					return &user.User{
						Uid: "0",
						Gid: "gid",
					}, nil
				}
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"process current user, jwt error": {
			f: Factory{
				Valid: authuser.Validators{
					Job: invalidJWT,
				},
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.failed.example",
						ServerURL: "failed.example",
					},
				},
			},
			mockUser: func() {
				curUsr = authuser.CurrentUser
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"process current user, data_dir error": {
			f: Factory{
				Valid: authuser.Validators{
					Job: validJWT,
				},
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
			},
			mockUser: func() {
				curUsr = authuser.CurrentUser
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to identify target directories: unsupported configuration, data_dir must be defined")
			},
		},
		"process current user, success": {
			f: Factory{
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
				},
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Valid: authuser.Validators{
					Job: validJWT,
				},
			},
			mockUser: func() {
				curUsr = authuser.CurrentUser
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockUser != nil {
				tt.mockUser()
			}

			got, err := tt.f.EstablishUser()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertAuth != nil {
				tt.assertAuth(t, got)
			}
		})
	}
}

func Test_Authorized_Interface(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	job := mock_jobtoken.NewMockEstablishedContext(ctrl)
	job.EXPECT().ProjectPath().Return("group/project").AnyTimes()

	working := targetCtx{
		usrCtx: authuser.UserContext{
			Username:  "username",
			HomeDir:   "/home/username",
			UID:       1000,
			GID:       2000,
			Groups:    []uint32{},
			BaseDir:   "/base",
			BuildsDir: "/builds",
			CacheDir:  "/cache",
			ScriptDir: "/script",
		},
		job: job,
	}

	t.Run("verify basic interface functionality implemented", func(tt *testing.T) {
		working.BuildState()
		assert.Equal(tt, "Running as username UID: 1000 GID: 2000\n", working.PrepareNotification())
		assert.NotNil(tt, working.JobContext())
	})
}
