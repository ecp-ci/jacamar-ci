// Package flock leverages the underlying system call (fcntl(2)) in conjunction
// with pre-established rules to generate unique directories on supporting systems
// that will not conflict with other jobs running across a range of resources.
package flock

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

var (
	// mngLogger supports optional debug logging during ManageLock lock process.
	mngLogger *logrus.Logger
)

const (
	// maxConcurrent is an arbitrary value established to prevent potential infinite loops. Any
	// currently supported deployment would not be able to simultaneous run anywhere near this many
	// jobs at the same time for a single project.
	maxConcurrent   = 999
	defaultTimeout  = 3600
	filePermissions = 0600

	reValidTime = 5 * time.Second

	rightDelimiter  = "}}"
	leftDelimiter   = "{{"
	lockDebugFolder = "lock_debug"
)

// LimitedDirAllowed identifies if the limited_build_dir feature is enabled based upon a
// combination of configuration bas optional user variables.
func LimitedDirAllowed(gen configure.General) bool {
	if gen.LimitBuildDir || gen.FFLimitBuildDir {
		if gen.UserEnabledLimit {
			return envparser.TrueEnvVar(envkeys.UserEnvPrefix + envkeys.JacamarLimitedDir)
		}
		return true
	}
	return false
}

// ProposedBuildsDir uses the current base directory to identify and generate a new
// proposed location that can be used in locking workflow. Please note that no
// directory will be created or concurrent value identified.
func ProposedBuildsDir(baseDir string, claims gljobctx.Claims) string {
	index := strings.LastIndex(claims.ProjectPath, "/")
	projectName := strings.ToLower(claims.ProjectPath[index+1:])

	return baseDir + "/builds/" + projectName + "_" + claims.ProjectID
}

// UnwrapConcurrentDir should be run to identify and verify the output from a
// 'jacamar lock' command. This can be used as the concurrent directory.
func UnwrapConcurrentDir(s string) (string, error) {
	out, _, _ := strings.Cut(s, rightDelimiter)
	_, out, _ = strings.Cut(out, leftDelimiter)
	_, err := strconv.Atoi(out)
	if err != nil {
		return "", errors.New("integer directory not identified")
	} else if out == "" {
		return "", errors.New("no directory provided")
	}

	return out, nil
}

func wrapConcurrentDir(s string) string {
	return fmt.Sprintf("%s%s%s", leftDelimiter, s, rightDelimiter)
}

// ManageLock maintains the workflow for generating a valid builds directory and returning
// the target (e.g., 003) via stdout if completed successfully.
func ManageLock(msg logging.Messenger, lCmd arguments.LockCmd, sysErr, exitZero func()) {
	lCmd.ProposedDir = filepath.Clean(lCmd.ProposedDir)

	// This directory may need generated prior to prepare_exec. As such it cannot
	// be assumed the downscoped user has the necessary permissions.
	if err := os.MkdirAll(lCmd.ProposedDir, convertPerms(lCmd.DirPermissions)); err != nil {
		msg.Stderr("Unable to generate builds dir: %s", err.Error())
		sysErr()
	}

	establishLogger(lCmd)

	cDir, err := generate(lCmd)
	if err != nil {
		mngLogger.Error(err.Error())
		msg.Stderr(err.Error())
		sysErr()
	}

	msg.Stdout(wrapConcurrentDir(cDir))
	exitZero()
}

// Release attempts to remove the expiration from the lock allowing potential re-use
// and cleanup of the target directory.
func Release(env envparser.ExecutorEnv) {
	// Errors identified either opening the file or obtaining the lock should result
	// in a skip. Future jobs that attempt to utilize these folders will encounter the
	// same error (if system/permission related) and the resulting message will be
	// better presented during prepare_exec.

	file, err := openLocked(statefulLockName(env.StatefulEnv), syscall.F_SETLKW)
	if err != nil {
		return
	}
	defer relinquishLock(file)

	ld := read(file)
	_ = ld.release()
}

// Cleanup removes expired subdirectories whose concurrent value exceeds the maximum allowed.
func Cleanup(gen configure.General, env envparser.ExecutorEnv) error {
	if gen.MaxBuildDir == 0 {
		// Zero indicates no maximum concurrent folder restrictions.
		return nil
	}

	targetDir := statefulBaseName(env.StatefulEnv)
	subDirs, err := currentSubDirs(targetDir)
	if err != nil {
		return err
	}

	if len(subDirs) > gen.MaxBuildDir {
		for i := gen.MaxBuildDir; i < len(subDirs); i++ {
			if subDirs[i].Name() == lockDebugFolder {
				continue
			}

			result := cleanupLock(targetDir, subDirs[i].Name())
			if result && !gen.UncapBuildDirCleanup {
				break
			}
		}
	}

	return nil
}

// generate uses the file locking workflows to create a dedicated concurrent
// folder within the proposed directory that will be unique for the job's duration.
// If required the new directory with concurrent value post-fixed will be created
// (example: /test/directory -> /test/directory/004) and the utilized concurrent value
// is returned to support updating the auth processes identified builds_dir. Any created
// directory will observe the provided FileMode.
func generate(lCmd arguments.LockCmd) (string, error) {
	// Loop until we identify a valid directory or the maximum concurrent value is reached.
	for concurrent := range maxConcurrent {
		obtained, tmpCon, err := generateLock(lCmd, concurrent)
		if err != nil {
			mngLogger.Infof(
				"unable to lock file %v: %s",
				concurrent,
				err.Error(),
			)
			continue
		} else if obtained {
			mngLogger.Infof("identified concurrent target: %s", tmpCon)
			return tmpCon, nil
		}
	}

	return "", fmt.Errorf("surpassed maximum concurrent value (%v)", maxConcurrent)
}

func generateLock(lCmd arguments.LockCmd, concurrent int) (bool, string, error) {
	targetCon := zeroPadConcurrent(concurrent)
	targetDir := targetDirName(targetCon, lCmd.ProposedDir)
	targetLock := lockFileName(targetCon, lCmd.ProposedDir)

	file, err := openCreate(targetLock)
	if err != nil {
		return false, "", fmt.Errorf("failed to create lock file: %w", err)
	}

	if err = fcntlSyscall(file.Fd(), syscall.F_WRLCK, syscall.F_SETLK); err != nil {
		return false, "", fmt.Errorf("fcntl syscall error: %w", err)
	}
	defer relinquishLock(file)

	ld := read(file)
	if !ld.expired() {
		mngLogger.Infof("lock file %s has not expired", targetLock)
		return false, "", nil
	}
	err = ld.claim(lCmd)
	if err != nil {
		mngLogger.Warnf("failed to claim %s: %s", targetLock, err.Error())
	}

	if err == nil {
		// This directory may need generated prior to prepare_exec.
		err := os.MkdirAll(targetDir, convertPerms(lCmd.DirPermissions))
		if err != nil {
			return false, "", fmt.Errorf("failed to create lock dir: %w", err)
		}
	}

	// I'm not confident this will address all issues but due to limits in my ability
	// to test all cases this optional check attempts to ensure the locks file is
	// still restricted to this specific job.
	if lCmd.Revalidate {
		time.Sleep(reValidTime)
		if err = fcntlSyscall(file.Fd(), syscall.F_RDLCK, syscall.F_GETLK); err != nil {
			return false, "", fmt.Errorf("failed to re-obtain F_GETLK: %w", err)
		}

		reLd := read(file)
		if reLd.JobID != lCmd.JobID {
			return false, "", fmt.Errorf("failed revalidation, claimed by %s", reLd.JobID)
		}
	}

	mngLogger.Infof("file claimed with %s expiration on %s host", ld.Expiration, ld.Hostname)

	return err == nil, targetCon, err
}

// cleanupLock attempts to remove the lock file and associated directory. If successful
// will return 'true'.
func cleanupLock(baseDir, subDir string) bool {
	file, err := openLocked(lockFileName(subDir, baseDir), syscall.F_SETLK)
	if err != nil {
		return false
	}
	defer relinquishLock(file)

	if (read(file)).expired() {
		err = os.RemoveAll(fmt.Sprintf("%s/%s", baseDir, subDir))
		return err == nil
	}

	return false
}

func openLocked(fileName string, lockCmd int) (*os.File, error) {
	file, err := openCreate(fileName)
	if err == nil {
		err = fcntlSyscall(file.Fd(), syscall.F_WRLCK, lockCmd)
	}

	return file, err
}

func zeroPadConcurrent(concurrent int) string {
	cID := strconv.Itoa(concurrent)
	for len(cID) < 3 {
		cID = "0" + cID
	}

	return cID
}

func openCreate(fileName string) (*os.File, error) {
	/* #nosec */
	// variable file path required
	return os.OpenFile(fileName, os.O_CREATE|os.O_RDWR, filePermissions)
}

func relinquishLock(file *os.File) {
	if file != nil {
		/* #nosec */
		// Errors cannot be handled and no user notification possible.
		_ = fcntlSyscall(file.Fd(), syscall.F_UNLCK, syscall.F_SETLKW)
		_ = file.Close()
	}
}

func fcntlSyscall(fd uintptr, how int16, cmd int) error {
	// https://man7.org/linux/man-pages/man2/fcntl.2.html
	lk := syscall.Flock_t{
		Type:   how,
		Start:  0,
		Len:    0,
		Whence: io.SeekStart,
	}

	return syscall.FcntlFlock(fd, cmd, &lk)
}

func establishLogger(lCmd arguments.LockCmd) {
	mngLogger = logrus.New()
	mngLogger.SetOutput(io.Discard)

	if lCmd.Debug {
		debugDir := fmt.Sprintf("%s/%s", lCmd.ProposedDir, lockDebugFolder)
		_ = os.MkdirAll(debugDir, convertPerms(lCmd.DirPermissions))

		filePath := fmt.Sprintf("%s/%s.json", debugDir, lCmd.JobID)

		/* #nosec */
		// variable file path required
		f, err := os.OpenFile(filePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, filePermissions)

		// We have no way to deal with this error and will just need to defer to
		// the default of no logging.
		if err == nil {
			mngLogger.SetFormatter(&logrus.JSONFormatter{DisableTimestamp: false})
			mngLogger.SetOutput(f)
		}
	}
}
