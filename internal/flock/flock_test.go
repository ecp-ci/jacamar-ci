package flock

import (
	"os"
	"sync"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func createLock(t *testing.T) string {
	fileName := t.TempDir() + "/release.lock"
	f, _ := openCreate(fileName)
	defer func() { _ = f.Close() }()

	_, _ = f.WriteString(`{"job_id":"123","expiration":"TEST"}`)

	return fileName
}

func generateReleaseDirs(t *testing.T, env envparser.ExecutorEnv, baseDir string) {
	establishLogger(arguments.LockCmd{Debug: false})
	_ = os.MkdirAll(baseDir, 0700)

	_, _ = generate(arguments.LockCmd{
		ProposedDir:    baseDir,
		JobID:          env.RequiredEnv.JobID,
		JobTimeout:     "3600",
		DirPermissions: "448",
	})
	_, _ = generate(arguments.LockCmd{
		ProposedDir:    baseDir,
		JobID:          "2002",
		JobTimeout:     "3600",
		DirPermissions: "448",
	})
	_, _ = generate(arguments.LockCmd{
		ProposedDir:    baseDir,
		JobID:          "3003",
		JobTimeout:     "3600",
		DirPermissions: "448",
	})

	subs, _ := currentSubDirs(baseDir)
	assert.Len(t, subs, 3, "generate failed")

	Release(env)

	env.RequiredEnv.JobID = "2002"
	env.StatefulEnv.BuildsDir = baseDir + "/001"
	Release(env)

	env.RequiredEnv.JobID = "3003"
	env.StatefulEnv.BuildsDir = baseDir + "/002"
	Release(env)
}

func Test_LimitedDirAllowed(t *testing.T) {
	t.Run("user enabled", func(t *testing.T) {
		t.Setenv(envkeys.UserEnvPrefix+envkeys.JacamarLimitedDir, "1")
		assert.True(t, LimitedDirAllowed(configure.General{
			LimitBuildDir:    true,
			UserEnabledLimit: true,
		}))
	})
}

func Test_DirStructures(t *testing.T) {
	t.Run("ProposedBuildsDir - basic structure", func(t *testing.T) {
		got := ProposedBuildsDir("/ci/user", gljobctx.Claims{
			ProjectID:   "123",
			ProjectPath: "group/sub-group/project",
		})
		assert.Equal(t, "/ci/user/builds/project_123", got)
	})

	t.Run("statefulLockName", func(t *testing.T) {
		got := statefulLockName(envparser.StatefulEnv{
			BuildsDir: "/test/dir/project_1/000",
		})
		assert.Equal(t, "/test/dir/project_1/.000.lock", got)
	})
}

func Test_UnwrapConcurrentDir(t *testing.T) {
	t.Run("valid output", func(t *testing.T) {
		got, err := UnwrapConcurrentDir("{{005}}Logout")
		assert.NoError(t, err)
		assert.Equal(t, "005", got)
	})

	t.Run("invalid output", func(t *testing.T) {
		_, err := UnwrapConcurrentDir("{{abc}}")
		assert.Error(t, err)
	})

	t.Run("no output", func(t *testing.T) {
		_, err := UnwrapConcurrentDir("")
		assert.Error(t, err)
	})
}

func TestManageLock(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	sysErr := func() { return }
	exitZero := func() { return }

	tests := map[string]struct {
		msg  logging.Messenger
		lCmd arguments.LockCmd
	}{
		"invalid timeout results in default": {
			lCmd: arguments.LockCmd{
				ProposedDir:    t.TempDir() + "/timeout",
				DirPermissions: "448",
				JobTimeout:     "abc",
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stderr(gomock.Any(), gomock.Any()).AnyTimes()
				m.EXPECT().Stdout(gomock.Any()).AnyTimes()
				return m
			}(),
		},
		"valid workflow": {
			lCmd: arguments.LockCmd{
				JobID:       "123",
				JobTimeout:  "1000",
				ProposedDir: t.TempDir(),
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stdout("{{000}}").AnyTimes()
				return m
			}(),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ManageLock(tt.msg, tt.lCmd, sysErr, exitZero)
		})
	}
}

func TestGenerate(t *testing.T) {
	tests := map[string]struct {
		proposedDir string
		jobID       string
		jobTimeout  string
		revalidate  bool

		assertString func(*testing.T, string, string)
		assertError  func(*testing.T, error)
	}{
		"single generate process": {
			proposedDir: t.TempDir() + "/single",
			jobID:       "123",
			jobTimeout:  "3600",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s, dir string) {
				assert.Equal(t, "000", s)
				_, err := os.ReadDir(dir + "/000")
				assert.NoError(t, err, "missing directory")
			},
		},
		"revalidate": {
			proposedDir: t.TempDir() + "/revalidate",
			jobID:       "456",
			jobTimeout:  "3600",
			revalidate:  true,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s, dir string) {
				assert.Equal(t, "000", s)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			lCmd := arguments.LockCmd{
				ProposedDir:    tt.proposedDir,
				JobID:          tt.jobID,
				JobTimeout:     tt.jobTimeout,
				DirPermissions: "448",
				Debug:          true,
				Revalidate:     tt.revalidate,
			}

			establishLogger(lCmd)

			got, err := generate(lCmd)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got, tt.proposedDir)
			}
		})
	}
	t.Run("multiple generate processes", func(t *testing.T) {
		multiDir := t.TempDir() + "/multi"
		_ = os.MkdirAll(t.TempDir()+"/multi", 0700)

		var wg sync.WaitGroup
		wg.Add(2)

		establishLogger(arguments.LockCmd{
			ProposedDir: multiDir,
			JobID:       "000",
			Debug:       true,
		})

		got, err := generate(arguments.LockCmd{
			ProposedDir:    multiDir,
			JobID:          "111",
			JobTimeout:     "3600",
			DirPermissions: "448",
		})
		assert.NoError(t, err)
		assert.Equal(t, "000", got)

		got, err = generate(arguments.LockCmd{
			ProposedDir:    multiDir,
			JobID:          "222",
			JobTimeout:     "3600",
			DirPermissions: "448",
		})
		assert.NoError(t, err)
		assert.Equal(t, "001", got)

		subDirs, _ := currentSubDirs(multiDir)
		assert.Len(t, subDirs, 3, "initial 2 dirs + debug")

		got, err = generate(arguments.LockCmd{
			ProposedDir:    multiDir,
			JobID:          "333",
			JobTimeout:     "3600",
			DirPermissions: "448",
		})
		assert.NoError(t, err)
		assert.Equal(t, "002", got)

		subDirs, _ = currentSubDirs(multiDir)
		assert.Len(t, subDirs, 4)

		b, _ := os.ReadFile(multiDir + "/lock_debug/000.json")
		assert.Contains(t, string(b), "identified concurrent target")

	})
	t.Run("re-used expired locks", func(t *testing.T) {
		expiredDir := t.TempDir() + "/expired"
		_ = os.MkdirAll(t.TempDir()+"/expired", 0700)

		var wg sync.WaitGroup
		wg.Add(1)

		establishLogger(arguments.LockCmd{
			ProposedDir: expiredDir,
			JobID:       "000",
			Debug:       true,
		})

		go func() {
			defer wg.Done()
			_, err := generate(arguments.LockCmd{
				ProposedDir:    expiredDir,
				JobID:          "111",
				JobTimeout:     "-1000",
				DirPermissions: "448",
			})
			assert.NoError(t, err)
		}()

		wg.Wait()

		got, err := generate(arguments.LockCmd{
			ProposedDir:    expiredDir,
			JobID:          "222",
			JobTimeout:     "3600",
			DirPermissions: "448",
		})
		assert.NoError(t, err)
		assert.Equal(t, "000", got)

		subDirs, _ := currentSubDirs(expiredDir)
		assert.Len(t, subDirs, 2)

		b, _ := os.ReadFile(expiredDir + "/lock_debug/000.json")
		assert.Contains(t, string(b), "identified concurrent target")
	})
}

func TestRelease(t *testing.T) {
	testDir := t.TempDir()

	tests := map[string]struct {
		env envparser.ExecutorEnv

		prepDirs    func(*testing.T, envparser.ExecutorEnv)
		assertFiles func(*testing.T, envparser.ExecutorEnv)
	}{
		"missing directory": {
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: t.TempDir() + "/missing/000",
				},
			},
		},
		"release file": {
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "1001",
				},
				StatefulEnv: envparser.StatefulEnv{
					BaseDir:   testDir + "/release",
					BuildsDir: testDir + "/release/project_1/000",
				},
			},
			prepDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				lCmd := arguments.LockCmd{
					ProposedDir:    env.StatefulEnv.BaseDir + "/project_1",
					JobID:          env.RequiredEnv.JobID,
					JobTimeout:     "3600",
					DirPermissions: "448",
				}
				os.MkdirAll(lCmd.ProposedDir, 0700)
				establishLogger(lCmd)
				got, err := generate(lCmd)
				assert.NoError(t, err, "preparing test directories")
				assert.Equal(t, "000", got)
			},
			assertFiles: func(t *testing.T, env envparser.ExecutorEnv) {
				file, _ := openCreate(env.StatefulEnv.BaseDir + "/project_1/.000.lock")
				ld := read(file)
				assert.Equal(t, "", ld.Expiration)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepDirs != nil {
				tt.prepDirs(t, tt.env)
			}

			Release(tt.env)

			if tt.assertFiles != nil {
				tt.assertFiles(t, tt.env)
			}
		})
	}
}

func TestCleanup(t *testing.T) {
	testDir := t.TempDir()

	tests := map[string]struct {
		gen configure.General
		env envparser.ExecutorEnv

		prepDirs    func(*testing.T, envparser.ExecutorEnv)
		assertError func(*testing.T, error)
		assertDirs  func(*testing.T, envparser.ExecutorEnv)
	}{
		"skip when MaxBuildDir == 0": {
			gen: configure.General{
				MaxBuildDir: 0,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"cleanup folder (single released)": {
			gen: configure.General{
				LimitBuildDir: true,
				MaxBuildDir:   1,
			},
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "1001",
				},
				StatefulEnv: envparser.StatefulEnv{
					BaseDir:   testDir + "/release",
					BuildsDir: testDir + "/release/project_1/000",
				},
			},
			prepDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				generateReleaseDirs(t, env, env.StatefulEnv.BaseDir+"/project_1")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				subs, _ := currentSubDirs(env.StatefulEnv.BaseDir + "/project_1")
				assert.Len(t, subs, 2)
			},
		},
		"cleanup folder (uncapped)": {
			gen: configure.General{
				LimitBuildDir:        true,
				MaxBuildDir:          1,
				UncapBuildDirCleanup: true,
			},
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "1001",
				},
				StatefulEnv: envparser.StatefulEnv{
					BaseDir:   testDir + "/release",
					BuildsDir: testDir + "/release/project_2/000",
				},
			},
			prepDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				generateReleaseDirs(t, env, env.StatefulEnv.BaseDir+"/project_2")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				subs, _ := currentSubDirs(env.StatefulEnv.BaseDir + "/project_2")
				assert.Len(t, subs, 1)
			},
		},
		"cleanup folder (default)": {
			gen: configure.General{
				LimitBuildDir: true,
				MaxBuildDir:   1,
			},
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "1001",
				},
				StatefulEnv: envparser.StatefulEnv{
					BaseDir:   testDir + "/release",
					BuildsDir: testDir + "/release/project_3/000",
				},
			},
			prepDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				generateReleaseDirs(t, env, env.StatefulEnv.BaseDir+"/project_3")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				subs, _ := currentSubDirs(env.StatefulEnv.BaseDir + "/project_3")
				assert.Len(t, subs, 2)
			},
		},
		"skip debug": {
			gen: configure.General{
				LimitBuildDir:        true,
				MaxBuildDir:          1,
				UncapBuildDirCleanup: true,
			},
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobID: "1001",
				},
				StatefulEnv: envparser.StatefulEnv{
					BaseDir:   testDir + "/release",
					BuildsDir: testDir + "/release/project_100/000",
				},
			},
			prepDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				generateReleaseDirs(t, env, env.StatefulEnv.BaseDir+"/project_100")
				os.Mkdir(env.StatefulEnv.BaseDir+"/project_100/"+lockDebugFolder, 0700)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDirs: func(t *testing.T, env envparser.ExecutorEnv) {
				subs, _ := currentSubDirs(env.StatefulEnv.BaseDir + "/project_100")
				var found bool
				for _, v := range subs {
					if v.Name() == lockDebugFolder {
						found = true
					}
				}
				assert.True(t, found, lockDebugFolder+" removed")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepDirs != nil {
				tt.prepDirs(t, tt.env)
			}

			err := Cleanup(tt.gen, tt.env)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertDirs != nil {
				tt.assertDirs(t, tt.env)
			}
		})
	}
}

func Test_cleanupLock(t *testing.T) {
	t.Run("cleanup released file", func(t *testing.T) {
		testDir := t.TempDir()

		env := envparser.ExecutorEnv{
			RequiredEnv: envparser.RequiredEnv{
				JobID: "1234",
			},
			StatefulEnv: envparser.StatefulEnv{
				BaseDir:   testDir + "/release",
				BuildsDir: testDir + "/release/project_1/000",
			},
		}

		lCmd := arguments.LockCmd{
			ProposedDir:    testDir + "/release/project_1",
			JobID:          env.RequiredEnv.JobID,
			JobTimeout:     "-3600",
			DirPermissions: "448",
		}

		establishLogger(lCmd)
		os.MkdirAll(lCmd.ProposedDir, 0700)

		_, err := generate(lCmd)
		assert.NoError(t, err, "generate expired lock")
		Release(env)

		got := cleanupLock(testDir+"/release/project_1", "000")
		assert.True(t, got)
	})
}

func Test_establishLogger(t *testing.T) {
	t.Run("no logging", func(t *testing.T) {
		dir := t.TempDir() + "/none"

		establishLogger(arguments.LockCmd{
			JobID:          "123",
			ProposedDir:    dir,
			DirPermissions: "448",
		})
		mngLogger.Info("testing...")
		assert.NoFileExists(t, dir+"/lock_debug/123.json")
	})

	t.Run("logging enabled", func(t *testing.T) {
		dir := t.TempDir() + "/enable"
		establishLogger(arguments.LockCmd{
			JobID:          "123",
			ProposedDir:    dir,
			DirPermissions: "448",
			Debug:          true,
		})
		mngLogger.Info("testing...")
		assert.FileExists(t, dir+"/lock_debug/123.json")
	})
}
