package flock

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type lockedData struct {
	JobID      string `json:"job_id"`
	Expiration string `json:"expiration"`
	Hostname   string `json:"hostname"`

	file *os.File
}

// read examines the provided lock file and attempt to unmarshal the lockfile
// data. Regardless of error an empty structure will always be returned.
func read(file *os.File) *lockedData {
	var ld lockedData
	ld.file = file

	_, _ = file.Seek(0, 0)
	data := make([]byte, 0, 256)
	for {
		n, err := file.Read(data[len(data):cap(data)])
		data = data[:len(data)+n]
		if err != nil {
			if err == io.EOF {
				_ = json.Unmarshal(data, &ld)
			}
			break
		}
	}

	return &ld
}

func (l *lockedData) claim(lCmd arguments.LockCmd) error {
	jobTimeout, err := strconv.Atoi(lCmd.JobTimeout)
	if err != nil {
		jobTimeout = defaultTimeout
	}

	now := time.Now()
	dur := time.Second * time.Duration(jobTimeout+600)

	l.Expiration = strconv.FormatInt(now.Add(dur).Unix(), 10)
	l.JobID = lCmd.JobID

	// If no valid hostname is identified rely on default empty string.
	name, _ := os.Hostname()
	l.Hostname = name

	return l.update()
}

func (l *lockedData) release() error {
	l.Expiration = ""

	return l.update()
}

func (l *lockedData) update() error {
	var b []byte

	_, _ = l.file.Seek(0, 0)
	err := syscall.Truncate(l.file.Name(), 0)
	if err == nil {
		b, err = json.Marshal(l)
		if err == nil {
			_, err = l.file.Write(b)
		}
	}

	return err
}

// expired examines the identified lockfile date and determines if the expiration has been reached.
func (l *lockedData) expired() bool {
	if l.Expiration == "" {
		return true
	}

	now := time.Now()
	exp, _ := strconv.ParseInt(l.Expiration, 10, 64)

	return now.Unix() >= exp
}

// currentSubDirs returns a slice of subdirectories found within path.
func currentSubDirs(dirname string) (dirs []os.DirEntry, err error) {
	tmpDir, err := os.ReadDir(dirname)
	if err != nil {
		return
	}

	for _, d := range tmpDir {
		if d.IsDir() {
			dirs = append(dirs, d)
		}
	}
	sort.Slice(dirs, func(t, s int) bool {
		return dirs[t].Name() < dirs[s].Name()
	})

	return
}

func statefulBaseName(state envparser.StatefulEnv) string {
	i := strings.LastIndex(state.BuildsDir, "/")

	return state.BuildsDir[0:i]
}

func statefulConcurrent(state envparser.StatefulEnv) string {
	return state.BuildsDir[len(state.BuildsDir)-3:]
}

func statefulLockName(state envparser.StatefulEnv) string {
	dir := statefulBaseName(state)
	concurrent := statefulConcurrent(state)

	return lockFileName(concurrent, dir)
}

func targetDirName(concurrent, dirname string) string {
	return fmt.Sprintf("%s/%s", dirname, concurrent)
}

func lockFileName(concurrent, dirname string) string {
	return fmt.Sprintf("%s/.%s.lock", dirname, concurrent)
}

func convertPerms(dirPermissions string) os.FileMode {
	i, err := strconv.ParseUint(dirPermissions, 10, 32)
	if err != nil {
		return os.FileMode(usertools.OwnerPermissions)
	}

	return os.FileMode(uint32(i))
}
