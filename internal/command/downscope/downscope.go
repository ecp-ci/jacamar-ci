package downscope

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"syscall"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/verifycaps"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

var (
	// signalsAllowed identify if traditional kill(2) signals can be used
	// with setuid or if wrapper required.
	signalsAllowed func() bool
)

const (
	// byteLimit is the maximum capacity for any output captured.
	byteLimit = 50
)

func init() {
	signalsAllowed = verifycaps.SignalAllowed
}

// Factory is used to defined requirements and interface to all related methods.
type Factory struct {
	AbsCmdr  *command.AbstractCommander
	SysLog   *logrus.Entry
	Cfg      configure.Configurer
	AuthUser authuser.Authorized
	Concrete arguments.ConcreteArgs
}

type shell struct {
	abs   *command.AbstractCommander
	sig   command.Signaler
	cmd   *exec.Cmd
	pty   bool
	mutex sync.Mutex
}

func (s *shell) PipeOutput(stdin string) error {
	cmd := s.CopiedCmd()

	var stdoutBuf, stderrBuf bytes.Buffer
	cmd.Stdout = io.MultiWriter(os.Stdout, &stdoutBuf)
	cmd.Stderr = io.MultiWriter(os.Stderr, &stderrBuf)
	if stdin != "" {
		s.prepStdin(stdin, cmd)
	}

	return s.abs.RunCmd(cmd, s.sig)
}

func (s *shell) ReturnOutput(stdin string) (string, error) {
	cmd := s.CopiedCmd()

	b := newBuffer(byteLimit)
	cmd.Stdout = b
	cmd.Stderr = b
	if stdin != "" {
		s.prepStdin(stdin, cmd)
	}

	err := s.abs.RunCmd(cmd, s.sig)

	return b.String(), err
}

func (s *shell) CommandDir(dir string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.cmd.Dir = dir
}

func (s *shell) RequestContext() context.Context {
	return s.abs.SignalContext()
}

func (s *shell) SigtermReceived() bool {
	return s.abs.SigtermReceived()
}

func (s *shell) AppendEnv(e []string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.cmd.Env = append(s.cmd.Env, e...)
}

func (s *shell) CopiedCmd() *exec.Cmd {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	cmd := &exec.Cmd{}
	command.CloneCmd(s.cmd, cmd)

	return cmd
}

func (s *shell) ModifyCmd(name string, arg ...string) {
	log.Println("ModifyCmd not supported for Downscoping")
}

func (s *shell) build(name string, arg ...string) {
	/* #nosec */
	// launching subprocess with variables required
	cmd := exec.Command(name, arg...)
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	s.cmd = cmd
}

func (s *shell) prepStdin(stdin string, cmd *exec.Cmd) {
	var sb strings.Builder
	sb.WriteString(stdin)

	if s.pty {
		cmd.Args = append(cmd.Args, sb.String())
	} else {
		cmd.Stdin = bytes.NewBufferString(sb.String())
	}
}

func (f Factory) identifyWorkDir() string {
	tar := f.Cfg.Auth().DownscopeCmdDir
	if tar != "" {
		return filepath.Clean(tar)
	}

	// JacamarCmd will always return a path in this workflow.
	path, _ := command.JacamarCmd(f.Cfg.Auth(), f.Concrete)
	return filepath.Dir(path)
}

// safeEnv generate a safe exec.Command environment for any downscoping by
// limiting scope to a select number of predefined variables and plausible
// CUSTOM_ENV_ and JACAMAR_CI_ ones.
func safeEnv(au authuser.Authorized, cfg configure.Auth) []string {
	var safe []string
	for _, e := range os.Environ() {
		if envparser.SupportedPrefix(e) {
			safe = append(safe, e)
		}
	}

	name := au.CIUser().Username
	sys, build := envparser.ExitCodes()

	safe = append(safe, []string{
		fmt.Sprintf("%s=%d", envkeys.SysExitCode, sys),
		fmt.Sprintf("%s=%d", envkeys.BuildExitCode, build),
		fmt.Sprintf("USER=%s", name),
		fmt.Sprintf("LOGNAME=%s", name),
		fmt.Sprintf("HOME=%s", au.CIUser().HomeDir),
		"PATH=/usr/bin:/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin",
	}...)

	return append(safe, cfg.DownscopeEnv...)
}

// runSuppressOutput run the provided command with all stdout/stderr discarded.
// This is used in cases where output would be ignored by the runner
// (e.g., during job termination).
func runSuppressOutput(cmd *exec.Cmd) (err error) {
	cmd.Stdout = io.Discard
	cmd.Stderr = io.Discard
	cmd.Stdin = nil

	err = cmd.Start()
	if err == nil {
		err = cmd.Wait()
	}

	return
}

// limitedBuffer implements an io.Writer interface while strict maximum capacity
// on the bytes.Buffer. Any byte array attempting to write to a "full" buffer will
// simply be ignored.
type limitedBuffer struct {
	buf *bytes.Buffer
	cap int
}

func newBuffer(cap int) *limitedBuffer {
	return &limitedBuffer{
		buf: bytes.NewBuffer(make([]byte, 0, cap)),
		cap: cap,
	}
}

func (l *limitedBuffer) Write(p []byte) (int, error) {
	if l.buf.Len() < l.cap {
		if len(p) >= l.cap-l.buf.Len() {
			l.buf.Write(p[0:l.cap])
		} else {
			l.buf.Write(p)
		}
	}

	return len(p), nil
}

func (l *limitedBuffer) String() string {
	return l.buf.String()
}
