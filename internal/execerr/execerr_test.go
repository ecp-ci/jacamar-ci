package execerr

import (
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/exectype"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func Test_ExecPotentialBuildError(t *testing.T) {
	t.Run("handle nil errors", func(t *testing.T) {
		assert.Nil(t, ExecPotentialBuildError("step_script", nil))
	})
}

func Test_IdentifyExit(t *testing.T) {
	tests := map[string]struct {
		env       envparser.ExecutorEnv
		err       error
		assertGot func(*testing.T, int, string)
	}{
		"standard go errors": {
			err: fmt.Errorf("testing: %w", errors.New("error type")),
			assertGot: func(t *testing.T, i int, s string) {
				assert.Equal(t, i, 1)
				assert.Equal(t, s, "testing: error type")
			},
		},
		"CustomBuildError": {
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: t.TempDir(),
				},
			},
			err: func() error {
				return CustomBuildError(42, errors.New("error message"))
			}(),
			assertGot: func(t *testing.T, i int, s string) {
				assert.Equal(t, i, 1)
				assert.Equal(t, s, "error message")
			},
		},
		"CustomSysError": {
			err: func() error {
				return CustomSysError(errors.New("error message"))
			}(),
			assertGot: func(t *testing.T, i int, s string) {
				assert.Equal(t, i, 2)
				assert.Equal(t, s, "error message")
			},
		},
		"nested system error": {
			err: func() error {
				return fmt.Errorf("nested: %w", ExecPotentialBuildError("get_sources", errors.New("system")))
			}(),
			assertGot: func(t *testing.T, i int, s string) {
				assert.Equal(t, i, 2)
				assert.Equal(t, s, "nested: system")
			},
		},
		"exec sys error": {
			err: func() error {
				return ExecPotentialBuildError("step_script", errors.New("unknown"))
			}(),
			assertGot: func(t *testing.T, i int, s string) {
				assert.Equal(t, i, 2)
			},
		},
		"exec build error": {
			err: func() error {
				cmd := exec.Command("bash", []string{"-c", "'exit 42"}...)
				_, err := cmd.CombinedOutput()
				return ExecPotentialBuildError("step_script", err)
			}(),
			assertGot: func(t *testing.T, i int, s string) {
				assert.Equal(t, i, 1)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			code, msg := IdentifyExit(tt.env, tt.err)

			if tt.assertGot != nil {
				tt.assertGot(t, code, msg)
			}
		})
	}
}

func Test_ReportRunExit(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ll := logrus.New()
	ll.Out = io.Discard
	sysLog := logrus.NewEntry(ll)

	auth := mock_authuser.NewMockAuthorized(ctrl)
	auth.EXPECT().CIUser().Return(authuser.UserContext{
		Username:  "user",
		HomeDir:   "/home/user",
		BuildsDir: "/home/user/builds",
		CacheDir:  "/home/user/cache",
		UID:       1000,
		GID:       2000,
		Groups:    []uint32{1000},
	}).AnyTimes()

	tests := map[string]struct {
		absExec    *abstracts.Executor
		auth       authuser.Authorized
		path       string
		assertFile func(*testing.T, string)
	}{
		"older runners": {
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(
						configure.Auth{},
					).AnyTimes()
					return m
				}(),
			},
		},
		"cfg skip": {
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(
						configure.Auth{
							SkipExitCodeFile: true,
						},
					).AnyTimes()
					return m
				}(),
			},
		},
		"failed read file": {
			path: t.TempDir() + "/invalid",
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(
						configure.Auth{
							Downscope: "none",
							JacamarPath: func() string {
								s := t.TempDir() + "/jacamar"
								usertools.CreateScript(s, "#!/bin/bash\nexit 1")
								return s
							}(),
						},
					).AnyTimes()
					m.EXPECT().General().Return(configure.General{
						KillTimeout: "5s",
					})
					return m
				}(),
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Stderr(gomock.Any())
					return m
				}(),
			},
			assertFile: func(t *testing.T, s string) {
				_, err := os.ReadFile(s)
				assert.Error(t, err, "reading exit file")
			},
		},
		"valid exit code": {
			path: t.TempDir() + "/invalid",
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(
						configure.Auth{
							Downscope: "none",
							JacamarPath: func() string {
								s := t.TempDir() + "/jacamar"
								usertools.CreateScript(s, "#!/bin/bash\necho '{{{101}}}'")
								return s
							}(),
						},
					).AnyTimes()
					m.EXPECT().General().Return(configure.General{
						KillTimeout: "5s",
					})
					return m
				}(),
			},
			assertFile: func(t *testing.T, s string) {
				b, err := os.ReadFile(s)
				assert.NoError(t, err, "reading exit file")
				assert.Equal(t, "101", string(b))
			},
		},
		"invalid string": {
			path: t.TempDir() + "/invalid",
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(
						configure.Auth{
							Downscope: "none",
							JacamarPath: func() string {
								s := t.TempDir() + "/jacamar"
								usertools.CreateScript(s, "#!/bin/bash\necho '$Test'")
								return s
							}(),
						},
					).AnyTimes()
					m.EXPECT().General().Return(configure.General{
						KillTimeout: "5s",
					})
					return m
				}(),
			},
			assertFile: func(t *testing.T, s string) {
				_, err := os.ReadFile(s)
				assert.Error(t, err, "reading exit file")
			},
		},
		"empty file": {
			path: t.TempDir() + "/empty",
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(
						configure.Auth{
							Downscope: "none",
							JacamarPath: func() string {
								s := t.TempDir() + "/jacamar"
								usertools.CreateScript(s, "#!/bin/bash\necho ''\nexit 0")
								return s
							}(),
						},
					).AnyTimes()
					m.EXPECT().General().Return(configure.General{
						KillTimeout: "5s",
					})
					return m
				}(),
			},
			assertFile: func(t *testing.T, s string) {
				_, err := os.ReadFile(s)
				assert.Error(t, err, "reading exit file")
			},
		},
		"cmdr error": {
			path: t.TempDir() + "/cmdr",
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(
						configure.Auth{},
					).AnyTimes()
					m.EXPECT().General().Return(configure.General{
						KillTimeout: "5s",
					})
					return m
				}(),
			},
		},
		"unsupported executor": {
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(
						configure.Auth{},
					).AnyTimes()
					return m
				}(),
				TargetExec: exectype.CobaltType,
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.path != "" {
				t.Setenv(envkeys.BuildExitFile, tt.path)
			}

			ReportRunExit(tt.absExec, auth, sysLog)

			if tt.assertFile != nil {
				tt.assertFile(t, tt.path)
			}
		})
	}
}
