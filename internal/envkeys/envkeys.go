// Package envkeys maintains constants for all potentially required environment
// variable keys.
package envkeys

const (
	// CustomBuildsDir key for user proposed build directory location.
	CustomBuildsDir = "CUSTOM_CI_BUILDS_DIR"

	/*
		Stateful Jacamar CI variables
	*/
	JacamarBuildsDir = StatefulEnvPrefix + "BUILDS_DIR"
	// JacamarLimitedDir key indicates the user wants to leverage the optional limit_build_dir
	// feature for their CI job.
	JacamarLimitedDir = "JACAMAR_LIMITED_DIR"
	// JacamarNoProfile key indicates the users wishes to have a minimum Bash shell.
	JacamarNoProfile = "JACAMAR_NO_BASH_PROFILE"
	// JacamarShell key to indicate to user profile Jacamar CI is in use.
	JacamarShell = StatefulEnvPrefix + "SHELL"
	// RunnerTimeout key for stateful runner timeout, string duration.
	RunnerTimeout = StatefulEnvPrefix + "RUNNER_TIMEOUT"
	// ScriptContentsPrefix is the prefix for all environment variables set with encoded
	// script contents.
	ScriptContentsPrefix = "JACAMAR_SCRIPT_CONTENTS_"
	// StatefulEnvPrefix is the required application defined prefix for stateful (e.g. job)
	// environment variables.
	StatefulEnvPrefix = "JACAMAR_CI_"

	/*
		GitLab defined variables
	*/
	// BuildExitCode is the key for the preferred build failure exit status.
	BuildExitCode = "BUILD_FAILURE_EXIT_CODE"
	// BuildExitFile key indicating where the file containing the exit code should be stored.
	BuildExitFile = "BUILD_EXIT_CODE_FILE"
	CIRunnerVer   = "CI_RUNNER_VERSION"
	GitTrace      = "GIT_TRACE"
	CIProjectDir  = "CI_PROJECT_DIR"
	// UserEnvPrefix is the expected runner defined prefix for all environment variables
	// provided to the custom executor driver.
	UserEnvPrefix = "CUSTOM_ENV_"
	// SysExitCode is the key for the preferred system failure exit status.
	SysExitCode = "SYSTEM_FAILURE_EXIT_CODE"
	// JobResponse location for the CI job response file with full payload contents. This
	// is only accessible by the auth user.
	JobResponse = "JOB_RESPONSE_FILE"

	/*
		Executor
	*/
	// CopySchedulerLogs key for user proposed location to copy batch scheduling related logs.
	CopySchedulerLogs = "COPY_SCHEDULER_LOGS"
	// SchedulerAction key allows for more directly influence of a batch executor and how jobs
	// are submitted to the underlying scheduling system. The exact behavior is unique to
	// each executor type.
	SchedulerAction = "SCHEDULER_ACTION"
	// SchedulerJobKey allows for influence how jobs are named when they are submitted,
	// managed, and closed when using a non-standard action. It highly advised users
	// specify a unique key but there is no way to enforce this.
	SchedulerJobKey = "SCHEDULER_JOB_KEY"
	// SchedulerJobPrefix key is a user defined prefix for name for a scheduled job.
	SchedulerJobPrefix = "SCHEDULER_JOB_PREFIX"
	// SchedulerSignal optional key allowing users to declare custom signal (name
	// or number) sent to the scheduler when a job is canceled.
	SchedulerSignal = "SCHEDULER_CANCEL_SIGNAL"
	// SchedulerOutputLocation will be optionally observed during select states to allow
	// users to override defaults job output locations.
	SchedulerOutputLocation = "SCHEDULER_OUTPUT_LOCATION"
	// JacamarDisableDynamic key indicating dynamic argument should be skipped.
	JacamarDisableDynamic = "JACAMAR_DISABLE_DYNAMIC"

	/*
		RunMechanism
	*/
	// JacamarNoMechanism key for a user defined variable requesting no non-standard
	// run_mechanism is used.
	JacamarNoMechanism = UserEnvPrefix + "JACAMAR_CI_NO_MECHANISM"
	// JacamarHostname key defines a user defined hostname for a container run mechanism.
	JacamarHostname = "JACAMAR_CI_HOSTNAME"
)
