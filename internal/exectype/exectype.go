package exectype

import (
	"fmt"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

type Type int

const (
	ShellType = iota
	FluxType
	SlurmType
	PBSType
	LSFType
	CobaltType
)

const (
	shellTypeStr  = "shell"
	fluxTypeStr   = "flux"
	slurmTypeStr  = "slurm"
	pbsTypeStr    = "pbs"
	lsfTypeStr    = "lsf"
	cobaltTypeStr = "cobalt"
)

func IdentifyExecutor(opt configure.Options) (Type, error) {
	switch strings.TrimSpace(strings.ToLower(opt.General.Executor)) {
	case "shell":
		return ShellType, nil
	case "flux":
		return FluxType, nil
	case "slurm", "sbatch":
		return SlurmType, nil
	case "pbs":
		return PBSType, nil
	case "lsf", "bsub":
		return LSFType, nil
	case "cobalt", "qsub":
		return CobaltType, nil
	default:
		return ShellType, fmt.Errorf("unrecognized executor '%s' defined", opt.General.Executor)
	}
}

func (t Type) String() string {
	return []string{
		shellTypeStr,
		fluxTypeStr,
		slurmTypeStr,
		pbsTypeStr,
		lsfTypeStr,
		cobaltTypeStr,
	}[t]
}
