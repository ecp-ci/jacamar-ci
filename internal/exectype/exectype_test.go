package exectype

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

func Test_IdentifyExecutor(t *testing.T) {
	tests := map[string]struct {
		executor string
		wantType string
		wantErr  bool
	}{
		"unknown": {wantErr: true},
		"flux":    {executor: "flux", wantType: fluxTypeStr},
		"sbatch":  {executor: "sbatch", wantType: slurmTypeStr},
		"pbs":     {executor: "pbs", wantType: pbsTypeStr},
		"lsf":     {executor: "lsf", wantType: lsfTypeStr},
		"cobalt":  {executor: "cobalt", wantType: cobaltTypeStr},
		"shell":   {executor: "shell", wantType: shellTypeStr},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := IdentifyExecutor(configure.Options{
				General: configure.General{
					Executor: tt.executor,
				},
			})

			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tt.wantType, got.String())
			}
		})
	}
}
