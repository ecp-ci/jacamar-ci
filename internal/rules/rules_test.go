package rules

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func makeIllegalStringLength() string {
	b := make([]byte, maxHeaderKB+1)
	for i := range b {
		b[i] = "a"[0]
	}
	return string(b)
}

func Test_regexp(t *testing.T) {
	tests := map[string]struct {
		r string
	}{
		"authTokenRegexp": {
			r: authTokenRegexp,
		},
		"directoryRegexp": {
			r: directoryRegexp,
		},
		"jwtRegexp": {
			r: jwtRegexp,
		},
		"projPathRegexp": {
			r: projPathRegexp,
		},
		"customUserKeyRegexp": {
			r: customUserKeyRegexp,
		},
		"unexpandedDirRegexp": {
			r: unexpandedDirRegexp,
		},
		"usernameRegexp": {
			r: usernameRegexp,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			_, err := regexp.Compile(tt.r)
			assert.NoError(t, err)
		})
	}
}

func Test_checkAuthToken(t *testing.T) {
	tests := map[string]struct {
		val     interface{}
		wantErr bool
	}{
		"empty": {
			val:     "",
			wantErr: false,
		},
		"valid, alphanumeric + underscore + hyphen": {
			val:     "EArXoCj8g_miAa8e-ZzP_iR",
			wantErr: false,
		},
		"period starting": {
			val:     ".EArXoCj8g_miAa8e-ZzP_iR",
			wantErr: true,
		},
		"illegal character": {
			val:     "test$(%)string",
			wantErr: true,
		},
		"invalid length": {
			val:     makeIllegalStringLength(),
			wantErr: true,
		},
		"valid random jwt": {
			val:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
			wantErr: false,
		},
		"val tilde used": {
			val:     "token~123456",
			wantErr: false,
		},
		"integer": {
			val:     123,
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()
			if err := v.Var(tt.val, "authToken"); (err != nil) != tt.wantErr {
				t.Errorf("CheckAuthToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func Test_checkDirectory(t *testing.T) {
	tests := map[string]struct {
		val     interface{}
		wantErr bool
	}{
		"empty": {
			val:     "",
			wantErr: true,
		},
		"simple filepath + script": {
			val:     "/dir/test/file.bash",
			wantErr: false,
		},
		"filepath ony": {
			val:     "/dir/test/some.dir/test_123/new-example/",
			wantErr: false,
		},
		"unexpanded variable": {
			val:     "/dir/$TEST/",
			wantErr: true,
		},
		"bash command": {
			val:     "$(env)",
			wantErr: true,
		},
		"home directory": {
			val:     "~/.test",
			wantErr: true,
		},
		".. elements in path": {
			val:     "/folder/user/../test",
			wantErr: false,
		},
		"backslash": {
			val:     "/test\n\\/folder",
			wantErr: true,
		},
		"special characters": {
			val:     "/test!folder/one",
			wantErr: true,
		},
		"integer": {
			val:     123,
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()
			if err := v.Var(tt.val, "directory"); (err != nil) != tt.wantErr {
				t.Errorf("CheckDirectory() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func Test_checkGitLabToken(t *testing.T) {
	tests := map[string]struct {
		val     interface{}
		wantErr bool
	}{
		"empty": {
			val:     "",
			wantErr: false,
		},
		"valid, alphanumeric": {
			val:     "EArXoCj8gmiAa8eZzPiR",
			wantErr: false,
		},
		"valid, underscore encountered": {
			val:     "PG_buQjszuoCoxxayMZG",
			wantErr: false,
		},
		"integer": {
			val:     123,
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()
			if err := v.Var(tt.val, "gitlabToken"); (err != nil) != tt.wantErr {
				t.Errorf("CheckGitLabToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func Test_checkJWT(t *testing.T) {
	tests := map[string]struct {
		val     interface{}
		wantErr bool
	}{
		"empty": {
			val:     "",
			wantErr: true,
		},
		"invalid length": {
			val:     makeIllegalStringLength(),
			wantErr: true,
		},
		"valid random jwt": {
			val:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
			wantErr: false,
		},
		"missing segment": {
			val:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ",
			wantErr: true,
		},
		"integer": {
			val:     123,
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()
			if err := v.Var(tt.val, "jwt"); (err != nil) != tt.wantErr {
				t.Errorf("CheckAuthToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func Test_checkProjectPath(t *testing.T) {
	tests := map[string]struct {
		val     interface{}
		wantErr bool
	}{
		"empty": {
			val:     "",
			wantErr: true,
		},
		"begin with -": {
			val:     "-group/project",
			wantErr: true,
		},
		"invalid characters": {
			val:     "group/$(project)/1",
			wantErr: true,
		},
		"valid project path": {
			val:     "group/sub-group.1/my_project",
			wantErr: false,
		},
		"invalid length": {
			val:     makeIllegalStringLength(),
			wantErr: true,
		},
		"integer": {
			val:     123,
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()
			if err := v.Var(tt.val, "projectPath"); (err != nil) != tt.wantErr {
				t.Errorf("CheckProjectPath() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func Test_checkCustomUserKey(t *testing.T) {
	tests := map[string]struct {
		val     interface{}
		wantErr bool
	}{
		"empty": {
			val:     "",
			wantErr: true,
		},
		"env": {
			val:     "ci-$CI_PIPELINE_ID",
			wantErr: true,
		},
		"valid": {
			val:     "ci-1234",
			wantErr: false,
		},
		"integer": {
			val:     123,
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()
			if err := v.Var(tt.val, "customUserKey"); (err != nil) != tt.wantErr {
				t.Errorf("CheckAuthToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func Test_checkQualifiedDirectory(t *testing.T) {
	tests := map[string]struct {
		val     interface{}
		wantErr bool
	}{
		"empty": {
			val:     "",
			wantErr: true,
		},
		".. elements in path": {
			val:     "/home/user/../test",
			wantErr: true,
		},
		"unexpanded variable": {
			val:     "/dir/$TEST/",
			wantErr: true,
		},
		"simple filepath + script": {
			val:     "/dir/test/file.bash",
			wantErr: false,
		},
		"filepath ony": {
			val:     "/dir/test/some.dir/test_123/new-example/",
			wantErr: false,
		},
		"integer": {
			val:     123,
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()
			if err := v.Var(tt.val, "qualifiedDir"); (err != nil) != tt.wantErr {
				t.Errorf("CheckAbsDirectory() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func Test_checkUnexpandedDirectory(t *testing.T) {
	tests := map[string]struct {
		val     string
		wantErr bool
	}{
		"empty": {
			val:     "",
			wantErr: true,
		},
		"simple filepath + script": {
			val:     "/dir/test/file.bash",
			wantErr: false,
		},
		"filepath ony": {
			val:     "/dir/test/some.dir/test_123/new-example/",
			wantErr: false,
		},
		"unexpanded variable": {
			val:     "/dir/$TEST/",
			wantErr: false,
		},
		"multiple unexpanded variables": {
			val:     "/dir/$TEST/something/${USER}",
			wantErr: false,
		},
		"bash command": {
			val:     "$(env)",
			wantErr: true,
		},
		"home directory": {
			val:     "~/.test",
			wantErr: true,
		},
		".. elements in path": {
			// Not enforced in this rule set.
			val:     "/folder/user/../test",
			wantErr: false,
		},
		"home variable": {
			val:     "$HOME/.runner",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()
			if err := v.Var(tt.val, "unexpandedDirectory"); (err != nil) != tt.wantErr {
				t.Errorf("CheckUnexpandedDirectory() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func Test_checkUsername(t *testing.T) {
	tests := map[string]struct {
		val     interface{}
		wantErr bool
	}{
		"empty": {
			val:     "",
			wantErr: false,
		},
		"greater than 255 character": {
			val:     "abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123",
			wantErr: true,
		},
		"hyphen in middle": {
			val:     "user-name01",
			wantErr: false,
		},
		"alphanumeric": {
			val:     "user123",
			wantErr: false,
		},
		"underscore in middle": {
			val:     "user_name01",
			wantErr: false,
		},
		"space in middle": {
			val:     "user name01",
			wantErr: true,
		},
		"bash command": {
			val:     "user$(env)",
			wantErr: true,
		},
		"period starting": {
			val:     ".username",
			wantErr: true,
		},
		"period in middle": {
			val:     "user.name",
			wantErr: false,
		},
		"underscore starting": {
			val:     "_username",
			wantErr: true,
		},
		"integer": {
			val:     123,
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := NewValidator()
			if err := v.Var(tt.val, "username"); (err != nil) != tt.wantErr {
				t.Errorf("CheckUsername() return = %v, want %v", err, tt.wantErr)
			}
		})
	}
}
