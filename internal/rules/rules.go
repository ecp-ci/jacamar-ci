// Package rules maintains established validation for shared aspects relating
// to either Jacamar specifically or the ECP CI effort as a whole. All checks
// that have been defined realize the validator.Func interface for the
// github.com/go-playground/validator/v10 and are meant to be a component in
// any validation strategy.
package rules

import (
	"path/filepath"
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
)

const (
	// Observe 8KB size maximum fallback for any check. Most can enforce
	// lower values if identified (nginx.org/en/docs/http/ngx_http_core_module.html).
	maxHeaderKB = 8192

	authTokenRegexp     = `^[a-zA-Z0-9-_][a-zA-Z0-9-_.~]*$` // #nosec G101
	directoryRegexp     = `^(/[\w\-.^ ]+)+/?$`
	jwtRegexp           = `^[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+$`
	projPathRegexp      = `^[a-zA-Z0-9][a-zA-Z0-9-_./]+$`
	customUserKeyRegexp = `^([a-zA-Z0-9-_]{4,64})$`
	unexpandedDirRegexp = `^(/?[\w\-.^${}]+)+/?$`
	usernameRegexp      = `^([a-zA-Z0-9][a-zA-Z0-9._-]{0,254})$`
)

// NewValidator generates a new validator with the following custom rules registered:
//   - authToken (string): optionally ensures a valid generic authorization token has been
//     supplied that contains no potentially malicious characters and generic maximum length.
//   - directory (string): verifies the validity of an expected Unix path without checking for
//     its existence on the system. Non-absolute paths are allowed.
//   - gitlabToken (string): optionally ensures a potential GitLab job/personal/deploy token is present
//     and potentially invalid web characters are not included.
//   - jwt (string): provide basic check of characters found in a valid JSON Web Token.
//   - projectPath (string): ensures the values adheres to expectations of group/project.
//   - customUserKey (string): alphanumeric, underscore, and hyphen characters only, requires length between
//     4 and 64. Intended for cases where a user define key/id/name is required.
//   - qualifiedDir (string): verifies the validity of an expected Unix path without checking
//     for its existence on the system. Absolute and fully qualified (filepath.Clean should not
//     be required by provided value) is required.
//   - unexpandedDirectory (string): identify if a valid directory is found with potentially
//     un-expanded keys allowed, without checking for its existence on the system or required
//     it be fully qualified.
//   - username (string): optionally examines the provided username for validity based upon
//     the GitLab server requirements while observing potentially egregious unix characters.
func NewValidator() *validator.Validate {
	v := validator.New()

	_ = v.RegisterValidation("authToken", checkAuthToken)
	_ = v.RegisterValidation("directory", checkDirectory)
	_ = v.RegisterValidation("gitlabToken", checkGitLabToken)
	_ = v.RegisterValidation("jwt", checkJWT)
	_ = v.RegisterValidation("projectPath", checkProjectPath)
	_ = v.RegisterValidation("customUserKey", checkCustomUserKey)
	_ = v.RegisterValidation("qualifiedDir", checkQualifiedDirectory)
	_ = v.RegisterValidation("unexpandedDirectory", checkUnexpandedDirectory)
	_ = v.RegisterValidation("username", checkUsername)

	return v
}

var checkAuthToken validator.Func = func(fl validator.FieldLevel) bool {
	if s, ok := fl.Field().Interface().(string); ok {
		if maximumHeader([]byte(s)) {
			return false
		} else if s == "" {
			return true
		}
		return regexp.MustCompile(authTokenRegexp).MatchString(s)
	}

	return false
}

var checkDirectory validator.Func = func(fl validator.FieldLevel) bool {
	if s, ok := fl.Field().Interface().(string); ok {
		if s == "" || len([]byte(s)) > 4096 {
			return false
		}
		return regexp.MustCompile(directoryRegexp).MatchString(s)
	}

	return false
}

var checkGitLabToken validator.Func = func(fl validator.FieldLevel) bool {
	return checkAuthToken(fl)
}

var checkJWT validator.Func = func(fl validator.FieldLevel) bool {
	if s, ok := fl.Field().Interface().(string); ok {
		if maximumHeader([]byte(s)) {
			return false
		}
		return regexp.MustCompile(jwtRegexp).MatchString(s)
	}

	return false
}

var checkProjectPath validator.Func = func(fl validator.FieldLevel) bool {
	if s, ok := fl.Field().Interface().(string); ok {
		if maximumHeader([]byte(s)) {
			return false
		}
		return regexp.MustCompile(projPathRegexp).MatchString(s)
	}

	return false
}

var checkCustomUserKey validator.Func = func(fl validator.FieldLevel) bool {
	if s, ok := fl.Field().Interface().(string); ok {
		return regexp.MustCompile(customUserKeyRegexp).MatchString(s)
	}

	return false
}
var checkQualifiedDirectory validator.Func = func(fl validator.FieldLevel) bool {
	return checkDirectory(fl) && cleanPath(fl.Field().String())
}

var checkUnexpandedDirectory validator.Func = func(fl validator.FieldLevel) bool {
	if s, ok := fl.Field().Interface().(string); ok {
		if s == "" || len([]byte(s)) > 4096 {
			return false
		}
		return regexp.MustCompile(unexpandedDirRegexp).MatchString(s)
	}

	return false
}

var checkUsername validator.Func = func(fl validator.FieldLevel) bool {
	if s, ok := fl.Field().Interface().(string); ok {
		if s == "" {
			return true
		}
		return regexp.MustCompile(usernameRegexp).MatchString(s)
	}

	return false
}

func cleanPath(path string) bool {
	// Legitimate cases where trailing "/" suffix is expected.
	str := strings.TrimSuffix(path, "/")

	if !filepath.IsAbs(str) || str != filepath.Clean(str) {
		return false
	}

	return true
}

func maximumHeader(b []byte) bool {
	// This is not strictly true for all potentially instance, but does at least set a
	// ceiling for otherwise uncapped values.
	return len(b) > maxHeaderKB
}
