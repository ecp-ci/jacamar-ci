// Package augmenter offers a rule enforced mechanism for updating
// the GitLab generated CI job script to conform to required behaviors.
package augmenter

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

// Rules defines strict regulations for any actions that will be directed towards a
// provided script. No values are required and default actions are defined in all cases.
type Rules struct {
	Gen configure.General
}

const (
	gitCIUser      = `gitlab-ci-token`
	artifactsExp   = `gitlab-runner.{1,24}?artifacts\-`
	gitURLExp      = `http[s]{0,1}?://gitlab-ci-token`
	evalExp        = `^[\:\|\s\(\$]{0,8}eval`
	configCountExp = `GIT_CONFIG_COUNT`
	askVar         = `GIT_ASKPASS`
)

// JobScript accepts the filepath of a CI job script, the associated sub-stage
// (https://docs.gitlab.com/runner/executors/custom.html#run), and a trusted
// CI_PROJECT_DIR equivalent. These details are used in conjunction with the constraints
// established by the configuration to ingest the script and restructure it accordingly.
// The finalized script is returned in its completion as an un-encoded string.
func JobScript(
	filepath,
	stage string,
	env envparser.ExecutorEnv,
	gen configure.General,
) (string, error) {
	oldContents, err := openScript(filepath)
	if err != nil {
		return "", err
	}

	var newContents []string
	msg := logging.NewMessenger()

	// Changes are not required for the prepare_script or in cases where the
	// UnrestrictedCmdline configuration has been enabled.
	if gen.UnrestrictedCmdline || stage == "prepare_script" {
		return strings.Join(oldContents, "\n"), nil
	}

	for _, line := range oldContents {
		re := regexp.MustCompile(evalExp)
		if len(re.FindStringSubmatch(line)) > 0 {
			if potentialGitStage(stage) {
				newContents = appendGitVariables(newContents, line, env, msg)
			}

			eval := restrictCmdLine(stage, strings.Split(line, `\n`))
			newContents = append(newContents, strings.Join(eval, `\n`))
		} else {
			newContents = append(newContents, line)
		}
	}

	return strings.Join(newContents, "\n"), nil
}

func restrictCmdLine(stage string, eval []string) []string {
	if stage == "get_sources" {
		eval = containsAction(eval, gitURLExp, gitRemoveToken)
	}

	if strings.Contains(stage, "artifacts") {
		eval = containsAction(eval, artifactsExp, artifactsExportToken)
	}

	return eval
}

// potentialGitStage identifies if the stage has the potential for Git command.
func potentialGitStage(stage string) bool {
	if stage == "get_sources" || stage == "after_script" || stage == "build_script" {
		return true
	}

	// We are currently unclear on the functionality of all potential step_* scripts,
	// as such it is safest to treat them all as we had a build_script.
	return strings.HasPrefix(stage, "step_")
}

// appendGitVariables is designed to be call for cases (see potentialGitStage) where a script
// or user action may interact with Git. All modifications are focused on adding Git observed
// environment variables in the least disruptive way possible. In all cases we defer to user
// defined variables even if it means potentially breaking a job script.
func appendGitVariables(
	newContents []string,
	evalLine string,
	env envparser.ExecutorEnv,
	msg logging.Messenger,
) []string {
	newContents = append(newContents, fmt.Sprintf(
		"export %s='%s'",
		askVar,
		gitAskpassFile(env),
	))

	re := regexp.MustCompile(configCountExp)
	if len(re.FindStringSubmatch(evalLine)) > 0 {
		// We cannot handle a case where the user has set their own variables as it would lead
		// back to having to directly modify large potions of the eval line. Instead, we skip
		// the changes and simply notify the user. More information can be found in the user
		// documentation.
		msg.Stdout(
			"Please set GIT_CONFIG_* variables if you have configured a global credential.helper",
		)
	} else {
		// These variables only work with Git version 2.31+; however, since they are not
		// strictly required for the job to function it should remain optional. Using
		// these removes the risk of failed jobs due to a globally configured credential.helper
		newContents = append(newContents, "export GIT_CONFIG_COUNT='1'")
		newContents = append(newContents, "export GIT_CONFIG_KEY_0='credential.helper'")
		newContents = append(newContents, "export GIT_CONFIG_VALUE_0=''")
	}

	return newContents
}

func containsAction(eval []string, expStr string, action func(string) string) []string {
	re := regexp.MustCompile(expStr)
	for i, val := range eval {
		loc := re.FindStringSubmatch(val)
		if len(loc) > 0 {
			eval[i] = action(val)
		}
	}

	return eval
}

// gitRemoveToken parses any provided git command for a token in the https:// url
// and removes it. Example url where the goal is to remove 'ciJobToken':
// "https://gitlab-ci-token:ciJobToken@gitlab.example.com/user/scratch-space.git"
func gitRemoveToken(s string) string {
	i := strings.Index(s, gitCIUser)
	if i == -1 {
		return s
	}

	j := strings.Index(s[i:], "@")
	if i > -1 && j > -1 {
		pre := s[:i]
		post := (s[i:])[j+1:]
		s = pre + post
	}
	return s
}

func openScript(path string) (s []string, err error) {
	/* #nosec */
	// variable file path required
	b, err := os.ReadFile(filepath.Clean(path))
	if err != nil {
		return
	}
	s = strings.Split(string(b), "\n")
	return
}

// artifactsExportToken removes the use of --token to provide a token to the
// artifact uploader/downloader and instead relies on export (Bash).
func artifactsExportToken(s string) string {
	split := strings.Split(s, " ")

	var jobToken string
	var newCmd []string

	for k, v := range split {
		if strings.Contains(v, "--token") {
			jobToken = split[k+1]
			continue
		} else if jobToken != "" {
			jobToken = ""
			continue
		}

		newCmd = append(newCmd, v)
	}

	return strings.Join(newCmd, " ")
}

// CreateGitAskpass safely generates a script for handling Git credentials
// (see https://git-scm.com/docs/gitcredentials) that will provide the token
// back to any git command so long as the GIT_ASKPASS environment variable
// is set. Invoking this function is required to realize the AllowUserCredentials
// rule but remain distinct to support distinct workflows that come with
// executing CI on a behalf of a user.
func CreateGitAskpass(
	env envparser.ExecutorEnv,
	msg logging.Messenger,
	gen configure.General,
) (err error) {
	if !gen.UnrestrictedCmdline {
		file := gitAskpassFile(env)
		err = usertools.GitPassCreation(gen, file, env.RequiredEnv.JobToken)
		if err == nil && envparser.GitTrace() {
			msg.Stdout("GIT_ASKPASS script created created: %s", file)
		}
	}

	return err
}

// CleanupGitAskpass removes the previously creates GIT_ASKPASS related script. Any error
// encountered in the process will be ignored.
func CleanupGitAskpass(env envparser.ExecutorEnv, msg logging.Messenger, gen configure.General) {
	if env.StatefulEnv.BuildsDir != "" || !gen.UnrestrictedCmdline {
		file := gitAskpassFile(env)
		err := os.Remove(file)
		if err == nil && envparser.GitTrace() {
			msg.Stdout("GIT_ASKPASS script removed: %s", file)
		}
	}
}

// gitAskpassFile returns the full path to any credentials script created for the job.
func gitAskpassFile(env envparser.ExecutorEnv) string {
	var sb strings.Builder
	sb.WriteString(env.StatefulEnv.BuildsDir)
	sb.WriteString("/.credentials/")
	if env.StatefulEnv.ProjectPath != "" {
		sb.WriteString(env.StatefulEnv.ProjectPath)
		sb.WriteString("/")
	}
	sb.WriteString("pass")

	return filepath.Clean(sb.String())
}
