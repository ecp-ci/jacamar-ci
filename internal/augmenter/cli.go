package augmenter

import (
	"fmt"
	"strings"

	"github.com/google/shlex"
)

// maxLenCLI defines a maximum length for any user proposed CLI
const maxLenCLI = 8000

// ConstructUserArgs aims to support userspace command line interface input creation
// and provides best effort at shell quoting. This is not a replacement for running the
// resulting commands in userspace and simply aims to avoid supporting cases where
// difficult to address CI behaviors are introduced as workarounds to correct
// GitLab CI focused solutions.
func ConstructUserArgs(userArgs string) (string, error) {
	if len(userArgs) > maxLenCLI {
		return "", fmt.Errorf("proposed CLI len surpasses maximum %v", maxLenCLI)
	}

	s, err := shlex.Split(userArgs)
	if err != nil {
		return "", err
	}

	return cliBuilder(s)
}

func cliBuilder(s []string) (string, error) {
	var sb strings.Builder
	sb.WriteString(" ")

	for _, v := range s {
		sb.WriteString(fmt.Sprintf("'%s' ", strings.Replace(v, "'", "", -1)))
	}

	// Verify issues haven't been introduced.
	_, err := shlex.Split(sb.String())

	return sb.String(), err
}
