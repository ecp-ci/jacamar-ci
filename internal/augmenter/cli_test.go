package augmenter

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func makeIllegalStringLength() string {
	b := make([]string, maxLenCLI+1)
	for i := range b {
		b[i] = "a"
	}
	return strings.Join(b, "")
}

func Test_ConstructUserArgs(t *testing.T) {
	tests := map[string]rTests{
		"empty input": {
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"simple working example": {
			stage: "-N1 -t 2",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, " '-N1' '-t' '2' ", s)
			},
		},
		"invalid quote": {
			stage: "--example test'",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "EOF found when expecting closing quote")
			},
		},
		"remove quote": {
			stage: "--example \"test'\"",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"reverify quote": {
			stage: "--example \"test\"\"",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "EOF found when expecting closing quote")
			},
		},
		"string size": {
			stage: makeIllegalStringLength(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "proposed CLI len surpasses maximum")
			},
		},
		"new line characters": {
			stage: "\nexample\nnewline\n",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, " 'example' 'newline' ", s)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := ConstructUserArgs(tt.stage)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}
