// Package preparations realizes universal actions taken by different applications / sub-commands
// to prepare for an upcoming CI job. Stage specific triggers to the appropriate execution layer
// are maintained here, any further distinctions in workflows should be realized in the appropriate
// command package.
package preparations

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/fullauth"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/noauth"
	"gitlab.com/ecp-ci/jacamar-ci/internal/exectype"
	"gitlab.com/ecp-ci/jacamar-ci/internal/verifycaps"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

var (
	estValid     func(string, configure.Options, envparser.ExecutorEnv) (authuser.Validators, error)
	isTerminal   func() bool
	noLoginFiles []string
)

func init() {
	estValid = authuser.EstablishValidators
	isTerminal = verifycaps.IsTerminal
	noLoginFiles = []string{"/etc/nologin", "/var/run/nologin"}
}

// StdError output provided error message using mechanism appropriate to the current stage.
func StdError(c arguments.ConcreteArgs, message string, msg logging.Messenger, exit func()) {
	prefix := "Error encountered during job: %s"
	if c.Run != nil || c.Prepare != nil {
		msg.Warn(prefix, message)
	} else {
		msg.Stderr(prefix, message)
	}
	exit()
}

// JobContext establishes initial structure for the executor (configuration and environment)
// in addition to a system logger. These actions are driven through a combination of CLI
// arguments and GitLab-Runner provided details (environment variables + job).
func JobContext(
	c arguments.ConcreteArgs,
	exit func(),
) (*abstracts.Executor, *logrus.Entry, error) {
	cfg, err := newConfigurer(c)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to establish Configurer: %w", err)
	}

	env, err := newExecutorEnv(stateReq(c), c, cfg.Options())
	if err != nil {
		return nil, nil, fmt.Errorf("unable to identify ExecutorEnv: %w", err)
	}

	tarExec, err := exectype.IdentifyExecutor(cfg.Options())
	if err != nil {
		return nil, nil, err
	}

	ae := &abstracts.Executor{
		Cfg:        cfg,
		Env:        env,
		TargetExec: tarExec,
	}

	stageConfig(ae, c)
	execOptions(ae, c, exit)

	var sysLog *logrus.Entry
	sysLog, err = logging.EstablishLogger(ae.Stage, ae.Cfg.Options(), ae.Env.RequiredEnv)

	if c.Config != nil && sysLog != nil {
		sysLog.Info(fmt.Sprintf("configuration file (%s) opened", c.Config.Configuration))
	}

	return ae, sysLog, err
}

// NewAuthorizedUser identifies required user context.
func NewAuthorizedUser(
	absExec *abstracts.Executor,
	args arguments.ConcreteArgs,
	sysLog *logrus.Entry,
) (auth authuser.Authorized, err error) {
	valid, err := estValid(absExec.Stage, absExec.Cfg.Options(), absExec.Env)
	if err != nil {
		return
	}

	if args.NoAuth {
		auth, err = noauth.Factory{
			Env:   absExec.Env,
			Opt:   absExec.Cfg.Options(),
			Valid: valid,
			Msg:   absExec.Msg,
		}.EstablishUser()
	} else {
		auth, err = fullauth.Factory{
			SysLog: sysLog,
			Env:    absExec.Env,
			Opt:    absExec.Cfg.Options(),
			Valid:  valid,
			Msg:    absExec.Msg,
		}.EstablishUser()
	}

	return
}

//

func newConfigurer(c arguments.ConcreteArgs) (configure.Configurer, error) {
	if c.Config != nil {
		return configure.NewConfig(c.Config.Configuration)
	} else if len(os.Getenv(configure.EnvVariable)) == 0 && c.Cleanup != nil {
		return configure.NewConfig(c.Cleanup.Configuration)
	}
	return configure.NewConfig(configure.EnvVariable)
}

func newExecutorEnv(
	stateful bool,
	c arguments.ConcreteArgs,
	opt configure.Options,
) (env envparser.ExecutorEnv, err error) {
	env, err = envparser.Fetcher(stateful, c, opt)
	if err != nil {
		return envparser.ExecutorEnv{}, fmt.Errorf(
			"unable to parse runner environment: %w", err,
		)
	}
	return
}

// stateReq determines based upon stage if job (stateful) variables should be relied upon.
func stateReq(c arguments.ConcreteArgs) bool {
	_, found := os.LookupEnv("JACAMAR_CI_BASE_DIR")
	if c.Config != nil {
		return false
	} else if c.Cleanup != nil && !found {
		return false
	}
	return true
}

// stageConfig updates the abstract Executor with details based upon stage specific arguments.
func stageConfig(ae *abstracts.Executor, c arguments.ConcreteArgs) {
	switch {
	case c.Config != nil:
		ae.Stage = "config_exec"
	case c.Prepare != nil:
		ae.Stage = "prepare_exec"
	case c.Run != nil:
		ae.Stage = c.Run.Stage
		ae.ScriptPath = c.Run.Script
	case c.Cleanup != nil:
		ae.Stage = "cleanup_exec"
	}
}

// execOptions realizes the establishment of the configuration environment variable for sharing
// the contents of a --configuration amongst stages.
func execOptions(ae *abstracts.Executor, c arguments.ConcreteArgs, exit func()) {
	req := true
	if c.Config != nil {
		req = false
	}

	ec, err := ae.Cfg.PrepareState(req)
	if err != nil {
		StdError(c,
			"unable to PrepareState for configuration (--configuration), please verify encoding",
			ae.Msg,
			exit,
		)
	}
	_ = os.Setenv(configure.EnvVariable, ec)
	ae.ExecOptions = ec
}
