package preparations

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/flock"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/internal/version"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

// proj represents the excepted json STDOUT of the config_exec stage.
type proj struct {
	Builds   string            `json:"builds_dir"`
	Cache    string            `json:"cache_dir"`
	Share    bool              `json:"builds_dir_is_shared"`
	Hostname string            `json:"hostname"`
	State    map[string]string `json:"job_env"`
	Driver   map[string]string `json:"driver"`
}

// ConfigExec realizes the config_exec custom executor stage by generating a valid JSON payload created for the upstream runner to ingest.
// No previously established configuration or system settings will be modified.
func ConfigExec(
	absExec *abstracts.Executor,
	auth authuser.Authorized,
	args arguments.ConcreteArgs,
	exit func(),
	sysLog *logrus.Entry,
) {
	js, err := configJSON(absExec, auth, sysLog)
	if err != nil {
		errMsg := fmt.Sprintf("unable to construct job configuration payload: %s", err.Error())
		StdError(args, errMsg, absExec.Msg, exit)
		return
	}

	absExec.Msg.Stdout(js) // Print configuration.
	sysLog.Debugf("configuration payload established'%s'", js)
}

//

// configJSON encodes the included proj struct as json and returns it as a string for stdout.
func configJSON(
	absExec *abstracts.Executor,
	auth authuser.Authorized,
	sysLog *logrus.Entry,
) (string, error) {
	// Failure to retrieve hostname (resulting in empty string)
	// does not present any risk.
	host, _ := os.Hostname()

	p := proj{
		Share:    false,
		Hostname: host,
		Driver: map[string]string{
			"name":    "Jacamar CI",
			"version": version.Version(),
		},
	}

	if err := updateDirs(absExec, auth, &p, sysLog); err != nil {
		return "", err
	}

	state, err := buildState(absExec, auth)
	if err != nil {
		return "", err
	}

	// Configuration (managed outside StatefulEnv structure) and potential
	// directory overrides.
	state[configure.EnvVariable] = absExec.ExecOptions
	state["JACAMAR_CI_BUILDS_DIR"] = p.Builds
	p.State = state

	data, err := json.Marshal(&p)

	return string(data), err
}

func updateDirs(
	absExec *abstracts.Executor,
	auth authuser.Authorized,
	p *proj,
	sysLog *logrus.Entry,
) (err error) {
	p.Builds = auth.CIUser().BuildsDir
	p.Cache = auth.CIUser().CacheDir

	if flock.LimitedDirAllowed(absExec.Cfg.General()) {
		var dir string
		dir, err = identifyConcurrent(absExec, auth, sysLog)
		if err != nil {
			return
		}

		sysLog.Debugf("concurrent directory identified via flock: %s", dir)
		p.Builds = p.Builds + "/" + dir
	}

	return
}

// buildState generates a mapping of values to StatefulEnv keys.
func buildState(
	absExec *abstracts.Executor,
	auth authuser.Authorized,
) (map[string]string, error) {
	state := auth.BuildState()
	absExec.Env.JobResponse.ExpandState(&state)

	state.PipelineID = auth.JobContext().PipelineID()

	env := make(map[string]string)

	t := reflect.TypeOf(&state).Elem()
	v := reflect.ValueOf(&state).Elem()
	for i := 0; i < t.NumField(); i++ {
		fieldT := t.Field(i)
		fieldV := v.Field(i)

		tag := fieldT.Tag.Get(envparser.UserVarTagName)
		req := fieldT.Tag.Get(envparser.RequiredKey)

		if req == "true" && fieldV.String() == "" {
			return env, fmt.Errorf("unable to identify required stateful variable %s", tag)
		} else if req == "false" && fieldV.String() == "" {
			continue
		}

		env[tag] = fieldV.String()
	}

	return env, nil
}

func identifyConcurrent(
	absExec *abstracts.Executor,
	auth authuser.Authorized,
	sysLog *logrus.Entry,
) (string, error) {
	ctx := context.Background()
	defer ctx.Done()

	args := arguments.ConcreteArgs{
		Lock: &arguments.LockCmd{
			ProposedDir:    auth.CIUser().BuildsDir,
			JobID:          absExec.Env.JobID,
			JobTimeout:     strconv.Itoa(absExec.Env.JobResponse.RunnerInfo.Timeout),
			DirPermissions: prepPermissions(absExec.Cfg.General()),
			Debug:          absExec.Cfg.General().FileLockDebug,
		},
	}

	// Build and use a new commander interface to ensure that any configured
	// underlying downscope methods are observed.
	cmdr, err := NewCommander(ctx, absExec, args, auth, sysLog)
	if err != nil {
		return "", err
	}

	out, cmdErr := cmdr.ReturnOutput("")
	concurrent, strErr := flock.UnwrapConcurrentDir(out)

	if strErr != nil || cmdErr != nil {
		err = fmt.Errorf("failed to request file lock in %s dir", auth.CIUser().BuildsDir)

		// Inform the user of the error message associated with locking, in some
		// cases this could be caused by their profile/settings. The downscope library
		// enforces a maximum length of any related output.
		sysLog.Errorf("%s: '%s'", err.Error(), out)
	}

	return concurrent, err
}

func prepPermissions(gen configure.General) string {
	// In order to simply passing the target via CLI regardless of downscope we
	// will convert permissions to a numerical uint string. The creation as well
	// as prepare_exec will enforce secure defaults in case of errors.
	mode := usertools.IdentifyPermissions(gen)
	return strconv.FormatUint(uint64(mode), 10)
}
