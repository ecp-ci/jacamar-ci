package preparations

import (
	"context"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func Test_Factory_NewCommander(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	//  Mock configurations
	missing := mock_configure.NewMockConfigurer(ctrl)
	missing.EXPECT().Auth().Return(
		configure.Auth{},
	).AnyTimes()
	missing.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	none := mock_configure.NewMockConfigurer(ctrl)
	none.EXPECT().Auth().Return(
		configure.Auth{
			Downscope:   "none",
			JacamarPath: "/opt/jacamar/bin/jacamar",
		},
	).AnyTimes()
	none.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	su := mock_configure.NewMockConfigurer(ctrl)
	su.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "su",
		},
	).AnyTimes()
	su.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	sudo := mock_configure.NewMockConfigurer(ctrl)
	sudo.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "sudo",
		},
	).AnyTimes()
	sudo.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	setgid := mock_configure.NewMockConfigurer(ctrl)
	setgid.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "setgid",
		},
	).AnyTimes()
	setgid.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	setuid := mock_configure.NewMockConfigurer(ctrl)
	setuid.EXPECT().Auth().Return(
		configure.Auth{
			Downscope:   "setuid",
			JacamarPath: "/bin",
		},
	).AnyTimes()
	setuid.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	// Mock authorizations
	pass := mock_authuser.NewMockAuthorized(ctrl)
	pass.EXPECT().CIUser().Return(authuser.UserContext{
		Username:  "user",
		HomeDir:   "/home/user",
		BuildsDir: "/home/user/builds",
		CacheDir:  "/home/user/cache",
		UID:       1000,
		GID:       2000,
		Groups:    []uint32{1000},
	}).AnyTimes()

	// Mock concrete arguments
	authPrepare := arguments.ConcreteArgs{
		Prepare: &arguments.PrepareCmd{},
	}
	noAuthPrepare := arguments.ConcreteArgs{
		Prepare: &arguments.PrepareCmd{},
		NoAuth:  true,
	}

	noTerm := func() bool { return false }
	yesTerm := func() bool { return true }

	tests := map[string]prepTests{
		"no configuration during authorization": {
			ae: &abstracts.Executor{
				Cfg: missing,
			},
			c: authPrepare,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid runner configuration, ensure supported downscope is defined")
			},
		},
		"no configuration during no-auth start": {
			ae: &abstracts.Executor{
				Cfg: missing,
			},
			c:    noAuthPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Equal(t, "/usr/bin/env", cmd.CopiedCmd().Path)
			},
		},
		"none downscope mechanism defined": {
			ae: &abstracts.Executor{
				Cfg: none,
			},
			c:    authPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Equal(t, command.DefaultJacamarBin+"/jacamar", cmd.CopiedCmd().Path)
			},
		},
		"su downscope mechanism defined": {
			ae: &abstracts.Executor{
				Cfg: su,
			},
			c:    authPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Contains(t, cmd.CopiedCmd().Path, "/bin/su")
			},
		},
		"sudo downscope mechanism defined": {
			ae: &abstracts.Executor{
				Cfg: sudo,
			},
			c:    authPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Contains(t, cmd.CopiedCmd().Path, "sudo")
			},
		},
		"misspelled downscope in configuration with authorization": {
			ae: &abstracts.Executor{
				Cfg: setgid,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid runner configuration, ensure supported downscope is defined")
			},
		},
		"configuration, setuid run mechanism": {
			ae: &abstracts.Executor{
				Cfg: setuid,
			},
			c:    authPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Equal(t, "/bin/jacamar", cmd.CopiedCmd().Path)
			},
		},
		"active terminal, setuid run mechanism": {
			ae: &abstracts.Executor{
				Cfg: setuid,
			},
			auth:     pass,
			c:        authPrepare,
			mockTerm: yesTerm,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "setuid downscoping via active terminal (TTY) is not supported at this time")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockTerm != nil {
				isTerminal = tt.mockTerm
			} else {
				isTerminal = noTerm
			}

			cmd, err := NewCommander(context.TODO(), tt.ae, tt.c, tt.auth, testSysLog)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertCommander != nil {
				tt.assertCommander(t, cmd)
			}
		})
	}
}

func Test_enforceNoLogin(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		opts        configure.Auth
		msg         logging.Messenger
		mockFiles   func(*testing.T) []string
		assertError func(*testing.T, error)
	}{
		"file encountered": {
			opts: configure.Auth{
				EnforceNoLogin: true,
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Error(gomock.Any())
				m.EXPECT().Stderr("login disabled")
				return m
			}(),
			mockFiles: func(t *testing.T) []string {
				filename := t.TempDir() + "/nologin"
				os.WriteFile(filename, []byte("login disabled"), 0644)
				return []string{filename}
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"not enforced": {
			mockFiles: func(t *testing.T) []string {
				filename := t.TempDir() + "/nologin"
				os.WriteFile(filename, []byte("login disabled"), 0644)
				return []string{filename}
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"no file encountered": {
			opts: configure.Auth{
				EnforceNoLogin: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockFiles != nil {
				noLoginFiles = tt.mockFiles(t)
			}

			err := enforceNoLogin(tt.opts, tt.msg)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
