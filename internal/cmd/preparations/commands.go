package preparations

import (
	"context"
	"errors"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command/bash"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command/downscope"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

func NewCommander(
	ctx context.Context,
	absExec *abstracts.Executor,
	args arguments.ConcreteArgs,
	auth authuser.Authorized,
	sysLog *logrus.Entry,
) (command.Commander, error) {
	absCmdr := command.NewAbsCmdr(
		ctx,
		absExec.Cfg.General(),
		absExec.Msg,
		absExec.Env,
		sysLog,
	)

	if args.NoAuth {
		return nonAuthLevel(absCmdr, absExec, auth), nil
	}

	// Before generating an auth level command interface or running any additional
	// components check if the nologin file should be observed.
	if err := enforceNoLogin(absExec.Cfg.Auth(), absExec.Msg); err != nil {
		return nil, err
	}

	return authorizationLevel(absCmdr, absExec, auth, args, sysLog)
}

//

func nonAuthLevel(
	absCmdr *command.AbstractCommander,
	absExec *abstracts.Executor,
	auth authuser.Authorized,
) command.Commander {
	factory := bash.Factory{
		AbsCmdr: absCmdr,
		Cfg:     absExec.Cfg,
	}

	return factory.CreateBaseShell(auth.CIUser().HomeDir)
}

// enforceNoLogin based upon the configuration will optionally check for the existence of
// the "/etc/nologin" or "/var/run/nologin" files. If present, the user will be notified
// (via stderr) of the contents of this file and an error returned.
func enforceNoLogin(opts configure.Auth, msg logging.Messenger) error {
	if !opts.EnforceNoLogin {
		return nil
	}

	for _, v := range noLoginFiles {
		b, err := os.ReadFile(v) // #nosec G304
		if err == nil {
			errMsg := fmt.Errorf("job prevented due to %s file", v)
			msg.Error(errMsg.Error())
			msg.Stderr(string(b))
			return errMsg
		}
	}

	return nil
}

func authorizationLevel(
	absCmdr *command.AbstractCommander,
	absExec *abstracts.Executor,
	auth authuser.Authorized,
	args arguments.ConcreteArgs,
	sysLog *logrus.Entry,
) (command.Commander, error) {
	absCmdr.EnableNotifyTerm()

	var cmdr command.Commander
	var err error

	factory := downscope.Factory{
		AbsCmdr:  absCmdr,
		SysLog:   sysLog,
		Cfg:      absExec.Cfg,
		AuthUser: auth,
		Concrete: args,
	}

	switch absExec.Cfg.Auth().Downscope {
	case "setuid":
		if isTerminal() && !factory.Cfg.Auth().DevMode {
			err = errors.New("setuid downscoping via active terminal (TTY) is not supported at this time")
		} else {
			cmdr = factory.CreateSetuidShell()
		}
	case "sudo":
		cmdr = factory.CreateSudoShell()
	case "su":
		cmdr = factory.CreateSuShell()
	case "none":
		cmdr = factory.CreateStdShell()
	default:
		err = errors.New("invalid runner configuration, ensure supported downscope is defined")
	}

	return cmdr, err
}
