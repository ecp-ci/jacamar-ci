// Package auth realizes the core of the jacamar-auth application by ensuring the strict
// adherence to the authorization flow and the validity of the subsequent user context used
// to execute the configured downscope mechanism.
package auth

import (
	"context"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/internal/verifycaps"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	// Minimum functional runner version required.
	minMajor = 14
	minMinor = 1
)

var (
	sysCode, buildCode int

	// Functions to facilitate associated process exit.
	sysExit, buildExit, earlyExit func()
)

func init() {
	// Initialize exit status in order to facilitate potential failures at any stage.
	sysCode, buildCode = envparser.ExitCodes()

	sysExit = func() { os.Exit(sysCode) }
	buildExit = func() { os.Exit(buildCode) }
	earlyExit = func() { os.Exit(0) }
}

// Start initializes the jacamar-auth application process, requires valid command arguments and
// custom executor provided job context.
func Start(args arguments.ConcreteArgs) {
	ctx := context.Background()
	msg := logging.NewMessenger()

	defer exitPanic(args, msg)

	verifycaps.CurrentExec(args, msg, sysExit)
	stagePrep(args, msg)

	c, err := establishControls(ctx, args, msg)
	if err != nil {
		// All errors encountered here will be considered system errors, even if they are
		// potentially caused by a user influenced settings.
		errorhandling.MessageError(args, msg, err)
		sysExit()
		return
	}

	c.systemPrep()
	c.mngExec()
}

//

func exitPanic(c arguments.ConcreteArgs, msg logging.Messenger) {
	if r := recover(); r != nil {
		err := fmt.Errorf("panic encountered: %v", r)
		errorhandling.MessageError(c, msg, errorhandling.NewPanicError(err))
		sysExit()
	}
}

func stagePrep(args arguments.ConcreteArgs, msg logging.Messenger) {
	switch {
	case args.Config != nil:
		// Runner version validation offers us the ability to provide a better actionable
		// error message that would otherwise rely upon missing environment variables.
		if !envparser.ValidRunnerVersion(minMajor, minMinor) {
			msg.Stderr(fmt.Sprintf(
				"Invalid runner version detected, minimum supported: %d.%d",
				minMajor,
				minMinor,
			))
		}
	case args.Cleanup != nil:
		// Only fail cleanup during explicit cleanup errors, not
		// due to a repeat of an error from config_exec.
		sysExit = func() { os.Exit(0) }
	}
}

// cleanupCfgCheck supports additional verification of the supplied configuration.
func cleanupCfgCheck(logger *logrus.Entry, msg logging.Messenger, file string) {
	meta, opts, err := configure.MetaOptions(file)
	if err != nil {
		// We can ignore errors as they are captured and shared elsewhere.
		return
	}

	// warn/log of unrecognized keys
	if len(meta.Undecoded()) > 0 {
		m := fmt.Sprintf(
			"unrecognized key(s) detected in configuration (%s)",
			meta.Undecoded(),
		)
		msg.Stderr(m)
		logger.Warn(m)
	}

	if opts.Auth.TokenScopeEnforced {
		logger.Warn(
			"job_token_scope_enforced configuration is no longer supported due to changes in " +
				"GitLab server 17.0+. Please see https://gitlab.com/ecp-ci/jacamar-ci/-/issues/156 for details.",
		)
	}
}
