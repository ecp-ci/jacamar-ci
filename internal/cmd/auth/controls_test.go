package auth

import (
	"bytes"
	"context"
	"errors"
	"os"
	"os/exec"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_jobtoken"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_establishControls(t *testing.T) {
	var tarErr errorhandling.ObfuscatedErr

	tests := map[string]authTests{
		"config_exec - failed to establish job context": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.True(t, errorhandling.IsObfuscatedErr(err, &tarErr), "expect obfuscated error type")
			},
		},
		"config_exec - failed to authorize user": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/auth_test_config.toml",
				},
			},
			targetEnv: workingCfgEnv,
			assertError: func(t *testing.T, err error) {
				assert.True(t, errorhandling.IsObfuscatedErr(err, &tarErr), "expect obfuscated error type")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got, err := establishControls(context.TODO(), tt.c, tt.msg)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, got.absExec)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_mngExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	buildExit = func() {}
	sysExit = func() {}

	tokenM := mock_jobtoken.NewMockEstablishedContext(ctrl)
	tokenM.EXPECT().PipelineID().Return("123")

	authM := mock_authuser.NewMockAuthorized(ctrl)
	authM.EXPECT().CIUser().Return(
		authuser.UserContext{
			Username:  "username",
			BuildsDir: t.TempDir() + "/builds",
			CacheDir:  t.TempDir() + "/cache",
		},
	).AnyTimes()
	authM.EXPECT().BuildState().Return(
		envparser.StatefulEnv{},
	).AnyTimes()
	authM.EXPECT().JobContext().Return(tokenM).AnyTimes()

	t.Run("unsupported concrete arguments", func(t *testing.T) {
		ctls := controls{
			args: arguments.ConcreteArgs{},
			absExec: &abstracts.Executor{
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Stderr(gomock.Any(), gomock.Eq("Invalid subcommand - See $ jacamar-auth --help")).Times(1)
					return m
				}(),
			},
			sysLog: testSysLog,
		}

		ctls.mngExec()
	})

	t.Run("running in dev_mode", func(t *testing.T) {
		ctls := controls{
			args: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "file",
				},
			},
			absExec: &abstracts.Executor{
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Stderr(gomock.Any(), gomock.Any())
					return m
				}(),
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(
						configure.Auth{
							Downscope: "none",
							DevMode:   true,
						},
					).AnyTimes()
					m.EXPECT().General().Return(
						configure.General{},
					).AnyTimes()
					return m
				}(),
			},
			auth:   authM,
			sysLog: testSysLog,
		}

		ctls.mngExec()
	})
}

func Test_controls_rootDirManagement(t *testing.T) {
	cur, _ := user.Current()
	if cur.Uid != "0" {
		t.Skip("rootDirManagement tests must be run as root")
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// create new directory
	newDir := t.TempDir()
	create := mock_authuser.NewMockAuthorized(ctrl)
	create.EXPECT().CIUser().Return(authuser.UserContext{
		BaseDir: newDir,
		UID:     0,
		GID:     0,
	}).AnyTimes()

	// chmod on existing directory
	chmodDir := t.TempDir()
	_ = os.Chmod(chmodDir, 0770)
	existing := mock_authuser.NewMockAuthorized(ctrl)
	existing.EXPECT().CIUser().Return(authuser.UserContext{
		BaseDir: chmodDir,
		UID:     0,
		GID:     0,
	}).AnyTimes()

	tests := map[string]authTests{
		"create new directory": {
			auth: create,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDir: func(t *testing.T, dir string) {
				info, err := os.Stat(dir)
				assert.NoError(t, err, "unable to stat directory")
				assert.Equal(t, "drwx------", info.Mode().String())
			},
		},
		"chmod on existing directory": {
			auth: existing,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDir: func(t *testing.T, dir string) {
				info, err := os.Stat(dir)
				assert.NoError(t, err, "unable to stat directory")
				assert.Equal(t, "drwx------", info.Mode().String())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ctls := controls{
				auth: tt.auth,
			}
			err := ctls.rootDirManagement()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertDir != nil {
				tt.assertDir(t, tt.auth.CIUser().BaseDir)
			}
		})
	}
}

func Test_enforceRunStageAllowlist(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	earlyExit = func() {}

	tests := map[string]authTests{
		"no allowlist defined": {
			ae: &abstracts.Executor{
				Stage: "archive_cache",
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(configure.Auth{}).Times(1)
					return m
				}(),
			},
		},
		"stage encountered missing on list": {
			ae: &abstracts.Executor{
				Stage: "archive_cache",
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(configure.Auth{
						RunStageAllowlist: []string{
							"step_script",
							"get_sources",
						},
					}).Times(1)
					return m
				}(),
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Warn("Stage archive_cache skipped due to runner configuration")
					return m
				}(),
			},
		},
		"stage encountered on list": {
			ae: &abstracts.Executor{
				Stage: "step_script",
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(configure.Auth{
						RunStageAllowlist: []string{
							"step_script",
							"get_sources",
						},
					}).Times(1)
					return m
				}(),
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ctls := controls{
				absExec: tt.ae,
				sysLog:  testSysLog,
			}
			ctls.enforceRunStageAllowlist()
		})
	}
}

func Test_controls_runExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	buildExit = func() {}
	sysExit = func() {}

	cmd := exec.Command("/bin/bash")
	cmd.Stdin = bytes.NewBufferString("exit 1")
	_, execErr := cmd.CombinedOutput()

	tests := map[string]struct {
		absExec *abstracts.Executor
		auth    authuser.Authorized
		args    arguments.ConcreteArgs
	}{
		"exec.ExitError": {
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()
					m.EXPECT().Options().Return(configure.Options{})
					m.EXPECT().General().Return(configure.General{})
					return m
				}(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().TransferScript(
						gomock.Eq("success_transfer"),
						gomock.Eq("step_script"),
						gomock.Any(),
						gomock.Any(),
					).Return(nil)
					m.EXPECT().PipeOutput(gomock.Eq("")).Return(execErr)
					return m
				}(),
				Env: envparser.ExecutorEnv{},
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					return m
				}(),
				Stage: "step_script",
			},
			args: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Script: "success_transfer",
				},
			},
		},
		"functional step_script": {
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(configure.Auth{
						RunStageAllowlist: []string{"step_script", "get_sources"},
					}).AnyTimes()
					m.EXPECT().Options().Return(configure.Options{})
					return m
				}(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().TransferScript(
						gomock.Eq("functional_step_script"),
						gomock.Eq("step_script"),
						gomock.Any(),
						gomock.Any(),
					).Return(nil)
					m.EXPECT().PipeOutput(gomock.Eq("")).Return(nil)
					return m
				}(),
				Env: envparser.ExecutorEnv{},
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					return m
				}(),
				Stage: "step_script",
			},
			args: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Script: "functional_step_script",
				},
			},
		},
		"failed transfer": {
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(configure.Auth{})
					m.EXPECT().Options().Return(configure.Options{})
					return m
				}(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().TransferScript(
						gomock.Eq("failed_transfer"),
						gomock.Eq("step_script"),
						gomock.Any(),
						gomock.Any(),
					).Return(errors.New("transfer error"))
					return m
				}(),
				Env: envparser.ExecutorEnv{},
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Warn(gomock.Any(), gomock.Eq("failed to transfer job script (step_script): transfer error"))
					return m
				}(),
				Stage: "step_script",
			},
			args: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Script: "failed_transfer",
				},
			},
		},
		"bypass exit file check": {
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(configure.Auth{})
					m.EXPECT().Options().Return(configure.Options{})
					m.EXPECT().General().Return(configure.General{
						BypassExitFile: true,
					})
					return m
				}(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().TransferScript(
						gomock.Eq("success_transfer"),
						gomock.Eq("step_script"),
						gomock.Any(),
						gomock.Any(),
					).Return(nil)
					m.EXPECT().PipeOutput(gomock.Eq("")).Return(errors.New("failed run"))
					return m
				}(),
				Env: envparser.ExecutorEnv{},
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Warn(gomock.Any(), gomock.Eq("error executing run_exec: failed run"))
					return m
				}(),
				Stage: "step_script",
			},
			args: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Script: "success_transfer",
				},
			},
		},
		"non exec.ExitError": {
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Auth().Return(configure.Auth{})
					m.EXPECT().Options().Return(configure.Options{})
					m.EXPECT().General().Return(configure.General{})
					return m
				}(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().TransferScript(
						gomock.Eq("success_transfer"),
						gomock.Eq("step_script"),
						gomock.Any(),
						gomock.Any(),
					).Return(nil)
					m.EXPECT().PipeOutput(gomock.Eq("")).Return(errors.New("failed run"))
					return m
				}(),
				Env: envparser.ExecutorEnv{},
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Warn(gomock.Any(), gomock.Eq("error executing run_exec: failed run"))
					return m
				}(),
				Stage: "step_script",
			},
			args: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Script: "success_transfer",
				},
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			c := controls{
				absExec: tt.absExec,
				auth:    tt.auth,
				args:    tt.args,
				sysLog:  testSysLog,
			}

			c.runExec()
		})
	}
}
