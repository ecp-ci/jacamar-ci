package auth

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/preparations"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/internal/execerr"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/basic"
	"gitlab.com/ecp-ci/jacamar-ci/internal/seccomp"
	"gitlab.com/ecp-ci/jacamar-ci/internal/verifycaps"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

type controls struct {
	absExec *abstracts.Executor
	auth    authuser.Authorized
	args    arguments.ConcreteArgs
	sysLog  *logrus.Entry
}

func establishControls(
	ctx context.Context,
	args arguments.ConcreteArgs,
	msg logging.Messenger,
) (controls, error) {
	absExec, sysLog, err := preparations.JobContext(args, sysExit)
	if err != nil {
		// Jacamar system logger likely not available at this point, we will need to
		// rely on runner's built in logging.
		return controls{}, errorhandling.NewJobCtxError(err)
	}
	absExec.Msg = msg

	auth, err := preparations.NewAuthorizedUser(absExec, args, sysLog)
	if err != nil {
		// Logging managed by authuser package.
		return controls{}, errorhandling.NewAuthError(err)
	}

	cmdr, err := preparations.NewCommander(ctx, absExec, args, auth, sysLog)
	if err != nil {
		sysLog.Error("unable to build runner/commander interface: ", err.Error())
		return controls{}, errorhandling.NewRunMechanismError(err)
	}
	absExec.Runner = basic.NewMechanism(cmdr)

	return controls{
		absExec: absExec,
		args:    args,
		auth:    auth,
		sysLog:  sysLog,
	}, nil
}

//

func (c controls) systemPrep() {
	err := seccomp.Factory{
		Stage:  c.absExec.Stage,
		Opt:    c.absExec.Cfg.Options(),
		Usr:    c.auth.CIUser(),
		SysLog: c.sysLog,
	}.Establish()
	if err != nil {
		c.sysLog.Error("unable to establish seccomp filter: ", err.Error())
		preparations.StdError(c.args, err.Error(), c.absExec.Msg, sysExit)
	}

	if c.absExec.Cfg.Options().Auth.NoNewPrivs {
		err = seccomp.SetNoNewPrivs()
		if err != nil {
			c.sysLog.Error("unable to establish set no_new_privs: ", err.Error())
			preparations.StdError(c.args, err.Error(), c.absExec.Msg, sysExit)
		}
	}
}

func (c controls) mngExec() {
	switch {
	case c.args.Config != nil:
		if c.absExec.Cfg.Auth().DevMode {
			c.sysLog.Info("application running in auth.dev_mode")
		}
		verifycaps.IsNoNewPrivs(c.sysLog)
		preparations.ConfigExec(c.absExec, c.auth, c.args, sysExit, c.sysLog)
	case c.args.Prepare != nil:
		c.prepareExec()
	case c.args.Run != nil:
		c.runExec()
	case c.args.Cleanup != nil:
		c.cleanupExec()
	default:
		// Catch all in case additional subcommands are ever supported for jacamar-auth.
		c.sysLog.Error("invalid jacamar-auth subcommand encountered: ", c.args)
		preparations.StdError(
			c.args,
			"Invalid subcommand - See $ jacamar-auth --help",
			c.absExec.Msg,
			sysExit,
		)
	}
}

func (c controls) prepareExec() {
	if (c.absExec.Cfg.Auth()).RootDirCreation {
		if err := c.rootDirManagement(); err != nil {
			err = errorhandling.NewAuthError(fmt.Errorf("error managing base directory: %w", err))
			c.sysLog.Error(err)
			errorhandling.MessageError(c.args, c.absExec.Msg, err)
			sysExit()
		}
	}

	if err := c.absExec.Runner.PipeOutput(""); err != nil {
		errMsg := fmt.Sprintf("Error executing prepare_exec: %s", err.Error())
		c.sysLog.Error(errMsg)
		preparations.StdError(c.args, errMsg, c.absExec.Msg, sysExit)
	}
}

func (c controls) runExec() {
	c.enforceRunStageAllowlist()

	err := c.absExec.Runner.TransferScript(
		c.args.Run.Script,
		c.absExec.Stage,
		c.absExec.Env,
		c.absExec.Cfg.Options(),
	)
	if err != nil {
		errMsg := fmt.Sprintf(
			"failed to transfer job script (%s): %s",
			c.absExec.Stage,
			err.Error(),
		)
		preparations.StdError(c.args, errMsg, c.absExec.Msg, sysExit)
		return
	}

	if err = c.absExec.Runner.PipeOutput(""); err != nil {
		c.mngRunExit(err)
	}
}

func (c controls) cleanupExec() {
	cleanupCfgCheck(c.sysLog, c.absExec.Msg, c.args.Cleanup.Configuration)

	if err := c.absExec.Runner.PipeOutput(""); err != nil {
		errMsg := fmt.Sprintf("Error executing cleanup_exec: %s", err.Error())
		c.sysLog.Warning(errMsg)
		preparations.StdError(c.args, errMsg, c.absExec.Msg, buildExit)
	}
}

//

// rootDirManagement creates/manages the root directory for an authorization user during
// the privileged authorization process.
func (c controls) rootDirManagement() error {
	usrCtx := c.auth.CIUser()
	info, err := os.Stat(usrCtx.BaseDir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if os.IsNotExist(err) {
		/* #nosec */
		// directory 700 permissions required
		if err = os.Mkdir(usrCtx.BaseDir, 0700); err != nil {
			return err
		}
	} else if info.Mode().String() != "drwx------" {
		/* #nosec */
		// directory 700 permissions required
		if err = os.Chmod(usrCtx.BaseDir, 0700); err != nil {
			return err
		}
	}

	// enforce permissions
	return os.Chown(
		usrCtx.BaseDir,
		int(usrCtx.UID),
		int(usrCtx.GID),
	)
}

func (c controls) enforceRunStageAllowlist() {
	allowList := c.absExec.Cfg.Auth().RunStageAllowlist

	if len(allowList) > 0 {
		for _, v := range allowList {
			if strings.EqualFold(v, c.absExec.Stage) {
				return
			}
		}

		warnMsg := fmt.Sprintf("Stage %s skipped due to runner configuration", c.absExec.Stage)
		c.sysLog.Warning(warnMsg)
		c.absExec.Msg.Warn(warnMsg)
		earlyExit()
	}
}

func (c controls) mngRunExit(err error) {
	errMsg := fmt.Sprintf("error executing run_exec: %s", err.Error())
	c.sysLog.Error(errMsg)

	if c.absExec.Cfg.General().BypassExitFile {
		preparations.StdError(c.args, errMsg, c.absExec.Msg, buildExit)
		return
	}

	var eErr *exec.ExitError
	if errors.As(err, &eErr) && eErr.ExitCode() == buildCode {
		execerr.ReportRunExit(c.absExec, c.auth, c.sysLog)
		buildExit()
	} else {
		preparations.StdError(c.args, errMsg, c.absExec.Msg, sysExit)
	}
}
