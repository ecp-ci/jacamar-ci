package auth

import (
	"io"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type authTests struct {
	ae        *abstracts.Executor
	c         arguments.ConcreteArgs
	targetEnv map[string]string

	auth *mock_authuser.MockAuthorized
	msg  *mock_logging.MockMessenger

	assertError    func(*testing.T, error)
	assertAbstract func(*testing.T, *abstracts.Executor)
	assertDir      func(*testing.T, string)
}

var (
	testSysLog    *logrus.Entry
	workingCfgEnv map[string]string
)

func init() {
	// override initialized exit behaviors
	sysExit = func() {}
	buildExit = func() {}

	workingCfgEnv = map[string]string{
		"TRUSTED_CI_CONCURRENT_PROJECT_ID":   "1",
		"TRUSTED_CI_JOB_ID":                  "123",
		"TRUSTED_CI_JOB_TOKEN":               "JoBt0k3n",
		"TRUSTED_CI_RUNNER_SHORT_TOKEN":      "abc",
		"TRUSTED_CI_BUILDS_DIR":              "/gitlab/builds",
		"TRUSTED_CI_CACHE_DIR":               "/gitlab/cache",
		envkeys.UserEnvPrefix + "CI_JOB_JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		"TRUSTED_CI_SERVER_URL":              "gitlab.url",
	}

	ll := logrus.New()
	ll.Out = io.Discard
	testSysLog = logrus.NewEntry(ll)
}

//

func Test_exitPanic(t *testing.T) {
	sysExit = func() {}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	configMsg := mock_logging.NewMockMessenger(ctrl)
	configMsg.EXPECT().Stderr(
		gomock.Eq("Error encountered during job: %s"),
		gomock.Eq("unexpected fatal error (refer to system logs for additional details)"),
	)

	runMsg := mock_logging.NewMockMessenger(ctrl)
	runMsg.EXPECT().Warn(
		gomock.Eq("Error encountered during job: %s"),
		gomock.Eq("unexpected fatal error (refer to system logs for additional details)"),
	)

	tests := map[string]struct {
		c   arguments.ConcreteArgs
		msg logging.Messenger
	}{
		"panic encountered during config_exec": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			msg: configMsg,
		},
		"panic encountered during run_exec": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			msg: runMsg,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			defer exitPanic(tt.c, tt.msg)
			panic(1)
		})
	}
}

func Test_stagePrep(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Stderr("Invalid runner version detected, minimum supported: 14.1").Times(2)

	tests := map[string]struct {
		c         arguments.ConcreteArgs
		msg       logging.Messenger
		targetEnv map[string]string
	}{
		"valid runner version during config": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "14.1.0",
			},
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			msg: msg,
		},
		"invalid runner version during config": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "13.8.0",
			},
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			msg: msg,
		},
		"cleanup stage, invalid runner versions": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "13.8.0",
			},
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			msg: msg,
		},
		"run stage, do nothing": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			msg: msg,
		},
		"prepare stage, do nothing": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			msg: msg,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for v, k := range tt.targetEnv {
				t.Setenv(k, v)
			}

			sysExit = func() { return }

			stagePrep(tt.c, tt.msg)
		})
	}
}

func Test_cleanupCfgCheck(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	missKey := mock_logging.NewMockMessenger(ctrl)
	missKey.EXPECT().Stderr("unrecognized key(s) detected in configuration ([general.custom_data_dirs])")

	tests := map[string]struct {
		msg  logging.Messenger
		file string
	}{
		"undecoded keys encountered": {
			file: "../../../test/testdata/configs/invalid_decoder.toml",
			msg:  missKey,
		},
		"valid auth": {
			file: "../../test/testdata/configs/valid_auth.toml",
			msg:  mock_logging.NewMockMessenger(ctrl),
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			cleanupCfgCheck(testSysLog, tt.msg, tt.file)
		})
	}
}
