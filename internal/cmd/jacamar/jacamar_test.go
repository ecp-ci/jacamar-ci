package jacamar

import (
	"os"
	"os/exec"
	"os/user"
	"reflect"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/exectype"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type jacamarTests struct {
	c         arguments.ConcreteArgs
	s         string
	targetEnv map[string]string

	msg *mock_logging.MockMessenger

	assertAbstract func(*testing.T, *abstracts.Executor)
	assertGeneric  func(*testing.T)
}

var (
	workingCfgEnv map[string]string
)

func init() {
	// override initialized exit behaviors
	sysExit = func() {}
	buildExit = func() {}
	exitZero = func() {}

	workingCfgEnv = map[string]string{
		"TRUSTED_CI_CONCURRENT_PROJECT_ID":   "1",
		"TRUSTED_CI_JOB_ID":                  "123",
		"TRUSTED_CI_JOB_TOKEN":               "JoBt0k3n",
		"TRUSTED_CI_RUNNER_SHORT_TOKEN":      "abc",
		"TRUSTED_CI_BUILDS_DIR":              "/gitlab/builds",
		"TRUSTED_CI_CACHE_DIR":               "/gitlab/cache",
		envkeys.UserEnvPrefix + "CI_JOB_JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		"TRUSTED_CI_SERVER_URL":              "gitlab.url",
	}
}

func TestStart(t *testing.T) {
	t.Run("jacamar signal test", func(t *testing.T) {
		cmd := exec.Command("sleep", "30")
		err := cmd.Start()
		if err != nil {
			assert.NoError(t, err, "error starting sleep command")
			return
		}

		Start(arguments.ConcreteArgs{
			Signal: &arguments.SignalCmd{
				Signal: "SIGKILL",
				PID:    strconv.Itoa(cmd.Process.Pid),
			},
		})
	})

	_, found := os.LookupEnv("CI_JOB_JWT")
	if !found {
		// Test more comprehensive shell based workflow against functional CI environment.
		// Without access to a GitLab server and related CI predefined variable testing
		// via Pavilion would be required for similar results.
		t.Skip("TestStart only written for CI environment")
	}

	usr, _ := user.Current()

	cmd := exec.Command(
		"go",
		"run",
		os.ExpandEnv("${CI_PROJECT_DIR}/tools/config2base64/config2base64.go"),
		os.ExpandEnv("${CI_PROJECT_DIR}/test/testdata/configs/shell_no_setuid.toml"),
	)
	b, err := cmd.CombinedOutput()
	if err != nil {
		assert.NoError(t, err, "filed to prepare configuration file: "+string(b))
		return
	}

	targetEnv := map[string]string{
		"JACAMAR_CI_CONFIG_STR":     string(b),
		"JACAMAR_CI_BASE_DIR":       "/tmp/" + usr.Username,
		envkeys.JacamarBuildsDir:    "/tmp/" + usr.Username + "/builds",
		"JACAMAR_CI_CACHE_DIR":      "/tmp/" + usr.Username + "/cache",
		"JACAMAR_CI_SCRIPT_DIR":     "/tmp/" + usr.Username + "/script",
		"JACAMAR_CI_AUTH_USERNAME":  usr.Username,
		"JACAMAR_CI_RUNNER_TIMEOUT": "5m",
	}

	tests := []jacamarTests{
		{
			s:         "prepare working directories",
			targetEnv: targetEnv,
			c: arguments.ConcreteArgs{
				NoAuth:  true,
				Prepare: &arguments.PrepareCmd{},
			},
			assertGeneric: func(t *testing.T) {
				_, err := os.Stat("/tmp/" + usr.Username)
				assert.NoError(t, err, "JACAMAR_CI_BASE_DIR not found")
			},
		},
		{
			s:         "step_script successfully executed",
			targetEnv: targetEnv,
			c: arguments.ConcreteArgs{
				NoAuth: true,
				Run: &arguments.RunCmd{
					Stage:  "step_script",
					Script: "../../../test/testdata/job-scripts/touch_file.bash",
				},
			},
			assertGeneric: func(t *testing.T) {
				assert.FileExists(t, "/tmp/example-file")
			},
		},
		{
			s:         "cleanup working directories",
			targetEnv: targetEnv,
			c: arguments.ConcreteArgs{
				NoAuth: true,
				Cleanup: &arguments.CleanupCmd{
					Configuration: os.ExpandEnv("${CI_PROJECT_DIR}/test/testdata/configs/shell_no_setuid.toml"),
				},
			},
		},
		{
			s:         "cleanup with invalid downscope",
			targetEnv: targetEnv,
			c: arguments.ConcreteArgs{
				NoAuth: true,
				Cleanup: &arguments.CleanupCmd{
					Configuration: os.ExpandEnv("${CI_PROJECT_DIR}/test/testdata/configs/invalid_auth.toml"),
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			for _, v := range os.Environ() {
				if strings.HasPrefix(v, "CI_") || strings.HasPrefix(v, "GITLAB_") {
					loc := strings.Index(v, "=")
					key := v[:loc]
					val := v[loc+1:]
					t.Setenv("CUSTOM_ENV_"+key, val)
				}
			}

			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			Start(tt.c)

			if tt.assertGeneric != nil {
				tt.assertGeneric(t)
			}
		})
	}
}

func Test_newExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		absExec     *abstracts.Executor
		assertExec  func(*testing.T, executors.Executor)
		assertError func(*testing.T, error)
	}{
		"no executor defined": {
			absExec: &abstracts.Executor{
				TargetExec: -1,
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "unrecognized executor")
			},
		},
		"slurm executor defined": {
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Batch().Return(configure.Batch{})
					return m
				}(),
				TargetExec: exectype.SlurmType,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExec: func(t *testing.T, e executors.Executor) {
				assert.Equal(t, "*slurm.executor", reflect.TypeOf(e).String())
			},
		},
		"lsf executor defined": {
			absExec: &abstracts.Executor{
				TargetExec: exectype.LSFType,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExec: func(t *testing.T, e executors.Executor) {
				assert.Equal(t, "*lsf.executor", reflect.TypeOf(e).String())
			},
		},
		"cobalt executor defined": {
			absExec: &abstracts.Executor{
				TargetExec: exectype.CobaltType,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExec: func(t *testing.T, e executors.Executor) {
				assert.Equal(t, "*cobalt.executor", reflect.TypeOf(e).String())
			},
		},
		"shell executor defined": {
			absExec: &abstracts.Executor{
				TargetExec: exectype.ShellType,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExec: func(t *testing.T, e executors.Executor) {
				assert.Equal(t, "*shell.executor", reflect.TypeOf(e).String())
			},
		},
		"pbs executor defined": {
			absExec: &abstracts.Executor{
				TargetExec: exectype.PBSType,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExec: func(t *testing.T, e executors.Executor) {
				assert.Equal(t, "*pbs.executor", reflect.TypeOf(e).String())
			},
		},
		"flux executor defined": {
			absExec: &abstracts.Executor{
				TargetExec: exectype.FluxType,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExec: func(t *testing.T, e executors.Executor) {
				assert.Equal(t, "*flux.executor", reflect.TypeOf(e).String())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := newExecutor(tt.absExec)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExec != nil {
				tt.assertExec(t, got)
			}
		})
	}
}

func Test_staticDirCleanup(t *testing.T) {
	tests := map[string]struct {
		gen      configure.General
		baseDir  string
		now      time.Time
		prepDir  func(string)
		checkDir func(*testing.T, string)
	}{
		"limited static directory": {
			gen: configure.General{
				StaticBuildsDir:    true,
				StaticMinDays:      1,
				StaticCleanupLimit: 1,
			},
			baseDir: t.TempDir() + "/limit",
			now:     time.Now().AddDate(0, 0, 3),
			prepDir: func(baseDir string) {
				_ = os.MkdirAll(baseDir+"/static/1", 0700)
				_ = os.MkdirAll(baseDir+"/static/2", 0700)
			},
			checkDir: func(t *testing.T, baseDir string) {
				folders, _ := os.ReadDir(baseDir + "/static")
				assert.Len(t, folders, 1)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepDir != nil {
				tt.prepDir(tt.baseDir)
			}

			staticDirCleanup(tt.gen, tt.baseDir, tt.now)

			if tt.checkDir != nil {
				tt.checkDir(t, tt.baseDir)
			}
		})
	}
}
