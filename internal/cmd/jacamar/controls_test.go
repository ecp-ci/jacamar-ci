package jacamar

import (
	"context"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func Test_establishControls(t *testing.T) {
	ctx := context.Background()
	cancelCtx, cancelFun := context.WithCancel(ctx)
	defer cancelFun()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Stderr(gomock.Any(), gomock.Any()).AnyTimes()

	tests := map[string]jacamarTests{
		"config_exec - failed to establish job context": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			msg: m,
		},
		"config_exec - failed to authorize user": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/auth_test_config.toml",
				},
			},
			msg:       m,
			targetEnv: workingCfgEnv,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got := establishControls(cancelCtx, tt.c, tt.msg)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, got.absExec)
			}
		})
	}
}

func Test_controls_mngExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	buildExit = func() {}
	sysExit = func() {}

	t.Run("unsupported concrete arguments", func(t *testing.T) {
		ctls := controls{
			args: arguments.ConcreteArgs{},
			absExec: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().General().Return(configure.General{}).AnyTimes()
					return m
				}(),
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Stderr(gomock.Any(), gomock.Eq("Invalid subcommand - See $ jacamar --help")).Times(1)
					return m
				}(),
			},
		}

		ctls.mngExec()
	})
}

func Test_controls_prepareExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	jobMsg := mock_logging.NewMockMessenger(ctrl)
	jobMsg.EXPECT().Notify("This is a job message!")

	jobCfg := mock_configure.NewMockConfigurer(ctrl)
	jobCfg.EXPECT().General().Return(configure.General{
		JobMessage: "This is a job message!",
	}).AnyTimes()

	sysExit = func() {}

	tests := map[string]struct {
		ae *abstracts.Executor
		c  arguments.ConcreteArgs
	}{
		"job message configured": {
			ae: &abstracts.Executor{
				Cfg: jobCfg,
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						BaseDir:   t.TempDir() + "/base",
						BuildsDir: t.TempDir() + "/builds",
						CacheDir:  t.TempDir() + "/cache",
						ScriptDir: t.TempDir() + "/script",
					},
				},
				Msg: jobMsg,
			},
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ctls := controls{
				absExec: tt.ae,
				args:    tt.c,
			}
			ctls.prepareExec()
		})
	}
}

func Test_controls_cleanupExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	sysExit = func() {}

	tests := map[string]struct {
		ae            *abstracts.Executor
		c             arguments.ConcreteArgs
		prepareDirs   func(*testing.T, *abstracts.Executor)
		assertAbsExec func(*testing.T, *abstracts.Executor)
	}{
		"standard cleanup directories": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			ae: &abstracts.Executor{
				Cfg: func() configure.Configurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().General().Return(configure.General{}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						BaseDir:   t.TempDir() + "/standard",
						BuildsDir: t.TempDir() + "/standard/builds",
						CacheDir:  t.TempDir() + "/standard/cache",
						ScriptDir: t.TempDir() + "/standard/script",
					},
				},
			},
			prepareDirs: func(t *testing.T, ae *abstracts.Executor) {
				_ = usertools.CreateDirectories(ae.Env.StatefulEnv, ae.Cfg.General())
			},
		},
		"invalid cleanup directories": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			ae: &abstracts.Executor{
				Cfg: func() configure.Configurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().General().Return(configure.General{}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						BaseDir: t.TempDir(),
					},
				},
				Msg: func() logging.Messenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Stderr(gomock.Any(), gomock.Any())
					return m
				}(),
			},
		},
		"static builds cleanup directories": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			ae: &abstracts.Executor{
				Cfg: func() configure.Configurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().General().Return(configure.General{
						StaticBuildsDir: true,
						StaticMinDays:   -1,
					}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						BaseDir:   t.TempDir() + "/builds",
						BuildsDir: t.TempDir() + "/builds/static",
						CacheDir:  t.TempDir() + "/builds/cache",
						ScriptDir: t.TempDir() + "/builds/script",
					},
				},
			},
			prepareDirs: func(t *testing.T, ae *abstracts.Executor) {
				_ = usertools.CreateDirectories(ae.Env.StatefulEnv, ae.Cfg.General())
				_ = os.Mkdir(ae.Env.StatefulEnv.BuildsDir+"/123", 0700)
			},
			assertAbsExec: func(t *testing.T, ae *abstracts.Executor) {
				_, err := os.ReadDir(ae.Env.BuildsDir + "/1234")
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepareDirs != nil {
				tt.prepareDirs(t, tt.ae)
			}

			ctls := controls{
				absExec: tt.ae,
				args:    tt.c,
			}

			ctls.cleanupExec()

			if tt.assertAbsExec != nil {
				tt.assertAbsExec(t, tt.ae)
			}
		})
	}
}
