package jacamar

import (
	"context"
	"errors"
	"fmt"
	"os"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/exectype"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/cobalt"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/flux"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/lsf"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/pbs"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/shell"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/slurm"
	"gitlab.com/ecp-ci/jacamar-ci/internal/flock"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

var (
	// Function to facilitate associated process exit.
	sysExit, buildExit, exitZero func()
)

func init() {
	// Initialize exit status in order to facilitate potential failures at any stage.
	sys, build := envparser.ExitCodes()

	sysExit = func() { os.Exit(sys) }
	buildExit = func() { os.Exit(build) }
	exitZero = func() { os.Exit(0) }
}

// Start initializes the 'jacamar' application process, requires valid command arguments and
// custom executor provided context to be present.
func Start(c arguments.ConcreteArgs) {
	ctx := context.Background()
	msg := logging.NewMessenger()

	defer exitPanic(msg)

	switch {
	case c.Signal != nil:
		usertools.ManageSignal(msg, *c.Signal, sysExit, exitZero)
		return
	case c.Lock != nil:
		flock.ManageLock(msg, *c.Lock, sysExit, exitZero)
		return
	case c.Cleanup != nil:
		sysExit = exitZero
	case c.Read != nil:
		readCmd(c.Read, msg)
		return
	}

	ctls := establishControls(ctx, c, msg)
	ctls.mngExec()
}

//

func newExecutor(ae *abstracts.Executor) (executors.Executor, error) {
	switch ae.TargetExec {
	case exectype.ShellType:
		return shell.NewExecutor(ae), nil
	case exectype.FluxType:
		return flux.NewExecutor(ae)
	case exectype.SlurmType:
		return slurm.NewExecutor(ae)
	case exectype.PBSType:
		return pbs.NewExecutor(ae)
	case exectype.LSFType:
		return lsf.NewExecutor(ae)
	case exectype.CobaltType:
		return cobalt.NewExecutor(ae)
	default:
		return nil, errors.New("unrecognized executor type defined")
	}
}

func staticDirCleanup(gen configure.General, baseDir string, now time.Time) {
	if !gen.StaticBuildsDir || gen.StaticMinDays == 0 {
		// Account for potential configurations or invalid state that would allow for us
		// to skip the static directory cleanup process.
		return
	}

	// As we've already verified the folder structure/permissions in an earlier
	// step any error encountered here is unlikely but more important an indication
	// that the files no longer exists and thus attempting to notify the user
	// would be unnecessary.
	folders, _ := os.ReadDir(baseDir + "/static")

	dur := time.Duration(gen.StaticMinDays*24) * time.Hour

	removedDirs := 0
	for _, f := range folders {
		folderInfo, _ := f.Info()
		if folderInfo.ModTime().Add(dur).Before(now) {
			err := os.RemoveAll(baseDir + "/static/" + folderInfo.Name())

			if err == nil {
				removedDirs += 1
				if removedDirs >= gen.StaticCleanupLimit {
					return
				}
			}
		}
	}
}

func readCmd(c *arguments.ReadCmd, msg logging.Messenger) {
	out, err := usertools.RestrictedRead(c.TarFile)
	if err != nil {
		sysExit()
	} else {
		msg.Stdout(out)
	}
}

func exitPanic(msg logging.Messenger) {
	if r := recover(); r != nil {
		msg.Error(fmt.Sprintf("Unexpected error encountered: %v", r))
		sysExit()
	}
}
