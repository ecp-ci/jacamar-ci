package jacamar

import (
	"context"
	"os"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/preparations"
	"gitlab.com/ecp-ci/jacamar-ci/internal/execerr"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/flock"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/runnerinit"
	"gitlab.com/ecp-ci/jacamar-ci/internal/ulimit"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

type controls struct {
	absExec *abstracts.Executor
	auth    authuser.Authorized
	args    arguments.ConcreteArgs
	sysLog  *logrus.Entry
}

func establishControls(
	ctx context.Context,
	args arguments.ConcreteArgs,
	msg logging.Messenger,
) controls {
	absExec, sysLog, err := preparations.JobContext(args, sysExit)
	if err != nil {
		preparations.StdError(args, err.Error(), msg, sysExit)
		return controls{}
	}
	absExec.Msg = msg

	auth, err := preparations.NewAuthorizedUser(absExec, args, sysLog)
	if err != nil {
		preparations.StdError(args, err.Error(), msg, sysExit)
		return controls{}
	}

	cmdr, err := preparations.NewCommander(ctx, absExec, args, auth, sysLog)
	if err != nil {
		preparations.StdError(args, err.Error(), msg, sysExit)
		return controls{}
	}

	runner, err := runnerinit.InitializeUserRunner(
		cmdr,
		absExec.Env,
		absExec.Cfg.General(),
		msg,
		absExec.Stage,
	)
	if err != nil {
		preparations.StdError(args, err.Error(), msg, sysExit)
		return controls{}
	}
	absExec.Runner = runner

	return controls{
		absExec: absExec,
		args:    args,
		auth:    auth,
		sysLog:  sysLog,
	}
}

//

func (c controls) mngExec() {
	if err := ulimit.UserManage(c.absExec.Cfg.General()); err != nil {
		preparations.StdError(c.args, err.Error(), c.absExec.Msg, sysExit)
	}

	switch {
	case c.args.Config != nil:
		preparations.ConfigExec(c.absExec, c.auth, c.args, sysExit, c.sysLog)
	case c.args.Prepare != nil:
		c.prepareExec()
	case c.args.Run != nil:
		exec, err := newExecutor(c.absExec)
		if err != nil {
			preparations.StdError(c.args, err.Error(), c.absExec.Msg, sysExit)
		}

		c.absExec.Runner.CommandDir(c.auth.CIUser().ScriptDir)
		c.runExec(exec)
	case c.args.Cleanup != nil:
		c.cleanupExec()
	default:
		preparations.StdError(
			c.args,
			"Invalid subcommand - See $ jacamar --help",
			c.absExec.Msg,
			sysExit,
		)
	}
}

func (c controls) prepareExec() {
	jobMessage := c.absExec.Cfg.General().JobMessage
	if jobMessage != "" {
		c.absExec.Msg.Notify(jobMessage)
	}

	if err := executors.Prepare(c.absExec); err != nil {
		preparations.StdError(c.args, err.Error(), c.absExec.Msg, sysExit)
	}
}

func (c controls) runExec(exec executors.Executor) {
	if err := executors.Run(c.absExec, exec, c.auth); err != nil {
		exitCode, errMsg := execerr.IdentifyExit(c.absExec.Env, err)
		preparations.StdError(c.args, errMsg, c.absExec.Msg, func() { os.Exit(exitCode) })
	}
}

func (c controls) cleanupExec() {
	err := usertools.PreCleanupVerification(
		c.absExec.Env.StatefulEnv,
		c.absExec.Cfg.General(),
	)
	if err != nil {
		preparations.StdError(c.args, err.Error(), c.absExec.Msg, sysExit)
	}

	executors.Cleanup(c.absExec)

	if flock.LimitedDirAllowed(c.absExec.Cfg.General()) {
		flock.Release(c.absExec.Env)
		err := flock.Cleanup(c.absExec.Cfg.General(), c.absExec.Env)
		if err != nil {
			c.absExec.Msg.Stderr("file lock cleanup error: %s", err.Error())
		}
	}
	staticDirCleanup(c.absExec.Cfg.General(), c.absExec.Env.StatefulEnv.BaseDir, time.Now())
}
