package runmechanisms

import (
	"encoding/base64"
	"errors"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type AuthFileContents struct {
	Credentials map[string]AuthFileCredentials `json:"auths"`
}

type AuthFileCredentials struct {
	Auth     string `json:"auth"`
	Username string `json:"-"`
	Password string `json:"-"`
}

func ParseRegistryCredentials(env envparser.ExecutorEnv) (AuthFileContents, error) {
	afc := AuthFileContents{
		Credentials: make(map[string]AuthFileCredentials),
	}

	if env.StatefulEnv.RegistryCredentials == "" {
		return afc, nil
	}

	encodedCreds := strings.Split(env.StatefulEnv.RegistryCredentials, ",")
	for _, ec := range encodedCreds {
		tmp := strings.Split(ec, ":")
		if len(tmp) != 2 {
			return afc, errors.New("invalid registry credentials in stateful environment")
		}

		bURL, _ := base64.RawURLEncoding.DecodeString(tmp[0])
		bCreds, _ := base64.RawURLEncoding.DecodeString(tmp[1])

		tmpUP := strings.Split(string(bCreds), ":")
		if len(tmpUP) != 2 {
			return afc, errors.New("invalid username:password encoding")
		}

		afc.Credentials[string(bURL)] = AuthFileCredentials{
			Auth:     tmp[1],
			Username: tmpUP[0],
			Password: tmpUP[1],
		}
	}

	return afc, nil
}
