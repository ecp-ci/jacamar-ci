package runmechanisms

import (
	"errors"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_command"
)

func Test_FileOutput(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		filename string
		stdin    string
		cmdr     command.Commander

		want    string
		wantErr bool
	}{
		"output on error": {
			filename: func() string {
				f := t.TempDir() + "error.out"
				_ = os.WriteFile(f, []byte("test"), 0700)
				return f
			}(),
			cmdr: func() *mock_command.MockCommander {
				m := mock_command.NewMockCommander(ctrl)
				m.EXPECT().ReturnOutput(gomock.Any()).Return("error output", errors.New("error message"))
				return m
			}(),
			want:    "test",
			wantErr: true,
		},
		"no output": {
			filename: t.TempDir() + "missing.out",
			cmdr: func() *mock_command.MockCommander {
				m := mock_command.NewMockCommander(ctrl)
				m.EXPECT().ReturnOutput(gomock.Any()).Return("", errors.New("error message"))
				return m
			}(),
			want:    "",
			wantErr: true,
		},
		"output on success": {
			filename: func() string {
				f := t.TempDir() + "success.out"
				_ = os.WriteFile(f, []byte("test"), 0700)
				return f
			}(),
			cmdr: func() *mock_command.MockCommander {
				m := mock_command.NewMockCommander(ctrl)
				m.EXPECT().ReturnOutput(gomock.Any()).Return("", nil)
				return m
			}(),
			want:    "test",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := FileOutput(tt.filename, tt.stdin, tt.cmdr)
			if (err != nil) != tt.wantErr {
				t.Errorf("FileOutput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("FileOutput() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_Stage(t *testing.T) {
	t.Run("UserRuneStage step_*", func(t *testing.T) {
		assert.True(t, UserRunStage("step_example"))
	})

	t.Run("StepScriptStage step_*", func(t *testing.T) {
		assert.False(t, StepScriptStage("step_example"))
	})
}

func Test_ParseRegistryCredentials(t *testing.T) {
	t.Run("missing credentials", func(t *testing.T) {
		_, err := ParseRegistryCredentials(envparser.ExecutorEnv{})
		assert.NoError(t, err)
	})

	t.Run("valid credentials", func(t *testing.T) {
		_, err := ParseRegistryCredentials(envparser.ExecutorEnv{
			StatefulEnv: envparser.StatefulEnv{
				RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
			},
		})
		assert.NoError(t, err)
	})

	t.Run("invalid pass", func(t *testing.T) {
		_, err := ParseRegistryCredentials(envparser.ExecutorEnv{
			StatefulEnv: envparser.StatefulEnv{
				RegistryCredentials: "ZXhhbXBsZS5jb20:pass",
			},
		})
		assert.Error(t, err)
	})

	t.Run("missing segment", func(t *testing.T) {
		_, err := ParseRegistryCredentials(envparser.ExecutorEnv{
			StatefulEnv: envparser.StatefulEnv{
				RegistryCredentials: "ZXhhbXBsZS5jb20:",
			},
		})
		assert.Error(t, err)
	})
}
