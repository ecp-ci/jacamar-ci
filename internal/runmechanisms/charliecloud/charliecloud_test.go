package charliecloud

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"os/user"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command/bash"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_command"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

const (
	regCreds = "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz"
)

type chTests struct {
	image  string
	stage  string
	tarEnv map[string]string

	cmdr command.Commander
	env  envparser.ExecutorEnv
	gen  configure.General
	msg  logging.Messenger
	run  runmechanisms.Runner

	assertError  func(*testing.T, error)
	assertRunner func(*testing.T, runmechanisms.Runner)
	assertString func(*testing.T, string)
	assertSlice  func(*testing.T, []string)
}

// testSysLog is an empty logger, syslog tested via Pavilion2 primarily.
var testSysLog *logrus.Entry

func init() {
	ll := logrus.New()
	ll.Out = io.Discard
	testSysLog = logrus.NewEntry(ll)
}

func TestPrepSetup(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]chTests{
		"valid request with allowlist": {
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage: "registry.example.com/runner:version",
					ImageAllowlist: []string{
						`^registry.example.com\default/image:.*$`,
						`^registry.example.com\/group/.*$`,
					},
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName: "registry.example.com/group/test:latest",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().PipeOutput(gomock.Any()).Times(2)
				return m
			}(),
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stdout(gomock.Any()).AnyTimes()
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid request with allowlist": {
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage: "registry.example.com/runner:version",
					ImageAllowlist: []string{
						`^registry.example.com\default/image:.*$`,
						`^registry.example.com\/group/.*$`,
					},
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName: "registry.example.io/group/test:latest",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Error(gomock.Any()).Times(1)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "image not allowed")
			},
		},
		"missing runner image": {
			gen: configure.General{
				Executor:       "shell",
				ForceMechanism: true,
				Charliecloud:   configure.Charliecloud{},
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "runner_image")
			},
		},
		"no default image": {
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Error(gomock.Any()).Times(1)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "missing user image")
			},
		},
		"invalid regex": {
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage: "registry.example.com/runner:version",
					ImageAllowlist: []string{
						`^registry.example.com\default/image:.*$`,
						`^test[1,`,
					},
					DefaultImage: "test:latest",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "error parsing regexp")
			},
		},
		"valid request no allowlist": {
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName: "registry.example.io/group/test:latest",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stdout(gomock.Any()).AnyTimes()
				return m
			}(),
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().PipeOutput(gomock.Any()).Times(2)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := PrepSetup(tt.env, tt.gen, tt.msg, tt.run)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewMechanism(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]chTests{
		"prepare_exec user messages": {
			stage: "prepare_exec",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage:        "registry.example.com/runner:version",
					UserVolumeVariable: "TEST_VOLUMES",
					StepScriptOnly:     true,
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Notify(gomock.Any()).AnyTimes()
				m.EXPECT().Stdout(gomock.Any(), gomock.Any()).AnyTimes()
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "basic.Mechanism", reflect.TypeOf(r).String())
			},
		},
		"cleanup_exec basic mechanism": {
			stage: "cleanup_exec",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "basic.Mechanism", reflect.TypeOf(r).String())
			},
		},
		"step_script only": {
			stage: "step_script",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage:    "registry.example.com/runner:version",
					StepScriptOnly: true,
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stdout(gomock.Eq("Charliecloud run command: %s"), gomock.Any()).AnyTimes()
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "charliecloud.mechanism", reflect.TypeOf(r).String())
			},
		},
		"step_script (build_script) only": {
			stage: "build_script",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage:    "registry.example.com/runner:version",
					StepScriptOnly: true,
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Stdout(gomock.Eq("Charliecloud run command: %s"), gomock.Any()).AnyTimes()
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "charliecloud.mechanism", reflect.TypeOf(r).String())
			},
		},
		"after_script step only": {
			stage: "cleanup_exec",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage:    "registry.example.com/runner:version",
					StepScriptOnly: true,
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "basic.Mechanism", reflect.TypeOf(r).String())
			},
		},
		"sigterm received": {
			stage: "after_script",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			cmdr: func() command.Commander {
				m := mock_command.NewMockCommander(ctrl)
				m.EXPECT().SigtermReceived().Return(true).Times(2)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				_, err := r.JobScriptReturn("test")
				assert.NoError(t, err, "JobScriptReturn")
				assert.NoError(t, r.JobScriptOutput("test"), "JobScriptReturn")
			},
		},
		"ch-run script errors": {
			stage: "after_script",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			cmdr: func() command.Commander {
				m := mock_command.NewMockCommander(ctrl)
				m.EXPECT().SigtermReceived().Return(false).Times(2)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				_, err := r.JobScriptReturn("")
				assert.Error(t, err, "JobScriptReturn")
				assert.Error(t, r.JobScriptOutput(""), "JobScriptReturn")
			},
		},
		"PrepScript error": {
			stage: "prepare_exec",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					ApplicationPath: "/usr/bin",
					RunnerImage:     "registry.example.com/runner:version",
					PrepScript:      t.TempDir() + "/missing.bash",
				},
			},
			cmdr: func() command.Commander {
				m := mock_command.NewMockCommander(ctrl)
				m.EXPECT().ReturnOutput(gomock.Any()).Return("script error", errors.New("error message"))
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "failed to run PrepScript")
			},
		},
		"get_sources no image": {
			stage: "get_sources",
			gen: configure.General{
				Executor:     "shell",
				RunMechanism: "charliecloud",
				Charliecloud: configure.Charliecloud{},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "basic.Mechanism", reflect.TypeOf(r).String())
			},
		},
		"get_sources runner image": {
			stage: "get_sources",
			gen: configure.General{
				Executor:     "shell",
				RunMechanism: "charliecloud",
				Charliecloud: configure.Charliecloud{
					RunnerImage: "registry.example.com/runner:version",
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "charliecloud.mechanism", reflect.TypeOf(r).String())
			},
		},
		"PrepScript inline": {
			stage: "prepare_exec",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					ApplicationPath: "/usr/bin",
					RunnerImage:     "registry.example.com/runner:version",
					PrepScript: fmt.Sprintf(
						"#!/bin/bash\ntouch %s\ndate",
						t.TempDir()+"/test",
					),
				},
			},
			cmdr: func() command.Commander {
				absCmdr := command.NewAbsCmdr(
					context.TODO(),
					configure.General{
						KillTimeout: "5s",
					},
					logging.NewMessenger(),
					envparser.ExecutorEnv{},
					testSysLog,
				)

				cur, _ := user.Current()

				return (bash.Factory{
					AbsCmdr: absCmdr,
					Cfg:     configure.Config{},
				}).CreateBaseShell(cur.HomeDir)
			}(),
			msg: logging.NewMessenger(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "basic.Mechanism", reflect.TypeOf(r).String())
			},
		},
		"after_script skipped": {
			stage: "get_sources",
			gen: configure.General{
				Executor: "shell",
				Charliecloud: configure.Charliecloud{
					RunnerImage:    "registry.example.com/runner:version",
					StepScriptOnly: true,
				},
			},
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertRunner: func(t *testing.T, r runmechanisms.Runner) {
				assert.Equal(t, "basic.Mechanism", reflect.TypeOf(r).String())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.env.StatefulEnv.ScriptDir != "" {
				_ = os.MkdirAll(tt.env.StatefulEnv.ScriptDir, 0700)
			}

			got, err := NewMechanism(
				tt.cmdr,
				tt.env,
				tt.gen,
				tt.msg,
				tt.stage,
			)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertRunner != nil {
				tt.assertRunner(t, got)
			}
			if tt.assertString != nil {
				tt.assertString(t, tt.env.StatefulEnv.ScriptDir)
			}
		})
	}
}
