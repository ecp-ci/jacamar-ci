package charliecloud

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_mechanism_chRunScript(t *testing.T) {
	t.Run("handle missing script", func(t *testing.T) {
		m := mechanism{}
		_, err := m.chRunScript("")

		assert.ErrorContains(t, err, "missing target")
	})

	t.Run("generate script", func(t *testing.T) {
		m := mechanism{
			internal: internal{
				shell:     "/bin/bash",
				scriptDir: t.TempDir(),
				chRun:     "ch-run --write-fake test:latest --",
				stage:     "step_script",
			},
		}
		script := t.TempDir() + "/job-script.sh"
		got, err := m.chRunScript(script)

		assert.NoError(t, err, "chRunScript")

		b, fErr := os.ReadFile(got)
		assert.NoError(t, fErr, "reading generate script")
		assert.Contains(t, string(b), "/job-script.sh")
	})
}
