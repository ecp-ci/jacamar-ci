package charliecloud

import (
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

func Test_authVariables(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]chTests{
		"credentials error": {
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: "testing",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Empty(t, s)
			},
		},
		"usable credentials, no match": {
			image: "registry.gitlab.com/ecp-ci/jacamar-ci/suse-builder:latest",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: regCreds,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Empty(t, s)
			},
		},
		"usable credentials, matching": {
			image: "reg.example.com/test:latest",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: regCreds,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Contains(t, s, fmt.Sprintf("%s='test'", authUserEnv))
				assert.Contains(t, s, fmt.Sprintf("%s='pass'", authPassEnv))
			},
		},
		"user provided credentials": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + authPassEnv: "password",
				envkeys.UserEnvPrefix + authUserEnv: "username",
			},
			image: "reg.example.com/test:latest",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: regCreds,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Contains(t, s, fmt.Sprintf("%s='username'", authUserEnv))
				assert.Contains(t, s, fmt.Sprintf("%s='password'", authPassEnv))
			},
		},
		"user provided invalid credentials": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + authPassEnv: "password",
				envkeys.UserEnvPrefix + authUserEnv: "username\"",
			},
			image: "reg.example.com/test:latest",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					RegistryCredentials: regCreds,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Contains(t, s, fmt.Sprintf("%s=", authUserEnv))
				assert.Contains(t, s, fmt.Sprintf("%s='password'", authPassEnv))
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.tarEnv {
				t.Setenv(k, v)
			}

			got, err := authVariables(tt.env, tt.image, false)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertSlice != nil {
				tt.assertSlice(t, got)
			}
		})
	}
}
