package charliecloud

import (
	"fmt"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

const (
	authUserEnv = "CH_IMAGE_USERNAME"
	authPassEnv = "CH_IMAGE_PASSWORD"
)

func authVariables(env envparser.ExecutorEnv, image string, runnerImg bool) ([]string, error) {
	if !runnerImg {
		pass, pFound, _ := envparser.GetUserVar(authPassEnv, "")
		user, uFound, _ := envparser.GetUserVar(authUserEnv, "")
		if pFound && uFound {
			return []string{
				fmt.Sprintf("%s=%s", authPassEnv, prepVar(pass)),
				fmt.Sprintf("%s=%s", authUserEnv, prepVar(user)),
			}, nil
		}
	}

	afc, err := runmechanisms.ParseRegistryCredentials(env)
	if err != nil {
		return []string{}, err
	}

	for k, v := range afc.Credentials {
		if strings.HasPrefix(image, k+"/") {
			return []string{
				fmt.Sprintf("%s='%s'", authPassEnv, v.Password),
				fmt.Sprintf("%s='%s'", authUserEnv, v.Username),
			}, nil
		}
	}

	return []string{}, nil
}

func prepVar(v string) string {
	// In this case an error will not result in an invalid response and
	// the error will surface itself in other ways more beneficial ways
	// during the job.
	s, _ := augmenter.ConstructUserArgs(v)
	return strings.TrimSpace(s)
}
