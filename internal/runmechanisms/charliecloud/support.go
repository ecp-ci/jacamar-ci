package charliecloud

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	imgStoreEnv = "CH_IMAGE_STORAGE"
	userArgKey  = "JACAMAR_CI_CHARLIECLOUD_ARGS"

	bindOpt = "--bind"
)

func (m mechanism) chRunScript(script string) (string, error) {
	if script == "" {
		return "", errors.New("chRunScript missing target filename")
	}

	var sb strings.Builder
	sb.WriteString("#!" + m.shell + "\n\nset -eo pipefail\nset +o noclobber\n")
	for _, v := range m.defEnv {
		sb.WriteString(fmt.Sprintf("export %s\n", v))
	}
	sb.WriteString(m.chRun)
	sb.WriteString(" ")
	sb.WriteString(script)
	sb.WriteString("\nexit 0\n")

	filename := fmt.Sprintf("%s/%v-charliecloud.bash", m.scriptDir, m.stage)
	err := usertools.CreateScript(filename, sb.String())

	return filename, err
}

//

func defaultEnv(env envparser.ExecutorEnv, gen configure.General) (s []string) {
	if gen.Charliecloud.ImageStorageDir != "" {
		s = append(s, fmt.Sprintf(
			"%s=%s/%s.ch",
			imgStoreEnv,
			strings.TrimRight(gen.Charliecloud.ImageStorageDir, "/"),
			env.StatefulEnv.Username,
		))
	}

	return
}

func buildChRun(
	gen configure.General,
	env envparser.ExecutorEnv,
	msg logging.Messenger,
	image, stage string,
) string {
	var sb strings.Builder

	sb.WriteString(chApplication("ch-run", gen))
	sb.WriteString(" --write-fake")
	sb.WriteString(buildsMountOpt(env))
	sb.WriteString(cacheMountOpt(env))
	sb.WriteString(scriptMountOpt(env))

	if !gen.Charliecloud.DisableUserArgs {
		proposedArgs := os.Getenv(envkeys.UserEnvPrefix + userArgKey)
		userArgs, err := augmenter.ConstructUserArgs(proposedArgs)
		if err != nil {
			msg.Warn(
				"Error encountered parsing %s value: %s",
				userArgKey,
				err.Error(),
			)
		} else {
			sb.WriteString(userArgs)
		}
	}

	sb.WriteString(image)
	if runmechanisms.StepScriptStage(stage) {
		msg.Stdout("Charliecloud run command: %s", sb.String())
	}
	sb.WriteString(" -- ")

	return prepStdin(sb.String(), msg)
}

func chApplication(tarApp string, gen configure.General) string {
	if gen.Charliecloud.ApplicationPath != "" {
		path := filepath.Clean(strings.TrimSpace(gen.Charliecloud.ApplicationPath))
		f, _ := os.Stat(path)
		// We may not be able to verify the file path exists. It is trusted
		// that when that occurs the admin configuration is accurate.
		if f != nil && f.IsDir() {
			if !strings.HasSuffix(gen.Charliecloud.ApplicationPath, "/") {
				path += "/"
			}
			path += tarApp
		}

		return path
	}

	path, err := exec.LookPath(tarApp)
	if err == nil {
		path, err = filepath.Abs(path)
		if err == nil {
			return path
		}
	}

	// Fallback to the default location for most deployments.
	return "/usr/bin/" + tarApp
}

func userNotification(
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	stage string,
) {
	msg.Notify("Charliecloud support enabled")
	msg.Stdout("Target Image: %s", userImage(gen, env, stage))
	if gen.Charliecloud.RunnerImage != "" {
		msg.Stdout("Runner Image: %s", gen.Charliecloud.RunnerImage)
	}

	if gen.Charliecloud.StepScriptOnly {
		msg.Stdout("Utilization limited to step_script/build_script only")
	}
	if gen.Charliecloud.UserVolumeVariable != "" {
		msg.Stdout(
			"User volume mounting supported with %s* variables",
			gen.Charliecloud.UserVolumeVariable,
		)
	}
}

func buildsMountOpt(env envparser.ExecutorEnv) string {
	return fmt.Sprintf(" %s=%s", bindOpt, env.StatefulEnv.BuildsDir)
}

func cacheMountOpt(env envparser.ExecutorEnv) string {
	return fmt.Sprintf(" %s=%s", bindOpt, env.StatefulEnv.CacheDir+"/"+env.StatefulEnv.ProjectPath)
}

func scriptMountOpt(env envparser.ExecutorEnv) string {
	return fmt.Sprintf(" %s=%s", bindOpt, env.StatefulEnv.ScriptDir)
}

func prepStdin(stdin string, msg logging.Messenger) string {
	s, err := augmenter.ConstructUserArgs(stdin)
	if err != nil {
		// By this point we've already parsed user provided arguments
		// so this error likely (but not always) indicate something is
		// wrong internally and not that the user's job is invalid.
		msg.Warn(
			"Internal error encountered constructing command input: %s",
			err.Error(),
		)
		return ""
	}

	return s
}
