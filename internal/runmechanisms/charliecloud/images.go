package charliecloud

import (
	"fmt"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

func pullImages(
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	run runmechanisms.Runner,
	stage string,
) error {
	if gen.Charliecloud.RunnerImage != "" {
		if err := execPull(
			env,
			gen,
			msg,
			run,
			gen.Charliecloud.RunnerImage,
			gen.Charliecloud.RunnerPullPolicy,
			true,
		); err != nil {
			return fmt.Errorf("unable to pull runner (%s): %w", gen.Charliecloud.RunnerImage, err)
		}
	}

	usrImage := userImage(gen, env, stage)
	if err := execPull(env, gen, msg, run, usrImage, env.PullPolicy, false); err != nil {
		return fmt.Errorf("unable to pull user image (%s): %w", usrImage, err)
	}

	return execConvert(gen, env, msg, run, usrImage, true)
}

func execPull(
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	run runmechanisms.Runner,
	image, policy string,
	runnerImg bool,
) error {
	if policy == "never" {
		return nil
	}

	if policy == "if-not-present" {
		var stdin strings.Builder
		for _, v := range defaultEnv(env, gen) {
			stdin.WriteString(fmt.Sprintf("%s ", v))
		}
		stdin.WriteString(chApplication("ch-image", gen))
		stdin.WriteString(" list | grep ")
		stdin.WriteString(prepStdin(image, msg))

		_, err := run.ReturnOutput(stdin.String())
		if err == nil {
			return nil
		}
	}

	stdin := buildChPull(env, gen, msg, image, runnerImg)
	return run.PipeOutput(stdin)
}

//

func buildChPull(
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	image string,
	runnerImg bool,
) string {
	authEnv, err := authVariables(env, image, runnerImg)
	if err != nil {
		msg.Warn("Unable to establish auth variables: %s", err.Error())
	}

	var sb strings.Builder
	sb.WriteString(chApplication("ch-image", gen))
	sb.WriteString(" pull")
	if len(authEnv) > 0 {
		sb.WriteString(" --auth")
	}
	sb.WriteString(fmt.Sprintf(" %s", image))

	var stdin strings.Builder
	for _, v := range append(defaultEnv(env, gen), authEnv...) {
		stdin.WriteString(fmt.Sprintf("%s ", v))
	}
	stdin.WriteString(prepStdin(sb.String(), msg))

	return stdin.String()
}

func execConvert(
	gen configure.General,
	env envparser.ExecutorEnv,
	msg logging.Messenger,
	run runmechanisms.Runner,
	image string,
	userImage bool,
) error {
	if convertDisabled(gen) {
		return nil
	}

	var sb strings.Builder
	sb.WriteString(chApplication("ch-convert", gen))
	sb.WriteString(fmt.Sprintf(" %s", image))
	sb.WriteString(fmt.Sprintf(" %s", squashName(env, userImage)))

	var stdin strings.Builder
	for _, v := range defaultEnv(env, gen) {
		stdin.WriteString(fmt.Sprintf("%s ", v))
	}
	stdin.WriteString(prepStdin(sb.String(), msg))

	out, err := run.ReturnOutput(stdin.String())
	if err != nil {
		msg.Stderr(out)
		err = fmt.Errorf("unable to save image: %w", err)
	}

	return err
}

// convertDisabled identifies if based upon configuration if the image should
// be converted to SquashFS.
func convertDisabled(gen configure.General) bool {
	if gen.Charliecloud.DisableConvert {
		return true
	}

	switch strings.TrimSpace(strings.ToLower(gen.Executor)) {
	case "shell":
		return true
	}

	return false
}

// getImage for the appropriate stage based upon configuration.
func getImage(env envparser.ExecutorEnv, gen configure.General, stage string) string {
	if runmechanisms.UserRunStage(stage) {
		return userImage(gen, env, stage)
	}

	return gen.Charliecloud.RunnerImage
}

func userImage(gen configure.General, env envparser.ExecutorEnv, stage string) (img string) {
	if env.StatefulEnv.ImageName == "" {
		img = gen.Charliecloud.DefaultImage
	} else {
		img = env.StatefulEnv.ImageName
	}

	if stage == "prepare_exec" {
		// During prepare stages we only use the default image name to be used
		// when pulling/saving the image.
		return
	} else if convertDisabled(gen) {
		return
	}

	return squashName(env, runmechanisms.UserRunStage(stage))
}

func squashName(env envparser.ExecutorEnv, userImage bool) string {
	if userImage {
		return fmt.Sprintf("%s/image.sqfs", env.StatefulEnv.ScriptDir)
	}

	return fmt.Sprintf("%s/runner.sqfs", env.StatefulEnv.ScriptDir)
}
