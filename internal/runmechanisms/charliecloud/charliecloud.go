package charliecloud

import (
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/basic"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

type internal struct {
	cmdr command.Commander
	// stage currently of the custom executor.
	stage string
	// shell that should be used to running scripts.
	shell string
	// scriptDir fully qualified directory where all job scripts should be stored.
	scriptDir string
	// chRun prepared 'ch-run' command (without job script)
	chRun  string
	defEnv []string
}

type mechanism struct {
	internal
	basic.Mechanism
}

// PrepSetup provides basic validation for both the Jacamar CI configuration and user provided
// variables for the Charliecloud mechanism. It must be ran during prepare_exec stage in order to
// ensure that once utilized in a downscoped environments known errors are avoided. Users will
// be provided error messages (stderr) where appropriate.
func PrepSetup(
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	run runmechanisms.Runner,
) error {
	if gen.Charliecloud.RunnerImage == "" && gen.ForceMechanism {
		// We do not currently support a default runner registry/version.
		return errors.New("missing Charliecloud runner_image configuration")
	}

	if env.StatefulEnv.ImageName == "" && gen.Charliecloud.DefaultImage == "" {
		msg.Error("Please define an image for your CI job to continue.")
		return errors.New("missing user image with no default defined")
	}

	var imageAllowed bool
	if len(gen.Charliecloud.ImageAllowlist) == 0 {
		imageAllowed = true
	}

	for _, v := range gen.Charliecloud.ImageAllowlist {
		r, err := regexp.Compile(v)
		if err != nil {
			return err
		}

		if r.MatchString(env.StatefulEnv.ImageName) {
			imageAllowed = true
			break
		}
	}

	if !imageAllowed {
		msg.Error("Image defined is not allowed due to runner configuration.")
		return errors.New("user defined image not allowed")
	}

	return pullImages(env, gen, msg, run, "prepare_exec")
}

// NewMechanism generates an implementation of the Runner interface supporting the
// goal of launching all GitLab generated job scripts via the container runtime locally
// within the user's namespace.
func NewMechanism(
	cmdr command.Commander,
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	stage string,
) (runmechanisms.Runner, error) {
	if stage == "prepare_exec" {
		if gen.Charliecloud.PrepScript != "" {
			out, err := cmdr.ReturnOutput(gen.Charliecloud.PrepScript)
			if err != nil {
				return nil, fmt.Errorf(
					"failed to run PrepScript (%s): %s",
					gen.Charliecloud.PrepScript,
					out,
				)
			}
		}

		userNotification(env, gen, msg, stage)
		// We cannot pull/use images at this point in the stage as we rely upon the creation
		// of several standard directories to mount into the container. These are only
		// present after 'jacamar' has created them, after this point the 'PrepSetup' function
		// should be called to finalize setup.
		return basic.NewMechanism(cmdr), nil
	} else if stage == "cleanup_exec" {
		// Cleanup does not invoke runner generated scripts. Additionally, we cannot
		// rely upon a functional container in case of failures.
		return basic.NewMechanism(cmdr), nil
	}

	if !runmechanisms.UserRunStage(stage) && gen.Charliecloud.RunnerImage == "" && !gen.ForceMechanism {
		return basic.NewMechanism(cmdr), nil
	}

	if !runmechanisms.StepScriptStage(stage) && gen.Charliecloud.StepScriptOnly {
		return basic.NewMechanism(cmdr), nil
	}

	image := getImage(env, gen, stage)
	runCmd := buildChRun(gen, env, msg, image, stage)

	mech := mechanism{
		internal{
			cmdr:      cmdr,
			stage:     stage,
			shell:     command.IdentifyShell(gen),
			scriptDir: env.StatefulEnv.ScriptDir,
			chRun:     runCmd,
			defEnv:    defaultEnv(env, gen),
		},
		basic.NewMechanism(cmdr),
	}

	return mech, nil
}

//

func (m mechanism) JobScriptOutput(script string, stdin ...interface{}) error {
	if m.cmdr.SigtermReceived() {
		return nil
	}

	chScript, err := m.chRunScript(script)
	if err != nil {
		return err
	}

	return m.PipeOutput(m.prepRun(chScript, stdin...))
}

func (m mechanism) JobScriptReturn(script string, stdin ...interface{}) (string, error) {
	if m.cmdr.SigtermReceived() {
		return "", nil
	}

	chScript, err := m.chRunScript(script)
	if err != nil {
		return "", err
	}

	return m.ReturnOutput((m.prepRun(chScript, stdin...)))
}

//

func (m mechanism) prepRun(script string, stdin ...interface{}) string {
	var sb strings.Builder

	// Pre-append the stdin to the "ch-run ..." script since there will be cases
	// where this can be submitted to a supported scheduler.
	if !reflect.ValueOf(stdin).IsZero() {
		sb.WriteString(fmt.Sprint(stdin...))
		sb.WriteString(" ")
	}
	sb.WriteString(script)

	return sb.String()
}
