package charliecloud

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_pullImages(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := logging.NewMessenger()

	tests := map[string]chTests{
		"pull runner image with storage": {
			gen: configure.General{
				Charliecloud: configure.Charliecloud{
					ApplicationPath: "/usr/bin",
					RunnerImage:     "test:latest",
					ImageStorageDir: "/tmp",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: regCreds,
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().PipeOutput("CH_IMAGE_STORAGE=/tmp/.ch  '/usr/bin/ch-image' 'pull' 'test:latest' ").Return(errors.New("error message"))
				return m
			}(),
			stage: "prepare_exec",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "error message")
			},
		},
		"pull runner image with credentials ": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + authPassEnv: "custom_pass",
				envkeys.UserEnvPrefix + authUserEnv: "custom_user",
			},
			gen: configure.General{
				Charliecloud: configure.Charliecloud{
					ApplicationPath: "/usr/bin",
					RunnerImage:     "example.com/test:latest",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: regCreds,
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().PipeOutput(
					"CH_IMAGE_PASSWORD='pass' CH_IMAGE_USERNAME='user'  '/usr/bin/ch-image' 'pull' '--auth' 'example.com/test:latest' ",
				).Return(errors.New("error message"))
				return m
			}(),
			stage: "prepare_exec",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "error message")
			},
		},
		"successfully pull user image with custom credentials": {
			tarEnv: map[string]string{
				envkeys.UserEnvPrefix + authPassEnv: "custom_pass",
				envkeys.UserEnvPrefix + authUserEnv: "custom_user",
			},
			gen: configure.General{
				Charliecloud: configure.Charliecloud{
					ApplicationPath: "/usr/bin",
					DisableConvert:  true,
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: regCreds,
					ImageName:           "user:latest",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().PipeOutput("CH_IMAGE_PASSWORD='custom_pass' CH_IMAGE_USERNAME='custom_user'  '/usr/bin/ch-image' 'pull' '--auth' 'user:latest' ").Return(nil)
				return m
			}(),
			stage: "prepare_exec",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"never pull_policy": {
			gen: configure.General{
				Charliecloud: configure.Charliecloud{
					ApplicationPath: "/usr/bin",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: regCreds,
					ImageName:           "user:latest",
					PullPolicy:          "never",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().ReturnOutput(
					" '/usr/bin/ch-convert' 'user:latest' '/tmp/script/job/image.sqfs' ",
				)
				return m
			}(),
			stage: "prepare_exec",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"failed conversion": {
			gen: configure.General{
				Charliecloud: configure.Charliecloud{
					ApplicationPath: "/usr/bin",
					ImageStorageDir: "/tmp",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					Username:            "tester",
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: regCreds,
					ImageName:           "user:latest",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().PipeOutput(
					"CH_IMAGE_STORAGE=/tmp/tester.ch  '/usr/bin/ch-image' 'pull' 'user:latest' ",
				).Return(nil)
				m.EXPECT().ReturnOutput(
					"CH_IMAGE_STORAGE=/tmp/tester.ch  '/usr/bin/ch-convert' 'user:latest' '/tmp/script/job/image.sqfs' ",
				).Return("convert output", errors.New("convert error"))
				return m
			}(),
			stage: "prepare_exec",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "convert error")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.tarEnv {
				t.Setenv(k, v)
			}
			err := pullImages(tt.env, tt.gen, msg, tt.run, tt.stage)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
