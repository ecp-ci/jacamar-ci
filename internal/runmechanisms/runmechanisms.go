package runmechanisms

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

const (
	stepPrefix    = "step_"
	stepScript    = "step_script"
	buildScript   = "build_script"
	afterScript   = "after_script"
	maxFileOutput = 16384
)

// Runner implements a supported interface with a goal of allowing commands (provided
// in stdin form) to be executed by an underlying layer (i.e., Bash) remaining consistent
// across the entire job execution.
type Runner interface {
	// JobScriptOutput interacts the underlying mechanism to execute the (GitLab generated)
	// script, integrating the executor specified stdin. The expected script (file path)
	// is provided and the target execution layer as well as admin defined downscope is observed:
	// 		<Commander defined environment> $ <stdin> <runner command (optional)> <script>
	// It is not true in all cases that the Runner will interject specific commands that
	// relate to script execution. All output from the job script's execution will be piped
	// to stdout/stderr.
	JobScriptOutput(script string, stdin ...interface{}) error
	// JobScriptReturn functions almost identical to JobScriptOutput but returns all
	// stdout/stderr as a string. It is inadvisable to utilize this in long-running commands.
	JobScriptReturn(script string, stdin ...interface{}) (string, error)
	// PipeOutput executes the provided stdin. All output from the command is piped directly
	// to stdout/stderr. Bypasses any runner command and instead relies solely on the
	// underlying base/environment layer.
	PipeOutput(stdin string) error
	// ReturnOutput executes the provided stdin. All output from the command is returned as
	// a string. Bypasses any runner command and instead relies solely on the
	// underlying base/environment layer.
	ReturnOutput(stdin string) (string, error)
	// FileOutput operates similar to ReturnOutput with the exception the output from the stdin
	// is temporarily directed to the supplied filename. This is useful in cases where a mechanism
	// will generate a clean login shell and potentially pollute output.
	FileOutput(filename, stdin string) (string, error)
	// CommandDir changes the directory where command will be executed.
	CommandDir(dir string)
	// SigtermReceived returns a boolean based upon if SIGTERM has been captured.
	SigtermReceived() bool
	// RequestContext returns the commander context, used to identify if SIGTERM has been captured
	// and Jacamar must begin the shutdown process.
	RequestContext() context.Context
	// TransferScript leverages any underlying commander to provide the job script
	// contents via Base64 encoded environment variables. Related configuration options observed.
	TransferScript(path, stage string, env envparser.ExecutorEnv, opt configure.Options) error
	// ModifyCmd updates the underlying command with the named program with the given arguments.
	// Will be ignored in cases where downscoping is enabled.
	ModifyCmd(name string, arg ...string)
}

// UserRunStage identifies stages where a user defined image would be appropriate.
func UserRunStage(stage string) bool {
	return strings.HasPrefix(stage, stepPrefix) || stage == buildScript || stage == afterScript
}

// StepScriptStage identifies if the provides stage is the 'step_script' (e.g.,
// combination before_script + script).
func StepScriptStage(stage string) bool {
	return stage == stepScript || stage == buildScript
}

func FileOutput(filename, stdin string, cmdr command.Commander) (string, error) {
	_, err := cmdr.ReturnOutput(stdin + fmt.Sprintf("&> '%s'", filename))

	f, fileErr := os.Open(filepath.Clean(filename)) // #nosec G304
	if fileErr != nil {
		return "", err
	}
	defer f.Close()

	var sb strings.Builder
	reader := bufio.NewReader(f)
	for {
		r, size, err := reader.ReadRune()
		if err != nil || size > maxFileOutput {
			break
		}
		sb.WriteRune(r)
	}

	return sb.String(), err
}
