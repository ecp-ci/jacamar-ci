// Package runnerinit handles methods associated with initializing the correct RunMechanism
// based upon configuration, stage, and user settings.
package runnerinit

import (
	"fmt"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/basic"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/charliecloud"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/podman"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	defaultMech = ""
	podmanMech  = "podman"
	chMech      = "charliecloud"
)

// InitializeUserRunner correctly identifies the target run mechanism based upon configuration and user
// variables to be leveraged in downscoped job execution.
func InitializeUserRunner(
	cmdr command.Commander,
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	stage string,
) (runner runmechanisms.Runner, err error) {
	rm := idUserMech(env, gen)
	switch rm {
	case podmanMech:
		runner, err = podman.NewMechanism(cmdr, env, gen, msg, stage)
	case chMech:
		runner, err = charliecloud.NewMechanism(cmdr, env, gen, msg, stage)
	case defaultMech:
		runner = basic.NewMechanism(cmdr)
	default:
		err = fmt.Errorf(
			"unknown run mechanism '%s' proposed",
			rm,
		)
	}

	return
}

// PrepareRunner supports actions during the prepare_exec stage for non-standard RunMechanism that
// require additional validation/setup even if they are not directly being invoked.
func PrepareRunner(
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	run runmechanisms.Runner,
) (err error) {
	switch idUserMech(env, gen) {
	case podmanMech:
		err = podman.PrepSetup(env, gen, msg, run)
	case chMech:
		err = charliecloud.PrepSetup(env, gen, msg, run)
	}

	return
}

func idUserMech(env envparser.ExecutorEnv, gen configure.General) string {
	if gen.ForceMechanism {
		return gen.RunMechanism
	} else if gen.RunMechanism == defaultMech {
		return defaultMech
	} else if envparser.TrueEnvVar(envkeys.JacamarNoMechanism) {
		return defaultMech
	}

	// Defer to underlying implementations to identify if the proposed mechanism
	// will be used based upon the known known elements of the job context.
	switch gen.RunMechanism {
	case podmanMech:
		if checkProposeMechanism(env) {
			return podmanMech
		}
	case chMech:
		if checkProposeMechanism(env) {
			return chMech
		}
	}

	return defaultMech
}

// checkProposeMechanism identifies if the container option can be used as a
// run_mechanism based upon the established environment.
func checkProposeMechanism(env envparser.ExecutorEnv) bool {
	if env.StatefulEnv.ImageName != "" {
		return true
	}
	return env.JobResponse.Image.Name != ""
}
