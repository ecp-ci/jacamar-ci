package podman

import (
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/basic"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

type internal struct {
	// stage currently of the custom executor.
	stage string
	// shell that should be used to running scripts.
	shell string
	// scriptDir fully qualified directory where all job scripts should be stored.
	scriptDir string
	// podmanRun prepared 'podman run' command (without [COMMAND [ARG...]])
	podmanRun string
}

type mechanism struct {
	internal
	basic.Mechanism
}

// PrepSetup provides basic validation for both the Jacamar CI configuration and user provided
// variables for the Podman mechanism. It should be run during prepare_exec stage in order to
// ensure that once utilized in a downscoped environments known errors are avoided. Users will
// be provided errors messages where appropriate.
func PrepSetup(
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	run runmechanisms.Runner,
) error {
	if gen.Podman.RunnerImage == "" && gen.ForceMechanism {
		// We do not currently support a default runner registry/version.
		return errors.New("missing Podman runner_image configuration")
	}

	if env.StatefulEnv.ImageName == "" && gen.Podman.DefaultImage == "" {
		msg.Error("Please define an image for your CI job to continue.")
		return errors.New("missing user image with no default defined")
	}

	var imageAllowed bool
	if len(gen.Podman.ImageAllowlist) == 0 {
		imageAllowed = true
	}

	for _, v := range gen.Podman.ImageAllowlist {
		r, err := regexp.Compile(v)
		if err != nil {
			return err
		}

		if r.MatchString(env.StatefulEnv.ImageName) {
			imageAllowed = true
			break
		}
	}

	if !imageAllowed {
		msg.Error("Image defined is not allowed due to runner configuration.")
		return errors.New("user defined image not allowed")
	}

	if err := generateAuthFile(env); err != nil {
		return fmt.Errorf("failed to generate auth file: %w", err)
	}

	return pullImages(env, gen, msg, run, "prepare_exec")
}

// NewMechanism generates an implementation of the Runner interface supporting the
// goal of launching all GitLab generated job scripts via the container runtime locally
// within the user's namespace.
func NewMechanism(
	cmdr command.Commander,
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	stage string,
) (runmechanisms.Runner, error) {
	if stage == "prepare_exec" {
		if gen.Podman.PrepScript != "" {
			out, err := cmdr.ReturnOutput(gen.Podman.PrepScript)
			if err != nil {
				return nil, fmt.Errorf(
					"failed to run PrepScript (%s): %s",
					gen.Podman.PrepScript,
					out,
				)
			}
		}

		userNotification(env, gen, msg, stage)
		// We cannot pull/use images at this point in the stage as we rely upon the creation
		// of several standard directories to mount into the container. These are only
		// present after 'jacamar' has created them, after this point the 'PrepSetup' function
		// should be called to finalize setup.
		return basic.NewMechanism(cmdr), nil
	} else if stage == "cleanup_exec" {
		// Cleanup does not invoke runner generated scripts. Additionally, we cannot
		// rely upon a functional container in case of failures.
		return basic.NewMechanism(cmdr), nil
	}

	if !runmechanisms.UserRunStage(stage) && gen.Podman.RunnerImage == "" && !gen.ForceMechanism {
		return basic.NewMechanism(cmdr), nil
	}

	if !runmechanisms.StepScriptStage(stage) && gen.Podman.StepScriptOnly {
		return basic.NewMechanism(cmdr), nil
	}

	image := getImage(env, gen, stage)
	options := buildOptions(gen, env, stage)
	runCmd := buildPodmanRun(gen, env, msg, image, options, stage)

	return mechanism{
		internal{
			stage:     stage,
			shell:     command.IdentifyShell(gen),
			scriptDir: env.StatefulEnv.ScriptDir,
			podmanRun: runCmd,
		},
		basic.NewMechanism(cmdr),
	}, nil
}

func (m mechanism) JobScriptOutput(script string, stdin ...interface{}) error {
	if m.SigtermReceived() {
		// SIGTERM captured, don't launch new job scripts.
		return nil
	}

	podmanScript, err := m.podmanRunScript(script)
	if err != nil {
		return err
	}

	return m.PipeOutput(m.prepRun(podmanScript, stdin...))
}

func (m mechanism) JobScriptReturn(script string, stdin ...interface{}) (string, error) {
	if m.SigtermReceived() {
		// SIGTERM captured, don't launch new job scripts.
		return "", nil
	}

	podmanScript, err := m.podmanRunScript(script)
	if err != nil {
		return "", err
	}

	return m.ReturnOutput(m.prepRun(podmanScript, stdin...))
}

//

// podmanRunScript generates a Bash script that can be used to easily launch the generated Podman
// command and GitLab job script. This offers better support for job submission via any of the
// supported schedulers.
func (m mechanism) podmanRunScript(script string) (string, error) {
	if script == "" {
		return "", errors.New("podmanRunScript missing target filename")
	}

	var sb strings.Builder

	sb.WriteString("#!" + m.shell + "\n\nset -eo pipefail\nset +o noclobber\n")
	sb.WriteString(m.podmanRun)
	sb.WriteString(" ")
	sb.WriteString(script)
	sb.WriteString("\nexit 0\n")

	filename := fmt.Sprintf("%s/%v-podman.bash", m.scriptDir, m.stage)
	err := usertools.CreateScript(filename, sb.String())

	return filename, err
}

func (m mechanism) prepRun(script string, stdin ...interface{}) string {
	var sb strings.Builder

	// Pre-append the stdin to the "podman run ..." script as we will have cases where
	// this can be submitted to a supported scheduler.
	r := reflect.ValueOf(stdin)
	if !r.IsZero() {
		sb.WriteString(fmt.Sprint(stdin...))
		sb.WriteString(" ")
	}
	sb.WriteString(script)

	return sb.String()
}
