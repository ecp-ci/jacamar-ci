package podman

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	podmanApp   = "podman"
	authFileKey = "REGISTRY_AUTH_FILE"
	userArgKey  = "JACAMAR_CI_PODMAN_ARGS"

	authOpt       = "--authfile"
	entrypointOpt = "--entrypoint"
	hostnameOpt   = "--hostname"
	rmOpt         = "--rm"
	volumeOpt     = "--volume"
	terminalOpt   = "--tty"
)

// getImage for the appropriate stage based upon configuration.
func getImage(env envparser.ExecutorEnv, gen configure.General, stage string) string {
	if runmechanisms.UserRunStage(stage) {
		return userImage(gen, env, stage)
	}

	return gen.Podman.RunnerImage
}

// pullImages implements the required 'podman pull' for both user and runner images adhering
// to configuration requirements. Baring any errors users should only see the output from
// their job image being retrieved.
func pullImages(
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	run runmechanisms.Runner,
	stage string,
) error {
	if gen.Podman.RunnerImage != "" {
		out, err := execPull(
			gen,
			env,
			run,
			gen.Podman.RunnerImage,
			gen.Podman.RunnerPullPolicy,
			stage,
			false,
		)
		if err != nil {
			msg.Stderr(out)
			return fmt.Errorf("unable to pull runner image (%s): %w", gen.Podman.RunnerImage, err)
		}
	}

	usrImage := userImage(gen, env, stage)
	out, err := execPull(gen, env, run, usrImage, env.PullPolicy, stage, true)
	msg.Stdout(out)
	if err != nil {
		return fmt.Errorf("unable to pull user image (%s): %w", usrImage, err)
	}

	return execSave(gen, env, msg, run, usrImage)
}

func execPull(
	gen configure.General,
	env envparser.ExecutorEnv,
	run runmechanisms.Runner,
	image, policy, stage string,
	usrImage bool,
) (string, error) {
	// We need to translate the GitLab runner defined policies
	// to those Podman will observe when required.
	// https://docs.gitlab.com/ee/ci/yaml/#imagepull_policy
	// https://docs.podman.io/en/latest/markdown/podman-run.1.html#pull-policy
	if policy == "never" {
		return "", nil
	}

	if policy == "if-not-present" {
		_, err := run.ReturnOutput(buildPodmanExists(gen, image))
		if err == nil {
			return "", nil
		}
	}

	// Treat "always" as the default policy.
	return run.ReturnOutput(buildPodmanPull(gen, env, image, stage, usrImage))
}

func userImage(gen configure.General, env envparser.ExecutorEnv, stage string) (img string) {
	if env.StatefulEnv.ImageName == "" {
		img = gen.Podman.DefaultImage
	} else {
		img = env.StatefulEnv.ImageName
	}

	if archiveDisabled(gen) {
		return
	} else if stage == "prepare_exec" {
		// During prepare stages we only use the default image name to be used
		// when pulling/saving the image.
		return
	}

	return archiveImage(gen, env)
}

func userNotification(
	env envparser.ExecutorEnv,
	gen configure.General,
	msg logging.Messenger,
	stage string,
) {
	msg.Notify("Podman support enabled")
	msg.Stdout("Target Image: %s", userImage(gen, env, stage))
	if gen.Podman.RunnerImage != "" {
		msg.Stdout("Runner Image: %s", gen.Podman.RunnerImage)
	}

	if gen.Podman.StepScriptOnly {
		msg.Stdout("Utilization limited to step_script/build_script only")
	}
	if gen.Podman.UserVolumeVariable != "" {
		msg.Stdout(
			"User volume mounting supported with %s* variables",
			gen.Podman.UserVolumeVariable,
		)
	}
}

func buildOptions(
	gen configure.General,
	env envparser.ExecutorEnv,
	stage string,
) (options string) {
	var sharedOptions string

	if runmechanisms.UserRunStage(stage) {
		options = strings.Join(gen.Podman.CustomOptions, " ")
		if len(gen.Podman.CustomEntryPoint) > 0 {
			sharedOptions = identifyEntrypoint(gen.Podman.CustomEntryPoint)
		} else if env.StatefulEnv.ImageEntryPoint != "" {
			sharedOptions = identifyEntrypoint(strings.Split(env.StatefulEnv.ImageEntryPoint, ","))
		}
	} else {
		options = strings.Join(gen.Podman.RunnerOptions, " ")
		sharedOptions = identifyEntrypoint(gen.Podman.RunnerEntryPoint)
	}

	options += sharedOptions

	return
}

func identifyEntrypoint(cfg []string) string {
	if len(cfg) == 0 {
		return ""
	}

	var sb strings.Builder
	if len(cfg) == 1 {
		sb.WriteString(cfg[0])
	} else {
		sb.WriteString(fmt.Sprintf("[\"%s\"", cfg[0]))
		for _, v := range cfg[1:] {
			sb.WriteString(fmt.Sprintf(",\"%s\"", v))
		}
		sb.WriteString("]")
	}

	return fmt.Sprintf(" %s '%s'", entrypointOpt, sb.String())
}

func buildPodmanExists(gen configure.General, image string) string {
	var sb strings.Builder

	sb.WriteString(podmanApplication(gen))
	sb.WriteString(" image exists")
	sb.WriteString(fmt.Sprintf(" '%s'", image))

	return sb.String()
}

func buildPodmanPull(
	gen configure.General,
	env envparser.ExecutorEnv,
	image, stage string,
	usrImage bool,
) string {
	var sb strings.Builder

	sb.WriteString(podmanApplication(gen))
	sb.WriteString(" pull")
	sb.WriteString(authFileOpt(env, stage, usrImage))
	sb.WriteString(fmt.Sprintf(" '%s'", image))

	return sb.String()
}

func buildPodmanRun(
	gen configure.General,
	env envparser.ExecutorEnv,
	msg logging.Messenger,
	image, options, stage string,
) string {
	var sb strings.Builder

	sb.WriteString(podmanApplication(gen))
	sb.WriteString(" run")
	sb.WriteString(buildsMountOpt(gen, env))
	sb.WriteString(cacheMountOpt(gen, env))
	sb.WriteString(scriptMountOpt(gen, env))
	sb.WriteString(userDefVolumes(gen, msg))
	sb.WriteString(userDefHostname(msg))
	sb.WriteString(authFileOpt(env, stage, runmechanisms.UserRunStage(stage)))
	sb.WriteString(" " + options)
	// The process to pull the image has already occurred and should be locally available.
	sb.WriteString(" --pull never ")
	sb.WriteString(rmContainerOpt(gen))
	sb.WriteString(userDefArgs(gen, msg))
	sb.WriteString(fmt.Sprintf(" %s '%s'", terminalOpt, image))

	if runmechanisms.StepScriptStage(stage) {
		msg.Stdout("Podman run command: %s", sb.String())
	}

	return sb.String()
}

// podmanApplication returns the correct fully qualified path.
func podmanApplication(gen configure.General) string {
	if gen.Podman.ApplicationPath != "" {
		path := filepath.Clean(strings.TrimSpace(gen.Podman.ApplicationPath))
		f, _ := os.Stat(path)
		// We may not be able to verify the file path exists. It is trusted
		// that when that occurs the admin configuration is accurate.
		if f != nil && f.IsDir() {
			if !strings.HasSuffix(gen.Podman.ApplicationPath, "/") {
				path += "/"
			}
			path += podmanApp
		}

		return path
	}

	path, err := exec.LookPath(podmanApp)
	if err == nil {
		path, err = filepath.Abs(path)
		if err == nil {
			return path
		}
	}

	// Fallback to the default location for most deployments.
	return "/usr/bin/" + podmanApp
}

func buildsMountOpt(gen configure.General, env envparser.ExecutorEnv) string {
	return fmt.Sprintf(
		" %s '%s:%s%s'",
		volumeOpt,
		env.StatefulEnv.BuildsDir,
		env.StatefulEnv.BuildsDir,
		volLabel(gen, ":Z"),
	)
}

func cacheMountOpt(gen configure.General, env envparser.ExecutorEnv) string {
	cacheDir := env.StatefulEnv.CacheDir + "/" + env.StatefulEnv.ProjectPath
	return fmt.Sprintf(" %s '%s:%s%s'", volumeOpt, cacheDir, cacheDir, volLabel(gen, ":z"))
}

func scriptMountOpt(gen configure.General, env envparser.ExecutorEnv) string {
	return fmt.Sprintf(
		" %s '%s:%s%s'",
		volumeOpt,
		env.StatefulEnv.ScriptDir,
		env.StatefulEnv.ScriptDir,
		volLabel(gen, ":Z"),
	)
}

func volLabel(gen configure.General, proposedLabel string) string {
	if gen.Podman.VolumeLabels {
		return proposedLabel
	}
	return ""
}

func rmContainerOpt(gen configure.General) string {
	if gen.Podman.DisableContainerRemoval {
		return ""
	}
	return " " + rmOpt
}

func userDefVolumes(gen configure.General, msg logging.Messenger) string {
	vols, err := envparser.ContainerVolumes(gen.Podman.UserVolumeVariable)
	if err != nil {
		msg.Warn(err.Error())
	}

	var sb strings.Builder
	for _, v := range vols {
		sb.WriteString(fmt.Sprintf(" %s '%s'", volumeOpt, v))
	}

	return sb.String()
}

func userDefHostname(msg logging.Messenger) (s string) {
	val, found, err := envparser.ContainerHostname()
	if err != nil {
		// We need to notify the user of this scenario so they can make corrections to the variable.
		msg.Warn(err.Error())
	} else if found {
		// The ContainerHostname function has already verified the value and we need to support cases
		// where specific values will have variables that need expanded in the Bash script.
		s = fmt.Sprintf(" %s \"%s\"", hostnameOpt, val)
	}

	return
}

func userDefArgs(gen configure.General, msg logging.Messenger) string {
	if gen.Podman.DisableUserArgs {
		return ""
	}

	proposedArgs := os.Getenv(envkeys.UserEnvPrefix + userArgKey)
	userArgs, err := augmenter.ConstructUserArgs(proposedArgs)
	if err != nil {
		msg.Warn("Error encountered parsing %s value: %s", userArgKey, err.Error())
		return ""
	}

	return userArgs
}
