package podman

import (
	"errors"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_pullImages(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := logging.NewMessenger()

	tests := map[string]podmanTests{
		"fail to pull runner image": {
			gen: configure.General{
				Podman: configure.Podman{
					ApplicationPath: "/usr/bin/podman",
					RunnerImage:     "test:latest",
					ArchiveFormat:   "none",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().ReturnOutput("/usr/bin/podman pull --authfile '/tmp/script/job/auth.json' 'test:latest'").Return("podman runner_image error", errors.New("error message"))
				return m
			}(),
			stage: "prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "error message")
			},
		},
		"pull runner image": {
			gen: configure.General{
				Podman: configure.Podman{
					ApplicationPath: "/usr/bin/podman",
					RunnerImage:     "test:latest",
					ArchiveFormat:   "none",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ImageName:           "user:latest",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().ReturnOutput("/usr/bin/podman pull --authfile '/tmp/script/job/auth.json' 'test:latest'").Return("podman success", nil)
				m.EXPECT().ReturnOutput("/usr/bin/podman pull --authfile '/tmp/script/job/auth.json' 'user:latest'").Return("podman user_image error", errors.New("error message"))
				return m
			}(),
			stage: "prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "error message")
			},
		},
		"pull user image": {
			gen: configure.General{
				Podman: configure.Podman{
					ApplicationPath: "/usr/bin/podman",
					ArchiveFormat:   "none",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ImageName:           "user:latest",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().ReturnOutput("/usr/bin/podman pull --authfile '/tmp/script/job/auth.json' 'user:latest'").Return("podman user_image success", nil)
				return m
			}(),
			stage: "prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"never pull user image (shell)": {
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					ApplicationPath: "/usr/bin/podman",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ImageName:           "user:latest",
					PullPolicy:          "never",
				},
			},
			stage: "prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"user image present (shell)": {
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					ApplicationPath: "/usr/bin/podman",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ImageName:           "user:latest",
					PullPolicy:          "if-not-present",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().ReturnOutput("/usr/bin/podman image exists 'user:latest'").Return("", nil)
				return m
			}(),
			stage: "prepare_exec",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"user image present (flux)": {
			gen: configure.General{
				Executor: "flux",
				Podman: configure.Podman{
					ApplicationPath: "/usr/bin/podman",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir:           "/tmp/script/job",
					RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
					ImageName:           "user:latest",
					PullPolicy:          "if-not-present",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().ReturnOutput("/usr/bin/podman image exists 'user:latest'").Return("", nil)
				m.EXPECT().ReturnOutput("/usr/bin/podman save --quiet -o '/tmp/script/job/image' --format docker-archive 'user:latest'").Return("", nil)
				return m
			}(),
			stage: "prepare_exec",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := pullImages(tt.env, tt.gen, msg, tt.run, tt.stage)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_identifyEntrypoint(t *testing.T) {
	t.Run("default entrypoints", func(t *testing.T) {
		entrypoint := identifyEntrypoint([]string{})
		assert.Empty(t, entrypoint, "empty entrypoint")
	})

	t.Run("user-defined entrypoints", func(t *testing.T) {
		entrypoint := identifyEntrypoint([]string{
			"/bin/test",
			"--example",
			"one",
		})
		assert.Equal(t, ` --entrypoint '["/bin/test","--example","one"]'`, entrypoint)
	})
}

func Test_buildPodmanRun(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tmpAuthFile := t.TempDir() + "/auth.json"
	_ = os.WriteFile(tmpAuthFile, []byte{}, 0600)

	stdMsg := mock_logging.NewMockMessenger(ctrl)
	stdMsg.EXPECT().Stdout(gomock.Eq("Podman run command: %s"), gomock.Any()).AnyTimes()

	defEnv := envparser.ExecutorEnv{
		StatefulEnv: envparser.StatefulEnv{
			BuildsDir:           "/tmp/builds",
			CacheDir:            "/tmp/cache",
			ScriptDir:           "/tmp'script",
			RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
			PullPolicy:          "never",
		},
	}

	tests := map[string]podmanTests{
		"standard command": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			msg:   stdMsg,
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "registry.example.com/test:latest")
				assert.Contains(t, s, authOpt)
			},
		},
		"standard after_script command": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "after_script",
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "registry.example.com/test:latest")
				assert.Contains(t, s, authOpt)
			},
		},
		"user auth file": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			msg:   stdMsg,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + authFileKey: tmpAuthFile,
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, tmpAuthFile)
			},
		},
		"no rm options": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			msg:   stdMsg,
			gen: configure.General{
				Podman: configure.Podman{
					DisableContainerRemoval: true,
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.NotContains(t, s, rmOpt)
			},
		},
		"user volumes": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			msg:   stdMsg,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "VOLUMES_0": "/opt/example:/opt/example:Z",
				envkeys.UserEnvPrefix + "VOLUMES_1": "/opt/test_1:/opt/test_1",
			},
			gen: configure.General{
				Podman: configure.Podman{
					UserVolumeVariable: "VOLUMES",
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "--volume '/opt/test_1:/opt/test_1'")
				assert.Contains(t, s, "--volume '/opt/example:/opt/example:Z'")
			},
		},
		"volume labels": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			msg:   stdMsg,
			gen: configure.General{
				Podman: configure.Podman{
					VolumeLabels: true,
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "--volume '/tmp/builds:/tmp/builds:Z'")
				assert.Contains(t, s, "--volume '/tmp/cache/:/tmp/cache/:z'")
			},
		},
		"valid user defined hostname": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			msg:   stdMsg,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.JacamarHostname: "test",
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "--hostname \"test\"")
			},
		},
		"invalid user defined hostname": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.JacamarHostname: "!test",
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn(gomock.Eq("invalid hostname declared, verify JACAMAR_CI_HOSTNAME key (see: https://tools.ietf.org/html/rfc1123)"))
				m.EXPECT().Stdout(gomock.Eq("Podman run command: %s"), gomock.Any())
				return m
			}(),
			assertString: func(t *testing.T, s string) {
				assert.NotContains(t, s, "--hostname")
			},
		},
		"invalid user defined args": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + userArgKey: "--test='val",
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn(
					gomock.Eq("Error encountered parsing %s value: %s"),
					gomock.Eq(userArgKey),
					gomock.Eq("EOF found when expecting closing quote"),
				)
				m.EXPECT().Stdout(gomock.Eq("Podman run command: %s"), gomock.Any())
				return m
			}(),
			assertString: func(t *testing.T, s string) {
				assert.NotContains(t, s, "--test")
			},
		},
		"valid user defined args": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			msg:   stdMsg,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + userArgKey: "--test \"val\"",
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, " '--test' 'val' ")
			},
		},
		"user defined args disabled": {
			env:   defEnv,
			image: "registry.example.com/test:latest",
			stage: "step_script",
			msg:   stdMsg,
			gen: configure.General{
				Podman: configure.Podman{
					DisableUserArgs: true,
				},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + userArgKey: "--test \"val\"",
			},
			assertString: func(t *testing.T, s string) {
				assert.NotContains(t, s, " '--test' 'val' ")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got := buildPodmanRun(tt.gen, tt.env, tt.msg, tt.image, tt.options, tt.stage)

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func Test_podmanApplication(t *testing.T) {
	tmpDir := t.TempDir()
	tests := map[string]podmanTests{
		"admin application": {
			gen: configure.General{
				Podman: configure.Podman{
					ApplicationPath: tmpDir + "/example/podman",
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, s, tmpDir+"/example/podman")
			},
		},
		"admin path-only": {
			gen: configure.General{
				Podman: configure.Podman{
					ApplicationPath: tmpDir,
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, s, tmpDir+"/podman")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := podmanApplication(tt.gen)

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func Test_buildOptions(t *testing.T) {
	tests := map[string]podmanTests{
		"prefer configured entrypoint": {
			stage: "step_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageEntryPoint: "/opt/script.bash,-l",
				},
			},
			gen: configure.General{
				Podman: configure.Podman{
					CustomOptions: []string{"--volume=/example"},
					CustomEntryPoint: []string{
						"/bin/bash",
						"-l",
						"-c",
					},
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, s, `--volume=/example --entrypoint '["/bin/bash","-l","-c"]'`)
			},
		},
		"incorporate runner options": {
			stage: "get_sources",
			gen: configure.General{
				Podman: configure.Podman{
					RunnerOptions: []string{"--volume=/example"},
					RunnerEntryPoint: []string{
						"/bin/bash",
						"-l",
						"-c",
					},
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, s, `--volume=/example --entrypoint '["/bin/bash","-l","-c"]'`)
			},
		},
		"allow user entrypoint": {
			stage: "step_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageEntryPoint: "/opt/script.bash",
				},
			},
			gen: configure.General{
				Podman: configure.Podman{
					CustomOptions: []string{"--volume=/example"},
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, s, `--volume=/example --entrypoint '/opt/script.bash'`)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := buildOptions(tt.gen, tt.env, tt.stage)

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func Test_userImage(t *testing.T) {
	tests := map[string]podmanTests{
		"default image": {
			gen: configure.General{
				Podman: configure.Podman{
					DefaultImage:  "debian:latest",
					ArchiveFormat: "none",
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "debian:latest", s)
			},
		},
		"default image for shell": {
			gen: configure.General{
				Executor: "shell",
				Podman: configure.Podman{
					DefaultImage: "debian:latest",
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "debian:latest", s)
			},
		},
		"default image for flux": {
			gen: configure.General{
				Executor: "flux",
				Podman: configure.Podman{
					DefaultImage: "debian:latest",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: "/script",
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "docker-archive:/script/image", s)
			},
		},
		"user image": {
			gen: configure.General{
				Podman: configure.Podman{
					DefaultImage:  "debian:latest",
					ArchiveFormat: "none",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ImageName: "alpine:latest",
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "alpine:latest", s)
			},
		},
		"user image archived": {
			gen: configure.General{
				Podman: configure.Podman{
					DefaultImage:  "debian:latest",
					ArchiveFormat: "docker-archive",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: "/script",
					ImageName: "alpine:latest",
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "docker-archive:/script/image", s)
			},
		},
		"dir archive": {
			gen: configure.General{
				Podman: configure.Podman{
					DefaultImage:  "debian:latest",
					ArchiveFormat: "docker-dir",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: "/script",
					ImageName: "alpine:latest",
				},
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "dir:/script/image", s)
			},
		},
		"prepare_exec": {
			gen: configure.General{
				Podman: configure.Podman{
					DefaultImage:  "debian:latest",
					ArchiveFormat: "docker-archive",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: "/script",
					ImageName: "alpine:latest",
				},
			},
			stage: "prepare_exec",
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "alpine:latest", s)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := userImage(tt.gen, tt.env, tt.stage)

			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}
