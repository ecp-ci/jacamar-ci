package podman

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_execSave(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := logging.NewMessenger()

	tests := map[string]podmanTests{
		"archive disabled": {
			gen: configure.General{
				Podman: configure.Podman{
					ArchiveFormat: "none",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"fail to save image": {
			gen: configure.General{
				Podman: configure.Podman{
					ApplicationPath: "/usr/bin/podman",
					ArchiveFormat:   "docker-archive",
				},
			},
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: "/tmp/script/job",
					ImageName: "alpine:latest",
				},
			},
			run: func() runmechanisms.Runner {
				m := mock_runmechanisms.NewMockRunner(ctrl)
				m.EXPECT().ReturnOutput("/usr/bin/podman save --quiet -o '/tmp/script/job/image' --format docker-archive 'alpine:latest'").Return("output...", errors.New("error message"))
				return m
			}(),
			image: "alpine:latest",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := execSave(tt.gen, tt.env, msg, tt.run, tt.image)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
