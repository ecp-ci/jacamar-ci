package podman

import (
	"fmt"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	formatDockerArchive = "docker-archive"
	formatNone          = "none"
)

// execSave executes a crafted 'podman save ...' command targeting the supplied image.
// If the feature is enabled. The image will be saved to the job's script directory.
func execSave(
	gen configure.General,
	env envparser.ExecutorEnv,
	msg logging.Messenger,
	run runmechanisms.Runner,
	image string,
) error {
	if archiveDisabled(gen) {
		return nil
	}

	var sb strings.Builder

	sb.WriteString(podmanApplication(gen))
	sb.WriteString(" save --quiet")
	sb.WriteString(fmt.Sprintf(" -o '%s'", archivePath(env)))
	sb.WriteString(fmt.Sprintf(" --format %s", archiveFormat(gen)))
	sb.WriteString(fmt.Sprintf(" '%s'", image))

	out, err := run.ReturnOutput(sb.String())
	if err != nil {
		msg.Stderr(out)
		err = fmt.Errorf("unable to save image: %w", err)
	}

	return err
}

// archiveDisabled identifies if based upon configuration archiving has been disabled.
func archiveDisabled(gen configure.General) bool {
	return archiveFormat(gen) == formatNone
}

// archiveFormat identifies based upon configuration or best guess what format should be used
// when deciding to archive an image.
func archiveFormat(gen configure.General) string {
	if gen.Podman.ArchiveFormat != "" {
		return gen.Podman.ArchiveFormat
	}

	switch strings.TrimSpace(strings.ToLower(gen.Executor)) {
	case "shell":
		return formatNone
	}

	return formatDockerArchive
}

// archivePath returns the full path to the image archive.
func archivePath(env envparser.ExecutorEnv) string {
	return fmt.Sprintf("%s/image", env.StatefulEnv.ScriptDir)
}

// archiveImage returns an image name with proper transport defined.
func archiveImage(gen configure.General, env envparser.ExecutorEnv) string {
	format := archiveFormat(gen)
	if format == "docker-dir" {
		return fmt.Sprintf("dir:%s", archivePath(env))
	}

	return fmt.Sprintf("%s:%s", format, archivePath(env))
}
