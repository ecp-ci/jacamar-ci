// Package basic implements a non-interfering runner interface that
// redirects all input to the underlying commander for execution.
package basic

import (
	"context"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type Mechanism struct {
	cmdr command.Commander
}

// NewMechanism generates a basic Runner interface that will insert
// no influence over stdin/script provided.
func NewMechanism(cmdr command.Commander) Mechanism {
	return Mechanism{
		cmdr: cmdr,
	}
}

func (m Mechanism) JobScriptOutput(script string, stdin ...interface{}) error {
	if m.cmdr.SigtermReceived() {
		// SIGTERM captured, don't launch new job scripts.
		return nil
	}

	return m.PipeOutput(m.prepStdin(script, stdin...))
}

func (m Mechanism) JobScriptReturn(script string, stdin ...interface{}) (string, error) {
	if m.cmdr.SigtermReceived() {
		// SIGTERM captured, don't launch new job scripts.
		return "", nil
	}

	return m.ReturnOutput(m.prepStdin(script, stdin...))
}

func (m Mechanism) PipeOutput(stdin string) error {
	return m.cmdr.PipeOutput(stdin)
}

func (m Mechanism) ReturnOutput(stdin string) (string, error) {
	return m.cmdr.ReturnOutput(stdin)
}

func (m Mechanism) FileOutput(filename, stdin string) (string, error) {
	return runmechanisms.FileOutput(filename, stdin, m.cmdr)
}

func (m Mechanism) CommandDir(dir string) {
	m.cmdr.CommandDir(dir)
}

func (m Mechanism) SigtermReceived() bool {
	return m.cmdr.SigtermReceived()
}

func (m Mechanism) RequestContext() context.Context {
	return m.cmdr.RequestContext()
}

func (m Mechanism) TransferScript(
	path string,
	stage string,
	env envparser.ExecutorEnv,
	opt configure.Options,
) error {
	contents, err := augmenter.JobScript(path, stage, env, opt.General)
	if err != nil {
		return err
	}

	m.cmdr.AppendEnv(envparser.EstablishScriptEnv(contents, opt.Auth.MaxEnvChars))

	return nil
}

func (m Mechanism) ModifyCmd(name string, arg ...string) {
	m.cmdr.ModifyCmd(name, arg...)
}

//

func (m Mechanism) prepStdin(script string, stdin ...interface{}) string {
	var sb strings.Builder

	r := reflect.ValueOf(stdin)
	if !r.IsZero() {
		sb.WriteString(fmt.Sprint(stdin...))
		sb.WriteString(" ")
	}

	sb.WriteString(script)

	return sb.String()
}
