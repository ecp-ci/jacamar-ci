package ulimit

import (
	"syscall"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

func Test_UserManage(t *testing.T) {
	tests := map[string]struct {
		gen          configure.General
		assertLimits func(t *testing.T)
	}{
		"no changes": {
			gen: configure.General{},
		},
		"stack size": {
			gen: configure.General{
				SetStackSize: true,
			},
			assertLimits: func(t *testing.T) {
				rl := syscall.Rlimit{}
				err := syscall.Getrlimit(syscall.RLIMIT_STACK, &rl)
				assert.NoError(t, err, "Getrlimit")
				assert.Equal(t, rl.Cur, rl.Max, "cur == max")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			UserManage(tt.gen)

			if tt.assertLimits != nil {
				tt.assertLimits(t)
			}
		})
	}
}
