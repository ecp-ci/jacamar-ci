package ulimit

import (
	"fmt"
	"syscall"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

// UserManage enforces configuration require ulimit modification/management
// within userspace. This should be invoked in the `jacamar` application.
func UserManage(gen configure.General) error {
	if gen.SetStackSize {
		if err := setStack(); err != nil {
			return fmt.Errorf("failed to set RLIMIT_STACK: %w", err)
		}
	}

	return nil
}

func setStack() (err error) {
	rl := syscall.Rlimit{}
	if err = syscall.Getrlimit(syscall.RLIMIT_STACK, &rl); err != nil {
		return
	}

	if rl.Cur < rl.Max {
		rl.Cur = rl.Max
		err = syscall.Setrlimit(syscall.RLIMIT_STACK, &rl)
	}

	return
}
