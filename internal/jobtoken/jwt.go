package jobtoken

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

type JobJWT struct {
	claims gljobctx.Claims
	req    envparser.RequiredEnv
}

func establishJWT(
	stage string,
	opt configure.Options,
	env envparser.ExecutorEnv,
) (*JobJWT, error) {
	expDelay, err := identifyExpDelay(stage, opt.Auth.JWTExpDelay, env.StatefulEnv.Username)
	if err != nil {
		return nil, err
	}

	claims, err := gljobctx.Options{
		JobID:               env.RequiredEnv.JobID,
		TLSCAFile:           opt.General.TLSCAFile,
		ExpDelay:            expDelay,
		ClaimsEnvValidation: true,
		RequiredAudience:    opt.Auth.JWTRequiredAud,
	}.ValidateJWT(env.RequiredEnv.CIJobJWT, env.RequiredEnv.ServerURL)
	if err != nil {
		if errors.Is(err, gljobctx.ErrorJWKS) {
			// In this case we want to notify the user of the error to better
			// clarify why the job might be failing.
			logging.NewMessenger().Error(err.Error())
		}

		err = fmt.Errorf(
			"unable to parse supplied id_token: %w",
			err,
		)
	}

	return &JobJWT{
		claims: claims,
		req:    env.RequiredEnv,
	}, err
}

func (j *JobJWT) Environment() []string {
	return j.claims.Environment()
}

func (j *JobJWT) PipelineID() string {
	return j.claims.PipelineID
}

func (j *JobJWT) ProjectID() string {
	return j.claims.ProjectID
}

func (j *JobJWT) ProjectPath() string {
	return j.claims.ProjectPath
}

func (j *JobJWT) PipelineSource() string {
	return j.claims.PipelineSource
}

func (j *JobJWT) DebugMsg() string {
	return fmt.Sprintf("key JWT claims: %+v", j.claims)
}

func (j *JobJWT) UserLogin() string {
	return j.claims.UserLogin
}

func (j *JobJWT) UserIdentities() []map[string]string {
	return j.claims.UserIdentities
}

func identifyExpDelay(stage, expDelay, username string) (time.Duration, error) {
	if username == "" {
		// If there exists no username (indicating lack of stateful variables)
		// we do not allow an expired JWT.
		return 0, nil
	}

	// Stages are runner defined: https://docs.gitlab.com/runner/executors/custom.html#run
	// these are not the same as user defined pipeline stages (via .gitlab-ci.yml).
	supportedStages := map[string]bool{
		"archive_cache":               true,
		"archive_cache_on_failure":    true,
		"upload_artifacts_on_failure": true,
		"upload_artifacts_on_success": true,
		"cleanup_file_variables":      true,
		"cleanup_exec":                true,
	}

	// Treat both empty string and zero value as a desire to no allow any
	// expiration regardless of the stage.
	if expDelay != "" && expDelay != "0" && supportedStages[stage] {
		return time.ParseDuration(expDelay)
	}

	// Default to not allowing any delay.
	return 0, nil
}
