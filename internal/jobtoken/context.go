package jobtoken

import (
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type EstablishedContext interface {
	// Environment prepare key/value pairs to be appended to a command's environment variables.
	Environment() []string
	// ProjectID the unique identification number of the current CI pipeline.
	PipelineID() string
	// PipelineSource how the pipelines was triggered.
	PipelineSource() string
	// ProjectID the unique identification number of the current project.
	ProjectID() string
	// ProjectPath return the project namespace with the project name included.
	ProjectPath() string
	// DebugMsg returns the complete underlying interface for debug/logging purposes.
	DebugMsg() string
	// UserLogin returns the user login (username) as defined by the server.
	UserLogin() string
	// UserIdentities returns the list of user identities as defined by the server.
	UserIdentities() []map[string]string
}

type Validator interface {
	Run() (EstablishedContext, error)
}

type factory struct {
	stage string
	opt   configure.Options
	env   envparser.ExecutorEnv
}

func NewJob(
	stage string,
	opt configure.Options,
	env envparser.ExecutorEnv,
) Validator {
	return factory{
		stage: stage,
		opt:   opt,
		env:   env,
	}
}

func (f factory) Run() (EstablishedContext, error) {
	return establishJWT(f.stage, f.opt, f.env)
}
