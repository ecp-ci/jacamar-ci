// Package arguments manages the command line subcommands and shared arguments
// for the Jacamar CI applications. This structure is to support the multi-application
// requirements and custom executor defined interactions.
package arguments

// ConfigCmd define the arguments for config_exec stage.
type ConfigCmd struct {
	// Configuration for the runner, expected TOML file or environment key.
	Configuration string
}

// PrepareCmd defines the arguments for the prepare_exec stage.
type PrepareCmd struct{}

// RunCmd defines the arguments for the run_exec stage.
type RunCmd struct {
	// Script that the runner generates or environment key.
	Script string
	// Stage within run_exec.
	Stage string
}

// CleanupCmd defines the arguments for the cleanup_exec stage.
type CleanupCmd struct {
	// Configuration for the runner, expected TOML file or environment key.
	Configuration string
}

// SignalCmd defines the arguments for Jacamar application wrapper around
// a Linux kill(2) system call.
type SignalCmd struct {
	Signal string
	PID    string
}

// LockCmd defines arguments for userspace file locking utilized when
// attempting to claim an open builds_dir.
type LockCmd struct {
	ProposedDir string
	JobID       string
	// JobTimeout (in seconds) defined in job response.
	JobTimeout string
	// DirPermissions defined the folder permissions (e.g., 448 for 0700).
	DirPermissions string
	// Debug when enabled will write relevant output/errors to a file.
	Debug bool
	// Revalidate when enabled will wait shortly and then re-verify the lock
	// before accepting.
	Revalidate bool
}

// ReadCmd defines arguments for a userspace file to be read and
// returned to the parent process via stdout.
type ReadCmd struct {
	TarFile string
}

// ConcreteArgs organizes all supported subcommands.
type ConcreteArgs struct {
	Config  *ConfigCmd
	Prepare *PrepareCmd
	Run     *RunCmd
	Cleanup *CleanupCmd
	Signal  *SignalCmd
	Lock    *LockCmd
	Read    *ReadCmd
	// UnobfuscatedError removes the default obfuscation from all errors.
	UnobfuscatedError bool
	// NoAuth skip all authorization level functionality.
	NoAuth bool
}
