package arguments

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	ver "gitlab.com/ecp-ci/jacamar-ci/internal/version"
)

const (
	usage = "Usage: %s [options] <command> [args]\nOptions:\n"

	verOptHelp = "Display version and exit."
	hlpOptHelp = "Display help and exit."
	obfOptHelp = "Allow all error messages in job log."
	usrOptHelp = "Skip authorization in user application."
	subCmdHelp = `
Commands:
  config   Execute configuration stage (config_exec)
  prepare  Execute preparation stage (prepare_exec)
  run      Execute run stage (run_exec)
  cleanup  Execute cleanup stage (cleanup_exec)
`
	missingSub = "missing expected subcommand {'config', 'prepare', 'run', 'cleanup'}"
)

type argumenter struct {
	helpOpt, obfOpt, usrOpt, verOpt bool
	auth                            bool
	name                            string
	errExit, zeroExit               func()
	nonFlagArgs                     []string
}

type fl struct {
	long, short, usage string
	target             *bool
}

// Resolve handles the limited scope of command line arguments for both jacamar-auth and
// jacamar applications. Any error or completion encountered will result in the
// appropriate exit status.
func Resolve(auth bool) ConcreteArgs {
	a := argumenter{
		auth:     auth,
		name:     programName(auth, os.Args),
		errExit:  func() { os.Exit(1) },
		zeroExit: func() { os.Exit(0) },
	}

	for _, val := range initFlags(&a) {
		flag.BoolVar(val.target, val.long, false, val.usage)
		flag.BoolVar(val.target, val.short, false, val.long)
	}
	flag.Parse()

	a.nonFlagArgs = flag.Args()

	return a.parse()
}

//

// initFlags generate all potential flags to be observed.
func initFlags(a *argumenter) []fl {
	f := []fl{
		{"version", "v", verOptHelp, &a.verOpt},
		{"help", "h", hlpOptHelp, &a.helpOpt},
	}

	if a.auth {
		f = append(f, fl{"unobfuscated", "u", obfOptHelp, &a.obfOpt})
	} else {
		f = append(f, fl{"no-auth", "n", usrOptHelp, &a.usrOpt})
	}

	return f
}

// parse a fully realized argumenter to either generate a valid ConcreteArgs structure
// or act as defined upon arguments.
func (a argumenter) parse() ConcreteArgs {
	if a.helpOpt {
		fmt.Print(buildHelp(a.auth, a.name))
		a.zeroExit()
		return ConcreteArgs{}
	} else if a.verOpt {
		fmt.Print(ver.Obtain())
		a.zeroExit()
		return ConcreteArgs{}
	}

	c := ConcreteArgs{
		UnobfuscatedError: a.obfOpt,
		NoAuth:            a.usrOpt,
	}

	if errMsg := a.subCommands(&c); errMsg != "" {
		fmt.Print(errMsg)
		a.errExit()
		return ConcreteArgs{}
	}

	return c
}

func (a argumenter) subCommands(c *ConcreteArgs) string {
	if len(a.nonFlagArgs) < 1 {
		return buildHelp(a.auth, a.name)
	}

	var msg string

	switch a.nonFlagArgs[0] {
	case "config":
		a.configExec(c)
	case "prepare":
		msg = a.prepareExec(c)
	case "run":
		msg = a.runExec(c)
	case "cleanup":
		a.cleanupExec(c)
	case "signal":
		msg = a.signalCmd(c)
	case "lock":
		msg = a.lockCmd(c)
	case "read":
		msg = a.readCmd(c)
	default:
		return missingSub
	}

	return msg
}

// buildHelp generates the general output to be used as a replacement for the
// system package's default output.
func buildHelp(auth bool, name string) string {
	var sb strings.Builder

	sb.WriteString(fmt.Sprintf(usage, name))

	if auth {
		sb.WriteString(fmt.Sprintf("  --unobfuscated, -u\t%s\n", obfOptHelp))
	} else {
		sb.WriteString(fmt.Sprintf("  --no-auth, -n\t%s\n", usrOptHelp))
	}

	sb.WriteString(fmt.Sprintf("  --help, -h\t%s\n", hlpOptHelp))
	sb.WriteString(fmt.Sprintf("  --version, -v\t%s\n", verOptHelp))
	sb.WriteString(subCmdHelp)

	if !auth {
		sb.WriteString("  signal   Wrapper to send signal to target process\n")
		sb.WriteString("  lock     Manage locking files to limited build directories\n")
		sb.WriteString("  read     Programmatically ingest target file and output to stdout.\n")
	}

	return sb.String()
}

func programName(auth bool, args []string) (name string) {
	if len(args) > 0 {
		name = filepath.Base(args[0])
	}

	if name == "" && auth {
		return "jacamar-auth"
	} else if name == "" && !auth {
		return "jacamar"
	}

	return
}

//

func (a argumenter) configExec(c *ConcreteArgs) {
	var cfgFile string

	configCmd := flag.NewFlagSet("config", flag.ExitOnError)
	configCmd.StringVar(&cfgFile, "configuration", "", "Target configuration file/variable.")

	_ = configCmd.Parse(a.nonFlagArgs[1:])
	c.Config = &ConfigCmd{
		Configuration: cfgFile,
	}
}

func (a argumenter) prepareExec(c *ConcreteArgs) string {
	prepareCmd := flag.NewFlagSet("prepare", flag.ContinueOnError)
	prepHelp := prepareCmd.Bool("help", false, hlpOptHelp)
	prepareCmd.BoolVar(prepHelp, "h", false, "help")

	_ = prepareCmd.Parse(a.nonFlagArgs[1:])
	if *prepHelp {
		return fmt.Sprintf("Usage: %s [options] prepare\n", a.name)
	}
	c.Prepare = &PrepareCmd{}

	return ""
}

func (a argumenter) runExec(c *ConcreteArgs) string {
	runCmd := flag.NewFlagSet("run", flag.ContinueOnError)
	runHelp := runCmd.Bool("help", false, hlpOptHelp)
	runCmd.BoolVar(runHelp, "h", false, "help")

	_ = runCmd.Parse(a.nonFlagArgs[1:])
	if runCmd.NArg() < 2 || *runHelp {
		return fmt.Sprintf("Usage: %s [options] run SCRIPT STAGE\n", a.name)
	}
	c.Run = &RunCmd{
		Script: runCmd.Arg(0),
		Stage:  runCmd.Arg(1),
	}

	return ""
}

func (a argumenter) cleanupExec(c *ConcreteArgs) {
	var cfgFile string

	cleanupCmd := flag.NewFlagSet("cleanup", flag.ExitOnError)
	cleanupCmd.StringVar(&cfgFile, "configuration", "", "Target configuration file/variable.")

	_ = cleanupCmd.Parse(a.nonFlagArgs[1:])
	c.Cleanup = &CleanupCmd{
		Configuration: cfgFile,
	}
}

func (a argumenter) signalCmd(c *ConcreteArgs) string {
	signalCmd := flag.NewFlagSet("signal", flag.ExitOnError)
	signalHelp := signalCmd.Bool("help", false, hlpOptHelp)
	signalCmd.BoolVar(signalHelp, "h", false, "help")

	if a.auth {
		return missingSub
	}
	_ = signalCmd.Parse(a.nonFlagArgs[1:])
	if signalCmd.NArg() < 2 || *signalHelp {
		return fmt.Sprintf("Usage: %s [options] signal SIGNAL PID\n", a.name)
	}
	c.Signal = &SignalCmd{
		Signal: signalCmd.Arg(0),
		PID:    signalCmd.Arg(1),
	}

	return ""
}

func (a argumenter) lockCmd(c *ConcreteArgs) string {
	lockCmd := flag.NewFlagSet("lock", flag.ExitOnError)
	lockHelp := lockCmd.Bool("help", false, hlpOptHelp)
	lockCmd.BoolVar(lockHelp, "h", false, "help")
	lockDebug := lockCmd.Bool("debug", false, "Debug logging file generated in proposed directory.")
	lockCmd.BoolVar(lockDebug, "d", false, "debug")
	lockReValidate := lockCmd.Bool("revalidate", false, "Attempt process to revalidate lock file(s).")
	lockCmd.BoolVar(lockReValidate, "r", false, "revalidate")

	if a.auth {
		return missingSub
	}
	_ = lockCmd.Parse(a.nonFlagArgs[1:])
	if lockCmd.NArg() < 3 || *lockHelp {
		return fmt.Sprintf(
			"Usage: %s [options] lock [-debug] PROPOSED_DIR ID TIMEOUT DIR_PERMISSIONS\n",
			a.name,
		)
	}
	c.Lock = &LockCmd{
		ProposedDir:    lockCmd.Arg(0),
		JobID:          lockCmd.Arg(1),
		JobTimeout:     lockCmd.Arg(2),
		DirPermissions: lockCmd.Arg(3),
		Debug:          *lockDebug,
		Revalidate:     *lockReValidate,
	}

	return ""
}

func (a argumenter) readCmd(c *ConcreteArgs) string {
	readCmd := flag.NewFlagSet("read", flag.ExitOnError)
	readHelp := readCmd.Bool("help", false, hlpOptHelp)
	readCmd.BoolVar(readHelp, "h", false, "help")

	if a.auth {
		return missingSub
	}
	_ = readCmd.Parse(a.nonFlagArgs[1:])
	if readCmd.NArg() < 1 || *readHelp {
		return fmt.Sprintf(
			"Usage: %s [options] read TARGET_FILE\n",
			a.name,
		)
	}
	c.Read = &ReadCmd{
		TarFile: readCmd.Arg(0),
	}

	return ""
}
