GO_VERSION ?= 1.23.5
GO_ARCH ?= amd64
GO_SHA ?= cbcad4a6482107c7c7926df1608106c189417163428200ce357695cc7e01d091
GO_URL ?= https://go.dev/dl/go${GO_VERSION}.linux-${GO_ARCH}.tar.gz

PAV_TAG ?= 60d55280bed2fb060626dea284068fca791fbed4
SLURM_VERSION ?= 21.08.6
PBS_VERSION ?= 20.0.1
FLUX_VERSION ?= 0.64.0
BCI_VERSION ?= 15.3
PODMAN_VERSION ?= 4.9.0

CI_REGISTRY=registry.gitlab.com/ecp-ci/jacamar-ci
export BUILD_IMG=${CI_REGISTRY}/centos7-builder:${GO_VERSION}
export PAVILION_IMG=${CI_REGISTRY}/pav-tester:${GO_VERSION}
export SLURM_IMG=${CI_REGISTRY}/slurm-tester:${SLURM_VERSION}
export PBS_IMG=${CI_REGISTRY}/pbs-tester:${PBS_VERSION}
export GO_IMG=docker.io/golang:${GO_VERSION}
export FLUX_IMG=${CI_REGISTRY}/flux-tester:${FLUX_VERSION}
export SUSE_IMG=${CI_REGISTRY}/suse-builder:${GO_VERSION}
export PODMAN_IMG=${CI_REGISTRY}/podman-tester:${PODMAN_VERSION}

ci_dir=/builds/ecp-ci/jacamar-ci

.PHONY: all-build-args
all-build-args:
	@echo "--build-arg GO_VERSION=${GO_VERSION} \
--build-arg GO_ARCH=${GO_ARCH} \
--build-arg GO_SHA=${GO_SHA} \
--build-arg GO_URL=${GO_URL} \
--build-arg PAV_TAG=${PAV_TAG} \
--build-arg PBS_VER=${PBS_VERSION} \
--build-arg FLUX_VERSION=${FLUX_VERSION} \
--build-arg SLURM_VERSION=${SLURM_VERSION} \
--build-arg BCI_VERSION=${BCI_VERSION} \
--build-arg PODMAN_VERSION=${PODMAN_VERSION}"

#
# rebuild/release images using Go
.PHONY: build-go-images
build-go-images:
	$(MAKE) build-centos7-builder
	$(MAKE) build-suse-builder
	$(MAKE) build-pav-tester

.PHONY: push-go-images
push-go-images:
	$(MAKE) push-centos7-builder
	$(MAKE) push-suse-builder
	$(MAKE) push-pav-tester

#
# build/containers/centos7-builder
.PHONY: build-centos7-builder
build-centos7-builder:
	${CONTAINER_RUNTIME} build \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		-t ${CI_REGISTRY}/centos7-builder:${GO_VERSION} \
		build/containers/centos7-builder

.PHONY: push-centos7-builder
push-centos7-builder:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/centos7-builder:${GO_VERSION}

.PHONY: run-centos7-builder
run-centos7-builder:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/centos7-builder:${GO_VERSION} /bin/bash


#
# build/containers/suse-builder
.PHONY: build-suse-builder
build-suse-builder:
	${CONTAINER_RUNTIME} build \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		--build-args BCI_VERSION=${BCI_VERSION} \
		-t ${SUSE_IMG} \
		build/containers/suse-builder

.PHONY: push-suse-builder
push-suse-builder:
	${CONTAINER_RUNTIME} push ${SUSE_IMG}

.PHONY: run-suse-builder
run-suse-builder:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${SUSE_IMG} /bin/bash

#
# build/containers/pbs-tester
.PHONY: build-pbs-tester
build-pbs-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg PBS_VER=${PBS_VERSION} \
		--build-arg PAV_TAG=${PAV_TAG} \
		-t ${PBS_IMG} \
		build/containers/pbs-tester

.PHONY: push-pbs-tester
push-pbs-tester:
	${CONTAINER_RUNTIME} push ${PBS_IMG}

.PHONY: run-pbs-tester
run-pbs-tester:
	${CONTAINER_RUNTIME} run -h buildkitsandbox \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		--cap-add=SYS_PTRACE \
		--cap-add=CAP_SYS_RESOURCE \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir} \
		-it ${PBS_IMG}

#
# build/containers/pav-tester
.PHONY: build-pav-tester
build-pav-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		--build-arg PAV_TAG=${PAV_TAG} \
		-t ${CI_REGISTRY}/pav-tester:${GO_VERSION} \
		build/containers/pav-tester

.PHONY: push-pav-tester
push-pav-tester:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/pav-tester:${GO_VERSION}

.PHONY: run-pav-tester
run-pav-tester:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/pav-tester:${GO_VERSION} /bin/bash

#
# build/containers/slurm-tester
.PHONY: build-slurm-tester
build-slurm-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg PAV_TAG=${PAV_TAG} \
		--build-arg SLURM_VERSION=${SLURM_VERSION} \
		-t ${CI_REGISTRY}/slurm-tester:${SLURM_VERSION} \
		build/containers/slurm-tester

.PHONY: push-slurm-tester
push-slurm-tester:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/slurm-tester:${SLURM_VERSION}

.PHONY: run-slurm-tester
run-slurm-tester:
	${CONTAINER_RUNTIME} run -h slurmctl \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		--cap-add=SYS_ADMIN \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/slurm-tester:${SLURM_VERSION}

#
# build/containers/flux-tester
.PHONY: build-flux-tester
build-flux-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg PAV_TAG=${PAV_TAG} \
		--build-arg FLUX_VERSION=${FLUX_VERSION} \
		-t ${CI_REGISTRY}/flux-tester:${FLUX_VERSION} \
		build/containers/flux-tester

.PHONY: push-flux-tester
push-flux-tester:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/flux-tester:${FLUX_VERSION}

.PHONY: run-flux-tester
run-flux-tester:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/flux-tester:${FLUX_VERSION}

#
# build/containers/podman-tester
.PHONY: build-podman-tester
build-podman-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg PODMAN_VERSION=${PODMAN_VERSION} \
  		--build-arg PAV_TAG=${PAV_TAG} \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		-t ${CI_REGISTRY}/podman-tester:${PODMAN_VERSION} \
		build/containers/podman-tester

.PHONY: latest-podman-tester
latest-podman-tester:
	${CONTAINER_RUNTIME} tag \
		${CI_REGISTRY}/podman-tester:${GO_VERSION} \
		${CI_REGISTRY}/podman-tester:latest
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/podman-tester:latest

.PHONY: run-podman-tester
run-podman-tester:
	${CONTAINER_RUNTIME} run \
		--rm \
		--privileged \
		-v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/podman-tester:${PODMAN_VERSION} /bin/bash
