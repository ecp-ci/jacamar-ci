%{!?_version: %define _version 0.10.1 }
%{!?_opt_prefix: %define _opt_prefix /opt }

# https://github.com/rpm-software-management/rpm/blob/master/macros.in
# build_id links are generated only when the __debug_package global is defined.
%define _build_id_links alldebug
%undefine _missing_build_ids_terminate_build

%global jac %{_opt_prefix}/jacamar

Name:           jacamar-ci
Version:        %{_version}
Release:        1%{?dist}
Summary:        HPC focused CI/CD driver for the GitLab custom executor
Vendor:         Exascale Computing Project - 2.4.4.04
Packager:       ECP CI Infrastructure <ecp-ci-infrastructure@elist.ornl.gov>

# License details: https://gitlab.com/ecp-ci/jacamar-ci/-/blob/develop/LICENSE
License:        Apache 2.0 or MIT
URL:            https://gitlab.com/ecp-ci/%{name}
Source0:        https://gitlab.com/ecp-ci/%{name}/-/archive/v%{version}/%{name}-v%{version}.tar.gz

# Conflicts with other official packages with different configuration/deployment methods.
Conflicts:  %{name}-caps

# Go is required for build but a newer version (see go.mod)
# than is available on most distributions.
BuildRequires:  make

Requires:   bash
Requires:   git
Requires:   gitlab-runner

Prefix: %{_opt_prefix}

%description
Jacamar CI is the HPC focused CI/CD driver for GitLab's custom executor. The
core goal is to offer a maintainable, yet extensively configurable tool that
will allow for the use of GitLab's robust testing model on unique test
resources. Allowing teams to integrate existing pipelines onto powerful
development environments.

%prep
%if %{defined _sha256sum}
    echo "%{_sha256sum}  %SOURCE0" | sha256sum -c -
%endif
%setup -q -n %{name}-v%{version}

%build
make build VERSION=%{version}

%install
mkdir -p %{buildroot}%{jac}/bin
make install PREFIX=%{buildroot}%{jac}

%files
%attr(0555, root, root) %{jac}
