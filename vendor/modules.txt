# github.com/BurntSushi/toml v1.3.2
## explicit; go 1.16
github.com/BurntSushi/toml
github.com/BurntSushi/toml/internal
# github.com/davecgh/go-spew v1.1.1
## explicit
github.com/davecgh/go-spew/spew
# github.com/gabriel-vasile/mimetype v1.4.3
## explicit; go 1.20
github.com/gabriel-vasile/mimetype
github.com/gabriel-vasile/mimetype/internal/charset
github.com/gabriel-vasile/mimetype/internal/json
github.com/gabriel-vasile/mimetype/internal/magic
# github.com/go-playground/locales v0.14.1
## explicit; go 1.17
github.com/go-playground/locales
github.com/go-playground/locales/currency
# github.com/go-playground/universal-translator v0.18.1
## explicit; go 1.18
github.com/go-playground/universal-translator
# github.com/go-playground/validator/v10 v10.22.0
## explicit; go 1.18
github.com/go-playground/validator/v10
# github.com/golang-jwt/jwt/v5 v5.2.0
## explicit; go 1.18
github.com/golang-jwt/jwt/v5
# github.com/golang/mock v1.6.0
## explicit; go 1.11
github.com/golang/mock/gomock
# github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
## explicit; go 1.13
github.com/google/shlex
# github.com/leodido/go-urn v1.4.0
## explicit; go 1.18
github.com/leodido/go-urn
github.com/leodido/go-urn/scim/schema
# github.com/pmezard/go-difflib v1.0.0
## explicit
github.com/pmezard/go-difflib/difflib
# github.com/seccomp/libseccomp-golang v0.10.0
## explicit; go 1.14
github.com/seccomp/libseccomp-golang
# github.com/sirupsen/logrus v1.8.1
## explicit; go 1.13
github.com/sirupsen/logrus
github.com/sirupsen/logrus/hooks/syslog
github.com/sirupsen/logrus/hooks/writer
# github.com/stretchr/testify v1.8.4
## explicit; go 1.20
github.com/stretchr/testify/assert
# gitlab.com/ecp-ci/gljobctx-go v0.8.0
## explicit; go 1.22
gitlab.com/ecp-ci/gljobctx-go
gitlab.com/ecp-ci/gljobctx-go/internal/jwks
gitlab.com/ecp-ci/gljobctx-go/internal/rules
gitlab.com/ecp-ci/gljobctx-go/internal/web
# golang.org/x/crypto v0.19.0
## explicit; go 1.18
golang.org/x/crypto/sha3
# golang.org/x/net v0.21.0
## explicit; go 1.18
golang.org/x/net/html
golang.org/x/net/html/atom
# golang.org/x/sys v0.22.0
## explicit; go 1.18
golang.org/x/sys/cpu
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.14.0
## explicit; go 1.18
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/tag
golang.org/x/text/language
# gopkg.in/yaml.v3 v3.0.1
## explicit
gopkg.in/yaml.v3
# kernel.org/pub/linux/libs/security/libcap/cap v1.2.70
## explicit; go 1.11
kernel.org/pub/linux/libs/security/libcap/cap
# kernel.org/pub/linux/libs/security/libcap/psx v1.2.70
## explicit; go 1.11
kernel.org/pub/linux/libs/security/libcap/psx
