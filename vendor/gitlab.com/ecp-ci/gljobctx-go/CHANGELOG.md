# Changelog #

## v0.8.0 (July 17, 2024) ##

* Update validator package + refactor validation rules and associated tests 
  ([!53](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/53))
* Update Go version to 1.22
  ([!52](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/52))

## v0.7.2 (February 26, 2024) ##

* Update dependencies
  ([!50](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/50))
* Correct `ErrorJWKS` type
  ([!49](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/49))

## v0.7.1 (February 1, 2024) ##

* Update dependencies and use vendor folder
  ([!47](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/47))
* Improve how JWKS responses are un-marshaled
  ([!46](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/46))

## v0.7.0 (December 13, 2023) ##

* Added identifiable `JWKSError` type
  ([!45](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/45))
* Migrate to `/oauth/discovery/keys` endpoint
  ([!44](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/44))
* Update linter versions and CI/CD pipeline scanning
  ([!43](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/43))

## v0.6.1 (August 14, 2023) ##

* Improve handling of `aud` claims
  ([!40](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/40),
  [!39](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/39))

## v0.6.0 (August 3, 2023) ##

* Updated to *golang-jwt/jwt/v5*
  ([!37](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/37))

## v0.5.0 (May 9, 2023) ##

* Added UserIdentities to Environment
  ([!35](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/35))

## v0.4.0 (March 9, 2023) ##

* Added support and tests for upcoming shift to configurable `id_tokens`
  and custom `aud`
  ([!30](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/30),
  [!32](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/32))
* Updated underlying libraries
  ([!31](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/31))

## v0.3.2 (December 16, 2022) ##

* Update support/testing to Go 1.19
  ([!28](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/28))

## v0.3.1 (September 12, 2022) ##

* Retry failed `/-/jwks` GET
  ([!26](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/26))

## v0.3.0 (July 15, 2022) ##

* Support optional user identities in JWT Claims
  ([!16](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/16))
* Add JWT ID (`jti`) to Claims
  ([!21](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/21))
* Constructs an environment for a command based upon valid Claims
  ([!18](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/18))
* Add `coverage` key to `.gitlab-ci.yaml`
  ([!20](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/20))
* Notes for JWT test keys
  ([!15](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/15))

## v0.2.1 (April 22, 2022) ##

* Improve rules validation for `env`
  ([!13](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/13))

## v0.2.0 (April 18, 2022) ##

* Clarify `Claims` validation option and supported payload
  ([!11](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/11))
* Add `Validator` interface and correct mocks
  ([!10](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/10),
  [!9](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/9))
* Update support/testing to Go 1.18.1
  ([!8](https://gitlab.com/ecp-ci/gljobctx-go/-/merge_requests/8))

## v0.1.0 (April 7, 2022) ##

* Initial release
