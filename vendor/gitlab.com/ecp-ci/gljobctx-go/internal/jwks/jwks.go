// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package jwks

import (
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"math/big"
	"strings"
	"time"

	"gitlab.com/ecp-ci/gljobctx-go/internal/rules"
	"gitlab.com/ecp-ci/gljobctx-go/internal/web"
)

const (
	maxRetry = 1
)

type Header struct {
	Alg string `json:"alg" validate:"startswith=RS,len=5,alphanum"`
	Kid string `json:"kid" validate:"kid"`
	Typ string `json:"typ" validate:"eq=JWT"`
}

type WebKey struct {
	Alg string    `json:"alg"`
	Kty string    `json:"kty"`
	Kid string    `json:"kid"`
	E   *exponent `json:"e"`
	N   *modulus  `json:"n"`
}

type WebKeySet struct {
	Keys []WebKey `json:"keys"`
}

func decodeHeader(encoded string) ([]byte, error) {
	return decodeBase64(strings.Split(encoded, ".")[0])
}

func decodeBase64(encoded string) ([]byte, error) {
	return base64.RawURLEncoding.DecodeString(encoded)
}

func FetchKey(client web.Client, url, kid, alg string) (interface{}, error) {
	jwks := WebKeySet{}
	var err error

	for i := 0; i < maxRetry; i++ {
		err = client.GetJSON(url, map[string]string{}, &jwks)
		if err == nil {
			break
		}
		time.Sleep(5 * time.Second)
	}
	if err != nil {
		return nil, err
	}

	key := jwks.findFirstKey(kid)

	if key == (WebKey{}) {
		return nil, fmt.Errorf("no matching entry for kid %s found", kid)
	}
	if key.Alg != alg {
		return nil, fmt.Errorf(
			"algorithm expected by JWKS (%s) does not match JWT (%s)",
			key.Alg,
			alg,
		)
	}

	return key.establishRSA()
}

func CheckHeader(encoded string) (Header, error) {
	h := Header{}
	seg, err := decodeHeader(encoded)
	if err != nil {
		return h, err
	}

	if err = json.Unmarshal(seg, &h); err != nil {
		return h, err
	}

	v := rules.NewValidator()

	err = v.Var(h, "kid")

	return h, err
}

func (wks WebKeySet) findFirstKey(kid string) WebKey {
	for _, k := range wks.Keys {
		if k.Kid == kid {
			return k
		}
	}
	return WebKey{}
}

type modulus struct {
	data *big.Int
}

func (m *modulus) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}

	buf, err := decodeBase64(s)
	if err != nil {
		return err
	}

	m.data = new(big.Int).SetBytes(buf)

	return nil
}

type exponent struct {
	data int
}

func (e *exponent) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}

	buf, err := decodeBase64(s)
	if err != nil {
		return err
	}
	e.data = int(new(big.Int).SetBytes(buf).Int64())

	return nil
}

func (wk WebKey) establishRSA() (*rsa.PublicKey, error) {
	if wk.N == nil || wk.E == nil {
		return nil, errors.New("failed to identify public key, verify JWKS algorithm type")
	}

	return &rsa.PublicKey{
		N: wk.N.data,
		E: wk.E.data,
	}, nil
}
