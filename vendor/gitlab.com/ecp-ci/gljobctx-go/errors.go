package gljobctx

import (
	"errors"
	"fmt"
)

var (
	// ErrorJWKS indicates an error associate with retrieving/identifying
	// the content of the JSON Web Key Store.
	ErrorJWKS = errors.New("unable to retrieve key from JWKS endpoint")
)

func newErrorJWKS(err error) error {
	return fmt.Errorf("%w: %w", ErrorJWKS, err)
}
