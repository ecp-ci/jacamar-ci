// SPDX-License-Identifier: (Apache-2.0 OR MIT)

package gljobctx

import (
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt/v5"

	"gitlab.com/ecp-ci/gljobctx-go/internal/rules"
)

var v *validator.Validate

const (
	defLeeway    = 30 * time.Second
	envKeyRegexp = `[A-Z0-9_]`
)

// Claims details CI job level information established based upon a verified CI_JOB_JWT.
// Source: https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
type Claims struct {
	NamespaceID   string `json:"namespace_id" validate:"required,max=10,number" env:"JWT_NAMESPACE_ID"`
	NamespacePath string `json:"namespace_path" validate:"required,max=500,projectPath" env:"JWT_NAMESPACE_PATH"`
	ProjectID     string `json:"project_id" validate:"required,max=10,number" env:"JWT_PROJECT_ID"`
	ProjectPath   string `json:"project_path" validate:"max=500,projectPath" env:"JWT_PROJECT_PATH"`
	UserID        string `json:"user_id" validate:"required,max=10,number" env:"JWT_USER_ID"`
	// UserIdentities is the list of externally connected accounts for job trigger user.
	// It is a dictionary of provider/id:  {provider: "github", "extern_uid": "#######"}
	UserIdentities []map[string]string `json:"user_identities"`
	UserLogin      string              `json:"user_login" validate:"required,max=250,username" env:"JWT_USER_LOGIN"`
	UserEmail      string              `json:"user_email" validate:"max=250,email" env:"JWT_USER_EMAIL"`
	PipelineID     string              `json:"pipeline_id" validate:"required,max=10,number" env:"JWT_PIPELINE_ID"`
	PipelineSource string              `json:"pipeline_source" validate:"max=100,pipelineSource" env:"JWT_PIPELINE_SOURCE"`
	JobID          string              `json:"job_id" validate:"required,max=10,number" env:"JWT_JOB_ID"`
	Ref            string              `json:"ref" validate:"required,max=250,ref" env:"JWT_REF"`
	RefType        string              `json:"ref_type" validate:"required,max=50,ref" env:"JWT_REF_TYPE"`
	RefProtected   string              `json:"ref_protected" validate:"required,boolean" env:"JWT_REF_PROTECTED"`

	// OverrideClaims https://tools.ietf.org/html/rfc7519#section-4.1
	OverrideClaims
}

type OverrideClaims struct {
	// Issuer (iss) https://tools.ietf.org/html/rfc7519#section-4.1
	Issuer string `json:"iss,omitempty" validate:"max=125,potentialURL" env:"JWT_ISS"`
	// Audience (aud) https://tools.ietf.org/html/rfc7519#section-4.1
	Audience jwt.ClaimStrings `json:"aud,omitempty" validate:"max=125,potentialURL"`
	// Subject (sub) https://tools.ietf.org/html/rfc7519#section-4.1
	Subject string `json:"sub,omitempty" validate:"required,max=500,ref" env:"JWT_SUB"`
	// ExpiresAt (exp) https://tools.ietf.org/html/rfc7519#section-4.1
	ExpiresAt *jwt.NumericDate `json:"exp,omitempty"`
	// NotBefore (nbf) https://tools.ietf.org/html/rfc7519#section-4.1
	NotBefore *jwt.NumericDate `json:"nbf,omitempty"`
	// IssuedAt (iat) https://tools.ietf.org/html/rfc7519#section-4.1
	IssuedAt *jwt.NumericDate `json:"iat,omitempty"`
	// JwtId (jti) https://tools.ietf.org/html/rfc7519#section-4.1
	JwtId string `json:"jti,omitempty" validate:"required,max=125" env:"JWT_ID"`
}

func (c Claims) verifyJobID(trustedID string) (err error) {
	if c.JobID == "" {
		err = errors.New("missing Job ID (job_id)")
	} else if c.JobID != trustedID {
		err = errors.New("stolen JWT detected")
	}

	return
}

func (j job) parseClaims(key interface{}) (jc Claims, err error) {
	token, err := jwt.ParseWithClaims(j.encoded, &jc, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			// We've previously validated the 'alg' in the Header structure.
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return key, nil
	}, j.generateParsers()...)

	if err == nil && token.Valid {
		err = j.verifications(jc)
	}

	return
}

func (j job) generateParsers() (parsers []jwt.ParserOption) {
	parsers = append(parsers, jwt.WithLeeway(defLeeway+j.expDelay))

	if j.requiredAud != "" {
		parsers = append(parsers, jwt.WithAudience(j.requiredAud))
	}

	return parsers
}

// verifications enforces all CI_JOB_JWT specific verification steps, not handled
// by the 'golang-jwt/jwt' package.
func (j job) verifications(jc Claims) (err error) {
	if err = jc.verifyJobID(j.jobID); err != nil {
		return
	}

	if j.claimEnvValidation {
		err = v.Struct(jc)
	}

	return
}

// Environment constructs an environment for a command based upon a valid structure with 'env' tags.
func (c Claims) Environment() []string {
	env := cmdEnv(
		reflect.TypeOf(c),
		reflect.ValueOf(&c).Elem(),
	)

	for _, val := range c.UserIdentities {
		re := regexp.MustCompile(envKeyRegexp)
		tmp := re.FindAllString(strings.ToUpper(val["provider"]), -1)
		env = append(env, envString(
			"JWT_IDENTITY_"+strings.Join(tmp, ""),
			val["extern_uid"],
		))
	}

	env = append(env, envString("JWT_AUD", strings.Join(c.Audience, ";")))

	return env
}

func cmdEnv(t reflect.Type, v reflect.Value) (env []string) {
	for i := 0; i < t.NumField(); i++ {
		vf := v.Field(i)
		tf := t.Field(i)

		value := vf.Interface()
		key := tf.Tag.Get("env")

		switch vf.Kind() {
		case reflect.String:
			if value != "" && key != "" {
				env = append(env, envString(key, value.(string)))
			}
		case reflect.Struct:
			// Iterate over the struct fields
			e := cmdEnv(tf.Type, reflect.ValueOf(value))
			env = append(env, e...)
		default:
			continue
		}
	}

	return
}

func envString(key, value string) string {
	return fmt.Sprintf(
		"%s=%s",
		strings.TrimSpace(key),
		strings.TrimSpace(value),
	)
}

func (c Claims) GetExpirationTime() (*jwt.NumericDate, error) {
	if c.ExpiresAt == nil {
		return nil, errors.New("missing expiration (exp)")
	}

	return c.ExpiresAt, nil
}

func (c Claims) GetIssuedAt() (*jwt.NumericDate, error) {
	return c.IssuedAt, nil
}

func (c Claims) GetNotBefore() (*jwt.NumericDate, error) {
	if c.NotBefore == nil {
		return nil, errors.New("missing not valid before (nbf)")
	}

	return c.NotBefore, nil
}

func (c Claims) GetIssuer() (string, error) {
	return c.Issuer, nil
}

func (c Claims) GetSubject() (string, error) {
	return c.Subject, nil
}

func (c Claims) GetAudience() (jwt.ClaimStrings, error) {
	return c.Audience, nil
}

func init() {
	v = rules.NewValidator()
}
