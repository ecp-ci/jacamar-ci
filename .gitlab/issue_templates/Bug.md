## Summary

<!-- Brief summary of issue. -->

## Software Versions

```shell
$ /opt/jacamar/bin/jacamar --version
  ...
  
$ gitlab-runner --version
  ...
```

GitLab Server: <!-- https://your-gitlab-url/help -->

## Steps to reproduce

<!-- Steps that can be taken to reproduce the issue. -->

## Expected behavior

<!-- Description of what is expected. -->

## Possible fix

<!-- Possible source of the bug or potential steps to fix. -->

/label ~bug
