## Proposal

<!-- Overview of the proposed features/enhancement. -->

## Target Users/Deployments

<!-- Potential individual users and/or deployments that will benefit from this feature. -->

## Benefits/Risks

<!-- Potential benefits and/or risks from proposal. -->

## Additional Requirements

<!-- Any additional requirements that go beyond any efforts in this project. -->

<!-- If this feature will require additional documentation and/or tutorials (https://ecp-ci.gitlab.io).  -->
* Documentation: ?
<!-- Expanded testing capability similar to 'make pav-container-*' -->
* Additional Testing: ?
<!-- Will the feature have implications on the jacamar-auth and the authorization/downscope functionality and thus require additional review.  -->
* Security Review: ?

/label ~suggestion
/label ~enhancement
