## Summary

<!-- Brief summary of issue. -->

## Software Versions

```shell
$ /opt/jacamar/bin/jacamar --version
  ...
  
$ gitlab-runner --version
  ...
```

GitLab Server: <!-- https://your-gitlab-url/help -->

### Scope of Issue

<!-- Scope of affected deployment/users and priority presented. -->

## Steps to reproduce

<!-- Steps that can be taken to reproduce the issue. -->

## Possible fix

<!-- Possible source of the bug or potential steps to fix. -->

/label ~Security

/assign @paulbry

<!-- Security related issues default to confidential, can be manually changed later. -->
/confidential
