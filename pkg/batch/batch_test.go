package batch

import (
	"context"
	"errors"
	"os"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

type batchTests struct {
	job      Job
	settings Settings
	batch    configure.Batch
	arg      string
	msg      logging.Messenger

	assertError   func(t *testing.T, err error)
	assertTime    func(t *testing.T, dur time.Duration)
	assertManager func(t *testing.T, mng Manager)
}

func Test_NewBatchJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	emptyMsg := mock_logging.NewMockMessenger(ctrl)

	tests := map[string]batchTests{
		"valid Manager created, no SchedulerBin or ArgumentsVariable defined": {
			settings: Settings{
				BatchCmd:    "batch",
				StateCmd:    "state",
				StopCmd:     "stop",
				IllegalArgs: []string{"-a"},
			},
			batch: configure.Batch{},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn(
					"No %s variable detected, please check your CI job if this is unexpected.",
					"SCHEDULER_PARAMETERS",
				).Times(1)
				return m
			}(),
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, mng.BatchCmd(""), "batch")
				assert.Equal(t, mng.StateCmd(), "state")
				assert.Equal(t, mng.StopCmd(), "stop")
				assert.Equal(
					t,
					mng.UserArgs(),
					"",
					"no user arguments should be defined",
				)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"valid Manager created, SchedulerBin and ArgumentsVariables defined": {
			settings: Settings{
				BatchCmd: "batch",
				StateCmd: "state",
				StopCmd:  "stop",
			},
			batch: configure.Batch{
				SchedulerBin:      "/ci",
				ArgumentsVariable: []string{"PARAMS"},
			},
			msg: emptyMsg,
			arg: "--foo=bar -a",
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, mng.BatchCmd("--foo=bar -a"), "/ci/batch --foo=bar -a")
				assert.Equal(t, mng.StateCmd(), "/ci/state")
				assert.Equal(t, mng.StopCmd(), "/ci/stop")
				assert.Equal(t, mng.UserArgs(), "--foo=bar -a")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"valid Manager created, SchedulerBin but no ArgumentsVariables": {
			settings: Settings{
				BatchCmd: "batch",
				StateCmd: "state",
				StopCmd:  "stop",
			},
			batch: configure.Batch{
				SchedulerBin: "/ci/",
			},
			msg: emptyMsg,
			arg: "--foo=bar -a",
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, mng.BatchCmd("--foo=bar -a"), "/ci/batch --foo=bar -a")
				assert.Equal(t, mng.StateCmd(), "/ci/state")
				assert.Equal(t, mng.StopCmd(), "/ci/stop")
				assert.Equal(t, mng.UserArgs(), "--foo=bar -a")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"multiple batch environment keys defined": {
			settings: Settings{
				BatchCmd: "batch",
				StateCmd: "state",
				StopCmd:  "stop",
			},
			batch: configure.Batch{
				SchedulerBin: "/ci",
				EnvVars: []string{
					"HELLO=WORLD",
					"CFG=FILE",
				},
			},
			msg: emptyMsg,
			arg: "--foo=bar -a",
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(
					t,
					mng.BatchCmd("--custom"),
					"export HELLO=WORLD && export CFG=FILE && /ci/batch --custom",
				)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"allow illegal arguments": {
			settings: Settings{
				BatchCmd:    "batch",
				IllegalArgs: []string{"-J", "--job-name"},
			},
			batch: configure.Batch{
				AllowIllegalArgs:  true,
				ArgumentsVariable: []string{"PARAMS"},
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn(
					gomock.Eq("Illegal argument detected. Please remove: --job-name"),
				)
				return m
			}(),
			arg: "--foo=bar -a --job-name=test",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"illegal arguments": {
			settings: Settings{
				BatchCmd:    "batch",
				IllegalArgs: []string{"-J", "--job-name"},
			},
			batch: configure.Batch{
				ArgumentsVariable: []string{"PARAMS"},
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn(
					gomock.Eq("Illegal argument detected. Please remove: --job-name"),
				)
				return m
			}(),
			arg: "--foo=bar -a --job-name=test",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "illegal arguments must be addressed")
			},
		},
		"invalid quote for user args": {
			settings: Settings{
				BatchCmd: "batch",
			},
			batch: configure.Batch{
				ArgumentsVariable: []string{"PARAMS"},
			},
			arg: "--foo=bar -a --job-name='test",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "expecting closing quote")
			},
		},
		"invalid default args": {
			batch: configure.Batch{
				DefaultArgs: []string{"-one'"},
				FFUserArgs:  true,
			},
			msg: emptyMsg,
			arg: "--foo=bar -a",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "expecting closing quote")
			},
		},
		"valid quotes": {
			batch: configure.Batch{
				DefaultArgs: []string{"-one"},
				FFUserArgs:  true,
			},
			msg: emptyMsg,
			arg: "--foo=bar -a",
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, "'-one' '--foo=bar' '-a'", mng.UserArgs())
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid dynamic script": {
			msg: emptyMsg,
			arg: func() string {
				scriptFile := t.TempDir() + "/script.bash"
				_ = usertools.CreateScript(scriptFile, `#!/bin/bash
						exit 1
						`)
				return scriptFile
			}(),
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "dynamic arguments")
			},
		},
		"disabled dynamic args": {
			batch: configure.Batch{
				DefaultArgs:        []string{"-one"},
				DisableDynamicArgs: true,
			},
			msg: emptyMsg,
			arg: func() string {
				scriptFile := t.TempDir() + "/valid.bash"
				_ = usertools.CreateScript(scriptFile, `#!/bin/bash
echo "{\"args\":\"--example -A1\"}"
`)
				return scriptFile
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertManager: func(t *testing.T, mng Manager) {
				assert.NotContains(t, "-A1", mng.UserArgs())
			},
		},
		"valid non-executable dynamic script": {
			batch: configure.Batch{
				DefaultArgs: []string{"-one"},
				FFUserArgs:  true,
			},
			msg: emptyMsg,
			arg: func() string {
				scriptFile := t.TempDir() + "/valid.bash"
				_ = os.WriteFile(scriptFile, []byte(`#!/bin/bash
echo "{\"args\":\"--example -A1\"}"
`), 0600)
				return scriptFile
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertManager: func(t *testing.T, mng Manager) {
				assert.Equal(t, "'-one' '--example' '-A1'", mng.UserArgs())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.arg != "" {
				if len(tt.batch.ArgumentsVariable) != 0 {
					t.Setenv(envkeys.UserEnvPrefix+tt.batch.ArgumentsVariable[0], tt.arg)
				} else {
					t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", tt.arg)
				}
			}

			mng, err := NewBatchJob(tt.settings, tt.batch, tt.msg)

			if tt.assertManager != nil {
				tt.assertManager(t, mng)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestJob_NFSTimeout(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(
		"Invalid nfs_timeout configuration specified by runner configuration: %s",
		"time: invalid duration \"string\"",
	).Times(1)

	tests := map[string]batchTests{
		"valid time duration string provided": {
			arg: "1s",
			msg: m,
		},
		"invalid time duration string provided": {
			arg: "string",
			msg: m,
		},
		"empty string provided": {
			arg: "",
			msg: m,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			tt.job.NFSTimeout(tt.arg, tt.msg)
		})
	}
}

func TestJob_MonitorTermination(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().RequestContext().Return(ctx).AnyTimes()
	run.EXPECT().ReturnOutput("cancel 101").Return("output", errors.New("error message"))

	t.Run("routine closed after job is completed", func(t *testing.T) {
		jobDone := make(chan struct{})

		j := Job{
			stopCmd: "cancel",
		}
		go func() {
			time.Sleep(100 * time.Millisecond)
			jobDone <- struct{}{}
		}()
		j.MonitorTermination(run, jobDone, "1", t.TempDir())
	})

	t.Run("job canceled and error encountered", func(t *testing.T) {
		jobDone := make(chan struct{})
		defer close(jobDone)

		j := Job{
			stopCmd: "cancel",
		}
		go func() {
			time.Sleep(100 * time.Millisecond)
			cancel()
			time.Sleep(2 * time.Second)
		}()
		j.MonitorTermination(run, jobDone, "101", t.TempDir())
	})
}

func TestCommandDelay(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(
		"Invalid command_delay specified by runner configuration: %s",
		gomock.Any(),
	).Times(1)

	tests := map[string]batchTests{
		"valid time duration string provided": {
			arg: "1s",
			msg: m,
			assertTime: func(t *testing.T, dur time.Duration) {
				assert.Equal(t, 1*time.Second, dur)
			},
		},
		"invalid time duration string provided": {
			arg: "string",
			msg: m,
			assertTime: func(t *testing.T, dur time.Duration) {
				assert.Equal(t, defaultSleep, dur)
			},
		},
		"empty string provided": {
			arg: "",
			msg: m,
			assertTime: func(t *testing.T, dur time.Duration) {
				assert.Equal(t, defaultSleep, dur)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := CommandDelay(tt.arg, tt.msg)

			if tt.assertTime != nil {
				tt.assertTime(t, got)
			}
		})
	}
}
