package batch

import (
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func Test_checkIllegalArgs(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]batchTests{
		"empty illegal arguments and empty user arguments": {
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"arguments defined but not illegal argument detected": {
			settings: Settings{
				IllegalArgs: []string{"-o", "-eo", "--Error", "--Time"},
			},
			arg: "-a test -b",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"multiple illegal arguments found": {
			settings: Settings{
				IllegalArgs: []string{"-o", "-eo", "--Error", "--Time"},
			},
			arg: "-o output -b --Time=1h",
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn(
					gomock.Eq("Illegal argument detected. Please remove: -o --Time"),
				)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "illegal arguments must be addressed before continuing")
			},
		},
		"single illegal argument found": {
			settings: Settings{
				IllegalArgs: []string{"-o", "-eo", "--Error", "--Time"},
			},
			arg: "-a test --Error=foo",
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn(
					gomock.Eq(`Illegal argument detected. Please remove: --Error`),
				)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "illegal arguments must be addressed before continuing")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := checkIllegalArgs(tt.msg, tt.arg, tt.settings.IllegalArgs)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_checkDynamicArgs(t *testing.T) {
	runScriptMaxSeconds = 5

	msg := logging.NewMessenger()
	skipExit = func() {}

	tests := map[string]struct {
		script       string
		proposedArgs string
		targetEnv    map[string]string
		assertError  func(*testing.T, error)
		assertString func(*testing.T, string)
	}{
		"standard arguments": {
			proposedArgs: "--test=1",
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "--test=1", s)
			},
		},
		"script error": {
			script: `#!/bin/bash
			echo "Error!"
			exit 1
			`,
			proposedArgs: t.TempDir() + "/invalid.sh",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "Error!")
			},
		},
		"no json": {
			script: `#!/bin/bash
						echo "json..."
						`,
			proposedArgs: t.TempDir() + "/json.sh",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "json...")
			},
		},
		"skip job": {
			script: `#!/bin/bash
			echo "{\"skip\":true}"
			`,
			proposedArgs: "skip.sh",
			assertString: func(t *testing.T, s string) {
				assert.Empty(t, s)
			},
		},
		"empty json": {
			script: `#!/bin/bash
				echo "{}"
				`,
			proposedArgs: t.TempDir() + "/empty.sh",
			assertString: func(t *testing.T, s string) {
				assert.Empty(t, s)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"valid": {
			script: `#!/bin/bash
				echo "{\"args\":\"--example -A1\"}"
				`,
			proposedArgs: t.TempDir() + "/valid.sh",
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, s, "--example -A1")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"timeout": {
			script: `#!/bin/bash
				sleep 15
				`,
			proposedArgs: t.TempDir() + "/timeout.sh",
			assertString: func(t *testing.T, s string) {
				assert.Empty(t, s)
			},
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "killed")
			},
		},
		"skip": {
			script: `#!/bin/bash
				echo "{\"args\":\"--example -A1\"}"
				`,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.JacamarDisableDynamic: "1",
			},
			proposedArgs: t.TempDir() + "/valid.sh",
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "/valid.sh")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ciProjectDir := t.TempDir()
			t.Setenv(envkeys.UserEnvPrefix+"CI_PROJECT_DIR", ciProjectDir)

			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			if tt.script != "" {
				var err error
				if strings.HasPrefix(tt.proposedArgs, "/") {
					err = usertools.CreateScript(tt.proposedArgs, tt.script)
				} else {
					err = usertools.CreateScript(ciProjectDir+"/"+tt.proposedArgs, tt.script)
				}
				assert.NoError(t, err, "failed to create test script")
			}

			got, err := checkDynamicArgs(tt.proposedArgs, msg)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func Test_cleanJsonOutput(t *testing.T) {
	tests := map[string]struct {
		b      []byte
		result []byte
	}{
		"no indexes": {
			b:      []byte("test"),
			result: []byte("test"),
		},
		"logout": {
			b:      []byte("{example}\nlogout"),
			result: []byte("{example}"),
		},
		"empty": {
			b:      []byte{},
			result: []byte{},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := cleanJsonOutput(tt.b)

			assert.Equal(t, tt.result, got)
		})
	}
}
