package batch

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/google/shlex"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

var (
	skipExit            func()
	runScriptMaxSeconds = 90
)

type DynamicUserArgs struct {
	Args string `json:"args"`
	Skip bool   `json:"skip"`
}

func init() {
	skipExit = func() { os.Exit(0) }
}

func constructUserArgs(s Settings, opt configure.Batch, msg logging.Messenger) (string, error) {
	var err error

	proposedArgs := identifyUserArgs(opt.ArgumentsVariable, msg)
	if proposedArgs != "" && !opt.DisableDynamicArgs {
		proposedArgs, err = checkDynamicArgs(proposedArgs, msg)
		if err != nil {
			return "", fmt.Errorf("error encountered with dynamic arguments: %w", err)
		}
	}

	err = checkIllegalArgs(msg, proposedArgs, s.IllegalArgs)
	if err != nil && !opt.AllowIllegalArgs {
		return "", err
	}

	proposedArgs = fmt.Sprintf("%s %s", strings.Join(opt.DefaultArgs, " "), proposedArgs)
	if opt.FFUserArgs {
		proposedArgs, err = augmenter.ConstructUserArgs(proposedArgs)
		if err != nil {
			return "", err
		}
	}

	return strings.TrimSpace(proposedArgs), nil
}

// checkIllegalArgs compares the provided list of arguments against the scheduler system defined
// list of illegal arguments. A warning message is returned.
func checkIllegalArgs(msg logging.Messenger, proposed string, illegalArgs []string) (err error) {
	split, err := shlex.Split(proposed)
	if err != nil {
		return err
	}

	var warnings string
	for _, v := range split {
		for _, k := range illegalArgs {
			if strings.HasPrefix(v, k) {
				warnings = warnings + " " + k
			}
		}
	}

	if warnings != "" {
		msg.Warn("Illegal argument detected. Please remove:" + warnings)
		err = errors.New("illegal arguments must be addressed before continuing")
	}

	return
}

// identifyUserArgs parses an environment provided by the custom executor. Will observer default
// SCHEDULER_PARAMETER value and warn users if nothing is found.
func identifyUserArgs(keys []string, msg logging.Messenger) string {
	// Default to "SCHEDULER_PARAMETERS" always established.
	keys = append(keys, "SCHEDULER_PARAMETERS")

	for _, k := range keys {
		// Lookup managed here as it is a guaranteed user process and not reasonable
		// validation can be identified.
		v, f := os.LookupEnv(envkeys.UserEnvPrefix + k)
		if f {
			return v
		}
	}

	msg.Warn(
		"No %s variable detected, please check your CI job if this is unexpected.",
		keys[0],
	)
	return ""
}

// checkDynamicArgs identifies cases where a user has indicated they may wish to leverage
// dynamic arguments features. The potentially updated args are returned.
func checkDynamicArgs(proposedArgs string, msg logging.Messenger) (string, error) {
	proposedFile := proposedArgs
	if !strings.HasPrefix(proposedFile, "/") {
		// We assume that the file is located in the project directory
		// if a full path hasn't been provided.
		proposedFile = fmt.Sprintf(
			"%s/%s",
			os.Getenv(envkeys.UserEnvPrefix+envkeys.CIProjectDir),
			proposedFile,
		)
	}

	filename := filepath.Clean(proposedFile)
	file, err := os.Stat(filename)
	if err != nil {
		// This is an acceptable case since dynamic argument is optional. Any such error
		// should be considered a decision to provide arguments via the variable instead.
		return proposedArgs, nil
	}

	if envparser.TrueEnvVar(envkeys.UserEnvPrefix + envkeys.JacamarDisableDynamic) {
		return proposedArgs, nil
	}

	if file.Mode()&0111 == 0 {
		/* #nosec */
		_ = os.Chmod(filename, 0700)
	}

	var out []byte
	out, err = runScript(filename)
	if err != nil {
		return "", err
	}

	da := DynamicUserArgs{}
	if err = json.Unmarshal(out, &da); err != nil {
		return "", fmt.Errorf("verify json response (%s): %v", string(out), err)
	}

	if da.Skip {
		msg.Notify("Skipping job with exist code 0")
		skipExit()
		return "", nil
	}

	if da.Args == "" {
		// This shouldn't result in a job failure but simply warn the user.
		msg.Warn("No arguments found in json response")
	}

	return da.Args, nil
}

func runScript(file string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Duration(runScriptMaxSeconds)*time.Second,
	)
	defer cancel()

	cmd := exec.CommandContext(ctx, file)

	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &out

	err := cmd.Run()

	b := make([]byte, 500)
	_, _ = io.ReadFull(&out, b)

	if err != nil {
		return []byte{}, fmt.Errorf("argument script failed (%w): %s", err, string(b))
	}

	return cleanJsonOutput(b), nil
}

func cleanJsonOutput(b []byte) []byte {
	fI := bytes.Index(b, []byte("{"))
	if fI == -1 {
		fI = 0
	}

	lI := bytes.Index(b, []byte("}"))
	if lI == -1 {
		lI = len(b) - 1
	}

	return b[fI : lI+1]
}
