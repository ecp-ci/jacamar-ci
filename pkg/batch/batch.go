package batch

import (
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	// defaultSleep is the duration to wait between scheduler commands/interactions.
	defaultSleep = 30 * time.Second
)

// Manager defines a shared set of procedures that can be leveraged by any
// supported batch executor.
type Manager interface {
	// BatchCmd returns the command for submitting to the scheduler.
	BatchCmd(string) string
	// StateCmd returns the command to obtaining a job's status from the scheduler.
	StateCmd() string
	// StopCmd returns the command used to stop a currently scheduled job.
	StopCmd() string
	// UserArgs returns all user defined argument for submission to the scheduler.
	UserArgs() string
	// NFSTimeout checks the validity of the duration string supplied and pauses the current
	// routine for  that period of time. Please note this is a measure to account for the
	// differences presented by site network file systems.
	NFSTimeout(string, logging.Messenger)
	// TailFiles monitors all files supplied until either an end of file is reached, error
	// encountered,  or channel done sent. Expected maximum time is the configured
	// nfs_timeout (time duration string) and potential the maximum wait that could be
	// required for the files to be available for tailing. Output from these files is
	// piped exclusively to stdout.
	TailFiles([]string, chan struct{}, time.Duration, logging.Messenger) error
	// MonitorTermination checks the job context until either closed a empty struct
	// signals completion. If context is done, scheduler job is canceled.
	MonitorTermination(runmechanisms.Runner, chan struct{}, string, string)
}

// Settings represents generic details that can apply to all job's that are planned
// to be submitted to a supported underlying scheduling system.
type Settings struct {
	// BatchCmd required for submission to the scheduler.
	BatchCmd string
	// StateCmd to gather job state.
	StateCmd string
	// StopCmd to cancel a requested allocation.
	StopCmd string
	// IllegalArgs reserved by the executor.
	IllegalArgs []string
}

// Job represents details/configurations associated with any batch executor
// that are established at the start of a CI job.
type Job struct {
	batchCmd string
	stateCmd string
	stopCmd  string
	userArgs string
	envVars  []string
}

func (j Job) BatchCmd(args string) string {
	var sb strings.Builder

	for _, kv := range j.envVars {
		sb.WriteString("export " + kv + " && ")
	}

	sb.WriteString(j.batchCmd)
	if args != "" {
		sb.WriteString(" " + args)
	}

	return sb.String()
}

func (j Job) StateCmd() string {
	return j.stateCmd
}

func (j Job) StopCmd() string {
	return j.stopCmd
}

func (j Job) UserArgs() string {
	return j.userArgs
}

func (j Job) NFSTimeout(duration string, msg logging.Messenger) {
	if duration != "" {
		timeout, err := time.ParseDuration(duration)
		if err != nil {
			msg.Warn(
				"Invalid nfs_timeout configuration specified by runner configuration: %s",
				err.Error(),
			)
		}
		time.Sleep(timeout)
	}
}

func (j Job) MonitorTermination(
	runner runmechanisms.Runner,
	jobsDone chan struct{},
	stopArg, scriptDir string,
) {
	ctx := runner.RequestContext()

	select {
	case <-jobsDone:
		return
	case <-ctx.Done():
		/* #nosec */
		// file only created in userspace
		f, fileErr := os.OpenFile(
			scriptDir+"/sigterm.log",
			os.O_CREATE|os.O_RDWR|os.O_APPEND,
			usertools.OwnerPermissions,
		)

		// If we are able to open a log file print debug information, cleanup phase
		// will remove these details if configuration allows for it.
		if fileErr == nil {
			defer func() {
				// Any error encountered should not be observed as the program
				// is already beginning to shut down due to the signal encountered.
				_ = f.Close()
			}()

			_, _ = f.WriteString(fmt.Sprintf(
				"Signal captured, attempting to cancel job (%s %s)\n",
				j.stopCmd,
				stopArg,
			))
		}

		out, err := runner.ReturnOutput(fmt.Sprintf("%s %s", j.stopCmd, stopArg))
		if fileErr == nil {
			_, _ = f.WriteString(fmt.Sprintf("command error: %v\n", err))
			_, _ = f.WriteString(fmt.Sprintf("command output: %s\n", out))
		}

		return
	}
}

// CommandDelay returns either a parsed duration (configure.batch.command_delay) or
// warns the user regarding the incorrect configuration and the default 30s time. If
// not duration is provided then the default time is returned without warning.
func CommandDelay(duration string, msg logging.Messenger) time.Duration {
	if duration != "" {
		timeout, err := time.ParseDuration(duration)
		if err == nil {
			return timeout
		}
		msg.Warn(
			"Invalid command_delay specified by runner configuration: %s",
			err.Error(),
		)
	}

	return defaultSleep
}

// PrefixSchedulerPath returns the full path to an application by checking for the admin
// defined SchedulerBin. THe validity of the SchedulerBin is not checked.
func PrefixSchedulerPath(app, path string) string {
	if strings.HasSuffix(path, "/") {
		return fmt.Sprintf("%s%s", path, app)
	} else if path != "" {
		return fmt.Sprintf("%s/%s", path, app)
	}

	return app
}

// NewBatchJob uses the configuration and baseline Settings to establish an interface for
// managing a scheduled CI job. At the same time warning messages will be created when
// potentially undesirable states (e.g. illegal arguments or missing parameters)
// are encountered.
func NewBatchJob(s Settings, opt configure.Batch, msg logging.Messenger) (Manager, error) {
	userArgs, err := constructUserArgs(s, opt, msg)
	if err != nil {
		return nil, fmt.Errorf("failed to construct scheduler args: %w", err)
	}

	return Job{
		batchCmd: PrefixSchedulerPath(s.BatchCmd, opt.SchedulerBin),
		stateCmd: PrefixSchedulerPath(s.StateCmd, opt.SchedulerBin),
		stopCmd:  PrefixSchedulerPath(s.StopCmd, opt.SchedulerBin),
		userArgs: userArgs,
		envVars:  opt.EnvVars,
	}, nil
}
