package batch

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

var (
	watchFactory = watch
	watchWait    = 5 * time.Second
)

func (j Job) TailFiles(
	filenames []string,
	done chan struct{},
	maxWait time.Duration,
	msg logging.Messenger,
) error {
	if maxWait < 0 {
		return fmt.Errorf("invalid time (%v) provided, must be >= 0", maxWait)
	}

	for _, file := range filenames {
		if err := waitFile(file, maxWait); err != nil {
			return fmt.Errorf("unable to locate %s before timeout", file)
		}
	}

	var wg sync.WaitGroup
	wg.Add(len(filenames))
	for _, file := range filenames {
		f := file
		go func() {
			defer wg.Done()
			err := watchFactory(filepath.Clean(f), done, msg)
			if err != nil {
				// Users should be informed of an error tailing the file but job
				// should be allowed to finish.
				msg.Warn("error encountered tailing job output: %s", err.Error())
				msg.Stdout("scheduled job will continue without output")
			}
		}()
	}
	wg.Wait()

	return nil
}

// waitFile waits until provided time for a file to exist.
func waitFile(filename string, maxWait time.Duration) error {
	sleep := 10 * time.Second // 10 second, default duration.

	for !fileExists(filename) {
		maxWait = maxWait - time.Duration(sleep)
		if maxWait < 0 {
			return fmt.Errorf("file %s not found", filename)
		}
		time.Sleep(sleep)
	}
	return nil
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// watch uses the defined watcher library to examine the provided file for any changes and
// print them to standard out.
func watch(filename string, done chan struct{}, msg logging.Messenger) error {
	/* #nosec */
	// variable file path required
	file, err := os.Open(filename)
	if err != nil {
		return err
	}

	endC := make(chan struct{}, 1)
	errC := make(chan error, 1)
	defer func() {
		close(errC)
		close(endC)
	}()

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()
		finished := false

		for {
			if !fileExists(filename) {
				errC <- fmt.Errorf("file (%s) modified", filename)
				return
			}

			scanner := bufio.NewScanner(file)
			for scanner.Scan() {
				text := scanner.Text()
				msg.Stdout(text)
			}

			if finished {
				return
			}
			// Take additional steps to throttle watching the file to limit system I/O.
			time.Sleep(watchWait)

			_, err = file.Seek(0, io.SeekCurrent)
			if err != nil {
				errC <- err
				return
			}

			select {
			case <-endC:
				finished = true
			default:
				continue
			}
		}
	}()

	select {
	case <-done:
		endC <- struct{}{}
	case err = <-errC:
	}

	wg.Wait()
	return err
}

// CreateFiles takes a list of files (prefer full path) and generates
// and empty file. Errors are printed as warnings.
func CreateFiles(files []string, msg logging.Messenger) {
	for _, file := range files {
		fp, _ := filepath.Abs(file)
		/* #nosec */
		// variable file path required
		f, err := os.OpenFile(fp, os.O_CREATE, 0600)
		if err != nil {
			msg.Warn("Error while attempting to create file (%s): %s", file, err.Error())
			return
		}
		_ = f.Close()
	}
}
