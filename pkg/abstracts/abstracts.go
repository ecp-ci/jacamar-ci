// Package abstracts maintains interfaces/structures that are shared between internal
// workflows and potential plugins.
package abstracts

import (
	"gitlab.com/ecp-ci/jacamar-ci/internal/exectype"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

// Executor organizes all aspects of job execution regardless of potential type, providing
// a complete picture of the available configuration, context, and tools.
type Executor struct {
	Cfg    configure.Configurer
	Env    envparser.ExecutorEnv
	Msg    logging.Messenger
	Runner runmechanisms.Runner
	// ExecOptions contents of the Jacamar configuration, for use outside of authorization.
	ExecOptions string
	// ScriptPath to the generated script during run_exec.
	ScriptPath string
	// Stage name during run_exec currently being executed.
	Stage      string
	TargetExec exectype.Type
}
