package envparser

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
)

func Test_StatefulEnv_DecodeRegistryCredentials(t *testing.T) {
	tests := map[string]envTests{
		"invalid stateful variable": {
			stateful: StatefulEnv{
				RegistryCredentials: "ZXhhbXBsZS5jb20:abc,invalid",
			},
			assertMap: func(t *testing.T, m map[string]string) {
				assert.Len(t, m, 1)
				assert.Equal(t, "abc", m["example.com"])
			},
		},
		"multiple valid": {
			stateful: StatefulEnv{
				RegistryCredentials: "ZXhhbXBsZS5jb20:dXNlcjpwYXNz,cmVnLmV4YW1wbGUuY29t:dGVzdDpwYXNz",
			},
			assertMap: func(t *testing.T, m map[string]string) {
				assert.Len(t, m, 2)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := tt.stateful.DecodeRegistryCredentials()

			if tt.assertMap != nil {
				tt.assertMap(t, got)
			}
		})
	}
}

func TestContainerVolumes(t *testing.T) {
	tests := map[string]envTests{
		"invalid variable provided": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_VAR_0": "$test",
			},
			prefix: "TEST_VAR",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "invalid directory")
			},
		},
		"missing value skipped": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_VAR_0":     "/var/test:/opt:z",
				envkeys.UserEnvPrefix + "TEST_VAR_EMPTY": "",
			},
			prefix: "TEST_VAR",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"multiple valid paths": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_VAR_0": "/var/test:/opt:z",
				envkeys.UserEnvPrefix + "TEST_VAR_1": "/test",
			},
			prefix: "TEST_VAR",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 2)
				assert.Contains(t, s, "/var/test:/opt:z")
				assert.Contains(t, s, "/test")
			},
		},
		"skip process": {
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 0)
			},
		},
		"invalid label": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_VAR_0": "/var/test:/opt:z",
				envkeys.UserEnvPrefix + "TEST_VAR_1": "/test",
				envkeys.UserEnvPrefix + "TEST_VAR_2": "/test:/labels:/case",
			},
			prefix: "TEST_VAR",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "invalid label")
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 2)
				assert.NotContains(t, s, "/labels")
			},
		},
		"invalid segments": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_VAR_0": "/var/test:/opt:z:Z",
			},
			prefix: "TEST_VAR",
			assertError: func(t *testing.T, err error) {
				assert.ErrorContains(t, err, "invalid number of segments")
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 0)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got, err := ContainerVolumes(tt.prefix)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertSlice != nil {
				tt.assertSlice(t, got)
			}
		})
	}
}

func TestContainerHostname(t *testing.T) {
	tests := map[string]struct {
		targetEnv map[string]string
		wantVal   string
		wantFound bool
		wantErr   assert.ErrorAssertionFunc
	}{
		"no variable": {
			wantFound: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		"empty variable": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.JacamarHostname: "",
			},
			wantFound: true,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		"correct variable": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.JacamarHostname: "test-host",
			},
			wantVal:   "test-host",
			wantFound: true,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		"invalid variable": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.JacamarHostname: "!TEST",
			},
			wantFound: true,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		"environment variable": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.JacamarHostname: "$PATH",
			},
			wantFound: true,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		"HOSTNAME variable": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.JacamarHostname: "${HOSTNAME}",
			},
			wantFound: true,
			wantVal:   "${HOSTNAME}",
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			gotVal, gotFound, err := ContainerHostname()
			if !tt.wantErr(t, err, "ContainerHostname()") {
				return
			}
			assert.Equalf(t, tt.wantVal, gotVal, "ContainerHostname()")
			assert.Equalf(t, tt.wantFound, gotFound, "ContainerHostname()")
		})
	}
}
