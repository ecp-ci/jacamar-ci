package envparser

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
)

const (
	genericDirErrMsg = "invalid directory provided"
)

// GetUserVar finds and optionally validates the given key set by the user. Note that an
// empty value is not considered found (unlike with os.LookupEnv), this is due to how
// GitLab variables are set, meaning overriding a CI variable at the job the level
func GetUserVar(key, tag string) (string, bool, error) {
	val := os.Getenv(envkeys.UserEnvPrefix + key)
	if tag != "" && val != "" {
		if err := vv.Var(val, tag); err != nil {
			return "", true, err
		}
	}

	return val, val != "", nil
}

// GitTrace if Git's tracing/debug is expected.
func GitTrace() bool {
	return TrueEnvVar(envkeys.UserEnvPrefix + envkeys.GitTrace)
}

// TrueEnvVar check if variable 'true' or '1'.
func TrueEnvVar(key string) bool {
	val, present := os.LookupEnv(key)
	if present {
		val = strings.ToLower(strings.TrimSpace(val))
		if val == "true" || val == "1" {
			return true
		}
	}

	return false
}

// ValidRunnerVersion identify if the runner version triggering the job is valid
// against the provided major.minor release version. Since we check against custom
// environmental variables this should only be used as a smoke test to avoid later
// difficult errors and not as a security requirement.
func ValidRunnerVersion(major, minor int) bool {
	val, found := os.LookupEnv(envkeys.UserEnvPrefix + envkeys.CIRunnerVer)
	if found {
		matched, _ := regexp.MatchString(regexpRunnerVer, val)
		if matched {
			split := strings.Split(val, ".")
			majorInt, _ := strconv.Atoi(split[0])
			minorInt, _ := strconv.Atoi(split[1])

			if major == majorInt && minor <= minorInt {
				return true
			} else if major < majorInt {
				return true
			}
		}
	}

	return false
}

// CustomBuildsDir retrieve and validate a user defined CUSTOM_BUILDS_DIR variable to be used
// in related directory identification and creation. The lack of any corresponding value is
// conveyed as a boolean and does not result in any error message. Unexpanded variables detected
// in the directory do not cause errors, and upon expansion the path should be re-validated.
func CustomBuildsDir() (dir string, found bool, err error) {
	return findDirEnv(envkeys.CustomBuildsDir, "unexpandedDirectory")
}

// SchedulerLogDir retrieve and validate a user defined COPY_SCHEDULER_LOGS variable to be used
// in related directory identification and creation. The lack of any corresponding value is
// conveyed as a boolean and does not result in any error message.
func SchedulerLogDir() (string, bool, error) {
	return findDirEnv(envkeys.CopySchedulerLogs, "qualifiedDir")
}

func findDirEnv(key, tag string) (dir string, found bool, err error) {
	dir, found, err = GetUserVar(key, tag)
	if err != nil {
		// Avoid returning a potentially malicious directory value.
		return "", true, errors.New(genericDirErrMsg)
	}

	return
}

// ValidateDirectory ensures that proposed directory path meets Unix criteria and absolute.
// The existence of the directory is not required.
func ValidateDirectory(dir string) (err error) {
	if err = vv.Var(dir, "qualifiedDir"); err != nil {
		return errors.New(genericDirErrMsg)
	}

	return err
}

// SchedulerSignal returns the name or number of the signal that should be sent to
// the supported scheduler when a job is canceled. Any signal the is declared
// but contains non-alphanumeric characters will result in an error.
func SchedulerSignal() (val string, found bool, err error) {
	val, found, err = GetUserVar(envkeys.SchedulerSignal, "alphanum,max=24")
	if err != nil {
		err = fmt.Errorf(
			"invalid signal declared, verify %s key is correct (alphanumeric)",
			envkeys.SchedulerSignal,
		)
	}

	return
}

// NoProfile identifies if the user has requested "JACAMAR_NO_BASH_PROFILE" option.
func NoProfile() bool {
	return TrueEnvVar(envkeys.UserEnvPrefix + envkeys.JacamarNoProfile)
}
