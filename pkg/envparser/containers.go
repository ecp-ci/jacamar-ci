package envparser

import (
	"encoding/base64"
	"errors"
	"fmt"
	"os"
	"regexp"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
)

// encodeRegistryCredentials encodes all registry credentials provided by the GitLab
// service in the job response in a format that can be passed to future stages via
// the StatefulEnv. Please note these are not encrypted and Base64 encoded.
func (j JobResponse) encodeRegistryCredentials() string {
	s := []string{}
	for _, v := range j.Credentials {
		if v.Type == "registry" {
			s = append(s, fmt.Sprintf(
				"%s:%s",
				base64.RawURLEncoding.EncodeToString([]byte(v.URL)),
				base64.RawStdEncoding.EncodeToString([]byte(v.Username+":"+v.Password)),
			))
		}
	}

	return strings.Join(s, ",")
}

func (j JobResponse) prepareImageEntrypoint() string {
	return strings.Join(j.Image.Entrypoint, ",")
}

// DecodeRegistryCredentials returns the decoded stateful credentials in a form
// (map[url]encodedAuth) that can be used by container runtime in auth files.
func (s StatefulEnv) DecodeRegistryCredentials() map[string]string {
	encodedC := strings.Split(s.RegistryCredentials, ",")
	creds := make(map[string]string, len(encodedC))

	for _, v := range encodedC {
		tmp := strings.Split(v, ":")
		if len(tmp) != 2 {
			// Avoid panic if such a case did occur.
			continue
		}
		bURL, _ := base64.RawURLEncoding.DecodeString(tmp[0])
		creds[string(bURL)] = tmp[1]
	}

	return creds
}

// ContainerVolumes identifies all user defined container volumes/mounts based
// upon the provided environment prefix. The required formant of '/dir:/dir:label'
// is enforced, though only the first directory is required. Errors encountered are
// combined into a single message with the goal of allowing you use valid mounts
// and still inform the user of any variables that may need modified.
func ContainerVolumes(varPrefix string) ([]string, error) {
	if varPrefix == "" {
		return []string{}, nil
	}

	volumes := make([]string, 0)
	errMsgs := make([]string, 0)

	env := os.Environ()
	for _, v := range env {
		if strings.HasPrefix(v, envkeys.UserEnvPrefix+varPrefix) {
			split := strings.Split(v, "=")
			key := strings.TrimPrefix(split[0], envkeys.UserEnvPrefix)

			if split[1] == "" {
				// skip empty variables
				continue
			}

			components := strings.Split(split[1], ":")
			if len(components) > 3 {
				errMsgs = append(errMsgs, fmt.Sprintf("invalid number of segments ':' in %s", key))
				continue
			}

			rvl := regexp.MustCompile(regexpVolLabel)

			for k, v := range components {
				if k == 2 {
					if !rvl.MatchString(v) {
						errMsgs = append(errMsgs, fmt.Sprintf("invalid label detected in %s", key))
						continue
					}
				} else {
					if err := ValidateDirectory(v); err != nil {
						errMsgs = append(errMsgs, fmt.Sprintf("invalid directory detected in %s", key))
						continue
					}
				}

				if k == len(components)-1 {
					volumes = append(volumes, split[1])
				}
			}
		}
	}

	var err error
	if len(errMsgs) > 0 {
		err = errors.New(strings.Join(errMsgs, ", "))
	}

	return volumes, err
}

// ContainerHostname identifies an optionally defined hostname a user can provide
// for a container run mechanism. The returned value is considered safe for CLI
// interactions and in select cases (e.g., HOSTNAME) should be expanded by Bash.
func ContainerHostname() (string, bool, error) {
	re := regexp.MustCompile(regexpHostVar)
	val, found := os.LookupEnv(envkeys.UserEnvPrefix + envkeys.JacamarHostname)
	if found && val != "" && !re.MatchString(val) {
		if err := vv.Var(val, "hostname"); err != nil {
			return "", true, fmt.Errorf(
				"invalid hostname declared, verify %s key (see: %s)",
				envkeys.JacamarHostname,
				"https://tools.ietf.org/html/rfc1123",
			)
		}
	}

	return val, found, nil
}
