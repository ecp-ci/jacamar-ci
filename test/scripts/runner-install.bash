#!/usr/bin/env bash

# Install appropriate runner version to the container

set -eo pipefail
set +o noclobber

echo "Create local user account..."

useradd -ms /bin/bash "${RUNAS_USERNAME}"
mkdir /ecp
chmod 777 /ecp

echo "Run verification on runner version..."

# https://docs.gitlab.com/runner/install/linux-repository.html#gpg-signatures-for-package-installation
rpm --import /certs/runner-gitlab-runner-49F16C5CC3A0F81F.pub.gpg
rpm -Kv gitlab_runner.rpm

echo "Verification OK"
echo "Installing runner version..."

cd /etc/gitlab-runner

rpm -i --nodeps gitlab_runner.rpm

echo "Runner installed."
echo "Checking runner version..."

gitlab-runner --version

echo "Set permissions to allow execution of username setting script..."
chmod +x /runas-nightly-test-user.bash
echo "Script permissions ok."

echo "Starting runner..."
echo "Press Ctrl+C to exit."

gitlab-runner run
