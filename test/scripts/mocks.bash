#!/usr/bin/env bash

# Re-build all mock files (https://github.com/golang/mock)

set -eo pipefail
set -o xtrace

mock_cmd="$(go env GOPATH)/bin/mockgen"
${mock_cmd} --version

# Internal Packages
${mock_cmd} -source=internal/authuser/authuser.go \
    -destination=test/mocks/mock_authuser/mock_authuser.go \
    -package=mock_authuser
${mock_cmd} -source=internal/authuser/validation/validation.go \
    -destination=test/mocks/mock_validation/mock_validation.go \
    -package=mock_validation
${mock_cmd} -source=internal/command/command.go \
    -destination=test/mocks/mock_command/mock_command.go \
    -package=mock_command
${mock_cmd} -source=internal/command/signal.go \
    -destination=test/mocks/mock_command/mock_signaler.go \
    -package=mock_command
${mock_cmd} -source=internal/executors/executors.go \
    -destination=test/mocks/mock_executors/mock_executors.go \
    -package=mock_executors
${mock_cmd} -source=internal/runmechanisms/runmechanisms.go \
    -destination=test/mocks/mock_runmechanisms/mock_run.go \
    -package=mock_runmechanisms
${mock_cmd} -source=internal/jobtoken/context.go \
    -destination=test/mocks/mock_jobtoken/mock_jobtoken.go \
    -package=mock_jobtoken

# Shared Packages
${mock_cmd} -source=pkg/batch/batch.go \
    -destination=test/mocks/mock_batch/mock_batch.go \
    -package=mock_batch
${mock_cmd} -source=pkg/configure/configure.go \
    -destination=test/mocks/mock_configure/mock_configure.go \
    -package=mock_configure
${mock_cmd} -source=pkg/logging/logging.go \
    -destination=test/mocks/mock_logging/mock_logging.go \
    -package=mock_logging
