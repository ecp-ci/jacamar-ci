#!/usr/bin/env bash

if command -v podman &> /dev/null; then
  echo "podman"
else
  echo "docker"
fi
