# Integration Testing Using Pavilion2

The goal with these tests is to leverage [Pavilion2](https://github.com/hpc/pavilion2)
along with targeted environments to realize integration testing
a close to a real production environment as possible.

## Getting Started

The majority of test (e.g., `pav-container-*`) environments have been accounted
for using either [Podman](https://podman.io/) or [Docker](https://www.docker.com/)
allowing tests to be run regardless of the environments.

## Local Install

Some tests (e.g., `pav-local-*`) are designed to run using resources
found on the host system. You will need to install Python and Pavilion2
before starting:

```shell
spack install python@3.9.19
spack install py-pip ^python@3.9.19
spack load python@3.9.19 py-pip ^python@3.9.19
```

Running `make pav-setup` will manage all `git` and `pip`
operations required, providing the `pavilion2/bin/` directory
with the version required for our existing test infrastructure.
For additional details, please see the official
[installation](https://pavilion2.readthedocs.io/en/latest/install.html)
instructions.

## Supported Tests

| Make                         | Description                                                                          |
|------------------------------|--------------------------------------------------------------------------------------|
| `pav-container-build`        | Build applications in the appropriate builder container for all subsequent tests.    |
| `pav-container-auth`         | Test general `jacamar-auth` functionality with `root` user and `setuid` downscoping. |
| `pav-container-capabilities` | Test select `jacamar-auth` functionality with Linux capabilities.                    |
| `pav-container-flux`         | Test Flux specific `jacamar` workflows in userspace.                                 |
| `pav-container-jacamar`      | Test general `jacamar` workflows in userspace.                                       |
| `pav-container-log`          | Verify system/files logging with `root` user.                                        |
| `pav-container-seccomp`      | Test limitations in seccomp filters enforced from `jacamar-auth`                     |
| `pav-container-su`           | Test `su` specific downscoping workflows.                                            |
| `pav-container-sudo`         | Test `sudo` specific downscoping workflows.                                          |
| `pav-container-strace`       | Capture summary results of `strace` on shell focused `jacamar-auth` workflows.       |
| `pav-local-slurm`            | Test Slurm executor locally, all tests run `jacamar` within userspace.               |

## Plugins

To ease test creation/management a select number of
[plugins](https://pavilion2.readthedocs.io/en/latest/plugins/index.html) have been added.

### JWT

```yaml
env:
  CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["job_id=1213","sub=job_1213"])}}'
```

Create a signed JSON Web Token that can be validated against the local test JWKS endpoint
(`pav mock-api`). A list can be passed (`key=value`) that will then be split and assigned
to the payload.

### ConfigToBase64

```yaml
env:
  JACAMAR_CI_CONFIG_STR: '{{config_base64("/example/config.toml")}}'
```

The `JACAMAR_CI_CONFIG_STR` variables expects a Base64 encoded configuration file.
This function will result in the supplied TOML file being corrected encoded using the
`tools/config2base64` tool.

### Base64ToConfig

```yaml
cmds:
  - 'echo {{base64_config("SEVMTE8gV09STEQK")}}'
```

Decode an encoded `JACAMAR_CI_CONFIG_STR` to a print friendly format using the
`tools/config2base64` tool.
