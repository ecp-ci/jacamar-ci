#!/bin/bash

set -eo pipefail
set +o noclobber

echo "Running ${1} series"
pushd "${PAV_CONFIG_DIR}" > /dev/null || exit
    pav series "${1}"
    sleep 5
    pav wait

    # Output organized results for easier visual review.
    printf "\n\n\n"
    pav status --failed --limit 1000
    printf "\n\n"
    pav status -s

    result=1
    pav status --limit 1000 | grep -q "FAIL" || result=0
    if [ "${result}" -eq "0" ]; then
        pav status --limit 1000 | grep -q "RUN_TIMEOUT" && result=1
    fi
popd > /dev/null || exit

echo "Test series exit status: ${result}"
