#!/bin/bash

echo "Setting up Pavilion2 locally"

git clone https://github.com/hpc/pavilion2.git
pushd pavilion2 || exit
    git checkout "${PAV_TAG}"
    git submodule update --init --recursive
    pip install -r requirements.txt
popd || exit

pip install pyyaml flask 'pyjwt[crypto]'
