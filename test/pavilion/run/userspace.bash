#!/usr/bin/env bash

# Execute Pavilion tests in userspace.

set -eo pipefail
set +o noclobber

echo "Running ${PAV_TARGET} series"
pushd "test/pavilion" > /dev/null
    pav series "${PAV_TARGET}"
    sleep 5
    pav wait

    # Output organized results for easier visual review.
    printf "\n\n\n"
    pav status --failed --limit 1000
    printf "\n\n"
    pav status -s

    pav status --limit 1000 | grep -q "FAIL" || result=0
    if [ "${result}" -eq "0" ]; then
        pav status --limit 1000 | grep -q "RUN_TIMEOUT" && result=1
    fi
popd > /dev/null

echo "Test series exit status: ${result}"
