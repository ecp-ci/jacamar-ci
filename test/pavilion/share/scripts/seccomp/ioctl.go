package main

import (
	"log"
	"os"
	"syscall"
	"unsafe"
)

func main() {
	cmd, _ := syscall.ByteSliceFromString("date\n")
	f, err := os.Open("/dev/tty")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	for _, c := range cmd {
		_, _, errno := syscall.Syscall(syscall.SYS_IOCTL,
			f.Fd(),
			syscall.TIOCSTI,
			uintptr(unsafe.Pointer(&c)),
		)
		if errno != 0 {
			log.Fatal(errno)
		}
	}
}
