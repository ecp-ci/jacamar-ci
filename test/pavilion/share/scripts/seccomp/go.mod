module pavilion/configs/seccomp

go 1.17

replace github.com/seccomp/libseccomp-golang => /go/pkg/mod/github.com/seccomp/libseccomp-golang@v0.10.0

require (
	github.com/seccomp/libseccomp-golang v0.10.0
	golang.org/x/sys v0.0.0-20220615213510-4f61da869c0c
)
