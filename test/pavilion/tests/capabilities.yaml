_cfg-env:
  variables:
    configs: $CI_PROJECT_DIR/test/pavilion/share/configs
    scripts: $CI_PROJECT_DIR/test/pavilion/share/scripts
    plugins: $CI_PROJECT_DIR/test/pavilion/share/go-plugins
    responses: $CI_PROJECT_DIR/test/pavilion/share/responses
    jacamar_auth: /opt/jacamar/bin/jacamar-auth
    jacamar_chown: /opt/jacamar/bin/jacamar-chown
    jacamar_perms: /opt/jacamar/bin/jacamar-perms
    jacamar_inheritable: /opt/jacamar/bin/jacamar-inheritable
  run:
    env:
      CUSTOM_ENV_CI_CONCURRENT_ID: 0
      CUSTOM_ENV_CI_JOB_ID: 123
      CUSTOM_ENV_CI_JOB_TOKEN: abc123efg456hij789kl
      CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN: token123
      CUSTOM_ENV_CI_SERVER_URL: http://127.0.0.1:5000
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("default")}}'
      CUSTOM_ENV_CI_RUNNER_VERSION: '14.5.0-rc1'
      SYSTEM_FAILURE_EXIT_CODE: 2
      BUILD_FAILURE_EXIT_CODE: 1
      JOB_RESPONSE_FILE: '{{responses}}/valid_default.json'
      JACAMAR_CI_RUNNER_TIMEOUT: '1h0m0s'
      JACAMAR_CI_PIPELINE_ID: 456

_user-home-setuid:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR:   /home/user/.jacamar-ci
      JACAMAR_CI_BUILDS_DIR: /home/user/.jacamar-ci/builds
      JACAMAR_CI_CACHE_DIR:  /home/user/.jacamar-ci/cache
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/capabilities/user_home_setuid.toml")}}'

_user-authorized-setuid:
  inherits_from: _user-home-setuid
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/capabilities/user_authorized_setuid.toml")}}'

_user-dir-setuid:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /ecp/user
      JACAMAR_CI_BUILDS_DIR: /ecp/user/builds
      JACAMAR_CI_CACHE_DIR: /ecp/user/cache
      JACAMAR_CI_SCRIPT_DIR: /ecp/user/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/capabilities/user_dir_setuid.toml")}}'

_user-dir-chown:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR:   /ecp-chown/user
      JACAMAR_CI_BUILDS_DIR: /ecp-chown/user/builds
      JACAMAR_CI_CACHE_DIR: /ecp-chown/user/cache
      JACAMAR_CI_SCRIPT_DIR: /ecp-chown/user/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/capabilities/chown_dir_setuid.toml")}}'

_user-authorized-sigterm:
  inherits_from: _user-home-setuid
  run:
    env:
      CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN: sigtermlogs
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/capabilities/user_home_sigterm.toml")}}'

###############################################################################
#  misc/basic tests
###############################################################################

verify-getcap:
  inherits_from: _cfg-env
  summary: Verify Jacamar-Auth binary has capabilities
  run:
    cmds:
      - '{{jacamar_auth}} --version'
      - 'ls -l /opt/jacamar/bin'
      - '/sbin/getcap {{jacamar_auth}}'
  result_parse:
    regex:
      output:
        regex: '/opt/jacamar/bin/jacamar-auth cap_setgid,cap_setuid=ep'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

check-data-dir:
  inherits_from: _cfg-env
  summary: Check permissions on created data_dir
  run:
    cmds:
      - 'ls -ld /ecp'
  result_parse:
    regex:
      output:
        regex: 'drwx-----x.*jacamar.*jacamar.*ecp'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

verify-getcap-chown:
  inherits_from: _cfg-env
  summary: Verify Jacamar-Auth binary has capabilities
  run:
    cmds:
      - '{{jacamar_chown}} --version'
      - '/sbin/getcap {{jacamar_chown}}'
  result_parse:
    regex:
      output:
        regex: '/opt/jacamar/bin/jacamar-chown cap_chown,cap_setgid,cap_setuid=ep'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

###############################################################################
# config_exec tests
###############################################################################

config-setuid-home:
  inherits_from: _cfg-env
  summary: Load a basic valid configuration with a setuid run mechanism and home data_dir.
  run:
    cmds:
      - '{{jacamar_auth}} config --configuration {{configs}}/capabilities/user_home_setuid.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"user"'
        action: store_true
      builds_dir:
        regex: '"JACAMAR_CI_BASE_DIR":"/home/user/.jacamar-ci"'
        action: store_true
  result_evaluate:
    result: 'username and builds_dir'

config-setuid-dir:
  inherits_from: _cfg-env
  summary: Load a basic valid configuration with a setuid run mechanism and ecp data_dir.
  run:
    cmds:
      - '{{jacamar_auth}} config --configuration {{configs}}/capabilities/user_dir_setuid.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"user"'
        action: store_true
      builds_dir:
        regex: '"JACAMAR_CI_BASE_DIR":"/ecp/user"'
        action: store_true
  result_evaluate:
    result: 'username and builds_dir'

config-invalid-permissions:
  inherits_from: _cfg-env
  summary: Attempt to run with invalid file permissions.
  run:
    cmds:
      - '{{jacamar_perms}} -u config --configuration {{configs}}/capabilities/user_dir_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'binary capabilities detected, ensure all group/world file permissions removed from Jacamar-Auth'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-invalid-caps:
  inherits_from: _cfg-env
  summary: Attempt to run with invalid file permissions.
  run:
    cmds:
      - '{{jacamar_inheritable}} -u config --configuration {{configs}}/capabilities/user_dir_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'capabilities inheritable \(cap_chown,cap_setgid,cap_setuid\=eip\), only effective/permitted'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

###############################################################################
# prepare_exec tests
###############################################################################

prepare-home-valid:
  inherits_from: _user-home-setuid
  summary: User's job environment prepared successfully in $HOME.
  run:
    cmds:
      - '{{jacamar_auth}} prepare'

prepare-dir-valid:
  inherits_from: _user-dir-setuid
  summary: User's job environment prepared successfully in /ecp.
  run:
    cmds:
      - '{{jacamar_auth}} prepare'

prepare-check-id:
  inherits_from: _user-dir-setuid
  summary: User's job environment prepare succesully with exected id return.
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/capabilities/check_id_only.toml")}}'
    cmds:
      - '{{jacamar_auth}} prepare'
  result_parse:
    regex:
      output:
        regex: 'uid=\d*\(user\).*gid=\d*\(user\).*groups=\d*\(user\),\d*\(ci\)'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

prepare-verify-uid-gid:
  inherits_from: _user-dir-setuid
  summary: User's job environment prepared successfully with effective, real, and saved ids.
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/capabilities/check_id_only.toml")}}'
    cmds:
      - '{{jacamar_auth}} prepare'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

prepare-dir-cap-chown:
  inherits_from: _user-dir-chown
  summary: User's job environment prepared successfully in dir using cap_chown.
  run:
    cmds:
      - '{{jacamar_chown}} prepare'
      # Run prepare again to make sure service account doesn't run into permission issues.
      - '{{jacamar_chown}} prepare'

###############################################################################
# run_exec tests
###############################################################################

run-home-valid:
  inherits_from: _user-home-setuid
  summary: User's job environment run successfully in $HOME.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/job-123-pass.bash'
      - '{{jacamar_auth}} run /tmp/job-123-pass.bash step_script'
  result_parse:
    regex:
      output:
        regex: 'user$'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

run-dir-valid:
  inherits_from: _user-dir-setuid
  summary: User's job environment run successfully in /ecp.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-dir-ecp.bash /tmp/job-123-dir.bash'
      - '{{jacamar_auth}} run /tmp/job-123-dir.bash step_script'
  result_parse:
    regex:
      output:
        regex: 'drwx------.*user.*user.*user'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

run-home-capsh:
  inherits_from: _user-home-setuid
  summary: Verify inherited capbitileis and groups.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-cap.bash /tmp/job-123-cap.bash'
      - '{{jacamar_auth}} run /tmp/job-123-cap.bash step_script'
  result_parse:
    regex:
      output:
        # Verify no +i based upon documented process
        regex: 'cap_setgid,cap_setuid,'
        action: store_true
      groups:
        regex: 'groups=\d*\(user\),\d*\(ci\)'
        action: store_true
  result_evaluate:
    result: 'output and groups and return_value == 0'

run-chown-dir:
  inherits_from: _user-dir-chown
  summary: Verify permissions in builds directory when cap_chown is used.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-dir-chown.bash /tmp/job-dir-chown.bash'
      - '{{jacamar_auth}} run /tmp/job-dir-chown.bash step_script'
  result_parse:
    regex:
      output:
        regex: 'drwx------\s*user\s*user\s*user$'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

###############################################################################
# cleanup_exec tests
###############################################################################

cleanup-blocklist-user:
  inherits_from: _cfg-env
  summary: CI user blocklisted by configuration, cleanup_exec detailed warning. Expect expanded error message.
  run:
    cmds:
      - '{{jacamar_auth}} cleanup --configuration {{configs}}/auth/user_blocklisted.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'user, is not in the user allowlist and is in the user blocklist'
        action: store_true
  result_evaluate:
    result: 'error_msg'

cleanup-always-exit0:
  summary: Cleanup should only fail if the cleanup itself fails.
  run:
    cmds:
      - '{{jacamar_auth}} cleanup --configuration not_found.toml'
  result_parse:
    regex:
      error_msg:
        regex: "open not_found.toml: no such file or directory"
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

###############################################################################
# scope basic checks
###############################################################################

scope-preapre:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification prepare_exec.
  run:
    cmds:
      - '{{jacamar_auth}} prepare'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-prepare_script:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + prepare_script.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-prepare_script.bash'
      - '{{jacamar_auth}} run /tmp/scope-run-prepare_script.bash prepare_script'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-get_sources:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + get_sources.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-get_sources.bash'
      - '{{jacamar_auth}} run /tmp/scope-run-get_sources.bash get_sources'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-restore_cache:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + restore_cache.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-restore_cache.bash'
      - '{{jacamar_auth}} run /tmp/scope-run-restore_cache.bash restore_cache'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-download_artifacts:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + download_artifacts.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-download_artifacts.bash'
      - '{{jacamar_auth}} run /tmp/scope-run-download_artifacts.bash download_artifacts'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-step_script:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + step_script.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-step_script.bash'
      - '{{jacamar_auth}} run /tmp/scope-run-step_script.bash step_script'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-after_script:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + after_script.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-after_script.bash'
      - '{{jacamar_auth}} run /tmp/scope-run-after_script.bash after_script'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-archive_cache:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + archive_cache.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-archive_cache.bash'
      - '{{jacamar_auth}} run /tmp/scope-run-archive_cache.bash archive_cache'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-upload_artifacts_on_success:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + upload_artifacts_on_success.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-upload_artifacts_on_success.bash'
      - '{{jacamar_auth}} run /tmp/scope-run-upload_artifacts_on_success.bash upload_artifacts_on_success'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-cleanup_file_variables:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + cleanup_file_variables.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-cleanup_file_variables.bash'
      - '{{jacamar_auth}} run /tmp/scope-run-cleanup_file_variables.bash cleanup_file_variables'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-cleanup:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification prepare_exec.
  run:
    cmds:
      - '{{jacamar_auth}} cleanup --configuration {{configs}}/capabilities/user_home_setuid.toml'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

###############################################################################
# sigterm tests - separate to avoid potential collision with scripting
###############################################################################

run-sigterm:
  inherits_from: _user-authorized-sigterm
  summary: Verify logging for captured SIGTERM.
  variables:
    jobscript: /tmp/sigtermlogsrun.bash
  run:
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - "{{jacamar_auth}} prepare"
      - "{{jacamar_auth}} run {{jobscript}} step_script &"
      - "export SIGTERM_PID=$!"
      - "sleep 2"
      - "kill -s SIGTERM ${SIGTERM_PID}"
      - "sleep 42"
      - "cat /tmp/sigterm.log"
      - "ps -p $SIGTERM_PID > /dev/null && echo 'sigtermlogsrun process is still running'"
      - 'cat /tmp/background-${CUSTOM_ENV_CI_JOB_ID} || echo "SUCCESS-BACKGROUND"'
  result_parse:
    regex:
      info:
        regex: 'SIGTERM captured, initiating job termination'
        action: store_true
      notify:
        regex: 'notifying process of SIGTERM'
        action: store_true
      failed:
        regex: 'failed to terminate process'
        action: store_false
      exited:
        regex: 'error executing run_exec'
        action: store_true
      ps:
        regex: 'sigtermlogs process is still running'
        action: store_false
      output:
        regex: 'SUCCESS-BACKGROUND'
        action: store_true
  result_evaluate:
    result: 'info and notify and failed and exited and ps and output'
