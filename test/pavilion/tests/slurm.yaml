_cfg-env:
  variables:
    configs: $CI_PROJECT_DIR/test/pavilion/share/configs
    scripts: $CI_PROJECT_DIR/test/pavilion/share/scripts
    plugins: $CI_PROJECT_DIR/test/pavilion/share/go-plugins
    responses: $CI_PROJECT_DIR/test/pavilion/share/responses
  run:
    timeout: 60
    env:
      CUSTOM_ENV_CI_CONCURRENT_ID: 0
      CUSTOM_ENV_CI_JOB_ID: 123
      CUSTOM_ENV_CI_JOB_TOKEN: abc123efg456hij789kl
      CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN: token123
      CUSTOM_ENV_CI_SERVER_URL: http://127.0.0.1:5000
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("default")}}'
      CUSTOM_ENV_CI_RUNNER_VERSION: '14.5.0-rc1'
      SYSTEM_FAILURE_EXIT_CODE: 2
      BUILD_FAILURE_EXIT_CODE: 1
      JOB_RESPONSE_FILE: '{{responses}}/valid_default.json'
      JACAMAR_CI_RUNNER_TIMEOUT: '0h3m0s'
      JACAMAR_CI_PIPELINE_ID: 456

_job-home-env:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /home/user/.jacamar-ci
      JACAMAR_CI_BUILDS_DIR: /home/user/.jacamar-ci/builds
      JACAMAR_CI_CACHE_DIR: /home/user/.jacamar-ci/cache
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/home_slurm.toml")}}'

_job-dir-env:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /var/tmp/user
      JACAMAR_CI_BUILDS_DIR: /var/tmp/user/builds
      JACAMAR_CI_CACHE_DIR: /var/tmp/user/cache
      JACAMAR_CI_SCRIPT_DIR: /var/tmp/user/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/custom_slurm.toml")}}'

_job-invalid-nfs:
  inherits_from: _job-home-env
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/slurm_invalid_nfs.toml")}}'

###############################################################################
# prepare_exec tests
###############################################################################

prepare-home-dir:
  inherits_from: _job-home-env
  summary: User's home directory prepared for job.
  run:
    cmds:
      - 'jacamar --no-auth prepare'
      - 'namei -l $JACAMAR_CI_BASE_DIR'
      - 'namei -l $JACAMAR_CI_BUILDS_DIR'
      - 'namei -l $JACAMAR_CI_CACHE_DIR'
      - 'namei -l $JACAMAR_CI_SCRIPT_DIR'
      # We don't need to confirm permissions, only the folder existence as we
      # rely on the $HOME directory permissions.

prepare-data-dir:
  inherits_from: _job-dir-env
  summary: Admin defined data directory prepared for job.
  run:
    cmds:
      - 'jacamar --no-auth prepare'
      - 'namei -l $JACAMAR_CI_BASE_DIR'
      - 'namei -l $JACAMAR_CI_BUILDS_DIR'
      - 'namei -l $JACAMAR_CI_CACHE_DIR'
      - 'namei -l $JACAMAR_CI_SCRIPT_DIR'
  result_parse:
    regex:
      base_dir:
        regex: 'drwx------ user user user'
        action: store_true
  result_evaluate:
    result: 'base_dir and return_value == 0'

###############################################################################
# run_exec tests
###############################################################################

run-default-invalid-submission:
  inherits_from: _job-home-env
  summary: Invalid schedule parameters.
  run:
    env:
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "--invalid -arg=foo"
    cmds:
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/job-env.bash'
      - 'echo "echo "PASS"" >> /tmp/job-env.bash'
      - 'jacamar --no-auth run /tmp/job-env.bash step_script'
  result_parse:
    regex:
      sbatch_option:
        regex: 'sbatch: unrecognized option'
        action: store_true
  result_evaluate:
    result: 'sbatch_option and return_value == 1'

run-valid-short-submission:
  inherits_from: _job-home-env
  summary: Execute short run successful job.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 456
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/456
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/pass-env.bash'
      - 'echo "echo "PASS"" >> /tmp/pass-env.bash'
      - 'jacamar --no-auth run /tmp/pass-env.bash step_script'
  result_parse:
    regex:
      slurm_nodes:
        regex: 'SLURM_NNODES=1'
        action: store_true
  result_evaluate:
    result: 'slurm_nodes and return_value == 0'

run-valid-sleep-submission:
  inherits_from: _job-home-env
  summary: Execute minute long successful job with custom env parameters.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 789
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/789
      CUSTOM_ENV_SLURM_CI_PARAMETERS: "-N2"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/sleep-env.bash'
      - 'echo "sleep 30" >> /tmp/sleep-env.bash'
      - 'jacamar --no-auth run /tmp/sleep-env.bash step_script'
  result_parse:
    regex:
      slurm_nodes:
        regex: 'SLURM_NNODES=2'
        action: store_true
  result_evaluate:
    result: 'slurm_nodes and return_value == 0'

run-failing-short-submission:
  inherits_from: _job-home-env
  summary: Execute minute long successful job.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1011
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/1011
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-failure.bash /tmp/job-failure.bash'
      - 'jacamar --no-auth run /tmp/job-failure.bash step_script'
  result_evaluate:
    result: 'return_value == 1'

run-compute-login-env:
  inherits_from: _job-home-env
  summary: Verify new login environment provided for compute.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1213
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/1213
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
      SOME_TEST_VALUE: 42
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/login-env.bash'
      - 'jacamar --no-auth run /tmp/login-env.bash step_script'
  result_parse:
    regex:
      slurm_output:
        regex: 'SOME_TEST_VALUE=42'
        action: store_false
  result_evaluate:
    result: 'slurm_output and return_value == 0'

run-precreate-output:
  inherits_from: _job-home-env
  summary: Wait in queue for existing job to pass.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1415
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/1415
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-whoami.bash /tmp/sleep-whoami.bash'
      - 'echo "sleep 20" >> /tmp/sleep-whoami.bash'
      - 'sbatch -N5 /tmp/sleep-whoami.bash'
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/precreate-pass.bash'
      - 'jacamar --no-auth run /tmp/precreate-pass.bash step_script'
  result_parse:
    regex:
      slurm_output:
        regex: 'Purposely Passing'
        action: store_true
  result_evaluate:
    result: 'slurm_output and return_value == 0'

run-invalid-nfs:
  inherits_from: _job-invalid-nfs
  summary: Invalid NFS caught and defaults used.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1617
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/1617
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/nfs-job-pass.bash'
      - 'jacamar --no-auth run /tmp/nfs-job-pass.bash step_script'
  result_parse:
    regex:
      slurm_output:
        regex: 'Invalid nfs_timeout configuration specified'
        action: store_true
  result_evaluate:
    result: 'slurm_output and return_value == 0'

run-streaming-submission:
  inherits_from: _job-home-env
  summary: Create a output stream and verify order.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1001
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/1001
      CUSTOM_ENV_SLURM_CI_PARAMETERS: "-N1"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-stream.bash /tmp/${CUSTOM_ENV_CI_JOB_ID}.bash'
      - 'jacamar --no-auth run /tmp/${CUSTOM_ENV_CI_JOB_ID}.bash step_script'
      - |
        lines=$(cat ${JACAMAR_CI_SCRIPT_DIR}/slurm-ci-1001.out)
        i=0
        for l in $lines; do
          if [[ ( $i -ne 0 ) && ( $i -eq $l ) ]]; then
            exit 1
          fi
          if [ $l -eq 99 ]; then
            echo "PASS"
            exit 0
          fi
          i=$(($i+1))
        done
  result_parse:
    regex:
      order_output:
        regex: 'PASS'
        action: store_true
  result_evaluate:
    result: 'order_output and return_value == 0'

run-invalid-parameters:
  inherits_from: _job-invalid-nfs
  summary: Invalid NFS caught and defaults used.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 2002
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/2002
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "--job-name=custom_job_name"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/nfs-job-pass.bash'
      - 'jacamar --no-auth run /tmp/nfs-job-pass.bash step_script'
  result_parse:
    regex:
      warn_output:
        regex: 'Illegal argument detected\. Please remove\: --job-name'
        action: store_true
      error_output:
        regex: 'illegal arguments must be addressed before continuing'
        action: store_true
  result_evaluate:
    result: 'warn_output and error_output and return_value == 2'

###############################################################################
# timeout tests
###############################################################################

run-nfs-timeout-stdout:
  inherits_from: _job-home-env
  summary: Verify NFS timeout observed after job completed.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1819
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/1819
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N2"
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/nfs_slurm.toml")}}'
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-failure.bash /tmp/job-failure.bash'
      - 'bash {{scripts}}/slurm/script-delay.bash&'
      - 'jacamar --no-auth run /tmp/job-failure.bash step_script'
  result_parse:
    regex:
      delay_output:
        regex: 'TEST-DELAYED-OUTPUT'
        action: store_true
  result_evaluate:
    result: 'delay_output and return_value == 1'

run-sigterm:
  inherits_from: _job-home-env
  summary: Verify logging for captured SIGTERM.
  variables:
    jobscript: /tmp/sigtermlogsrun.bash
  run:
    env:
      SLURM_CI_PARAMETERS: "-N1"
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - "jacamar --no-auth prepare"
      - "jacamar --no-auth run {{jobscript}} step_script &"
      - "export SIGTERM_PID=$!"
      - "sleep 2"
      - "kill -s SIGTERM ${SIGTERM_PID}"
      # Wait long enough for all output from the job to be picked up. Changes
      # to file tailing may leave process open for additional time.
      - "sleep 30"
      - "ps -p $SIGTERM_PID > /dev/null && echo 'sigtermlogsrun process is still running'"
      - 'cat /tmp/background-${CUSTOM_ENV_CI_JOB_ID} || echo "SUCCESS-BACKGROUND"'
  result_parse:
    regex:
      ps:
        regex: 'sigtermlogs process is still running'
        action: store_false
      output:
        regex: 'SUCCESS-BACKGROUND'
        action: store_true
  result_evaluate:
    result: 'ps and output'

run-context-timeout:
  inherits_from: _job-home-env
  summary: Encounter response defined timeout and cancel job.
  variables:
    jobscript: /tmp/contexttimeout.bash
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 5000
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/5000
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
      JACAMAR_CI_RUNNER_TIMEOUT: '4s'
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - 'jacamar --no-auth prepare'
      - 'jacamar --no-auth run {{jobscript}} step_script'
      - 'cat /tmp/background-${CUSTOM_ENV_CI_JOB_ID} || echo "SUCCESS-CONTEXT-TIMEOUT"'
  result_parse:
    regex:
      output:
        regex: 'SUCCESS-CONTEXT-TIMEOUT'
        action: store_true
  result_evaluate:
    result: 'output'

run-sigterm-logging:
  inherits_from: _job-home-env
  summary: Verify (user) log file for captured SIGTERM.
  variables:
    jobscript: /tmp/sigterm_logfile.bash
  run:
    env:
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
      CUSTOM_ENV_CI_JOB_ID: 1002
      JACAMAR_CI_SCRIPT_DIR:  /home/user/.jacamar-ci/scripts/group/project/1002
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - "jacamar --no-auth prepare"
      - "jacamar --no-auth run {{jobscript}} step_script &"
      - "export SIGTERM_PID=$!"
      - "sleep 2"
      - "kill -s SIGTERM ${SIGTERM_PID}"
      # Wait long enough for all output from the job to be picked up. Changes
      # to file tailing may leave process open for additional time.
      - "sleep 30"
      - "cat ${JACAMAR_CI_SCRIPT_DIR}/sigterm.log"
  result_parse:
    regex:
      output:
        regex: 'Signal captured, attempting to cancel job'
        action: store_true
      error_msg:
        regex: 'error\: <nil>'
        action: store_true
  result_evaluate:
    result: 'output and error_msg'

run-scancel-signal:
  inherits_from: _job-home-env
  summary: Verify (user) log file for captured SIGTERM.
  variables:
    jobscript: /tmp/scancel-signal.bash
  run:
    env:
      CUSTOM_ENV_SCHEDULER_CANCEL_SIGNAL: "KILL"
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
      CUSTOM_ENV_CI_JOB_ID: 1003
      JACAMAR_CI_SCRIPT_DIR:  /home/user/.jacamar-ci/scripts/group/project/1003
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - "jacamar --no-auth prepare"
      - "jacamar --no-auth run {{jobscript}} step_script &"
      - "export SIGTERM_PID=$!"
      - "sleep 2"
      - "kill -s SIGTERM ${SIGTERM_PID}"
      # Wait long enough for all output from the job to be picked up. Changes
      # to file tailing may leave process open for additional time.
      - "sleep 30"
      - "sacct"
  result_parse:
    regex:
      output:
        regex: 'Using signal KILL if scancel is required'
        action: store_true
      sacct:
        regex: 'ci-1003_.*CANCELLED'
        action: store_true
  result_evaluate:
    result: 'output and sacct'

###############################################################################
# scancel tests
###############################################################################

run-sacct-valid-submission:
  inherits_from: _job-home-env
  summary: Verify sacct check does not interfere with a valid job.
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/sacct_slurm.toml")}}'
      CUSTOM_ENV_CI_JOB_ID: 7777
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/7777
      CUSTOM_ENV_SLURM_CI_PARAMETERS: "-N2"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/sleep-env.bash'
      - 'jacamar --no-auth run /tmp/sleep-env.bash step_script'
  result_parse:
    regex:
      slurm_nodes:
        regex: 'SLURM_NNODES=2'
        action: store_true
  result_evaluate:
    result: 'slurm_nodes and return_value == 0'
