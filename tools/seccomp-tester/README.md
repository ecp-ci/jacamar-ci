## Seccomp Tester

This application allows for testing Seccomp filters and related
capability restrictions in a limited fashion. Valid Jacamar CI
configurations are utilized in conjunction with arbitrary command
execution directly via the CLI without the need to deal with
complicated deployments, downscoping, or CI pipelines.

### Compiling on Host

Verify you meet the requirement outlined in the project `README.md` then:

```shell
$ go build -trimpath -o seccomp-tester main.go
```

### Compiling w/Container

If you have a local deployment of Podman/Docker:

```shell
$ make run-centos7-builder
$ cd tools/seccomp-tester
$ go build -trimpath -o seccomp-tester main.go
```

### Seccomp Testing

Create your Jacamar CI configuration file:

```shell
$ cat /example/file/jacamar.toml
[auth.seccomp]
disabled = false
block_all = true
allow_calls = [ "mkdir", "poll"]
```

Run application and pass additional command/arguments in that will then
be executed under the defined filters.

```shell
$ ./seccomp-tester -cfg /example/file/jacamar.toml /usr/bin/id -G
Segmentation fault (core dumped)
```

The configuration is realized using the same functions as the
`jacamar-auth` application in a tradition deployment.

### PR_SET_NO_NEW_PRIVS Testing

Testing only with [no_new_priv](https://man7.org/linux/man-pages/man2/prctl.2.html)
is also possible; however, we recommend turning off seccomp (`disabled = true`):

```shell
$ ./seccomp-tester -cfg $(pwd)/example.toml /example/bin/prctl_check
0
$ ./seccomp-tester -no_new_priv -cfg $(pwd)/example.toml /example/bin/prctl_check
1
```
