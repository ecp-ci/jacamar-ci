// Package exec_testing supports basic testing/mocks for any of the "batch" executor types. Due
// to the overlap in functionally between packages and the reliance on integration testing for
// concrete scheduler interactions these can be used to avoid redundant test development.
package exec_testing

import (
	"errors"

	"github.com/golang/mock/gomock"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

// MockAbsExecutor provides general structure with all required functions mocked with no
// specific requirements. This is for use as a base AbsExec in testing when output/error
// is target for assertion.
func MockAbsExecutor(
	ctrl *gomock.Controller,
	opts configure.Options,
	stage string,
	runFailure bool,
) *abstracts.Executor {
	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(opts.Batch).AnyTimes()
	cfg.EXPECT().General().Return(opts.General).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Stdout(gomock.Any(), gomock.Any()).AnyTimes()
	msg.EXPECT().Stderr(gomock.Any(), gomock.Any()).AnyTimes()
	msg.EXPECT().Notify(gomock.Any(), gomock.Any()).AnyTimes()

	var script string

	run := mock_runmechanisms.NewMockRunner(ctrl)
	if runFailure {
		script = "/some/job/error.bash"
		run.EXPECT().JobScriptOutput(script).Return(errors.New("error message")).AnyTimes()
		run.EXPECT().JobScriptOutput(script, gomock.Any()).Return(errors.New("error message")).AnyTimes()
	} else {
		script = "/some/job/script.bash"
		run.EXPECT().JobScriptOutput(script).Return(nil).AnyTimes()
		run.EXPECT().JobScriptOutput(script, gomock.Any()).Return(nil).AnyTimes()
	}

	return &abstracts.Executor{
		Cfg:        cfg,
		Msg:        msg,
		Runner:     run,
		ScriptPath: script,
		Stage:      stage,
	}
}

// PopulateMockManager populates a mocked Batch.Manager with all potential functions.
func PopulateMockManager(m *mock_batch.MockManager) {
	m.EXPECT().BatchCmd(gomock.Any()).Return("batchCmd").AnyTimes()
	m.EXPECT().StateCmd().Return("stateCmd").AnyTimes()
	m.EXPECT().StopCmd().Return("stopCmd").AnyTimes()
	m.EXPECT().UserArgs().Return("--Account").AnyTimes()
	m.EXPECT().NFSTimeout(gomock.Any(), gomock.Any()).AnyTimes()
	m.EXPECT().MonitorTermination(
		gomock.Any(),
		gomock.Any(),
		gomock.Any(),
		gomock.Any(),
	).AnyTimes()
}
